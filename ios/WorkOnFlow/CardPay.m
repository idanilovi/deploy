//
//  CardPay.m
//  WorkOnFlow
//
//  Created by MacMini on 3/23/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "CardPay.h"
#import "React/RCTLog.h"
#import <CloudPaymentsAPI/CPService.h>

@implementation CardPay : NSObject
// This RCT (React) "macro" exposes the current module to JavaScript
RCT_EXPORT_MODULE();

// We must explicitly expose methods otherwise JavaScript can't access anything
RCT_EXPORT_METHOD(createCardCryptogram:(NSString *)cardNumber vcc:(NSString *)vcc expData:(NSString *)expData callback:(RCTResponseSenderBlock)callback)
{
  NSString *publicId = @"1234567890";
  CPService *_apiService = [[CPService alloc] init];
  NSString *cryptogramPacket = [_apiService makeCardCryptogramPacket:cardNumber
                                                          andExpDate:expData
                                                              andCVV:vcc
                                                    andStorePublicID:publicId];
  if ([CPService isCardNumberValid:cardNumber]) {
    callback(@[cryptogramPacket]);
  }
}
  @end
