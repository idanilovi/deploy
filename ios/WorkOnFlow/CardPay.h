//
//  CardPay.h
//  WorkOnFlow
//
//  Created by MacMini on 3/23/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"

@interface CardPay : NSObject <RCTBridgeModule>

@end
