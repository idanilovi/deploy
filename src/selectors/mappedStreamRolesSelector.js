import R from 'ramda';
import { createSelector } from 'reselect';
import { contactsSelector } from '../reducers/contacts';
import { streamByIdSelector } from '../reducers/streams';

const mappedStreamRolesSelector = createSelector(
  contactsSelector,
  streamByIdSelector,
  (contacts, stream) =>
    R.filter(
      _ => _,
      R.map(
        role => R.propOr(null, role, contacts),
        R.propOr([], 'roles', stream),
      ),
    ),
);

export default mappedStreamRolesSelector;
