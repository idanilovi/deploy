import R from 'ramda';
import { createSelector } from 'reselect';

export hydratedThreadSelector from './hydratedThreadSelector';
export userColorSelector from './userColorSelector';
export descriptionByStreamIdSelector from './descriptionByStreamIdSelector';

export mappedStreamAdminsSelector from './mappedStreamAdminsSelector';
export mappedStreamRolesSelector from './mappedStreamRolesSelector';
export diffBetweenStreamRolesAndAdminsSelector from './diffBetweenStreamRolesAndAdminsSelector';
export channelByIdAndTypeSelector from './channelByIdAndTypeSelector';

export const threadsSelector = state => state.threads || {};
export const commentsSelector = state => state.comments || {};
export const streamsSelector = state => state.streams || {};
export const statusesSelector = state => state.statuses || {};
export const contactsSelector = state => state.contacts || {};
export const descriptionsSelector = state => state.descriptions || {};
export const filesSelector = state => state.files || {};
export const userIdSelector = state =>
  state && state.userdata && state.userdata.userId;
export const mailsSelector = state => state.mailAccounts || {};

export const streamIdSelector = (state, ownProps) => {
  return R.path(['navigation', 'state', 'params', 'id'], ownProps);
};

export const filesByStreamIdSelector = createSelector(
  streamIdSelector,
  filesSelector,
  (streamId, files) =>
    R.filter(file => file.streamId === streamId, R.values(files)),
);

export const streamByIdSelector = createSelector(
  streamsSelector,
  streamIdSelector,
  (streams, streamId) => R.prop(streamId, streams),
);

export const threadsByStreamSelector = createSelector(
  streamIdSelector,
  threadsSelector,
  (streamId, threads) => {
    return R.filter(thread => thread.streamId === streamId, R.values(threads));
  },
);

export const commentsByStreamIdSelector = createSelector(
  streamIdSelector,
  commentsSelector,
  (streamId, comments) => {
    return R.filter(
      comment => comment.streamId === streamId,
      R.values(comments),
    );
  },
);
