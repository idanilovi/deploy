import R from 'ramda';
import { createSelector } from 'reselect';
import { mailsSelector } from '../reducers/mails';
import { trunksSelector } from '../reducers/trunks';
import { messangersSelector } from '../reducers/messangers';
// eslint-disable-next-line no-underscore-dangle
const channelIdSelector = (state, ownProps) => ownProps._id || ownProps.id;
const channelTypeSelector = (state, ownProps) => ownProps.type;

const channelsByTypeSelector = createSelector(
  channelTypeSelector,
  mailsSelector,
  trunksSelector,
  messangersSelector,
  (type, mails, trunks, messangers) => {
    if (type === 'trunks') {
      return trunks;
    }
    if (type === 'messangers') {
      return messangers;
    }
    if (type === 'mails') {
      return mails;
    }
    return {};
  },
);

const channelByIdAndTypeSelector = createSelector(
  channelIdSelector,
  channelsByTypeSelector,
  (id, channels) => R.propOr({}, id, channels),
);

export default channelByIdAndTypeSelector;
