import R from 'ramda';
import { createSelector } from 'reselect';
import { userIdSelector } from '../reducers/userdata';
import { contactsSelector } from '../reducers/contacts';

const userColorSelector = createSelector(
  userIdSelector,
  contactsSelector,
  (id, contacts) =>
    R.propOr('rgba(0, 206, 121, 1)', 'color', R.propOr({}, id, contacts)),
);

export default userColorSelector;
