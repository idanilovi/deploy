import R from 'ramda';
import { createSelector } from 'reselect';
import { threadByIdSelector } from '../reducers/threads';
import { streamsSelector } from '../reducers/streams';
import { statusesSelector } from '../reducers/statuses';

const hydratedThreadSelector = createSelector(
  threadByIdSelector,
  streamsSelector,
  statusesSelector,
  (thread, streams, statuses) => {
    const stream = R.propOr({}, thread.streamId, streams);
    const status = R.propOr({}, thread.status, statuses);
    const threadStatuses = R.propOr([], 'threadStatuses', stream);
    const progress =
      100 / threadStatuses.length * (threadStatuses.indexOf(thread.status) + 1);
    const threadWithProgress = R.assoc('progress', progress, thread);
    const threadWithStatus = R.assoc('status', status, threadWithProgress);
    return R.assoc('stream', stream, threadWithStatus);
  },
);

export default hydratedThreadSelector;
