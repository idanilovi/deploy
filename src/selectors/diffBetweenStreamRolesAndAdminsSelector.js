import R from 'ramda';
import { createSelector } from 'reselect';
import mappedStreamAdminsSelector from './mappedStreamAdminsSelector';
import mappedStreamRolesSelector from './mappedStreamRolesSelector';

const diffBetweenStreamRolesAndAdminsSelector = createSelector(
  mappedStreamAdminsSelector,
  mappedStreamRolesSelector,
  (admins, roles) => R.difference(roles, admins),
);

export default diffBetweenStreamRolesAndAdminsSelector;
