import R from 'ramda';
import { createSelector } from 'reselect';
import { descriptionsSelector } from '../reducers/description';
import { streamIdSelector } from '../reducers/streams';

const descriptionByStreamIdSelector = createSelector(
  descriptionsSelector,
  streamIdSelector,
  (descriptions, streamId) => R.propOr({}, streamId, descriptions),
);

export default descriptionByStreamIdSelector;
