import R from 'ramda';
import { createSelector } from 'reselect';
import { contactsSelector } from '../reducers/contacts';
import { streamByIdSelector } from '../reducers/streams';

const mappedStreamAdminsSelector = createSelector(
  contactsSelector,
  streamByIdSelector,
  (contacts, stream) =>
    R.filter(
      _ => _,
      R.map(
        admin => R.propOr(null, admin, contacts),
        R.propOr([], 'admins', stream),
      ),
    ),
);

export default mappedStreamAdminsSelector;
