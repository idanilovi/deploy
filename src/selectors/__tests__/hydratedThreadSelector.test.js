import hydratedThreadSelector from '../hydratedThreadSelector';

const state = {
  threads: {
    '5a2e83ee62b68e001f82dda6': {
      id: '5a2e83ee62b68e001f82dda6',
      status: '58f5eb96bc57da0014c68581',
      streamId: '58f5eb7fbc57da0014c6857f',
    },
  },
  streams: {
    '58f5eb7fbc57da0014c6857f': {
      id: '58f5eb7fbc57da0014c6857f',
      threadStatuses: [
        '5a05b155455b1f0016cfa35f',
        '5a9dee1c4a6c370019156900',
        '5a0c30410f84380017e6b6ea',
        '58f5eb8fbc57da0014c68580',
        '58f5eb96bc57da0014c68581',
        '5a55ceb1e0ab3e001455d0f6',
        '58f5eb9abc57da0014c68582',
        '5a72fea7e0ab3e001455d191',
        '58f5eb9fbc57da0014c68583',
        '5a85371d5c2d04001b7df1c0',
      ],
    },
  },
  statuses: {
    '5a05b155455b1f0016cfa35f': {
      id: '58dbd633ae7de1001629622a',
    },
    '5a9dee1c4a6c370019156900': {
      id: '58dbd633ae7de1001629622a',
    },
    '5a0c30410f84380017e6b6ea': {
      id: '58dbd633ae7de1001629622a',
    },
    '58f5eb8fbc57da0014c68580': {
      id: '58dbd633ae7de1001629622a',
    },
    '58f5eb96bc57da0014c68581': {
      id: '58dbd633ae7de1001629622a',
    },
    '5a55ceb1e0ab3e001455d0f6': {
      id: '58dbd633ae7de1001629622a',
    },
    '58f5eb9abc57da0014c68582': {
      id: '58dbd633ae7de1001629622a',
    },
    '5a72fea7e0ab3e001455d191': {
      id: '58dbd633ae7de1001629622a',
    },
    '58f5eb9fbc57da0014c68583': {
      id: '58dbd633ae7de1001629622a',
    },
    '5a85371d5c2d04001b7df1c0': {
      id: '58dbd633ae7de1001629622a',
    },
  },
};

const ownProps = {
  id: '5a2e83ee62b68e001f82dda6',
};

const hydratedThread = {
  id: '5a2e83ee62b68e001f82dda6',
  status: {
    id: '58dbd633ae7de1001629622a',
  },
  progress: 50,
  streamId: '58f5eb7fbc57da0014c6857f',
  stream: {
    id: '58f5eb7fbc57da0014c6857f',
    threadStatuses: [
      '5a05b155455b1f0016cfa35f',
      '5a9dee1c4a6c370019156900',
      '5a0c30410f84380017e6b6ea',
      '58f5eb8fbc57da0014c68580',
      '58f5eb96bc57da0014c68581',
      '5a55ceb1e0ab3e001455d0f6',
      '58f5eb9abc57da0014c68582',
      '5a72fea7e0ab3e001455d191',
      '58f5eb9fbc57da0014c68583',
      '5a85371d5c2d04001b7df1c0',
    ],
  },
};

describe('Testing hydrated thread selector: ', () => {
  it('expect selected state match snapshot', () => {
    expect(hydratedThreadSelector(state, ownProps)).toMatchSnapshot();
  });
  it('expect selected state equal to mock value', () => {
    expect(hydratedThreadSelector(state, ownProps)).toEqual(hydratedThread);
  });
});
