import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';

const icons = new Map();

function loadIcons() {
  return Promise.all(
    MaterialIcons.getImageSource('arrow back')
      .then(source => icons.set('arrow back', source))
      .catch(error => console.error(error)),
  );
}

export { loadIcons, icons };
