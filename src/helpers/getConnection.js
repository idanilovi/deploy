// import 'abortcontroller-polyfill/dist/polyfill-patch-fetch';
// import global from '../global';

export default function getConnection(
  url = 'https://login.workonflow.com/api',
) {
  // const AController = window.AbortController;
  // global.getConnectionConroller = new AController();
  // global.getConnectionSignal = global.getConnectionConroller.signal;

  return new Promise(resolve => {
    const timeout = setTimeout(() => resolve(false), 10000);
    fetch(url, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: 0,
      },
    })
      .then(res => {
        // global.getConnectionConroller = null;
        clearTimeout(timeout);
        if (res.ok) {
          resolve(true);
        }
        resolve(false);
      })
      .catch(() => resolve(false));
  });
}
