import RNFetchBlob from 'react-native-fetch-blob';
import { Platform, NetInfo } from 'react-native';
import realm from './Realm';

function getExtensionFromFilename(filename) {
  if (filename) {
    const ext = filename.split('.').pop();
    return `.${ext}`;
  }
  return '';
}

function getFilePath(id, filename) {
  if (Platform.OS === 'ios') {
    const ext = getExtensionFromFilename(filename);
    return `${RNFetchBlob.fs.dirs.CacheDir}/files/${id}${ext}`;
  }
  return `${RNFetchBlob.fs.dirs.CacheDir}/files/${id}`;
}

function getPreviewPath(id, filename) {
  if (Platform.OS === 'ios') {
    const ext = getExtensionFromFilename(filename);
    return `${RNFetchBlob.fs.dirs.CacheDir}/preview/${id}${ext}`;
  }
  return `${RNFetchBlob.fs.dirs.CacheDir}/preview/${id}`;
}

export default class FileCache {
  static exists({ id }) {
    const files = realm.objects('File');
    const file = files.find(_ => _.id === id);
    return !!file;
  }
  static async get({ id, filename, url, urlPreview, force = false }) {
    const fileExists = this.exists({ id });
    if (fileExists) {
      const files = realm.objects('File');
      const file = files.find(_ => _.id === id);
      return { ...file, download: false };
    }
    const connectionInfo = await NetInfo.getConnectionInfo();
    if (connectionInfo.type === 'cellular' && !force) {
      return {
        id,
        filename,
        download: true,
      };
    }
    const path = getFilePath(id, filename);
    return RNFetchBlob.config({ path })
      .fetch('GET', url)
      .then(res => {
        if (urlPreview) {
          const previewPath = getPreviewPath(id, filename);
          return RNFetchBlob.config({ path: previewPath })
            .fetch('GET', urlPreview)
            .then(() => {
              const file = {
                id,
                path,
                previewPath,
                filename,
                type: res.respInfo.headers['Content-Type'],
                size: res.respInfo.headers['Content-Length'],
                download: false,
              };
              try {
                realm.write(() => {
                  realm.create('File', file);
                });
              } catch (error) {
                console.error('Error on creation: ', error);
              }
              return file;
            });
        }
        const file = {
          id,
          path,
          filename,
          type: res.respInfo.headers['Content-Type'],
          size: res.respInfo.headers['Content-Length'],
          download: false,
        };
        try {
          realm.write(() => {
            realm.create('File', file);
          });
        } catch (error) {
          console.error('Error on creation: ', error);
        }
        return file;
      })
      .catch(error => console.error(error));
  }
  static getCachedFiles() {
    return Array.from(realm.objects('File'));
  }
}
