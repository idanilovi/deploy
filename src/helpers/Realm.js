import Realm from 'realm';

const FilesSchema = {
  name: 'File',
  properties: {
    id: 'string',
    path: 'string',
    type: 'string',
    size: 'string',
    filename: 'string',
    previewPath: 'string?',
  },
};

const config = {
  schema: [FilesSchema],
};

const realm = new Realm(config);

export default realm;
