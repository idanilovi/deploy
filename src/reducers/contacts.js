/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import { createSelector } from 'reselect';

export const REQUEST_CONTACT = 'project_managment/contacts/REQUEST_CONTACT';
export const RECEIVE_CONTACT = 'project_managment/contacts/RECEIVE_CONTACT';
export const RECEIVE_CONTACTS = 'project_managment/contacts/RECEIVE_CONTACTS';
export const DELETE_CONTACT = 'project_managment/contacts/DELETE_CONTACT';
export const SET_CONTACT_NAME = 'project_managment/contacts/SET_CONTACT_NAME';
export const SET_CONTACT_EMAIL = 'project_managment/contacts/SET_CONTACT_EMAIL';
export const SET_CONTACT_PHONE = 'project_managment/contacts/SET_CONTACT_PHONE';
export const SET_CONTACT_POSITION =
  'project_managment/contacts/SET_CONTACT_POSITION';
export const SET_CONTACT_DESCRIPTION =
  'project_managment/contacts/SET_CONTACT_DESCRIPTION';
export const DELETE_CONTACT_EMAIL =
  'project_managment/contacts/DELETE_CONTACT_EMAIL';
export const DELETE_CONTACT_PHONE =
  'project_managment/contacts/DELETE_CONTACT_PHONE';
export const SET_CONTACT_CUSTOM_FIELD =
  'project_managment/contacts/SET_CONTACT_CUSTOM_FIELD';
export const SET_CONTACT_SETTINGS =
  'project_managment/contacts/SET_CONTACT_SETTINGS';

const Contact = {};

function contactsReducer(state = Contact, action = {}) {
  if (state._id && state._id !== action.payload._id) {
    return state;
  }
  switch (action.type) {
    case RECEIVE_CONTACT: {
      return Object.assign({}, Contact, action.payload);
    }
    case SET_CONTACT_NAME: {
      return R.assocPath(['basicData', 'name'], action.payload.name, state);
    }
    case SET_CONTACT_EMAIL: {
      return R.assocPath(
        ['basicData', 'email', action.payload.index],
        action.payload.email,
        state,
      );
    }
    case DELETE_CONTACT_EMAIL: {
      return R.assocPath(
        ['basicData', 'email'],
        R.remove(
          action.payload.index,
          1,
          R.path(['basicData', 'email'], state),
        ),
        state,
      );
    }
    case DELETE_CONTACT_PHONE: {
      return R.assocPath(
        ['basicData', 'phone'],
        R.remove(
          action.payload.index,
          1,
          R.path(['basicData', 'phone'], state),
        ),
        state,
      );
    }
    case SET_CONTACT_PHONE: {
      return R.assocPath(
        ['basicData', 'phone', action.payload.index],
        action.payload.phone,
        state,
      );
    }
    case SET_CONTACT_CUSTOM_FIELD: {
      return R.assocPath(
        ['customFields', action.payload.index],
        {
          id: action.payload._id,
          label: action.payload.label,
          value: action.payload.value,
        },
        state,
      );
    }
    case SET_CONTACT_POSITION: {
      return R.assocPath(
        ['hrData', 'position'],
        action.payload.position,
        state,
      );
    }
    case SET_CONTACT_DESCRIPTION: {
      return R.assoc('description', action.payload.description, state);
    }
    case SET_CONTACT_SETTINGS: {
      return R.assocPath(
        ['settings', action.payload.type, action.payload.itemId],
        action.payload.settings,
        state,
      );
    }
    case RECEIVE_CONTACTS:
      return Object.assign({}, Contact, action.payload);
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_CONTACT:
    case SET_CONTACT_NAME:
    case SET_CONTACT_EMAIL:
    case SET_CONTACT_PHONE:
    case SET_CONTACT_POSITION:
    case SET_CONTACT_DESCRIPTION:
    case SET_CONTACT_CUSTOM_FIELD:
    case DELETE_CONTACT_EMAIL:
    case DELETE_CONTACT_PHONE:
    case SET_CONTACT_SETTINGS: {
      return R.assoc(
        action.payload._id,
        contactsReducer(state[action.payload._id], action),
      )(state);
    }
    case RECEIVE_CONTACTS: {
      return R.merge(
        state,
        R.mergeAll(
          R.map(contact => ({ [contact._id]: contact }), action.payload),
        ),
      );
      // const actions = R.map(receiveContact, action.payload);
      // return R.reduce(reducer, state, actions);
    }
    case DELETE_CONTACT: {
      return R.dissoc(action.payload._id, state);
    }
    default: {
      return state;
    }
  }
}

export function setContactSettings(_id, type, itemId, settings) {
  return {
    type: SET_CONTACT_SETTINGS,
    payload: { _id, type, itemId, settings },
  };
}

export function setContactCustomField(_id, label, value, index) {
  return {
    type: SET_CONTACT_CUSTOM_FIELD,
    payload: { _id, label, value, index },
  };
}

export function setContactName(_id, name) {
  return {
    type: SET_CONTACT_NAME,
    payload: { _id, name },
  };
}

export function setContactEmail(_id, email, index) {
  return {
    type: SET_CONTACT_EMAIL,
    payload: { _id, email, index },
  };
}

export function setContactPhone(_id, phone, index) {
  return {
    type: SET_CONTACT_PHONE,
    payload: { _id, phone, index },
  };
}

export function setContactPosition(_id, position) {
  return {
    type: SET_CONTACT_POSITION,
    payload: { _id, position },
  };
}

export function setContactDescription(_id, description) {
  return {
    type: SET_CONTACT_DESCRIPTION,
    payload: { _id, description },
  };
}

export function deleteContactEmail(_id, index) {
  return {
    type: DELETE_CONTACT_EMAIL,
    payload: { _id, index },
  };
}

export function deleteContactPhone(_id, index) {
  return {
    type: DELETE_CONTACT_PHONE,
    payload: { _id, index },
  };
}

export function receiveContact(contact) {
  return {
    type: RECEIVE_CONTACT,
    payload: contact,
  };
}

export function receiveContacts(contacts) {
  return {
    type: RECEIVE_CONTACTS,
    payload: contacts,
  };
}

export function deleteContact(_id) {
  return {
    type: DELETE_CONTACT,
    payload: { _id },
  };
}

export const contactsSelector = state => R.propOr({}, 'contacts', state);
export const contactSelector = (state, ownProps) => {
  const contact = R.propOr(
    R.propOr({}, 'contact', ownProps),
    R.prop('id', ownProps),
    R.prop('contacts', state),
  );
  if (ownProps.customer) {
    return {
      ...contact,
      basicData: R.propOr(
        R.prop('basicData', contact._source),
        'basicData',
        contact,
      ),
      ...(contact.billingType
        ? { billingType: contact.billingType }
        : { billingType: R.propOr('contacts', 'billingType', contact) }),
    };
  }
  return contact;
};

export const usersArraySelector = createSelector(contactsSelector, contacts =>
  R.filter(contact => contact.billingType === 'users', R.values(contacts)),
);
