import R from 'ramda';

export const RECEIVE_MAILS = 'pm/comments/RECEIVE_MAILS';
export const RECEIVE_MAIL = 'pm/comments/RECEIVE_MAIL';
export const REMOVE_MAILS = 'pm/comments/REMOVE_MAILS';

const Mails = {};

function mailFactory(mail) {
  // eslint-disable-next-line no-underscore-dangle
  return Object.assign({}, { [mail._id]: mail });
}

function mailsReducer(state = Mails, action = {}) {
  switch (action.type) {
    case RECEIVE_MAIL: {
      return Object.assign({}, state, action.payload.mail);
    }
    case RECEIVE_MAILS: {
      return R.mergeAll(R.map(mailFactory, action.payload.mails));
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_MAIL: {
      return mailsReducer(state, action);
    }
    case RECEIVE_MAILS: {
      return mailsReducer(state, action);
    }
    case REMOVE_MAILS: {
      return {};
    }
    default: {
      return state;
    }
  }
}

export const receiveMail = mail => ({
  type: RECEIVE_MAIL,
  payload: { mail },
});

export const receiveMails = mails => ({
  type: RECEIVE_MAILS,
  payload: { mails },
});

export const removeMails = () => ({
  type: REMOVE_MAILS,
});

export const mailsSelector = state => R.propOr({}, 'mails', state);
