import R from 'ramda';

export const RECEIVE_PAYMENTS = 'pm/comments/RECEIVE_PAYMENTS';
export const RECEIVE_PAYMENT = 'pm/comments/RECEIVE_PAYMENT';
export const REMOVE_PAYMENTS = 'pm/comments/REMOVE_PAYMENTS';

const Payments = {};

function paymentFactory(payment) {
  // eslint-disable-next-line no-underscore-dangle
  return Object.assign({}, { [payment._id]: payment });
}

function paymentsReducer(state = Payments, action = {}) {
  switch (action.type) {
    case RECEIVE_PAYMENT: {
      return Object.assign({}, state, action.payload.payment);
    }
    case RECEIVE_PAYMENTS: {
      return R.mergeAll(R.map(paymentFactory, action.payload.payments));
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_PAYMENT: {
      return paymentsReducer(state, action);
    }
    case RECEIVE_PAYMENTS: {
      return paymentsReducer(state, action);
    }
    case REMOVE_PAYMENTS: {
      return {};
    }
    default: {
      return state;
    }
  }
}

export const receivePayment = payment => ({
  type: RECEIVE_PAYMENT,
  payload: { payment },
});

export const receivePayments = payments => ({
  type: RECEIVE_PAYMENTS,
  payload: { payments },
});

export const removePayments = () => ({
  type: REMOVE_PAYMENTS,
});
