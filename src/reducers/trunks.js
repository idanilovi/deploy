import R from 'ramda';

export const RECEIVE_TRUNKS = 'pm/comments/RECEIVE_TRUNKS';
export const RECEIVE_TRUNK = 'pm/comments/RECEIVE_TRUNK';
export const REMOVE_TRUNKS = 'pm/comments/REMOVE_TRUNKS';

const Trunks = {};

function trunkFactory(trunk) {
  // eslint-disable-next-line no-underscore-dangle
  return Object.assign({}, { [trunk.id]: trunk });
}

function trunksReducer(state = Trunks, action = {}) {
  switch (action.type) {
    case RECEIVE_TRUNK: {
      return Object.assign({}, state, action.payload.trunk);
    }
    case RECEIVE_TRUNKS: {
      return R.mergeAll(R.map(trunkFactory, action.payload.trunks));
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_TRUNK: {
      return trunksReducer(state, action);
    }
    case RECEIVE_TRUNKS: {
      return trunksReducer(state, action);
    }
    case REMOVE_TRUNKS: {
      return {};
    }
    default: {
      return state;
    }
  }
}

export const receiveTrunk = trunk => ({
  type: RECEIVE_TRUNK,
  payload: { trunk },
});

export const receiveTrunks = trunks => ({
  type: RECEIVE_TRUNKS,
  payload: { trunks },
});

export const removeTrunks = () => ({
  type: REMOVE_TRUNKS,
});

export const trunksSelector = state => R.propOr({}, 'trunks', state);
