/* eslint-disable id-length */
import R from 'ramda';
import uuid from 'uuid';
import { createSelector } from 'reselect';

export const RECEIVE_TEAMS = 'workonflow/teams/RECEIVE_TEAMS';

function teamFactory(team) {
  // eslint-disable-next-line no-underscore-dangle
  const _id = uuid.v4();
  return Object.assign({}, { [_id]: Object.assign({}, team, { _id }) });
}

function teamsReducer(state = File, action = {}) {
  switch (action.type) {
    case RECEIVE_TEAMS: {
      return R.mergeAll(R.map(teamFactory, action.payload.teams));
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_TEAMS: {
      // Очищается стейт тим потому что у них нет уникального идентификатора
      return teamsReducer(state, action);
    }
    default: {
      return state;
    }
  }
}

export function receiveTeams(teams) {
  return {
    type: RECEIVE_TEAMS,
    payload: { teams },
  };
}

const sortByInvitedAt = R.sortBy(R.prop('invitedAt'));

export const teamsSelector = state => R.propOr({}, 'teams', state);
export const teamsArraySelector = createSelector(teamsSelector, teams =>
  R.reverse(
    sortByInvitedAt(
      R.filter(team => team.url === 'https://workonflow.com', R.values(teams)),
    ),
  ),
);
