export const SET_WIDTH = 'pm/ui/background/SET_WIDTH';
export const SET_HEIGHT = 'pm/ui/background/SET_HEIGHT';

const Background = {
  imgWidth: null,
  imgHeight: null,
  left: 0,
};

export default function reducer(state = Background, action = {}) {
  switch (action.type) {
    case SET_WIDTH: {
      return Object.assign({}, state, { imgWidth: action.payload.width });
    }
    case SET_HEIGHT: {
      return Object.assign({}, state, { imgHeight: action.payload.height });
    }
    default: {
      return state;
    }
  }
}

export function setWidth(width) {
  return {
    type: SET_WIDTH,
    payload: { width },
  };
}

export function setHeight(height) {
  return {
    type: SET_HEIGHT,
    payload: { height },
  };
}
