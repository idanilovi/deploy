export const SHOW_INVISIBLE_STREAMS = 'pm/ui/sidebar/SHOW_INVISIBLE_STREAMS';
export const HIDE_INVISIBLE_STREAMS = 'pm/ui/sidebar/HIDE_INVISIBLE_STREAMS';
export const SHOW_INVISIBLE_CONTACTS = 'pm/ui/sidebar/SHOW_INVISIBLE_CONTACTS';
export const HIDE_INVISIBLE_CONTACTS = 'pm/ui/sidebar/HIDE_INVISIBLE_CONTACTS';

const SideBar = {
  showInvisibleStreams: false,
  showInvisibleContacts: false,
};

export default function reducer(state = SideBar, action = {}) {
  switch (action.type) {
    case SHOW_INVISIBLE_STREAMS: {
      return Object.assign({}, state, { showInvisibleStreams: true });
    }
    case HIDE_INVISIBLE_STREAMS: {
      return Object.assign({}, state, { showInvisibleStreams: false });
    }
    case SHOW_INVISIBLE_CONTACTS: {
      return Object.assign({}, state, { showInvisibleContacts: true });
    }
    case HIDE_INVISIBLE_CONTACTS: {
      return Object.assign({}, state, { showInvisibleContacts: false });
    }
    default: {
      return state;
    }
  }
}

export function showStreams() {
  return {
    type: SHOW_INVISIBLE_STREAMS,
  };
}

export function hideStreams() {
  return {
    type: HIDE_INVISIBLE_STREAMS,
  };
}

export function showContacts() {
  return {
    type: SHOW_INVISIBLE_CONTACTS,
  };
}

export function hideContacts() {
  return {
    type: HIDE_INVISIBLE_CONTACTS,
  };
}
