import { combineReducers } from 'redux';
import commentForm from './commentForm';
import sidebar from './sidebar';
import background from './background';
import login from './login';

export default combineReducers({
  commentForm,
  sidebar,
  background,
  login,
});
