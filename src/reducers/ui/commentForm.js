import R from 'ramda';

export const CREATE = 'pm/ui/commentForm/CREATE';
export const SET_TEXT = 'pm/ui/commentForm/SET_TEXT';
export const CLEAR_COMMENT_FORM = 'pm/ui/commentForm/CLEAR_COMMENT_FORM';
export const SHOW = 'pm/ui/commentForm/SHOW';
export const HIDE_COMMENT_FORM = 'pm/ui/commentForm/HIDE_COMMENT_FORM';
export const ADD_ATTACHMENT = 'pm/ui/commentForm/ADD_ATTACHMENT';
// export const DELETE_ATTACHMENT = 'pm/ui/commentForm/DELETE_ATTACHMENT';
export const ADD_MENTION = 'pm/ui/commentForm/ADD_MENTION';
// export const DELETE_MENTION = 'pm/ui/commentForm/DELETE_MENTION';
export const FOCUS = 'pm/ui/commentForm/FOCUS';
export const BLUR = 'pm/ui/commentForm/BLUR';

const CommentForm = {
  text: '',
  show: true,
  attachments: [],
  mentions: [],
  focus: false,
};

function commentFormReducer(state = CommentForm, action = {}) {
  switch (action.type) {
    case CREATE: {
      return Object.assign({}, state);
    }
    case SHOW: {
      return Object.assign({}, state, { show: true });
    }
    case HIDE_COMMENT_FORM: {
      return Object.assign({}, state, { show: false });
    }
    case ADD_ATTACHMENT: {
      return Object.assign({}, state, {
        attachments: state.attachments.concat(action.payload.attachment),
      });
    }
    case ADD_MENTION: {
      return Object.assign({}, state, {
        mentions: state.mentions.concat(action.payload.mention),
      });
    }
    case FOCUS: {
      return Object.assign({}, state, { focus: true });
    }
    case BLUR: {
      return Object.assign({}, state, { focus: false });
    }
    case SET_TEXT: {
      return Object.assign({}, state, { text: action.payload.text });
    }
    case CLEAR_COMMENT_FORM: {
      return Object.assign({}, CommentForm);
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case CREATE:
    case SHOW:
    case HIDE_COMMENT_FORM:
    case ADD_ATTACHMENT:
    case ADD_MENTION:
    case FOCUS:
    case BLUR:
    case SET_TEXT:
    case CLEAR_COMMENT_FORM: {
      return R.assocPath(
        [action.payload.type, action.payload.id],
        commentFormReducer(
          R.path([action.payload.type, action.payload.id], state),
          action,
        ),
      )(state);
    }
    default: {
      return state;
    }
  }
}

export const create = (id, type) => ({
  type: CREATE,
  payload: { id, type },
});

export const focus = (id, type) => ({
  type: FOCUS,
  payload: { id, type },
});

export const blur = (id, type) => ({
  type: BLUR,
  payload: { id, type },
});

export const addMention = (id, type, mention) => ({
  type: ADD_MENTION,
  payload: { id, mention, type },
});

export const addAttachment = (id, type, attachment) => ({
  type: ADD_ATTACHMENT,
  payload: { id, type, attachment },
});

export const setText = (id, type, text) => ({
  type: SET_TEXT,
  payload: { id, type, text },
});

export const clear = (id, type) => ({
  type: CLEAR_COMMENT_FORM,
  payload: { id, type },
});

export const show = (id, type) => ({
  type: SHOW,
  payload: { id, type },
});

export const hide = (id, type) => ({
  type: HIDE_COMMENT_FORM,
  payload: { id, type },
});
