import R from 'ramda';

export const SET_EMAIL = 'pm/ui/login/SET_EMAIL';
export const SET_PASSWORD = 'pm/ui/login/SET_PASSWORD';
export const SET_ERROR = 'pm/ui/login/SET_ERROR';
export const CLEAR_FORM = 'pm/ui/login/CLEAR_FORM';
export const SHOW_PASSWORD = 'pm/ui/login/SHOW_PASSWORD';
export const HIDE_PASSWORD = 'pm/ui/login/HIDE_PASSWORD';

export const LoginForm = {
  email: null,
  password: null,
  error: null,
  showPassword: false,
};

export default function reducer(state = LoginForm, action = {}) {
  switch (action.type) {
    case SET_EMAIL: {
      return Object.assign({}, state, {
        email: action.payload.email,
        error: null,
      });
    }
    case SET_PASSWORD: {
      return Object.assign({}, state, {
        password: action.payload.password,
        error: null,
      });
    }
    case SET_ERROR: {
      return Object.assign({}, state, { error: action.payload.error });
    }
    case SHOW_PASSWORD: {
      return Object.assign({}, state, { showPassword: true });
    }
    case HIDE_PASSWORD: {
      return Object.assign({}, state, { showPassword: false });
    }
    case CLEAR_FORM: {
      return Object.assign({}, LoginForm);
    }
    default: {
      return state;
    }
  }
}

export const setEmail = email => ({
  type: SET_EMAIL,
  payload: { email },
});

export const setPassword = password => ({
  type: SET_PASSWORD,
  payload: { password },
});

export const setError = error => ({
  type: SET_ERROR,
  payload: { error },
});

export const clearForm = () => ({
  type: CLEAR_FORM,
});

export const showPassword = () => ({
  type: SHOW_PASSWORD,
});

export const hidePassword = () => ({
  type: HIDE_PASSWORD,
});

export const loginSelector = state => R.pathOr({}, ['ui', 'login'], state);
