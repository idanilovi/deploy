import loginReducer, {
  LoginForm,
  SET_EMAIL,
  SET_PASSWORD,
  SET_ERROR,
  CLEAR_FORM,
  SHOW_PASSWORD,
  HIDE_PASSWORD,
  setEmail,
  setPassword,
  setError,
  clearForm,
  showPassword,
  hidePassword,
  loginSelector,
} from '../login';

describe('Testing login reducer, selectors and action creators: ', () => {
  it('Returns the same state on an unhandled action', () => {
    expect(loginReducer(LoginForm, { type: '_NULL' })).toMatchSnapshot();
  });

  describe('SET_EMAIL: ', () => {
    const email = 'test@test.com';
    it('action creator creates SET_EMAIL action', () => {
      expect(setEmail(email)).toEqual({
        type: SET_EMAIL,
        payload: { email },
      });
    });

    it('login reducer handles SET_EMAIL action', () => {
      expect(loginReducer(LoginForm, setEmail(email))).toEqual({
        ...LoginForm,
        email,
      });
    });
  });

  describe('SET_PASSWORD: ', () => {
    const password = 'testtest';
    it('action creator creates SET_PASSWORD action', () => {
      expect(setPassword(password)).toEqual({
        type: SET_PASSWORD,
        payload: { password },
      });
    });

    it('login reducer handles SET_PASSWORD action', () => {
      expect(loginReducer(LoginForm, setPassword(password))).toEqual({
        ...LoginForm,
        password,
      });
    });
  });

  describe('SET_ERROR: ', () => {
    const error = new Error();
    it('action creator creates SET_ERROR action', () => {
      expect(setError(error)).toEqual({
        type: SET_ERROR,
        payload: { error },
      });
    });

    it('login reducer handles SET_ERROR action', () => {
      expect(loginReducer(LoginForm, setError(error))).toEqual({
        ...LoginForm,
        error,
      });
    });
  });

  describe('CLEAR_FORM: ', () => {
    it('action creator creates CLEAR_FORM action', () => {
      expect(clearForm()).toEqual({ type: CLEAR_FORM });
    });

    it('login reducer handles CLEAR_FORM action', () => {
      const initialState = Object.assign({}, LoginForm, {
        login: 'test@test.com',
        password: 'testtest',
        error: new Error(),
        showPassword: true,
      });
      expect(loginReducer(initialState, clearForm())).toEqual(LoginForm);
    });
  });

  describe('SHOW_PASSWORD: ', () => {
    it('action creator creates SHOW_PASSWORD action', () => {
      expect(showPassword()).toEqual({ type: SHOW_PASSWORD });
    });

    it('login reducer handles SHOW_PASSWORD action', () => {
      expect(loginReducer(LoginForm, showPassword())).toEqual({
        ...LoginForm,
        showPassword: true,
      });
    });
  });

  describe('HIDE_PASSWORD: ', () => {
    it('action creator creates HIDE_PASSWORD action', () => {
      expect(hidePassword()).toEqual({ type: HIDE_PASSWORD });
    });

    it('login reducer handles HIDE_PASSWORD action', () => {
      const initialState = Object.assign({}, LoginForm, { showPassword: true });
      expect(loginReducer(initialState, hidePassword())).toEqual({
        ...LoginForm,
        showPassword: false,
      });
    });
  });

  describe('loginSelector: ', () => {
    it('login selector should return state.ui.login', () => {
      const state = {
        ui: {
          login: {
            ...LoginForm,
          },
        },
      };
      expect(loginSelector(state)).toEqual(state.ui.login);
    });
  });
});
