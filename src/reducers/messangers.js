import R from 'ramda';

export const RECEIVE_MESSANGERS = 'pm/comments/RECEIVE_MESSANGERS';
export const RECEIVE_MESSANGER = 'pm/comments/RECEIVE_MESSANGER';
export const REMOVE_MESSANGERS = 'pm/comments/REMOVE_MESSANGERS';

const Messangers = {};

function messangerFactory(messanger) {
  // eslint-disable-next-line no-underscore-dangle
  return Object.assign({}, { [messanger._id]: messanger });
}

function messangersReducer(state = Messangers, action = {}) {
  switch (action.type) {
    case RECEIVE_MESSANGER: {
      return Object.assign({}, state, action.payload.messanger);
    }
    case RECEIVE_MESSANGERS: {
      return R.mergeAll(R.map(messangerFactory, action.payload.messangers));
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_MESSANGER: {
      return messangersReducer(state, action);
    }
    case RECEIVE_MESSANGERS: {
      return messangersReducer(state, action);
    }
    case REMOVE_MESSANGERS: {
      return {};
    }
    default: {
      return state;
    }
  }
}

export const receiveMessanger = messanger => ({
  type: RECEIVE_MESSANGER,
  payload: { messanger },
});

export const receiveMessangers = messangers => ({
  type: RECEIVE_MESSANGERS,
  payload: { messangers },
});

export const removeMessangers = () => ({
  type: REMOVE_MESSANGERS,
});

export const messangersSelector = state => R.propOr({}, 'messangers', state);
