import R from 'ramda';
import { createSelector } from 'reselect';

export const RECEIVE_NOTIFICATIONS = 'pm/comments/RECEIVE_NOTIFICATIONS';
export const RECEIVE_NOTIFICATION = 'pm/comments/RECEIVE_NOTIFICATION';
export const REMOVE_NOTIFICATIONS = 'pm/comments/REMOVE_NOTIFICATIONS';

const Notifications = {};

function notificationFactory(notification) {
  // eslint-disable-next-line no-underscore-dangle
  return Object.assign({}, { [notification._id]: notification });
}

function notificationsReducer(state = Notifications, action = {}) {
  switch (action.type) {
    case RECEIVE_NOTIFICATION: {
      return Object.assign({}, state, action.payload.notification);
    }
    case RECEIVE_NOTIFICATIONS: {
      return R.mergeAll(
        R.map(notificationFactory, action.payload.notifications),
      );
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_NOTIFICATION: {
      return notificationsReducer(state, action);
    }
    case RECEIVE_NOTIFICATIONS: {
      return notificationsReducer(state, action);
    }
    case REMOVE_NOTIFICATIONS: {
      return {};
    }
    default: {
      return state;
    }
  }
}

export const receiveNotification = notification => ({
  type: RECEIVE_NOTIFICATION,
  payload: { notification },
});

export const receiveNotifications = notifications => ({
  type: RECEIVE_NOTIFICATIONS,
  payload: { notifications },
});

export const removeNotifications = () => ({
  type: REMOVE_NOTIFICATIONS,
});

const sortByCreatedAt = R.sortBy(R.prop('createdAt'));

export const notificationsSelector = state =>
  R.propOr({}, 'notifications', state);
export const notificationsArraySelector = createSelector(
  notificationsSelector,
  notifications => R.values(notifications),
);
export const orderedNotificationsSelector = createSelector(
  notificationsArraySelector,
  notifications => sortByCreatedAt(notifications),
);
export const reversedOrderedNotificationsSelector = createSelector(
  orderedNotificationsSelector,
  notifications => R.reverse(notifications),
);
