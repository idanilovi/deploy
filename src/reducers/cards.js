import R from 'ramda';

export const RECEIVE_CARDS = 'pm/comments/RECEIVE_CARDS';
export const RECEIVE_CARD = 'pm/comments/RECEIVE_CARD';
export const REMOVE_CARDS = 'pm/comments/REMOVE_CARDS';

const Cards = {};

function cardFactory(card) {
  // eslint-disable-next-line no-underscore-dangle
  return Object.assign({}, { [card._id]: card });
}

function cardsReducer(state = Cards, action = {}) {
  switch (action.type) {
    case RECEIVE_CARD: {
      return Object.assign({}, state, action.payload.card);
    }
    case RECEIVE_CARDS: {
      return R.mergeAll(R.map(cardFactory, action.payload.cards));
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_CARD: {
      return cardsReducer(state, action);
    }
    case RECEIVE_CARDS: {
      return cardsReducer(state, action);
    }
    case REMOVE_CARDS: {
      return {};
    }
    default: {
      return state;
    }
  }
}

export const receiveCard = card => ({
  type: RECEIVE_CARD,
  payload: { card },
});

export const receiveCards = cards => ({
  type: RECEIVE_CARDS,
  payload: { cards },
});

export const removeCards = () => ({
  type: REMOVE_CARDS,
});
