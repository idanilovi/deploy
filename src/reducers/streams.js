import R from 'ramda';
import { createSelector } from 'reselect';

export const REQUEST_STREAM = 'pm/streams/REQUEST_STREAM';
export const RECEIVE_STREAM = 'pm/streams/RECEIVE_STREAM';
export const RECEIVE_STREAMS = 'pm/streams/RECEIVE_STREAMS';
export const REMOVE_STREAM = 'pm/streams/REMOVE_STREAM';
export const SET_WIDGETS = 'pm/streams/SET_WIDGETS';

export const SET_NAME = 'pm/streams/SET_NAME';
export const SET_RESPONSIBLE_USER = 'pm/streams/SET_RESPONSIBLE_USER';
export const SET_PRIVACY = 'pm/streams/SET_PRIVACY';
export const ADD_ADMIN = 'pm/streams/ADD_ADMIN';
export const DELETE_ADMIN = 'pm/streams/DELETE_ADMIN';
export const ADD_ROLE = 'pm/streams/ADD_ROLE';
export const DELETE_ROLE = 'pm/streams/DELETE_ROLE';

export const REQUEST_STREAM_THREADS = 'pm/streams/REQUEST_STREAM_THREADS';
export const RECEIVE_STREAM_THREADS = 'pm/streams/RECEIVE_STREAM_THREADS';
export const REQUEST_STREAM_COMMENTS = 'pm/streams/REQUEST_STREAM_COMMENTS';
export const RECEIVE_STREAM_COMMENTS = 'pm/streams/RECEIVE_STREAM_COMMENTS';

const Stream = {};

function streamsReducer(state = Stream, action = {}) {
  if (state.id && state.id !== action.payload.id) {
    return state;
  }
  switch (action.type) {
    case REQUEST_STREAM: {
      return Object.assign({}, state, { loading: true });
    }
    case REQUEST_STREAM_THREADS: {
      return Object.assign({}, state, {
        threadsLoading: true,
        threadsLoaded: false,
      });
    }
    case RECEIVE_STREAM_THREADS: {
      return Object.assign({}, state, {
        threadsLoading: false,
        threadsLoaded: true,
      });
    }
    case RECEIVE_STREAM_COMMENTS: {
      return Object.assign({}, state, {
        commentsLoaded: action.payload.commentsLoaded,
        commentsLoading: false,
      });
    }
    case REQUEST_STREAM_COMMENTS: {
      return Object.assign({}, state, {
        commentsLoading: true,
      });
    }
    case RECEIVE_STREAM: {
      return Object.assign({}, Stream, action.payload, {
        loading: false,
      });
    }
    case SET_WIDGETS: {
      return R.assocPath(
        ['settings', 'widgets'],
        action.payload.widgets,
        state,
      );
    }
    case SET_NAME: {
      return Object.assign({}, state, { name: action.payload.name });
    }
    case SET_RESPONSIBLE_USER: {
      return Object.assign({}, state, { owner: action.payload.userId });
    }
    case SET_PRIVACY: {
      return Object.assign({}, state, { isPrivate: !state.isPrivate });
    }
    case ADD_ADMIN: {
      if (state.admins.includes(action.payload.userId)) {
        return state;
      }
      return Object.assign({}, state, {
        admins: state.admins.concat(action.payload.userId),
      });
    }
    case DELETE_ADMIN: {
      if (!state.admins.includes(action.payload.userId)) {
        return state;
      }
      const index = R.propOr([], 'admins', state).indexOf(
        action.payload.userId,
      );
      const admins = state.admins
        .slice(0, index)
        .concat(state.admins.slice(index + 1));
      return Object.assign({}, state, { admins });
    }
    case ADD_ROLE: {
      if (state.roles.includes(action.payload.userId)) {
        return state;
      }
      return Object.assign({}, state, {
        roles: state.roles.concat(action.payload.userId),
      });
    }
    case DELETE_ROLE: {
      if (!state.roles.includes(action.payload.userId)) {
        return state;
      }
      const index = R.propOr([], 'roles', state).indexOf(action.payload.userId);
      const roles = state.roles
        .slice(0, index)
        .concat(state.roles.slice(index + 1));
      return Object.assign({}, state, { roles });
    }
    case RECEIVE_STREAMS: {
      return Object.assign({}, state, action.payload, { loading: false });
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SET_NAME:
    case SET_RESPONSIBLE_USER:
    case SET_PRIVACY:
    case ADD_ADMIN:
    case DELETE_ADMIN:
    case ADD_ROLE:
    case DELETE_ROLE:
    case SET_WIDGETS:
    case REQUEST_STREAM_COMMENTS:
    case RECEIVE_STREAM_COMMENTS:
    case REQUEST_STREAM_THREADS:
    case RECEIVE_STREAM_THREADS:
    case REQUEST_STREAM:
    case RECEIVE_STREAM: {
      return R.assoc(
        action.payload.id,
        streamsReducer(state[action.payload.id], action),
      )(state);
    }
    case RECEIVE_STREAMS: {
      return R.merge(
        state,
        R.mergeAll(
          R.map(
            stream => ({ [stream.id]: { ...stream } }),
            action.payload.streams,
          ),
        ),
      );
    }
    case REMOVE_STREAM: {
      return R.dissoc(action.payload.id, state);
    }
    default: {
      return state;
    }
  }
}

export const setName = (id, name) => ({
  type: SET_NAME,
  payload: { id, name },
});

export const setResponsibleUser = (id, userId) => ({
  type: SET_RESPONSIBLE_USER,
  payload: { id, userId },
});

export const setPrivacy = id => ({ type: SET_PRIVACY, payload: { id } });

export const addAdmin = (id, userId) => ({
  type: ADD_ADMIN,
  payload: { id, userId },
});

export const deleteAdmin = (id, userId) => ({
  type: DELETE_ADMIN,
  payload: { id, userId },
});

export const addRole = (id, userId) => ({
  type: ADD_ROLE,
  payload: { id, userId },
});

export const deleteRole = (id, userId) => ({
  type: DELETE_ROLE,
  payload: { id, userId },
});

export const setWidgetSettings = (id, widgets) => ({
  type: SET_WIDGETS,
  payload: { id, widgets },
});

export function requestStream(id) {
  return {
    type: REQUEST_STREAM,
    payload: { id },
  };
}

export function receiveStream(stream) {
  return {
    type: RECEIVE_STREAM,
    payload: stream,
  };
}

export function requestStreamThreads(id) {
  return {
    type: REQUEST_STREAM_THREADS,
    payload: { id },
  };
}

export function receiveStreamThreads(id) {
  return {
    type: RECEIVE_STREAM_THREADS,
    payload: { id },
  };
}

export function requestStreamComments(id) {
  return {
    type: REQUEST_STREAM_COMMENTS,
    payload: { id },
  };
}

export function receiveStreamComments(id, commentsLoaded) {
  return {
    type: RECEIVE_STREAM_COMMENTS,
    payload: { id, commentsLoaded },
  };
}

export function receiveStreams(streams) {
  return {
    type: RECEIVE_STREAMS,
    payload: { streams },
  };
}

export function removeStream(id) {
  return {
    type: REMOVE_STREAM,
    payload: { id },
  };
}

export const streamsSelector = state => R.propOr({}, 'streams', state);
export const streamsArraySelector = createSelector(streamsSelector, streams =>
  R.values(streams),
);
export const streamIdSelector = (state, ownProps) =>
  R.propOr(null, 'streamId', ownProps);
export const streamByIdSelector = createSelector(
  streamIdSelector,
  streamsSelector,
  (streamId, streams) => R.propOr({}, streamId, streams),
);
