/* eslint-disable id-length */
import R from 'ramda';

export const RECEIVE_STATUS = 'project_managment/statuses/RECEIVE_STATUS';
export const RECEIVE_STATUSES = 'project_managment/statuses/RECEIVE_STATUSES';
export const DELETE_STATUS = 'project_managment/statuses/DELETE_STATUS';
export const SET_STATUS_NAME = 'project_managment/statuses/SET_STATUS_NAME';

const Status = {
  color: '',
  id: '',
  name: '',
  streamId: '',
  teamId: '',
  type: '',
};

function statusesReducer(state = Status, action = {}) {
  if (state.id && state.id !== action.payload.id) return state;
  switch (action.type) {
    case SET_STATUS_NAME:
      return Object.assign({}, state, { name: action.payload.name });
    case RECEIVE_STATUS: {
      return Object.assign({}, state, action.payload.status);
    }
    case RECEIVE_STATUSES: {
      return Object.assign({}, state, action.payload);
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SET_STATUS_NAME:
    case RECEIVE_STATUS: {
      return R.assoc(
        action.payload.id,
        statusesReducer(state[action.payload.id], action),
      )(state);
    }
    case RECEIVE_STATUSES: {
      return R.merge(
        state,
        R.mergeAll(
          R.map(
            status => ({
              [status.id]: status,
            }),
            action.payload.statuses,
          ),
        ),
      );
    }
    case DELETE_STATUS: {
      return R.dissoc(action.payload.id, state);
    }
    default: {
      return state;
    }
  }
}

export function receiveStatus(status) {
  return {
    type: RECEIVE_STATUS,
    payload: { status },
  };
}

export function receiveStatuses(statuses) {
  return {
    type: RECEIVE_STATUSES,
    payload: { statuses },
  };
}

export function deleteStatus(id) {
  return {
    type: DELETE_STATUS,
    payload: { id },
  };
}

export const statusesSelector = state => R.propOr({}, 'statuses', state);
