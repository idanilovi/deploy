import { combineReducers } from 'redux';
import userData from './userdata';
import streams from './streams';
import comments from './comments';
import threads from './threads';
import files from './files';
import statuses from './statuses';
import contacts from './contacts';
import descriptions from './description';
import ui from './ui';
import notifications from './notifications';
import teams from './teams';
import cards from './cards';
import payments from './payments';
import trunks from './trunks';
import mails from './mails';
import messangers from './messangers';

const appReducer = combineReducers({
  userData,
  streams,
  comments,
  cards,
  payments,
  threads,
  files,
  statuses,
  contacts,
  descriptions,
  ui,
  notifications,
  teams,
  trunks,
  mails,
  messangers,
});

const USER_LOGOUT = 'pm/USER_LOGOUT';

export default (state, action) => {
  if (action.type === USER_LOGOUT) {
    state = undefined;
  }
  return appReducer(state, action);
};

export const userLogout = () => ({
  type: USER_LOGOUT,
});
