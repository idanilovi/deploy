import R from 'ramda';
import { createSelector } from 'reselect';

export const REQUEST_THREAD = 'pm/threads/REQUEST_THREAD';
export const RECEIVE_THREAD = 'pm/threads/RECEIVE_THREAD';
export const RECEIVE_THREADS = 'pm/threads/RECEIVE_THREADS';
export const REMOVE_THREAD = 'pm/threads/REMOVE_THREAD';
export const SET_THREAD_TITLE = 'pm/threads/SET_THREAD_TITLE';
export const SET_STATUS = 'pm/threads/SET_STATUS';
export const SET_POINTS = 'pm/threads/SET_POINTS';
export const SET_RESPONSIBLE = 'pm/threads/SET_RESPONSIBLE';
export const SET_CUSTOMER = 'pm/threads/SET_CUSTOMER';
export const DELETE_RESPONSIBLE = 'pm/threads/DELETE_RESPONSIBLE';
export const DELETE_CUSTOMER = 'pm/threads/DELETE_CUSTOMER';
export const SET_ROLE = 'pm/threads/SET_ROLE';
export const SET_PRIORITY = 'pm/threads/SET_PRIORITY';
export const DELETE_ROLE = 'pm/threads/DELETE_ROLE';
export const SET_BUDGET = 'pm/threads/SET_BUDGET';
export const SET_TIME = 'pm/threads/SET_TIME';
export const SET_STREAM = 'pm/threads/SET_STREAM';
export const SET_DEADLINE = 'pm/threads/SET_DEADLINE';
export const SET_POSITION = 'pm/threads/SET_POSITION';
export const SET_COMMENTS_LOADED = 'pm/threads/SET_COMMENTS_LOADED';

export const RECEIVE_THREAD_COMMENTS = 'pm/threads/RECEIVE_THREAD_COMMENTS';
export const REQUEST_THREAD_COMMENTS = 'pm/threads/REQUEST_THREAD_COMMENTS';

const Thread = {};

function threadsReducer(state = Thread, action = {}) {
  if (state.id && state.id !== action.payload.id) {
    return state;
  }
  switch (action.type) {
    case SET_THREAD_TITLE: {
      return R.assoc('title', action.payload.value, state);
    }
    case SET_STATUS: {
      return R.assoc('status', action.payload.statusId, state);
    }
    case SET_POINTS: {
      return R.assoc('mainRank', action.payload.points, state);
    }
    case SET_BUDGET: {
      return R.assoc('budget', action.payload.budget, state);
    }
    case SET_RESPONSIBLE: {
      return R.assoc('responsibleUserId', action.payload.userId, state);
    }
    case SET_CUSTOMER: {
      return R.assoc('customerId', action.payload.contactId, state);
    }
    case SET_STREAM: {
      return R.assoc('streamId', action.payload.streamId, state);
    }
    case SET_PRIORITY: {
      return R.assoc('priority', action.payload.priority, state);
    }
    case SET_TIME: {
      return R.assoc('resource', action.payload.time, state);
    }
    case SET_DEADLINE: {
      return R.assoc('deadline', action.payload.deadline, state);
    }
    case SET_POSITION: {
      return R.assoc('position', action.payload.position, state);
    }
    case SET_COMMENTS_LOADED: {
      return R.assoc('loaded', action.payload.loaded, state);
    }
    case DELETE_RESPONSIBLE: {
      return R.assoc('responsibleUserId', null, state);
    }
    case DELETE_CUSTOMER: {
      return R.assoc('customerId', null, state);
    }
    case SET_ROLE: {
      return R.assoc(
        'roles',
        state.roles.concat([action.payload.userId]),
        state,
      );
    }
    case DELETE_ROLE: {
      return R.assoc(
        'roles',
        R.filter(role => role !== action.payload.userId, state.roles),
        state,
      );
    }
    case RECEIVE_THREAD_COMMENTS: {
      return Object.assign({}, state, { commentsLoaded: true });
    }
    case REQUEST_THREAD_COMMENTS: {
      return Object.assign({}, state, { commentsLoaded: false });
    }
    case REQUEST_THREAD: {
      return Object.assign({}, state, { loading: true });
    }
    case RECEIVE_THREAD: {
      return Object.assign({}, Thread, action.payload.thread);
    }
    case RECEIVE_THREADS: {
      return R.assoc('loading', false, action.payload.thread);
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SET_POSITION:
    case SET_DEADLINE:
    case SET_THREAD_TITLE:
    case SET_STATUS:
    case SET_STREAM:
    case SET_TIME:
    case SET_BUDGET:
    case SET_PRIORITY:
    case SET_POINTS:
    case SET_RESPONSIBLE:
    case DELETE_RESPONSIBLE:
    case SET_CUSTOMER:
    case DELETE_CUSTOMER:
    case SET_ROLE:
    case DELETE_ROLE:
    case REQUEST_THREAD:
    case RECEIVE_THREAD_COMMENTS:
    case REQUEST_THREAD_COMMENTS:
    case SET_COMMENTS_LOADED:
    case RECEIVE_THREAD: {
      return R.assoc(
        action.payload.id,
        threadsReducer(state[action.payload.id], action),
      )(state);
    }
    case RECEIVE_THREADS: {
      return R.merge(
        state,
        R.mergeAll(
          R.map(thread => ({ [thread.id]: thread }), action.payload.threads),
        ),
      );
    }
    case REMOVE_THREAD: {
      return R.dissoc(action.payload.id, state);
    }
    default: {
      return state;
    }
  }
}

export function requestThreadComments(id) {
  return {
    type: REQUEST_THREAD_COMMENTS,
    payload: { id },
  };
}

export function receiveThreadComments(id) {
  return {
    type: RECEIVE_THREAD_COMMENTS,
    payload: { id },
  };
}

export function setThreadTitle(id, value) {
  return {
    type: SET_THREAD_TITLE,
    payload: { id, value },
  };
}

export function setStream(id, streamId) {
  return {
    type: SET_STREAM,
    payload: { id, streamId },
  };
}

export function setPoints(id, points) {
  return {
    type: SET_POINTS,
    payload: { id, points },
  };
}

export function setDeadline(id, deadline) {
  return {
    type: SET_DEADLINE,
    payload: { id, deadline },
  };
}

export function setPosition(id, position) {
  return {
    type: SET_POSITION,
    payload: { id, position },
  };
}

export function setLoaded(id, loaded) {
  return {
    type: SET_COMMENTS_LOADED,
    payload: { id, loaded },
  };
}

export function setTime(id, time) {
  return {
    type: SET_TIME,
    payload: { id, time },
  };
}

export function setBudget(id, budget) {
  return {
    type: SET_BUDGET,
    payload: { id, budget },
  };
}

export function setPriority(id, priority) {
  return {
    type: SET_PRIORITY,
    payload: { id, priority },
  };
}

export function setStatus(id, statusId) {
  return {
    type: SET_STATUS,
    payload: { id, statusId },
  };
}
export function setResponsible(id, userId) {
  return {
    type: SET_RESPONSIBLE,
    payload: { id, userId },
  };
}

export function deleteResponsible(id) {
  return {
    type: DELETE_RESPONSIBLE,
    payload: { id },
  };
}
export function setCustomer(id, userId) {
  return {
    type: SET_CUSTOMER,
    payload: { id, userId },
  };
}

export function deleteCustomer(id) {
  return {
    type: DELETE_CUSTOMER,
    payload: { id },
  };
}

export function setRole(id, userId) {
  return {
    type: SET_ROLE,
    payload: { id, userId },
  };
}

export function deleteRole(id, userId) {
  return {
    type: DELETE_ROLE,
    payload: { id, userId },
  };
}

export function requestThread(id) {
  return {
    type: REQUEST_THREAD,
    payload: { id },
  };
}

export function receiveThread(thread) {
  return {
    type: RECEIVE_THREAD,
    payload: { id: thread.id, thread },
  };
}

export function receiveThreads(threads) {
  return {
    type: RECEIVE_THREADS,
    payload: { threads },
  };
}

export function removeThread(id) {
  return {
    type: REMOVE_THREAD,
    payload: { id },
  };
}

export const threadsSelector = state => R.propOr({}, 'threads', state);

export const threadIdSelector = (state, ownProps) =>
  R.propOr(null, 'threadId', ownProps) || R.propOr(null, 'id', ownProps);

export const threadByIdSelector = createSelector(
  threadIdSelector,
  threadsSelector,
  (threadId, threads) => R.propOr({}, threadId, threads),
);
