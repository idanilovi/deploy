/* eslint-disable id-length */
import R from 'ramda';

export const REQUEST_FILE = 'project_managment/files/REQUEST_FILE';
export const RECEIVE_FILE = 'project_managment/files/RECEIVE_FILE';
export const RECEIVE_FILES = 'project_managment/files/RECEIVE_FILES';
export const DELETE_FILE = 'project_managment/files/DELETE_FILE';

const File = {};

function filesReducer(state = File, action = {}) {
  switch (action.type) {
    case REQUEST_FILE: {
      return Object.assign({}, state, { loading: true });
    }
    case RECEIVE_FILE: {
      return Object.assign({}, state, action.payload, { loading: false });
    }
    case RECEIVE_FILES: {
      return Object.assign({}, state, action.payload, { loading: false });
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case REQUEST_FILE:
    case RECEIVE_FILE: {
      if (!action.payload.id) {
        return state;
      }
      return R.assoc(
        action.payload.id,
        filesReducer(state[action.payload.id], action),
      )(state);
    }
    case RECEIVE_FILES: {
      return R.merge(
        state,
        R.mergeAll(R.map(file => ({ [file.id]: { ...file } }), action.payload)),
      );
    }
    case DELETE_FILE: {
      return R.dissoc(action.payload.id, state);
    }
    default: {
      return state;
    }
  }
}

export function requestFile(id) {
  return {
    type: REQUEST_FILE,
    payload: { id },
  };
}

export function receiveFile(file) {
  return {
    type: RECEIVE_FILE,
    payload: file,
  };
}

export function receiveFiles(files) {
  return {
    type: RECEIVE_FILES,
    payload: files,
  };
}

export function deleteFile(id) {
  return {
    type: DELETE_FILE,
    payload: { id },
  };
}
