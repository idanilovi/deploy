import R from 'ramda';

export const RECEIVE_COMMENT = 'pm/comments/RECEIVE_COMMENT';
export const DELETE_COMMENT = 'pm/comments/DELETE_COMMENT';
export const UPDATE_COMMENT = 'pm/comments/UPDATE_COMMENT';
export const RECEIVE_COMMENTS = 'pm/comments/RECEIVE_COMMENTS';

const Comment = {};

function commentsReducer(state = Comment, action = {}) {
  if (state._id && state._id !== action.payload.comment._id) {
    return state;
  }
  switch (action.type) {
    case RECEIVE_COMMENT: {
      return Object.assign({}, Comment, action.payload.comment);
    }
    case RECEIVE_COMMENTS: {
      return Object.assign({}, Comment, action.payload);
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RECEIVE_COMMENT: {
      return R.assoc(
        action.payload.comment._id,
        commentsReducer(state[action.payload.comment._id], action),
      )(state);
    }
    case RECEIVE_COMMENTS: {
      return R.merge(
        state,
        R.mergeAll(
          R.map(
            comment => ({ [comment._id]: { ...comment } }),
            action.payload.comments,
          ),
        ),
      );
    }
    case DELETE_COMMENT: {
      return R.dissoc(action.payload._id, state);
    }
    default: {
      return state;
    }
  }
}

export function receiveComment(comment) {
  return {
    type: RECEIVE_COMMENT,
    payload: { comment },
  };
}

export function receiveComments(comments) {
  return {
    type: RECEIVE_COMMENTS,
    payload: { comments },
  };
}
export function deleteComment(_id) {
  return {
    type: DELETE_COMMENT,
    payload: { _id },
  };
}
export function updateComment(_id, comment) {
  return {
    type: UPDATE_COMMENT,
    payload: { _id, comment },
  };
}
