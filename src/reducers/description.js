import R from 'ramda';

export const REQUEST_DESCRIPTION = 'pm/descriptions/REQUEST_DESCRIPTION';
export const RECEIVE_DESCRIPTION = 'pm/descriptions/RECEIVE_DESCRIPTION';
export const REMOVE_DESCRIPTION = 'pm/descriptions/REMOVE_DESCRIPTION';

const Description = {};

function descriptionsReducer(state = Description, action = {}) {
  if (state.id && state.id !== action.payload.id) {
    return state;
  }
  switch (action.type) {
    case REQUEST_DESCRIPTION: {
      return Object.assign({}, Description, { loading: true });
    }
    case RECEIVE_DESCRIPTION: {
      return Object.assign({}, Description, action.payload, {
        loading: false,
      });
    }
    default: {
      return state;
    }
  }
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case REQUEST_DESCRIPTION:
    case RECEIVE_DESCRIPTION: {
      return R.assoc(
        action.payload.id,
        descriptionsReducer(state[action.payload.id], action),
      )(state);
    }
    case REMOVE_DESCRIPTION: {
      return R.dissoc(action.payload, state);
    }
    default: {
      return state;
    }
  }
}

export function requestDescription(id) {
  return {
    type: REQUEST_DESCRIPTION,
    payload: { id },
  };
}

export function receiveDescription(description) {
  return {
    type: RECEIVE_DESCRIPTION,
    payload: description,
  };
}

export function removeDescription(id) {
  return {
    type: REMOVE_DESCRIPTION,
    payload: id,
  };
}

export const descriptionsSelector = state =>
  R.propOr({}, 'descriptions', state);
