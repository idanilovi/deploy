import R from 'ramda';
import { createSelector } from 'reselect';

export const SET_USER_DATA = 'pm/userdata/SET_USER_DATA';

const UserData = {};

export default function reducer(state = UserData, action = {}) {
  switch (action.type) {
    case SET_USER_DATA: {
      return Object.assign({}, state, action.payload);
    }
    default: {
      return state;
    }
  }
}

export function setUserData(userData) {
  return {
    type: SET_USER_DATA,
    payload: userData,
  };
}

export const userdataSelector = state => R.propOr({}, 'userData', state);
export const userIdSelector = createSelector(userdataSelector, userdata =>
  R.propOr(null, 'userId', userdata),
);
export const isAdminSelector = createSelector(userdataSelector, userdata =>
  R.propOr(false, 'isAdmin', userdata),
);
