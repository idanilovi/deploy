import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import Sidebar from '../../components/Sidebar';
import { userIdSelector, isAdminSelector } from '../../reducers/userdata';
import { userColorSelector } from '../../selectors';
import { unsubscribeNotify } from '../../actions';

class SidebarDrawer extends Component {
  static displayName = 'SidebarDrawer';
  static propTypes = {
    color: PropTypes.string,
    userId: PropTypes.string,
    isAdmin: PropTypes.bool,
    pushToLoginScreen: PropTypes.func.isRequired,
    pushToTeamsScreen: PropTypes.func.isRequired,
    pushToBotsScreen: PropTypes.func.isRequired,
    pushToBillingScreen: PropTypes.func.isRequired,
    pushToChannelsScreen: PropTypes.func.isRequired,
    pushToProfileScreen: PropTypes.func.isRequired,
  };
  render() {
    const {
      color,
      userId,
      isAdmin,
      pushToBotsScreen,
      pushToTeamsScreen,
      pushToLoginScreen,
      pushToBillingScreen,
      pushToChannelsScreen,
      pushToProfileScreen,
    } = this.props;
    return (
      <Sidebar>
        <Sidebar.Header id={userId} isAdmin={isAdmin} color={color} />
        <Sidebar.Content>
          <Sidebar.Button title="Channels" onPress={pushToChannelsScreen} />
          <Sidebar.Button title="Botstore" onPress={pushToBotsScreen} />
          <Sidebar.Button
            show={isAdmin}
            title="Billing"
            onPress={pushToBillingScreen}
          />
          <Sidebar.Button title="My profile" onPress={pushToProfileScreen} />
          <Sidebar.Button title="Change team" onPress={pushToTeamsScreen} />
          <Sidebar.Button title="Logout" onPress={pushToLoginScreen} />
        </Sidebar.Content>
      </Sidebar>
    );
  }
}

const mapStateToProps = state => ({
  userId: userIdSelector(state),
  isAdmin: isAdminSelector(state),
  color: userColorSelector(state),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  pushToLoginScreen: () => {
    ownProps.closeDrawer();
    dispatch(unsubscribeNotify({ deleteInstance: true }));
    ownProps.navigator.push({
      screen: 'workonflow.Login',
      passProps: { dontHideSplash: true },
    });
  },
  pushToTeamsScreen: () => {
    ownProps.closeDrawer();
    dispatch(unsubscribeNotify({ deleteInstance: true }));
    ownProps.navigator.push({
      screen: 'workonflow.LoginTeams',
      passProps: { dontEnter: true },
    });
  },
  pushToBotsScreen: () => {
    ownProps.closeDrawer();
    ownProps.navigator.push({
      screen: 'workonflow.Bots',
      title: 'Botstore',
    });
  },
  pushToBillingScreen: () => {
    ownProps.closeDrawer();
    Navigation.showModal({
      screen: 'workonflow.Billing',
      style: { backgroundBlur: 'dark' },
      passProps: {},
    });
  },
  pushToChannelsScreen: () => {
    ownProps.closeDrawer();
    Navigation.showModal({
      screen: 'workonflow.Channels',
      style: { backgroundBlur: 'dark' },
      passProps: {},
    });
  },
  pushToProfileScreen: () => {
    ownProps.closeDrawer();
    ownProps.scrollableTabViewRef.goToPage(4);
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarDrawer);
