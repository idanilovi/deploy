import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import {
  Dimensions,
  View,
  WebView,
  Text,
  TouchableOpacity,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import withSafeArea from '../withSafeArea';

const { width, height } = Dimensions.get('window');

class Email extends PureComponent {
  static displayName = 'Email';
  static propTypes = {
    title: PropTypes.string,
    html: PropTypes.string,
    text: PropTypes.string,
    to: PropTypes.string,
    from: PropTypes.string,
    subject: PropTypes.string,
  };
  static navigatorStyle = {
    navBarHidden: true,
  };
  constructor() {
    super();
    this.handleOnClose = this.handleOnClose.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnClose() {
    Navigation.dismissModal();
  }
  render() {
    const { title, html, text, to, from, subject } = this.props;
    return (
      <View
        style={{
          flex: 1,
          width,
          height,
          backgroundColor: 'rgba(40,30,42,1)',
          paddingHorizontal: 18,
          paddingTop: 18,
        }}
      >
        <Header
          title={title}
          LeftHeaderComponent={<BackButton onPress={this.handleOnClose} />}
        />
        <Body text={text} html={html} to={to} from={from} subject={subject} />
      </View>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

export default connect(mapStateToProps)(withSafeArea(Email));

class Body extends PureComponent {
  static displayName = 'Body';
  static propTypes = {
    html: PropTypes.string,
    text: PropTypes.string,
    to: PropTypes.string,
    from: PropTypes.string,
    subject: PropTypes.string,
  };
  render() {
    const { html, text, to, from, subject } = this.props;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#ffffff',
          borderRadius: 13,
        }}
      >
        <EmailModalHeader to={to} from={from} subject={subject} />
        <EmailModalContent text={text} html={html} />
      </View>
    );
  }
}

class EmailModalHeaderField extends PureComponent {
  static displayName = 'EmailModalHeaderField';
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.string),
    ]),
    label: PropTypes.string,
  };
  constructor() {
    super();
    this.renderValue = this.renderValue.bind(this);
  }
  renderValue() {
    const { value } = this.props;
    if (Array.isArray(value)) {
      return (
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 0 }}>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={{
                flex: 0,
                fontSize: 15,
                fontWeight: '300',
                color: '#9996a9',
              }}
            >
              {value[0]}
            </Text>
          </View>
          {value.length - 1 > 0 ? (
            <View
              style={{
                flex: 0,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#9996a9',
                marginLeft: 8,
                paddingHorizontal: 6,
                paddingVertical: 2,
              }}
            >
              <Text>{`+${value.length - 1}`}</Text>
            </View>
          ) : null}
        </View>
      );
    }
    return (
      <Text
        ellipsizeMode="tail"
        numberOfLines={1}
        style={{
          fontSize: 15,
          fontWeight: '300',
          color: '#9996a9',
        }}
      >
        {value}
      </Text>
    );
  }
  render() {
    const { label } = this.props;
    return (
      <View
        style={{
          flex: 0,
          paddingVertical: 3,
          flexDirection: 'row',
          alignItems: 'baseline',
          maxWidth: width - 112,
        }}
      >
        <Text
          style={{
            fontSize: 15,
            fontWeight: '300',
            color: '#c4c2cf',
            flexBasis: 42,
          }}
        >
          {label}
        </Text>
        {this.renderValue()}
      </View>
    );
  }
}

class EmailModalHeader extends PureComponent {
  static displayName = 'EmailModalHeader';
  static propTypes = {
    to: PropTypes.string,
    from: PropTypes.string,
    subject: PropTypes.string,
  };
  render() {
    const { to, from, subject } = this.props;
    return (
      <View
        style={{
          flex: 0,
          backgroundColor: '#f3eff2',
          paddingTop: 20,
          paddingBottom: 20,
          paddingHorizontal: 23,
          borderTopLeftRadius: 13,
          borderTopRightRadius: 13,
        }}
      >
        <EmailModalHeaderField label="To" value={to} />
        <EmailModalHeaderField label="From" value={from} />
        <EmailModalHeaderField label="Topic" value={subject} />
      </View>
    );
  }
}

class EmailModalContent extends PureComponent {
  static displayName = 'EmailModalContent';
  static propTypes = {
    html: PropTypes.string,
    text: PropTypes.string,
  };
  render() {
    const { html, text } = this.props;
    if (html) {
      return (
        <WebView
          source={{ html, baseUrl: '' }}
          automaticallyAdjustContentInsets={true}
          javaScriptEnabled={false}
          thirdPartyCookiesEnabled={false}
          allowsInlineMediaPlayback={false}
          style={{
            flex: 1,
            width: width - 36,
          }}
        />
      );
    }
    return (
      <View
        style={{
          flex: 0,
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}
      >
        <Text>{text}</Text>
      </View>
    );
  }
}

class Header extends PureComponent {
  static displayName = 'Header';
  static propTypes = {
    title: PropTypes.string,
    LeftHeaderComponent: PropTypes.node,
  };
  constructor() {
    super();
    this.renderLeftHeaderComponent = this.renderLeftHeaderComponent.bind(this);
  }
  renderLeftHeaderComponent() {
    const { LeftHeaderComponent } = this.props;
    if (LeftHeaderComponent) {
      return LeftHeaderComponent;
    }
    return null;
  }
  render() {
    const { title } = this.props;
    return (
      <View
        style={{
          flex: 0,
          height: 50,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        {this.renderLeftHeaderComponent()}
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text
            style={{ fontSize: 17, fontWeight: '400', color: '#ffffff' }}
            ellipsizeMode="tail"
            numberOfLines={1}
          >
            {title}
          </Text>
        </View>
      </View>
    );
  }
}

class BackButton extends PureComponent {
  static displayName = 'Button';
  static propTypes = {
    children: PropTypes.node,
    onPress: PropTypes.func,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    if (this.props.onPress) {
      this.props.onPress();
    }
  }
  render() {
    return (
      <TouchableOpacity
        style={{ paddingHorizontal: 4 }}
        onPress={this.handleOnPress}
      >
        <Icon name="ios-arrow-back" size={34} color="#ffffff" />
      </TouchableOpacity>
    );
  }
}
