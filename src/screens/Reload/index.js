import R from 'ramda';
import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import getConnection from '../../helpers/getConnection';
import Icon from '../../components/Icon';
import global from '../../global';

const { width, height } = Dimensions.get('window');
// const isConnected = NetInfo.isConnected.fetch()
export default class Reload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  componentDidMount() {
    this.props.navigator.setStyle({
      navBarHidden: true,
    });
    setTimeout(() => Platform.OS === 'ios' && SplashScreen.hide(), 1000);
  }
  render() {
    return (
      <View
        style={{
          position: 'absolute',
          width,
          height,
          top: 0,
          backgroundColor: '#302938',
        }}
      >
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 60,
            marginTop: 60,
          }}
        >
          <View
            style={{
              width: 70,
              height: 70,
              borderRadius: 50,
              borderWidth: 3,
              borderColor: '#ffffff',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Icon
              name="Cross"
              width={30}
              height={30}
              fill="#594c68"
              viewBox="0 0 512 512"
            />
          </View>
          <View style={{ paddingVertical: 40 }}>
            <Text style={{ color: '#ffffff' }}>
              {'Service is not available'}
            </Text>
          </View>
          <TouchableOpacity
            style={{
              backgroundColor: `rgba(49,113,208, ${
                this.state.loading ? 0.6 : 1
              })`,
              width: width - 40,
              height: 55,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 4,
            }}
            disabled={this.state.loading}
            onPress={() => {
              this.setState({ loading: true });
              getConnection().then(isConnected => {
                if (isConnected) {
                  this.props.navigator.push({
                    screen: 'workonflow.Root',
                    passProps: {},
                  });
                } else {
                  console.log('No Connect');
                }
                this.setState({ loading: false });
              });
            }}
          >
            <Text style={{ color: '#ffffff', fontSize: 20 }}>{'Refresh'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
