import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import URI from 'urijs';
import { WebView } from 'react-native';
import { createUser } from '../../actions';
import withLoader from '../withLoader';
import withSafeArea from '../withSafeArea';

const WebViewWithLoader = withLoader(WebView);

class Bots extends PureComponent {
  static displayName = 'Bots';
  static propTypes = {
    createUser: PropTypes.func.isRequired,
    navigator: PropTypes.shape({
      resetTo: PropTypes.func.isRequired,
      push: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
    }),
  };
  static navigatorStyle = {
    navBarTextfontWeight: '300',
    navBarBackgroundColor: 'rgba(43,32,45,1)',
    navBarTextColor: '#ffffff',
    navBarButtonColor: '#ffffff',
    topBarElevationShadowEnabled: false,
    navBarTitleTextCentered: true,
    // navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { url: 'https://botstore.workonflow.com/' };
    this.handleOnNavigationStateChange = this.handleOnNavigationStateChange.bind(
      this,
    );
    this.handleOnHireBot = R.once(this.handleOnHireBot.bind(this));
  }
  handleOnHireBot(email) {
    this.props.createUser(email, contactId => {
      this.props.navigator.resetTo({
        screen: 'workonflow.Main',
        animated: false,
      });
      this.props.navigator.push({
        screen: 'workonflow.Direct',
        passProps: {
          contactId,
          title: email,
          type: 'Direct',
        },
      });
    });
  }
  handleOnNavigationStateChange(navigationState) {
    const parsedUrl = URI.parse(navigationState.url);
    const parsedQuery = URI.parseQuery(parsedUrl.query);
    if (parsedQuery.bots) {
      this.handleOnHireBot(parsedQuery.bots);
    }
  }
  render() {
    const { url } = this.state;
    return (
      <WebViewWithLoader
        source={{ uri: url }}
        automaticallyAdjustContentInsets={true}
        onNavigationStateChange={this.handleOnNavigationStateChange}
        javaScriptEnabled={false}
        thirdPartyCookiesEnabled={false}
        allowsInlineMediaPlayback={false}
      />
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  createUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(withSafeArea(Bots));
