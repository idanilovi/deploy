import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  Platform,
  Image,
  StyleSheet,
  View,
  Button,
  Text,
  Dimensions,
} from 'react-native';
import { getToken, getSocketConnection } from '../../actions';


const { width, height } = Dimensions.get('window');

const style = StyleSheet.create({
  home: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: 32,
  },
  text: {
    fontSize: 36,
    color: '#ffffff',
  },
  textWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  buttonsWrapper: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'transparent',
    padding: 32,
  },
});

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  getToken,
  getSocketConnection,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class Home extends Component {
  static displayName = 'Home';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    getSocketConnection: PropTypes.func,
    getToken: PropTypes.func,
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.checkAuthtorization = this.checkAuthtorization.bind(this);
  }
  componentWillMount() {
    this.checkAuthtorization();
  }
  async checkAuthtorization() {
    const token = await this.props.getToken();
    if (token) {
      await this.props.getSocketConnection(token);
      this.props.navigator.push({
        screen: 'workonflow.Main',
      });
    }
  }
  render() {
    return (
      <View style={style.home}>
        <Image
          // eslint-disable-next-line global-require
          source={require('../../assets/images/bg.png')}
          style={{ width, height }}
        >
          <View style={style.textWrapper}>
            <Text style={style.text}>Konnichiwa!</Text>
          </View>
          <View style={style.buttonsWrapper}>
            <View>
              <Button
                onPress={() =>
                  this.props.navigator.push({
                    screen: 'workonflow.Auth',
                    passProps: { query: 'login' },
                  })}
                title="Authorization"
                color={Platform.OS === 'ios' ? '#ffffff' : '#00c69e'}
              />
            </View>
            <View>
              <Button
                onPress={() =>
                  this.props.navigator.push({
                    screen: 'workonflow.Auth',
                    passProps: { query: 'register' },
                  })}
                title="Registration"
                color={Platform.OS === 'ios' ? '#ffffff' : '#00c69e'}
              />
            </View>
          </View>
        </Image>
      </View>
    );
  }
}
