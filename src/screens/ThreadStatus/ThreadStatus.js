import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  Platform,
  Dimensions,
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { setStatus } from '../../reducers/threads';
import { setThreadStatus } from '../../actions';
import withSafeArea from '../withSafeArea';

const { width, height } = Dimensions.get('window');

class ThreadStatus extends PureComponent {
  static displayName = 'ThreadStatus';
  static propTypes = {
    threadId: PropTypes.string,
    items: PropTypes.array,
    currentItem: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
      color: PropTypes.string,
    }),
    setStatus: PropTypes.func.isRequired,
    setThreadStatus: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnClose = this.handleOnClose.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }
  handleOnPress(statusId) {
    const { threadId } = this.props;
    this.props.setStatus(threadId, statusId);
    this.props.setThreadStatus(threadId, statusId);
    Navigation.dismissModal();
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnClose() {
    Navigation.dismissModal();
  }
  renderItem({ item }) {
    const { currentItem } = this.props;
    return (
      <TouchableHighlight
        style={{
          height: 70,
          justifyContent: 'center',
          alignItems: 'flex-start',
          borderBottomWidth: StyleSheet.hairlineWidth,
        }}
        underlayColor="#f4f4f6"
        onPress={() => this.handleOnPress(item.id)}
      >
        <View
          style={{
            paddingHorizontal: 22,
            paddingVertical: Platform.OS === 'ios' ? 16 : 30,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={{
              flex: 1,
              fontSize: 20,
              fontWeight: '400',
              color: item.color || '#ffffff',
            }}
          >
            {item.name}
          </Text>
          {item.id === currentItem.id ? (
            <View
              style={{
                flex: 0,
                backgroundColor: '#00b69b',
                borderRadius: 8,
                width: 16,
                height: 16,
              }}
            />
          ) : null}
        </View>
      </TouchableHighlight>
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback onPress={this.handleOnClose}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(40,30,42,1)',
            paddingHorizontal: 18,
            paddingVertical: 18,
          }}
        >
          <View
            style={{
              flex: 0,
              height: 50,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#ffffff' }}
              >
                Status
              </Text>
            </View>
          </View>
          <FlatList
            style={{
              flex: 1,
              borderRadius: 13,
              backgroundColor: '#ffffff',
            }}
            keyExtractor={item => item.id}
            data={this.props.items}
            renderItem={this.renderItem}
            ListEmptyComponent={
              <Text
                style={{
                  color: '#cccccc',
                  fontSize: 16,
                  paddingVertical: 16,
                  paddingHorizontal: 8,
                  fontWeight: '300',
                }}
              >
                No statuses presented
              </Text>
            }
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  setStatus,
  setThreadStatus,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadStatus),
);
