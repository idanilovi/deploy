import R from 'ramda';
import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from '../../components/Icon';
import Card from './Card';
import { Navigation } from 'react-native-navigation';

export default class ScrollableMessangers extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={this.props.styles.container}>
        <View style={{ paddingLeft: 20, paddingBottom: 10 }}>
          <Text style={{ color: 'rgba(196,194,206, 1)' }}>
            {this.props.title}
          </Text>
        </View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {R.propOr([], 'messangers', this.props).map((messanger, i) => {
            if (messanger.type === 'addButton') {
              return (
                <TouchableOpacity
                  key={messanger.type}
                  style={{
                    width: 100,
                    height: 90,
                    borderWidth: 1,
                    borderColor: 'rgba(196,194,206, 1)',
                    borderRadius: 10,
                    marginRight: 10,
                    marginLeft: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={() =>
                    Navigation.showModal({
                      screen: 'workonflow.ChannelSettings',
                      passProps: {
                        // eslint-disable-next-line no-underscore-dangle
                        type: 'messangers',
                      },
                    })
                  }
                >
                  <Icon
                    name="Plus"
                    width={50}
                    height={50}
                    fill="rgba(196,194,206, 1)"
                    viewBox="0 0 24 24"
                  />
                </TouchableOpacity>
              );
            }
            return (
              <View
                key={messanger._id}
                style={{ marginLeft: i === 0 ? 20 : 0 }}
              >
                <Card
                  title={R.propOr('messanger', 'name', messanger)}
                  id={messanger._id}
                  subTitle={''}
                  streamName={R.propOr(
                    '',
                    'name',
                    R.propOr(
                      '',
                      R.propOr('', 'streamId', messanger),
                      R.propOr({}, 'streams', this.props),
                    ),
                  )}
                  connected={true}
                  isVisibleToAll={false}
                  styles={this.props.styles}
                  index={i}
                  navigator={this.props.navigator}
                  type="messangers"
                />
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}
