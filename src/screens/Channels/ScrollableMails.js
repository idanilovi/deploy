import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import Icon from '../../components/Icon';
import Card from './Card';
import { Navigation } from 'react-native-navigation';

export default class ScrollableMails extends Component {
  static propTypes = {
    navigator: PropTypes.object,
  };
  render() {
    return (
      <View style={this.props.styles.container}>
        <View style={{ paddingLeft: 20, paddingBottom: 10 }}>
          <Text style={{ color: 'rgba(196,194,206, 1)' }}>
            {this.props.title}
          </Text>
        </View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {R.propOr([], 'mails', this.props).map((mail, i) => {
            if (mail.type === 'addButton') {
              return (
                <TouchableOpacity
                  key={mail.type}
                  style={{
                    width: 100,
                    height: 90,
                    borderWidth: 1,
                    borderColor: 'rgba(196,194,206, 1)',
                    borderRadius: 10,
                    marginRight: 10,
                    marginLeft: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={() =>
                    Navigation.showModal({
                      screen: 'workonflow.ChannelSettings',
                      passProps: {
                        // eslint-disable-next-line no-underscore-dangle
                        type: 'mails',
                      },
                    })
                  }
                >
                  <Icon
                    name="Plus"
                    width={50}
                    height={50}
                    fill="rgba(196,194,206, 1)"
                    viewBox="0 0 24 24"
                  />
                </TouchableOpacity>
              );
            }
            return (
              <View key={mail._id} style={{ marginLeft: i === 0 ? 20 : 0 }}>
                <Card
                  title={R.propOr('mail', 'name', mail)}
                  id={mail._id}
                  subTitle={R.propOr('mail', 'email', mail)}
                  streamName={R.propOr(
                    '',
                    'name',
                    R.propOr(
                      '',
                      R.propOr('', 'streamId', mail),
                      R.propOr({}, 'streams', this.props),
                    ),
                  )}
                  connected={mail.status}
                  isVisibleToAll={mail.isVisibleToAll}
                  styles={this.props.styles}
                  navigator={this.props.navigator}
                  type="mails"
                />
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}
