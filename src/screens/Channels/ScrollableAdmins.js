import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import ContactIcon from '../../components/ContactIcon';
import AddButton from './AddButton';
import global from '../../global';
import { AdminsForm } from '../ChannelSettings/Form/Forms';
import { Navigation } from 'react-native-navigation';

export default class ScrollableAdmins extends Component {
  static displayName = 'ScrollableAdmins';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    admins: PropTypes.array,
  };
  constructor(props) {
    super(props);
    this.state = {
      admins: this.props.admins,
      update: false,
    };
  }

  componentWillReceiveProps(nextprops) {
    if (
      nextprops.admins.filter(
        (item, i) => !R.equals(this.props.admins[i], item),
      ).length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  render() {
    return (
      <View style={this.props.styles.container}>
        <View style={{ paddingLeft: 20 }}>
          <Text style={{ color: 'rgba(196,194,206, 1)' }}>
            {this.props.title}
          </Text>
        </View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {R.propOr([], 'admins', this.props).map((user, i) => {
            if (user.type === 'addButton') {
              return (
                <AddButton
                  key={user.type}
                  styles={this.props.styles}
                  action={() =>
                    Navigation.showModal({
                      screen: 'workonflow.ChannelSettings',
                      passProps: {
                        // eslint-disable-next-line no-underscore-dangle
                        id: this.props._id || this.props.id,
                        type: 'admins',
                      },
                    })
                  }
                  onCancel={() => this.props.navigator.dissmisModal()}
                  text={user.text}
                />
              );
            }
            return (
              <TouchableOpacity
                key={user.id}
                style={[
                  this.props.styles.userRow,
                  { paddingLeft: i == 0 ? 20 : 0 },
                ]}
              >
                <View
                  style={{
                    alignSelf: 'flex-start',
                    paddingVertical: 10,
                  }}
                >
                  <ContactIcon show={true} id={R.propOr(null, '_id', user)} />
                </View>

                <View
                  style={{
                    alignSelf: 'flex-end',
                    paddingLeft: 15,
                    paddingRight: 20,
                    flexDirection: 'column',
                  }}
                >
                  <Text style={[this.props.styles.textBold]}>
                    {R.pathOr('User Name', ['basicData', 'name'], user)}
                  </Text>
                  <Text style={{ color: '#4e4963' }}>
                    {R.pathOr(
                      R.propOr('Position', 'position', user),
                      ['hrData', 'position'],
                      user,
                    )}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}
