import R from 'ramda';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import Icons from '../../components/Icon';
import {
  getTrunks,
  getMails,
  getMessangers,
  setChannelAdmin,
} from '../../actions';
import ScrollableAdmins from './ScrollableAdmins';
import ScrollablePhones from './ScrollablePhones';
import ScrollableMails from './ScrollableMails';
import ScrollableMessangers from './ScrollableMessangers';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  sidebar: {
    flex: 1,
    width,
    height,
    backgroundColor: '#312a39',
    ...ifIphoneX({
      paddingTop: 34,
    }),
  },
  adminsContainer: {
    height: 110,
    width,
    paddingVertical: 20,
    // paddingLeft: 20,
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  trunksContainer: {
    width,
    paddingVertical: 10,
    // paddingLeft: 20,
  },
  userRow: {
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  phoneRow: {
    height: 90,
    width: 240,
    borderRadius: 8,
    paddingVertical: 5,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
  },
  textBold: { fontWeight: 'bold', color: '#4e4963' },
});
const usersSelector = state =>
  R.filter(
    user => user.billingType === 'users',
    R.values(R.propOr({}, 'contacts', state)),
  );
const adminsSelector = state =>
  R.filter(
    contact => contact.isAdmin || contact.isChannelsAdmin,
    R.values(R.propOr({}, 'contacts', state)),
  ).concat([{ type: 'addButton', text: 'Add admin' }]);
const trunksSelector = state =>
  R.values(R.propOr({}, 'trunks', state)).concat([
    { type: 'addButton', text: 'Add trunk' },
  ]);
const mailsSelector = state =>
  R.values(R.propOr({}, 'mails', state)).concat([
    { type: 'addButton', text: 'Add mail' },
  ]);

const messangersSelector = state =>
  R.values(R.propOr({}, 'messangers', state)).concat([
    { type: 'addButton', text: 'Add messanger' },
  ]);

const streamsSelector = state => R.propOr({}, 'streams', state);

class Channels extends Component {
  static displayName = 'Channels';
  static propTypes = {};
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  componentDidMount() {
    this.props.getTrunks({});
    this.props.getMails({});
    this.props.getMessangers({});
  }
  render() {
    return (
      <View style={styles.sidebar}>
        <View
          style={{
            width,
            paddingTop: 20,
            paddingBottom: 20,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}
        >
          <TouchableOpacity
            onPress={() => this.props.navigator.dismissModal()}
            style={{
              alignSelf: 'flex-start',
              height: 40,
              width: 40,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Icons
              name="ArrowLeft"
              width={24}
              height={24}
              fill="rgba(255, 255, 255, 1)"
              viewBox="0 0 32 32"
            />
          </TouchableOpacity>
          <View style={{ flex: 1, alignItems: 'center', marginLeft: -40 }}>
            <Text style={{ color: '#ffffff' }}>{'Channels'}</Text>
          </View>
        </View>
        <ScrollableAdmins
          styles={{
            container: styles.adminsContainer,
            textBold: styles.textBold,
            userRow: styles.userRow,
          }}
          admins={this.props.admins}
          title="Admins"
          setAdmin={this.props.setChannelAdmin}
          contacts={this.props.users}
        />
        <View
          style={{
            flex: 1,
            backgroundColor: '#f2eff2',
            paddingVertical: 10,
          }}
        >
          <ScrollablePhones
            styles={{
              container: styles.trunksContainer,
              textBold: styles.textBold,
              phoneRow: styles.phoneRow,
            }}
            trunks={this.props.trunks}
            streams={this.props.streams}
            title="Phones"
            navigator={this.props.navigator}
          />
          <ScrollableMails
            styles={{
              container: styles.trunksContainer,
              textBold: styles.textBold,
              phoneRow: styles.phoneRow,
            }}
            mails={this.props.mails}
            streams={this.props.streams}
            title="Email"
            navigator={this.props.navigator}
          />
          <ScrollableMessangers
            styles={{
              container: styles.trunksContainer,
              textBold: styles.textBold,
              phoneRow: styles.phoneRow,
            }}
            messangers={this.props.messangers}
            streams={this.props.streams}
            title="Messangers"
            navigator={this.props.navigator}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  admins: adminsSelector(state),
  trunks: trunksSelector(state),
  mails: mailsSelector(state),
  messangers: messangersSelector(state),
  streams: streamsSelector(state),
  users: usersSelector(state),
});

const mapDispatchToProps = {
  getTrunks,
  getMails,
  getMessangers,
  setChannelAdmin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Channels);
