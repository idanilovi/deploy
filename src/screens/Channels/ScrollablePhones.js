import R from 'ramda';
import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from '../../components/Icon';
import Card from './Card';
import { Navigation } from 'react-native-navigation';

const { width, height } = Dimensions.get('window');

export default class ScrollablePhones extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={this.props.styles.container}>
        <View style={{ paddingLeft: 20, paddingBottom: 10 }}>
          <Text style={{ color: 'rgba(196,194,206, 1)' }}>
            {this.props.title}
          </Text>
        </View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {R.propOr([], 'trunks', this.props).map((trunk, i) => {
            if (trunk.type === 'addButton') {
              return (
                <TouchableOpacity
                  key={trunk.type}
                  style={{
                    width: 100,
                    height: 90,
                    borderWidth: 1,
                    borderColor: 'rgba(196,194,206, 1)',
                    borderRadius: 10,
                    marginRight: 10,
                    marginLeft: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={() =>
                    Navigation.showModal({
                      screen: 'workonflow.ChannelSettings',
                      passProps: {
                        // eslint-disable-next-line no-underscore-dangle
                        type: 'trunks',
                      },
                    })
                  }
                >
                  <Icon
                    name="Plus"
                    width={50}
                    height={50}
                    fill="rgba(196,194,206, 1)"
                    viewBox="0 0 24 24"
                  />
                </TouchableOpacity>
              );
            }
            return (
              <View key={trunk.id} style={{ marginLeft: i === 0 ? 20 : 0 }}>
                <Card
                  title={R.propOr('username', 'cacheUsername', trunk)}
                  id={trunk.id}
                  subTitle={`${R.propOr(
                    'username',
                    'cacheUsername',
                    trunk,
                  )}@${R.propOr('domain', 'cacheDomain', trunk)}`}
                  streamName={R.propOr(
                    '',
                    'name',
                    R.propOr(
                      '',
                      R.propOr('', 'redirect', trunk),
                      R.propOr({}, 'streams', this.props),
                    ),
                  )}
                  connected={trunk.state === 'reged'}
                  isVisibleToAll={false}
                  styles={this.props.styles}
                  index={i}
                  navigator={this.props.navigator}
                  type="trunks"
                />
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}
