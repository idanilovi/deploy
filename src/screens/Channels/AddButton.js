import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

export default class AddButton extends Component {
  static displayName = 'AddButton';
  static propTypes = {
    text: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.state = {
      update: false,
    };
  }

  componentWillReceiveProps(nextprops) {
    if (nextprops.text !== this.props.text) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  render() {
    return (
      <TouchableOpacity
        style={[
          this.props.styles.userRow,
          {
            marginRight: 10,
            marginLeft: 10,
          },
        ]}
        onPress={this.props.action}
      >
        <View
          style={{
            alignSelf: 'flex-start',
            paddingVertical: 10,
          }}
        >
          <View
            style={{
              height: 30,
              width: 30,
              alignSelf: 'flex-start',
              borderRadius: 50,
              borderWidth: 1,
              borderColor: 'rgba(196,194,206, 1)',
              borderStyle: 'dotted',
            }}
          />
        </View>
        <View
          style={{
            alignSelf: 'flex-end',
            paddingVertical: 7,
            paddingLeft: 15,
            paddingRight: 20,
          }}
        >
          <Text style={{ color: 'rgba(196,194,206, 1)' }}>
            {this.props.text}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
