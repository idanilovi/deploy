import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Navigation } from 'react-native-navigation';

const debounce = (fn, ms = 0) => {
  let timeoutId;
  return function(...args) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => fn.apply(this, args), ms);
  };
};
export default class Card extends Component {
  static displayName = 'Card';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func,
    }),
    _id: PropTypes.string,
    type: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.state = {
      update: false,
    };
    this.handleOnPress = debounce(this.handleOnPress.bind(this), 500);
  }
  componentWillReceiveProps(nextprops) {
    if (
      this.props.title !== nextprops.title ||
      this.props.subTitle !== nextprops.subTitle ||
      this.props.id !== nextprops.id ||
      this.props.streamName !== nextprops.streamName ||
      this.props.connected !== nextprops.connected ||
      this.props.isVisibleToAll !== nextprops.isVisibleToAll
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  handleOnPress() {
    Navigation.showModal({
      screen: 'workonflow.ChannelSettings',
      passProps: {
        // eslint-disable-next-line no-underscore-dangle
        id: this.props._id || this.props.id,
        type: this.props.type,
      },
    });
  }
  render() {
    return (
      <TouchableOpacity
        key={this.props.id}
        onPress={this.handleOnPress}
        style={[
          this.props.styles.phoneRow,
          {
            paddingLeft: 10,
            marginRight: 15,
          },
        ]}
      >
        <View
          style={{
            flex: 1,
            alignSelf: 'flex-start',
            paddingVertical: 10,
          }}
        >
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={[this.props.styles.textBold, { width: 190 }]}
          >
            {this.props.title}
          </Text>
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={[this.props.styles.textBold, { width: 190 }]}
          >
            {this.props.subTitle}
          </Text>
          <View style={{ paddingTop: 6 }}>
            <Text ellipsizeMode="tail" numberOfLines={1} style={{ width: 190 }}>
              {`~${this.props.streamName}`}
            </Text>
          </View>
        </View>

        <View
          style={{
            alignSelf: 'flex-end',
            flexDirection: 'column',
            backgroundColor: 'red',
          }}
        />
        <View style={{ flexDirection: 'column' }}>
          <View
            style={{
              height: 15,
              width: 15,
              marginTop: 10,
              marginRight: 10,
              borderRadius: 50,
              backgroundColor: this.props.connected
                ? 'rgb(86, 195, 160)'
                : 'rgb(231, 8, 63)',
            }}
          />
          <View style={{ paddingVertical: 30 }}>
            {this.props.isVisibleToAll && (
              <Text style={{ color: 'rgb(59, 124,212)' }}>{'All'}</Text>
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
