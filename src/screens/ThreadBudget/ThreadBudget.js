import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  Dimensions,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { setBudget, threadByIdSelector } from '../../reducers/threads';
import { setThreadBudget } from '../../actions';
import withSafeArea from '../withSafeArea';

const { width } = Dimensions.get('window');

class ThreadBudget extends PureComponent {
  static displayName = 'ThreadBudget';
  static propTypes = {
    threadId: PropTypes.string,
    unit: PropTypes.string,
    thread: PropTypes.shape({
      budget: PropTypes.number,
    }),
    setBudget: PropTypes.func.isRequired,
    setThreadBudget: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { prevValue: props.thread.budget };
    this.handleChangeText = this.handleChangeText.bind(this);
    this.handleOnSubmitEditing = this.handleOnSubmitEditing.bind(this);
    this.handleOnClose = this.handleOnClose.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
    this.handleOnCancel = this.handleOnCancel.bind(this);
  }
  handleOnSave() {
    const {
      threadId,
      thread: { budget },
    } = this.props;
    this.props.setThreadBudget(threadId, budget);
    Navigation.dismissModal();
  }
  handleOnCancel() {
    const { threadId } = this.props;
    const { prevValue } = this.state;
    this.props.setBudget(threadId, prevValue);
    Navigation.dismissModal();
  }
  handleChangeText(budget) {
    const { threadId } = this.props;
    this.props.setBudget(threadId, budget);
  }
  handleOnSubmitEditing() {
    const {
      threadId,
      thread: { budget },
    } = this.props;
    this.props.setThreadBudget(threadId, budget);
    Navigation.dismissModal();
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnClose() {
    this.handleOnCancel();
  }
  render() {
    const {
      thread: { budget },
      unit,
    } = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.handleOnClose}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(40,30,42,1)',
            paddingHorizontal: 18,
            paddingVertical: 18,
          }}
        >
          <View
            style={{
              flex: 0,
              height: 50,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <TouchableOpacity
              style={{
                flex: 0,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.handleOnCancel}
            >
              <Text
                style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}
              >
                Cancel
              </Text>
            </TouchableOpacity>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#ffffff' }}
              >
                Budget
              </Text>
            </View>
            <TouchableOpacity
              style={{
                flex: 0,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.handleOnSave}
            >
              <Text
                style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}
              >
                Save
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              backgroundColor: 'transparent',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#ffffff',
                borderRadius: 13,
                width: width - 36,
                height: 68,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <TextInput
                style={{
                  flex: 1,
                  width: width - 36,
                  height: 68,
                  fontSize: 31,
                  fontWeight: '400',
                  color: '#4e4963',
                  paddingVertical: 15,
                  textAlign: 'right',
                  paddingHorizontal: 6,
                }}
                value={budget}
                selectionColor="#000000"
                autoFocus={true}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                returnKeyType="done"
                onChangeText={this.handleChangeText}
                onSubmitEditing={this.handleOnSubmitEditing}
                enablesReturnKeyAutomatically={true}
              />
              <Text
                style={{
                  flex: 1,
                  fontSize: 31,
                  fontWeight: '400',
                  color: '#4e4963',
                }}
              >
                {unit}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  thread: threadByIdSelector(state, ownProps),
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  setBudget,
  setThreadBudget,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadBudget),
);
