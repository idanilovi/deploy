import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  SafeAreaView,
  StyleSheet,
  ImageBackground,
  Dimensions,
} from 'react-native';
import hoistNonReactStatics from 'hoist-non-react-statics';

const { width, height } = Dimensions.get('window');

// const WithSafeAreaContext = React.createContext('SafeAreaContext');

const styles = StyleSheet.create({
  safeAreaContainer: { flex: 1, backgroundColor: '#ffffff' },
  safeAreaBackgroundImage: { flex: 1, width, height },
});

function withSafeArea(WrappedComponent) {
  class WithSafeArea extends Component {
    static displayName = `withSafeArea(${WrappedComponent.displayName ||
      WrappedComponent.name})`;
    static propTypes = {
      safeAreaBackgroundColor: PropTypes.string,
      safeAreaBackgroundImage: PropTypes.string,
      safeAreaBackgroundImageSource: PropTypes.string,
    };
    // TODO написать нормальный shouldComponentUpdate
    // shouldComponentUpdate(nextProps) {
    //   const {
    //     safeAreaBackgroundColor,
    //     safeAreaBackgroundImage,
    //     safeAreaBackgroundImageSource,
    //   } = this.props;
    //   const {
    //     safeAreaBackgroundColor: _safeAreaBackgroundColor,
    //     safeAreaBackgroundImage: _safeAreaBackgroundImage,
    //     safeAreaBackgroundImageSource: _safeAreaBackgroundImageSource,
    //   } = nextProps;
    //   if (safeAreaBackgroundColor !== _safeAreaBackgroundColor) {
    //     return true;
    //   }
    //   if (safeAreaBackgroundImage !== _safeAreaBackgroundImage) {
    //     return true;
    //   }
    //   if (safeAreaBackgroundImageSource !== _safeAreaBackgroundImageSource) {
    //     return true;
    //   }
    //   return false;
    // }
    render() {
      if (this.props.safeAreaBackgroundImage) {
        return (
          <ImageBackground
            source={this.props.safeAreaBackgroundImageSource}
            style={styles.safeAreaBackgroundImage}
            resizeMode="cover"
          >
            <SafeAreaView
              style={[
                styles.safeAreaContainer,
                { backgroundColor: this.props.safeAreaBackgroundColor },
              ]}
            >
              <WrappedComponent {...this.props} />
            </SafeAreaView>
          </ImageBackground>
        );
      }
      return (
        <SafeAreaView
          style={[
            styles.safeAreaContainer,
            { backgroundColor: this.props.safeAreaBackgroundColor },
          ]}
        >
          <WrappedComponent {...this.props} />
        </SafeAreaView>
      );
    }
  }
  hoistNonReactStatics(WithSafeArea, WrappedComponent);
  return WithSafeArea;
}

export default withSafeArea;
