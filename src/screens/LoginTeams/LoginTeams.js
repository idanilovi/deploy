import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  View,
  Platform,
  Text,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { teamsArraySelector } from '../../reducers/teams';
import {
  fetchAccessTeams,
  fetchAcceptAccess,
  fetchLoginRequest,
  fetchTokenFromRedirect,
  getDataFromAsyncStorage,
  getSocketConnection,
  getContact,
  getUserSettings,
  closeSocketConnection,
  setMultipleDataToAsyncStorage,
} from '../../actions';
import { setUserData } from '../../reducers/userdata';
import { userLogout } from '../../reducers';
import global from '../../global';
import backgroundImageSource from '../../assets/images/bg.png';
import withSafeArea from '../withSafeArea';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  header: { paddingVertical: 8, backgroundColor: 'transparent' },
  headerText: {
    fontSize: 24,
    fontWeight: '600',
    color: '#212121',
  },
  loader: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(40, 30, 42, 1)',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function getItemColor(index) {
  if (index % 3 === 0) {
    return '#9d80e5';
  }
  if (index % 3 === 1 % 3) {
    return '#3fd195';
  }
  if (index % 3 === 2 % 3) {
    return '#80c3e5';
  }
  return '#ffffff';
}

class LoginTeams extends Component {
  static displayName = 'LoginTeams';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
    }),
    teams: PropTypes.array,
    fetchAccessTeams: PropTypes.func.isRequired,
    fetchAcceptAccess: PropTypes.func.isRequired,
    fetchTokenFromRedirect: PropTypes.func.isRequired,
    getDataFromAsyncStorage: PropTypes.func.isRequired,
    getSocketConnection: PropTypes.func.isRequired,
    getContact: PropTypes.func.isRequired,
    getUserSettings: PropTypes.func.isRequired,
    setUserData: PropTypes.func.isRequired,
    userLogout: PropTypes.func.isRequired,
    closeSocketConnection: PropTypes.func.isRequired,
    setMultipleDataToAsyncStorage: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { loading: false };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.renderLoader = this.renderLoader.bind(this);
    this.handleOnItemPress = this.handleOnItemPress.bind(this);
    this.handleOnAcceptPress = this.handleOnAcceptPress.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }
  async componentDidMount() {
    clearTimeout(global.timoutId);
    if (this.props.dontEnter) {
      const email = await this.props.getDataFromAsyncStorage('email');
      const password = await this.props.getDataFromAsyncStorage('password');
      this.props.userLogout();
      this.props.fetchLoginRequest({
        email,
        password,
      });
    }
    this.props.fetchAccessTeams(async response => {
      await this.props.closeSocketConnection();
      if (!this.props.dontEnter) {
        await this.handleOnItemPress(response);
      }
    });
    global.tracker.trackScreenView('LoginTeams');
    if (Platform.OS === 'ios') {
      SplashScreen.hide();
    }
  }
  componentWillUnmount() {
    this.setState({ loading: false });
  }
  async onNavigatorEvent(event) {
    if (this.props.dontEnter && event.id === 'willAppear') {
      const email = await this.props.getDataFromAsyncStorage('email');
      const password = await this.props.getDataFromAsyncStorage('password');
      this.props.userLogout();
      this.props.fetchLoginRequest({
        email,
        password,
      });
    }
    this.props.fetchAccessTeams(
      !this.props.dontEnter && event.id === 'willAppear'
        ? response => {
            this.props.userLogout();
            this.handleOnItemPress(response);
          }
        : event.id === 'willAppear' && this.props.userLogout,
    );
  }
  async handleOnItemPress({ teamId, to, userId, teamName }) {
    global.tracker.trackEvent('Нажатия на кнопочки', 'Select team');
    this.setState({ loading: true });
    this.props.setUserData({ teamId, userId, teamName });
    await this.props.setMultipleDataToAsyncStorage([['teamName', teamName]]);
    await this.props.fetchTokenFromRedirect({ teamId, to, userId });
    const token = await this.props.getDataFromAsyncStorage('token');
    await this.props.getSocketConnection(token);
    await this.props.getContact(userId);
    await this.props.getUserSettings();
    this.props.navigator.push({ screen: 'workonflow.Main', passProps: {} });
  }
  handleOnAcceptPress(teamId, url) {
    this.props.fetchAcceptAccess({ teamId, url });
  }
  renderLoader() {
    if (this.state.loading) {
      return (
        <SafeAreaView
          style={[styles.loader, { width, height, backgroundColor: '#90748B' }]}
        >
          <ActivityIndicator animating={true} color="#ffffff" size="large" />
        </SafeAreaView>
      );
    }
    return null;
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item) {
    return item._id; // eslint-disable-line no-underscore-dangle
  }
  renderItem({ item, index }) {
    if (item.isActive) {
      return (
        <TouchableOpacity
          activeOpacity={0.9}
          style={[
            {
              paddingLeft: 5,
              borderRadius: 5,
              marginBottom: 12,
              backgroundColor: getItemColor(index),
            },
          ]}
          onPress={() => {
            this.handleOnItemPress({
              teamId: item.teamId,
              to: item.url,
              userId: item.userId,
              teamName: item.teamName || item.teamId,
            });
          }}
        >
          <View
            style={{
              borderRadius: 5,
              height: 62,
              backgroundColor: '#fefefe',
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              paddingHorizontal: 20,
              paddingVertical: 20,
            }}
          >
            <Text
              style={{
                fontSize: 20,
                fontWeight: '400',
                color: '#4e4963',
                flexShrink: 1,
              }}
              numberOfLines={1}
              ellipsizeMode="tail"
              textBreakStrategy="highQuality"
            >
              {item.teamName || item.teamId}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={{ marginBottom: 12 }}
        onPress={() => this.handleOnAcceptPress(item.teamId, item.url)}
      >
        <View
          style={{
            borderRadius: 5,
            borderWidth: 2,
            borderColor: '#fefefe',
            height: 62,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
          }}
        >
          <Text
            style={{
              fontSize: 20,
              fontWeight: '400',
              color: '#4e4963',
              flexShrink: 1,
            }}
            numberOfLines={1}
            ellipsizeMode="tail"
            textBreakStrategy="highQuality"
          >
            {item.teamName || item.teamId}
          </Text>
          <Text style={{ color: '#fefefe', fontSize: 20 }}>Accept</Text>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View>
        <View
          style={{
            paddingHorizontal: 0,
            paddingTop: Platform.OS === 'ios' ? 42 : 38,
            paddingBottom: 28,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              fontSize: 17,
              color: '#ffffff',
            }}
          >
            Select your team
          </Text>
        </View>
        <FlatList
          style={{
            paddingHorizontal: 16,
          }}
          data={this.props.teams}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
        {this.renderLoader()}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  teams: teamsArraySelector(state),
  userdata: state.userData,
  safeAreaBackgroundColor: '#90748B',
  safeAreaBackgroundImageSource: backgroundImageSource,
  safeAreaBackgroundImage: true,
});

const mapDispatchToProps = {
  fetchAccessTeams,
  fetchAcceptAccess,
  fetchLoginRequest,
  fetchTokenFromRedirect,
  getDataFromAsyncStorage,
  setMultipleDataToAsyncStorage,
  getSocketConnection,
  getContact,
  getUserSettings,
  setUserData,
  closeSocketConnection,
  userLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(LoginTeams),
);
