import PropTypes from 'prop-types';
import React, { Component } from 'react';
import CommentList from '../../components/CommentList';
import withSafeArea from '../withSafeArea';

class StreamChat extends Component {
  static displayName = 'StreamChat';
  static propTypes = {
    type: PropTypes.string,
    streamId: PropTypes.string,
    title: PropTypes.string,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      toggleTabs: PropTypes.func.isRequired,
      setTitle: PropTypes.func.isRequired,
      setStyle: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
      popToRoot: PropTypes.func.isRequired,
      resetTo: PropTypes.func.isRequired,
    }),
  };
  static navigatorStyle = {
    tabBarHidden: true,
  };
  componentWillMount() {
    if (this.props.title) {
      this.props.navigator.setTitle({
        title: this.props.title,
      });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.streamId !== nextProps.streamId) {
      if (this.props.title !== nextProps.title) {
        nextProps.navigator.setTitle({
          title: nextProps.title,
        });
      }
    }
  }
  render() {
    return (
      <CommentList
        streamId={this.props.streamId}
        type={this.props.type}
        navigator={this.props.navigator}
      />
    );
  }
}

export default withSafeArea(StreamChat);
