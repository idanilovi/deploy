import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  BackHandler,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import SplashScreen from 'react-native-splash-screen';
import Button from '../../components/Button';
import FloatingLabelInput from '../../components/FloatingLabelInput';
import PasswordButton from './PasswordButton';
import Icon from '../../components/Icon';
import global from '../../global';
import {
  fetchLoginRequest,
  closeSocketConnection,
  clearAsyncStorage,
} from '../../actions';
import { userLogout } from '../../reducers';
import {
  setEmail,
  setPassword,
  setError,
  clearForm,
  showPassword,
  hidePassword,
  loginSelector,
} from '../../reducers/ui/login';
import backgroundImageSource from '../../assets/images/bg.png';
import withSafeArea from '../withSafeArea';

class Login extends Component {
  static displayName = 'Login';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
    }),
    fetchLoginRequest: PropTypes.func.isRequired,
    login: PropTypes.shape({
      email: PropTypes.string,
      password: PropTypes.string,
      error: PropTypes.string,
      showPassword: PropTypes.bool,
    }),
    setEmail: PropTypes.func.isRequired,
    setPassword: PropTypes.func.isRequired,
    setError: PropTypes.func.isRequired,
    clearForm: PropTypes.func.isRequired,
    userLogout: PropTypes.func.isRequired,
    showPassword: PropTypes.func.isRequired,
    hidePassword: PropTypes.func.isRequired,
    clearAsyncStorage: PropTypes.func.isRequired,
    closeSocketConnection: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { showEmailInput: false, showPasswordInput: false };
    this.checkLoginFormData = this.checkLoginFormData.bind(this);
    this.checkEmailInputValue = this.checkEmailInputValue.bind(this);

    this.handleOnLoginPress = this.handleOnLoginPress.bind(this);
    this.renderShowPasswordButton = this.renderShowPasswordButton.bind(this);

    this.handleOnShowEmailPress = this.handleOnShowEmailPress.bind(this);
    this.handleOnEnterPassword = this.handleOnEnterPassword.bind(this);
    this.handleOnForgotPress = this.handleOnForgotPress.bind(this);
    this.handleOnRegistrationPress = this.handleOnRegistrationPress.bind(this);
    this.handleOnBackgroundPress = this.handleOnBackgroundPress.bind(this);

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  componentDidMount() {
    clearTimeout(global.timoutId);
    this.props.clearForm();
    this.props.userLogout();
    this.props.clearAsyncStorage();
    this.props.closeSocketConnection();
    global.tracker.trackScreenView('Login');
    if (Platform.OS === 'ios' && !this.props.dontHideSplash) {
      SplashScreen.hide();
    }
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', () => {
        BackHandler.exitApp();
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', () => {});
    }
  }
  // eslint-disable-next-line class-methods-use-this
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      if (Platform.OS === 'android') {
        BackHandler.addEventListener('hardwareBackPress', () => {
          BackHandler.exitApp();
        });
      }
    }
    if (event.id === 'willDisappear') {
      if (Platform.OS === 'android') {
        BackHandler.removeEventListener('hardwareBackPress', () => {});
      }
    }
  }
  checkLoginFormData() {
    if (!this.props.login.error) {
      if (this.props.login.email && this.props.login.password) {
        return true;
      }
    }
    return false;
  }
  checkEmailInputValue() {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg.test(this.props.login.email) === false) {
      this.props.setError({ message: 'Email is not correct' });
    } else {
      this.props.setError(false);
    }
  }
  handleOnShowEmailPress() {
    this.setState({ showEmailInput: true });
  }
  handleOnEnterPassword() {
    this.setState({ showPasswordInput: true });
  }
  handleOnRegistrationPress() {
    this.props.navigator.push({
      screen: 'workonflow.LoginRegistration',
    });
  }
  handleOnLoginPress() {
    // global.tracker.trackEvent('Нажатия на кнопочки', 'Log in');
    this.props.fetchLoginRequest({
      email: this.props.login.email,
      password: this.props.login.password,
      successCallback: () => {
        this.props.clearForm();
        this.props.navigator.push({ screen: 'workonflow.LoginTeams' });
      },
      failureCallback: error => {
        this.props.setError(error);
        if (this.emailInput) {
          this.emailInput.focus();
        }
      },
    });
  }
  renderShowPasswordButton() {
    return (
      <PasswordButton
        password={this.props.login.password}
        showPassword={this.props.login.showPassword}
        show={() => {
          this.props.showPassword();
          if (this.passwordInput) {
            this.passwordInput.focus();
          }
        }}
        hide={() => {
          this.props.hidePassword();
          if (this.passwordInput) {
            this.passwordInput.focus();
          }
        }}
      />
    );
  }
  handleOnForgotPress() {
    // global.tracker.trackEvent('Нажатия на кнопочки', 'Forgot your password');
    this.props.navigator.push({
      screen: 'workonflow.LoginResetPassword',
    });
  }
  handleOnBackgroundPress() {
    Keyboard.dismiss();
    if (
      this.state.showEmailInput &&
      !this.props.login.email &&
      !this.state.showPasswordInput
    ) {
      this.setState({ showEmailInput: false });
    }
    if (this.state.showPasswordInput && !this.props.login.password) {
      this.setState({ showPasswordInput: false });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  renderHeader() {
    return (
      <View
        style={{
          paddingHorizontal: 0,
          paddingTop: Platform.OS === 'ios' ? 42 : 38,
          paddingBottom: 8,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text
          style={{
            fontSize: 17,
            color: '#ffffff',
          }}
        >
          Log in
        </Text>
      </View>
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={this.handleOnBackgroundPress}
        accessible={false}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            paddingVertical: 16,
            paddingHorizontal: 16,
          }}
        >
          <View style={{ flex: 1, justifyContent: 'flex-start' }}>
            {this.renderHeader()}
            {
              <View style={{ paddingVertical: 10 }}>
                <FloatingLabelInput
                  autoFocus={true}
                  ref={ref => {
                    this.emailInput = ref;
                  }}
                  nativeID="LOGIN_FIELD"
                  error={this.props.login.error}
                  errorMessage={R.pathOr(
                    'Wrong login or password',
                    ['error', 'message'],
                    this.props.login,
                  )}
                  placeholder="Email: "
                  keyboardType="email-address"
                  value={this.props.login.email}
                  onChangeTextValue={this.props.setEmail}
                  // onBlur={this.checkEmailInputValue}
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    this.handleOnEnterPassword();
                    if (this.passwordInput) {
                      this.passwordInput.focus();
                    }
                  }}
                />
              </View>
            }
            {this.state.showPasswordInput && (
              <View style={{ paddingVertical: 10 }}>
                <FloatingLabelInput
                  ref={ref => {
                    this.passwordInput = ref;
                  }}
                  nativeID="PASSWORD_FIELD"
                  placeholder="Password: "
                  returnKeyType="next"
                  secureTextEntry={!this.props.login.showPassword}
                  value={this.props.login.password}
                  onChangeTextValue={this.props.setPassword}
                  renderButton={this.renderShowPasswordButton}
                  autoComplete={false}
                  autoFocus={true}
                  onSubmitEditing={this.handleOnLoginPress}
                />
              </View>
            )}
            {!this.state.showPasswordInput && (
              <View style={{ paddingVertical: 10 }}>
                <Button
                  activeOpacity={0.9}
                  title="Enter password"
                  onPress={this.handleOnEnterPassword}
                  styles={{
                    button: {
                      backgroundColor: this.props.login.email
                        ? 'rgba(13, 113, 215, 1)'
                        : 'rgba(13, 113, 215, 0.7)',
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 53,
                      borderRadius: 5,
                    },
                    title: {
                      fontSize: 25,
                      fontWeight: '400',
                      color: '#fefefe',
                    },
                  }}
                  disabled={!this.props.login.email}
                />
              </View>
            )}
            {this.state.showPasswordInput &&
              !this.props.login.error && (
                <View style={{ paddingVertical: 10 }}>
                  <Button
                    activeOpacity={0.9}
                    nativeID="LOG_IN_BUTTON"
                    styles={{
                      button: {
                        backgroundColor: this.checkLoginFormData()
                          ? 'rgba(0, 198, 158, 1)'
                          : 'rgba(0, 198, 158, 0.7)',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 53,
                        borderRadius: 5,
                      },
                      title: {
                        fontSize: 25,
                        fontWeight: '400',
                        color: '#fefefe',
                      },
                    }}
                    title="Let's work!"
                    onPress={this.handleOnLoginPress}
                    disabled={!this.checkLoginFormData()}
                  />
                </View>
              )}
            {this.props.login.error && (
              <View style={{ paddingVertical: 10 }}>
                <Button
                  activeOpacity={0.9}
                  styles={{
                    button: {
                      alignItems: 'center',
                      height: 53,
                      justifyContent: 'center',
                    },
                    title: {
                      fontSize: 17,
                      fontWeight: '400',
                      color: '#ffffff',
                    },
                  }}
                  title="Forgot your password?"
                  onPress={this.handleOnForgotPress}
                />
              </View>
            )}
          </View>
          {!this.state.showEmailInput && (
            <View style={{ flex: 0, justifyContent: 'flex-end' }}>
              <Button
                activeOpacity={0.9}
                title="Create account"
                onPress={this.handleOnRegistrationPress}
                styles={{
                  button: {
                    backgroundColor: 'rgba(42, 40, 50, 0.5)',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 53,
                    borderRadius: 5,
                  },
                  title: {
                    fontSize: 17,
                    fontWeight: '400',
                    color: '#ffffff',
                  },
                }}
                renderIcon={() => (
                  <Icon
                    name="Plus"
                    width={32}
                    height={32}
                    fill="rgb(120, 115,124)"
                    viewBox="0 0 24 24"
                  />
                )}
              />
            </View>
          )}
          {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => ({
  login: loginSelector(state),
  safeAreaBackgroundImageSource: backgroundImageSource,
  safeAreaBackgroundImage: true,
});

const mapDispatchToProps = {
  fetchLoginRequest,
  setEmail,
  setPassword,
  setError,
  clearForm,
  userLogout,
  closeSocketConnection,
  clearAsyncStorage,
  showPassword,
  hidePassword,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(Login),
);
