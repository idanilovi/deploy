import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';

const styles = StyleSheet.create({
  container: {
    padding: 8,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default class PasswordButton extends PureComponent {
  static displayName = 'PasswordButton';
  static propTypes = {
    password: PropTypes.string,
    showPassword: PropTypes.bool,
    show: PropTypes.func.isRequired,
    hide: PropTypes.func.isRequired,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    if (this.props.showPassword) {
      this.props.hide();
    } else {
      this.props.show();
    }
  }
  render() {
    if (this.props.password) {
      return (
        <TouchableOpacity onPress={this.handleOnPress} style={styles.container}>
          <Icon
            name={this.props.showPassword ? 'md-eye-off' : 'md-eye'}
            size={28}
            color="#ebe8de"
          />
        </TouchableOpacity>
      );
    }
    return null;
  }
}
