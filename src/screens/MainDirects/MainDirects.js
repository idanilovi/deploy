import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import SidebarList from '../../components/SidebarList';
import {
  View,
  TouchableOpacity,
  Text,
  Animated,
  StyleSheet,
} from 'react-native';
import { createUser } from '../../actions';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.2)',
    height: 40,
    justifyContent: 'center',
  },
  AView: { flex: 1 },
  header: { flexDirection: 'row', justifyContent: 'center' },
  text: { color: '#ffffff', fontSize: 16 },
  button: { paddingRight: 25 },
});

const userIdSelector = state => R.pathOr('', ['userData', 'userId'], state);
const contactsSelector = state => R.propOr({}, 'contacts', state);
const userDataSelector = state => R.propOr({}, 'userData', state);
// TODO уебанство removeAllCode()
const customersSelector = createSelector(contactsSelector, contacts =>
  R.filter(contact => contact.billingType === 'contacts', R.values(contacts)),
);
const botsSelector = createSelector(contactsSelector, contacts =>
  R.filter(
    contact => contact.billingType === 'bots',
    R.values(contacts),
  ).concat([
    {
      type: 'inviteButton',
      text: 'Hire bot',
    },
  ]),
);
const botIdsSelector = createSelector(contactsSelector, contacts =>
  R.filter(
    _ => _,
    R.map(
      contact => contact.billingType === 'bots' && contact._id,
      R.values(contacts),
    ),
  ),
);

const usersSelector = createSelector(contactsSelector, contacts =>
  R.filter(contact => contact.billingType === 'users', R.values(contacts)),
);
const visibleContactsSelector = createSelector(
  userDataSelector,
  usersSelector,
  contactsSelector,
  botIdsSelector,
  (userdata, users, contacts, botIds) => {
    const visibleContacts = R.pathOr([], ['visibleUsers', 'seen'], userdata);
    const invisibleContacts = R.pathOr(
      [],
      ['visibleUsers', 'unSeen'],
      userdata,
    );
    R.map(contact => {
      if (
        !visibleContacts.includes(contact._id) &&
        !invisibleContacts.includes(contact._id)
      ) {
        visibleContacts.push(contact._id);
      }
    }, users);
    const allContacts = R.concat(visibleContacts, invisibleContacts);
    const filteredContacts = R.filter(id => !botIds.includes(id), allContacts);
    return R.map(
      contactId => R.propOr({}, contactId, contacts),
      filteredContacts,
    ).concat([{ type: 'inviteButton', text: 'Invite user' }]);
  },
);

const invisibleContactsSelector = createSelector(
  userDataSelector,
  contactsSelector,
  (userdata, contacts) => {
    return R.map(
      contactId => R.propOr({}, contactId, contacts),
      R.pathOr([], ['visibleUsers', 'unSeen'], userdata),
    );
  },
);

const sidebarlistContactsSelector = createSelector(
  visibleContactsSelector,
  invisibleContactsSelector,
  (visibleContacts, invisibleContacts) =>
    R.concat(visibleContacts, invisibleContacts),
);

const mapStateToProps = state => ({
  items: visibleContactsSelector(state),
  customers: customersSelector(state),
  bots: botsSelector(state),
  userId: userIdSelector(state),
});

const mapDispatchToProps = {
  createUser,
};
@connect(
  mapStateToProps,
  mapDispatchToProps,
)
export default class MainDirects extends Component {
  static displayName = 'MainDirects';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setStyle: PropTypes.func.isRequired,
    }),
    items: PropTypes.array,
    customers: PropTypes.array,
    bots: PropTypes.array,
    userId: PropTypes.string,
  };
  static navigatorStyle = { navBarHidden: true };
  constructor(props) {
    super(props);
    this.state = {
      update: false,
      tabIndex: 0,
      items: this.props.items,
    };
  }
  componentWillReceiveProps(nextprops) {
    if (
      nextprops.items.filter((item, i) => !R.equals(this.props.items[i], item))
        .length ||
      nextprops.customers.filter(
        (item, i) => !R.equals(this.props.customers[i], item),
      ).length ||
      nextprops.bots.filter((item, i) => !R.equals(this.props.bots[i], item))
        .length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    if (
      this.state.tabIndex !== nextState.tabIndex ||
      this.props.userId !== nextProps.userId
    ) {
      return true;
    }
    return false;
  }
  getItems(index) {
    this.setState({ tabIndex: index });
    if (index === 0) {
      this.setState({ items: this.props.items });
    }
    if (index === 1) {
      this.setState({ items: this.props.customers });
    }
    if (index === 2) {
      this.setState({ items: this.props.bots });
    }
    return [];
  }

  render() {
    return (
      <Animated.View style={styles.AView}>
        <View style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={this.getItems.bind(this, 0)}
              style={styles.button}
            >
              <Text
                style={[
                  this.state.tabIndex === 0 && { fontWeight: 'bold' },
                  styles.text,
                ]}
              >
                {'Users'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.getItems.bind(this, 1)}
              style={styles.button}
            >
              <Text
                style={[
                  this.state.tabIndex === 1 && { fontWeight: 'bold' },
                  styles.text,
                ]}
              >
                {'Contacts'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.getItems.bind(this, 2)}>
              <Text
                style={[
                  this.state.tabIndex === 2 && { fontWeight: 'bold' },
                  styles.text,
                ]}
              >
                {'Bots'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {this.state.items.length === 0 ? null : (
          <SidebarList
            navigator={this.props.navigator}
            items={this.state.items}
            createUser={this.props.createUser}
            userId={this.props.userId}
          />
        )}
      </Animated.View>
    );
  }
}
