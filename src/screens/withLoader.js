import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import hoistNonReactStatics from 'hoist-non-react-statics';

const styles = StyleSheet.create({
  loader: {
    position: 'absolute',
    top: 0,
    left: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(40,30,42,1)',
  },
});

export default function withLoader(WrappedComponent) {
  class WithLoader extends Component {
    static displayName = `withLoader(${WrappedComponent.displayName ||
      WrappedComponent.name})`;
    constructor(props) {
      super(props);
      this.state = { loading: false, width: 0, height: 0 };
      this.renderLoader = this.renderLoader.bind(this);
      this.handleOnLoadStart = this.handleOnLoadStart.bind(this);
      this.handleOnLoadEnd = this.handleOnLoadEnd.bind(this);
      this.handleOnLayout = this.handleOnLayout.bind(this);
    }
    handleOnLoadStart() {
      this.setState(() => ({ loading: true }));
    }
    handleOnLoadEnd() {
      this.setState(() => ({ loading: false }));
    }
    handleOnLayout({ nativeEvent: { layout: { width, height } } }) {
      this.setState(() => ({ width, height }));
    }
    renderLoader() {
      const { loading, width, height } = this.state;
      if (loading) {
        return (
          <View style={[styles.loader, { width, height }]}>
            <ActivityIndicator animating={true} color="#ffffff" size="large" />
          </View>
        );
      }
      return null;
    }
    render() {
      return (
        <View style={{ flex: 1 }} onLayout={this.handleOnLayout}>
          <WrappedComponent
            onLoadStart={this.handleOnLoadStart}
            onLoadEnd={this.handleOnLoadEnd}
            {...this.props}
          />
          {this.renderLoader()}
        </View>
      );
    }
  }
  hoistNonReactStatics(WithLoader, WrappedComponent);
  return WithLoader;
}
