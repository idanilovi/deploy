import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from '../../components/Icon';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    width: 34,
    height: 34,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
    margin: 0,
  },
});

export default class ThreadSettingsButton extends PureComponent {
  static displayName = 'ThreadSettingsButton';
  static propTypes = {
    navigator: PropTypes.shape({
      showModal: PropTypes.func.isRequired,
    }),
    streamId: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }
  handleOnPress() {
    Navigation.showModal({
      screen: 'workonflow.ThreadSettings',
      passProps: {
        streamId: this.props.streamId,
      },
    });
  }
  renderIcon() {
    return (
      <Icon
        name="ThreadSettings"
        viewBox="0 0 512 512"
        width={19}
        height={19}
      />
    );
  }
  render() {
    return (
      <TouchableOpacity style={styles.button} onPress={this.handleOnPress}>
        {this.renderIcon()}
      </TouchableOpacity>
    );
  }
}
