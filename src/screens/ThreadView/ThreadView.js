import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { View, Platform } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Loader from '../../components/Loader';
import Thread from '../Thread';
import ThreadBar from '../ThreadBar';
import { setThreadTitle as _setThreadTitle } from '../../reducers/threads';
import {
  getFiles,
  getThreads,
  getComments,
  getContacts,
  getDescription,
  setThreadTitle,
} from '../../actions';
import global from '../../global';

const threadsSelector = state => R.propOr({}, 'threads', state);
const threadIdSelector = (state, ownProps) =>
  R.propOr(R.propOr(null, 'threadId', ownProps), 'id', ownProps);
const typeSelector = (state, ownProps) => R.propOr(null, 'type', ownProps);

const threadByIdSelector = createSelector(
  threadsSelector,
  threadIdSelector,
  (threads, threadId) => R.propOr({}, threadId, threads),
);

const descriptionsSelector = state => R.propOr({}, 'descriptions', state);
const descriptionByIdSelector = createSelector(
  descriptionsSelector,
  threadIdSelector,
  (descriptions, descriptionId) => R.propOr({}, descriptionId, descriptions),
);

const mapStateToProps = (state, ownProps) => ({
  thread: threadByIdSelector(state, ownProps),
  type: typeSelector(state, ownProps),
  description: descriptionByIdSelector(state, ownProps),
});

const mapDispatchToProps = {
  getFiles,
  getThreads,
  getComments,
  getContacts,
  getDescription,
  setThreadTitle,
  _setThreadTitle,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class ThreadView extends Component {
  static displayName = 'ThreadView';
  static propTypes = {
    navigator: PropTypes.object,
    title: PropTypes.string,
    thread: PropTypes.shape({
      title: PropTypes.string,
    }),
    getContacts: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarTextfontWeight: '300',
    navBarBackgroundColor: 'rgba(43,32,45,1)',
    navBarTextColor: '#ffffff',
    navBarButtonColor: '#ffffff',
    topBarElevationShadowEnabled: false,
    navBarTitleTextCentered: true,
  };
  constructor(props) {
    super(props);
    this.state = { visible: true, loading: true };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.handleOnChangeTab = this.handleOnChangeTab.bind(this);
    this.renderTabBar = this.renderTabBar.bind(this);
  }
  componentDidMount() {
    this.props.navigator.setTitle({
      title: this.props.thread.title || this.props.title,
    });
    this.props.navigator.setButtons({
      rightButtons: [
        {
          id: 'thread-header-button',
          component: 'ThreadHeaderButton',
          passProps: {
            streamId: this.props.thread.streamId,
          },
        },
      ],
      leftButtons: [
        Platform.OS === 'ios' && this.props.customLeftButton
          ? {
              id: 'direct-back-button',
              component: 'DirectBackButton',
              passProps: {
                customLeftButton: this.props.customLeftButton,
              },
            }
          : { id: 'back' },
      ],
    });
    this.props.getContacts(['contacts'], this.props.thread.roles);
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      this.setState({ visible: true });
    }
    if (event.id === 'willDisappear') {
      this.setState({ visible: false });
    }
  }
  handleOnChangeTab({ i }) {
    if (i === 0) {
      this.props.navigator.setButtons({
        rightButtons: [
          {
            id: 'thread-header-button',
            component: 'ThreadHeaderButton',
            passProps: { streamId: this.props.thread.streamId },
          },
        ],
        leftButtons: [{ id: 'back', passProps: {} }],
      });
    }
    if (i === 1) {
      this.props.navigator.setButtons({
        rightButtons: [
          this.props.thread.streamId
            ? {
                id: 'thread-settings-button',
                component: 'ThreadSettingsButton',
                passProps: {
                  streamId: this.props.thread.streamId,
                },
              }
            : {},
        ],
        leftButtons: [
          Platform.OS === 'ios'
            ? {
                id: 'thread-back-button',
                component: 'ThreadBackButton',
                passProps: {},
              }
            : { id: 'back', passProps: {} },
        ],
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  renderTabBar() {
    return <View />;
  }
  render() {
    return (
      <Loader
        type="thread"
        threadId={this.props.id || this.props.threadId}
        threadChildrenIds={this.props.thread.children}
        color="rgba(43,32,45,1)"
        toScroll={this.props.toScroll}
      >
        <ScrollableTabView
          initialPage={0}
          ref={ref => {
            global.threadTabBar = ref;
          }}
          renderTabBar={this.renderTabBar}
          onChangeTab={this.handleOnChangeTab}
          prerenderingSiblingsNumber={Infinity}
          contentProps={{
            keyboardShouldPersistTaps: 'always',
            style: {
              flex: this.state.visible ? 1 : 0,
              backgroundColor: 'rgba(43, 32, 45, 1)',
            },
          }}
        >
          <Thread
            tabLabel="Thread"
            navigator={this.props.navigator}
            id={this.props.id || this.props.threadId}
            thread={this.props.thread}
            type={this.props.type}
            description={this.props.description}
            toScroll={this.props.toScroll}
            focusOnTitle={this.props.focusOnTitle}
            getFiles={this.props.getFiles}
            getThreads={this.props.getThreads}
            getComments={this.props.getComments}
            getContacts={this.props.getContacts}
            getDescription={this.props.getDescription}
            setThreadTitle={this.props.setThreadTitle}
            _setThreadTitle={this.props._setThreadTitle}
          />
          <ThreadBar
            tabLabel="ThreadBar"
            navigator={this.props.navigator}
            id={this.props.id || this.props.threadId}
          />
        </ScrollableTabView>
      </Loader>
    );
  }
}
