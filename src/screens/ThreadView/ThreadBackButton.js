import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import global from '../../global';

const styles = StyleSheet.create({
  buttonContainer: {
    margin: 0,
    padding: 0,
    width: 34,
    height: 34,
    marginLeft: -6,
  },
});

export default class ThreadBackButton extends PureComponent {
  static displayName = 'ThreadBackButton';
  static propTypes = {
    id: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnPress() {
    if (R.pathOr(null, ['threadTabBar', 'goToPage'], global)) {
      global.threadTabBar.goToPage(0);
    }
  }
  // eslint-disable-next-line class-methods-use-this
  renderIcon() {
    return <Icon name="ios-arrow-back" size={34} color="#ffffff" />;
  }
  render() {
    return (
      <TouchableOpacity
        style={[styles.buttonContainer]}
        onPress={this.handleOnPress}
      >
        {this.renderIcon()}
      </TouchableOpacity>
    );
  }
}
