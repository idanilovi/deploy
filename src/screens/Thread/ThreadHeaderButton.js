import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Platform, TouchableOpacity } from 'react-native';
import Icon from '../../components/Icon';
import global from '../../global';

export default class ThreadHeaderButton extends PureComponent {
  static displayName = 'ThreadHeaderButton';
  static propTypes = {
    id: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnPress() {
    if (R.pathOr(null, ['threadTabBar', 'goToPage'], global)) {
      global.threadTabBar.goToPage(1);
    }
  }
  renderIcon() {
    if (this.props.streamId) {
      return (
        <Icon
          name="ThreadSettings"
          viewBox="0 0 512 512"
          width={19}
          height={19}
        />
      );
    }
    return (
      <Icon
        name="Rocket"
        width={24}
        height={24}
        fill="#aba7bb"
        viewBox="-30 25 50 50"
      />
    );
  }
  render() {
    return (
      <TouchableOpacity
        style={
          Platform.OS === 'ios'
            ? {}
            : {
                width: 48,
                height: 48,
                justifyContent: 'center',
                alignItems: 'center',
              }
        }
        onPress={this.handleOnPress}
      >
        {this.renderIcon()}
      </TouchableOpacity>
    );
  }
}
