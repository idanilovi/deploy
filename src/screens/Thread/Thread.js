// import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';
import {
  Animated,
  Platform,
  TextInput,
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { connect } from 'react-redux';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import InputToolbar from '../../components/InputToolbar';
import Description from '../../components/Description';
import CommentList from '../../components/CommentList';
import global from '../../global';
import withSafeArea from '../withSafeArea';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 16,
  },
  threadContent: {
    flex: 1,
  },
  threadFooter: {
    flex: 0,
  },
  threadComments: {
    flex: 1,
    paddingTop: 24,
  },
  title: {
    paddingTop: 16,
    fontSize: 36,
    fontWeight: '600',
  },
  inputWrapper: {
    flex: 0,
    flexDirection: 'row',
  },
  input: {
    flex: 1,
  },
  button: {
    flex: 0,
  },
});

class Thread extends Component {
  static displayName = 'Thread';
  static propTypes = {
    thread: PropTypes.shape({
      id: PropTypes.string,
      streamId: PropTypes.string,
    }),
    getComments: PropTypes.func.isRequired,
    getDescription: PropTypes.func.isRequired,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setButtons: PropTypes.func.isRequired,
      setStyle: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
      resetTo: PropTypes.func.isRequired,
    }),
    getFiles: PropTypes.func.isRequired,
    type: PropTypes.string,
    description: PropTypes.object,
    setThreadTitle: PropTypes.func,
    _setThreadTitle: PropTypes.func,
    getThreads: PropTypes.func.isRequired,
    getContacts: PropTypes.func.isRequired,
    focusOnTitle: PropTypes.bool,
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarBackgroundColor: 'rgba(43,32,45,1)',
    navBarTextColor: '#ffffff',
    navBarButtonColor: '#ffffff',
    topBarElevationShadowEnabled: false,
    navBarTitleTextCentered: true,
  };
  constructor(props) {
    super(props);
    this.state = { multilineInputHandles: null };
  }
  componentWillMount() {
    const { thread: { id } = {} } = this.props;
    this.props.getDescription({ id });
    this.props.getFiles({ threadId: id });
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.thread.id !== nextProps.thread.id) {
      if (nextProps.thread.id) {
        nextProps.getDescription({ id: nextProps.thread.id });
        nextProps.getComments({
          threadId: nextProps.thread.id,
          skip: 0,
          limit: 30,
          force: true,
        });
        nextProps.getFiles({
          threadId: nextProps.thread.id,
        });
        nextProps.getContacts(['contacts'], nextProps.thread.roles);
        // if (nextProps.thread.children && nextProps.thread.children.length) {
        //   nextProps.getThreads({ ids: nextProps.thread.children });
        // }
      }
    }
  }
  render() {
    return (
      <Animated.View style={{ flex: 1, backgroundColor: 'rgba(43,32,45,1)' }}>
        <ScrollView
          ref={component => (global.refCommentList = component)}
          style={styles.container}
        >
          <View style={{ paddingHorizontal: 16 }}>
            <TextField
              autoFocus={this.props.focusOnTitle}
              style={styles.title}
              value={this.props.thread.title}
              onChangeText={text => {
                // eslint-disable-next-line no-underscore-dangle
                this.props._setThreadTitle(
                  this.props.thread.id || this.props.id,
                  text,
                );
              }}
              onBlur={() => {
                this.props.setThreadTitle(
                  this.props.id || this.props.thread.id,
                  this.props.thread.title,
                );
              }}
            />
          </View>
          <Description
            descriptionId={this.props.thread.id}
            navigator={this.props.navigator}
          />
          <CommentList
            threadId={this.props.thread.id}
            loaded={this.props.thread.loaded}
            type="Thread"
            dontRenderInputToolbar={true}
            navigator={this.props.navigator}
          />
        </ScrollView>
        <InputToolbar
          refCommentList={this.refCommentList}
          streamId={this.props.thread.streamId}
          threadId={this.props.thread.id}
          type={this.props.type}
        />
        {Platform.OS === 'ios' && <KeyboardSpacer />}
      </Animated.View>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: '#ffffff',
});

export default connect(mapStateToProps)(withSafeArea(Thread));

class TextField extends PureComponent {
  static displayName = 'TextField';
  static propTypes = {
    autoFocus: PropTypes.bool,
    style: PropTypes.number,
    value: PropTypes.string,
    onBlur: PropTypes.func.isRequired,
    onChangeText: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { height: 0 };
    this.getValue = this.getValue.bind(this);
    this.updateHeight = this.updateHeight.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnChangeText = this.handleOnChangeText.bind(this);
    this.handleOnContentSizeChange = this.handleOnContentSizeChange.bind(this);
  }
  getValue() {
    if (typeof this.props.value !== 'string') {
      return String(this.props.value);
    }
    return this.props.value;
  }
  updateHeight(height) {
    this.setState({ height });
  }
  handleOnChangeText(text) {
    this.props.onChangeText(text);
  }
  handleOnChange({ nativeEvent }) {
    if (nativeEvent.contentSize) {
      this.updateHeight(nativeEvent.contentSize.height);
    }
  }
  handleOnBlur() {
    this.props.onBlur();
  }
  handleOnContentSizeChange({ nativeEvent }) {
    if (nativeEvent.contentSize) {
      this.updateHeight(nativeEvent.contentSize.height);
    }
  }
  render() {
    if (Platform.OS === 'ios') {
      return (
        <AutoGrowingTextInput
          autoFocus={this.props.autoFocus}
          value={this.getValue()}
          onBlur={this.handleOnBlur}
          onChangeText={this.handleOnChangeText}
          style={[this.props.style, { fontWeight: '300' }]}
          placeholder="Enter thread title"
          placeholderTextColor="#e2e2e2"
          minHeight={45}
        />
      );
    }
    return (
      <TextInput
        autoFocus={this.props.autoFocus}
        multiline={true}
        placeholder="Enter thread title..."
        placeholderTextColor="#e2e2e2"
        textBreakStrategy="highQuality"
        numberOfLines={3}
        style={[
          this.props.style,
          {
            height: Math.max(40, this.state.height),
            fontWeight: '300',
          },
        ]}
        onBlur={this.handleOnBlur}
        onChange={this.handleOnChange}
        onChangeText={this.handleOnChangeText}
        onContentSizeChange={this.handleOnContentSizeChange}
        value={this.getValue()}
      />
    );
  }
}
