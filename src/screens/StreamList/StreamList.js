import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import ThreadList from '../../components/ThreadList';
import { getThreads } from '../../actions';
// import Loader from '../../components/Loader';

const threadsSelector = state => R.propOr({}, 'threads', state);
const streamIdSelector = (state, ownProps) =>
  R.propOr(null, 'streamId', ownProps);
const streamTitleSelector = (state, ownProps) =>
  R.propOr(null, 'title', ownProps);

const threadsIdsByStreamIdSelector = createSelector(
  streamIdSelector,
  threadsSelector,
  (streamId, threads) =>
    R.map(
      thread => R.prop('id', thread),
      R.filter(
        thread => R.prop('streamId', thread) === streamId,
        R.values(threads),
      ),
    ),
);

const mapStateToProps = (state, ownProps) => ({
  threadsIds: threadsIdsByStreamIdSelector(state, ownProps),
  streamId: streamIdSelector(state, ownProps),
  title: streamTitleSelector(state, ownProps),
});

const mapDispatchToProps = {
  getThreads,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class StreamList extends Component {
  static displayName = 'StreamList';
  static propTypes = {
    title: PropTypes.string,
    threadsIds: PropTypes.array,
    streamId: PropTypes.string,
    getThreads: PropTypes.func.isRequired,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setTitle: PropTypes.func.isRequired,
      setStyle: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
      toggleTabs: PropTypes.func.isRequired,
      resetTo: PropTypes.func.isRequired,
    }),
  };
  static navigatorStyle = { tabBarHidden: true };
  componentWillMount() {
    if (this.props.title) {
      this.props.navigator.setTitle({
        title: this.props.title,
      });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.streamId !== nextProps.streamId) {
      if (nextProps.streamId) {
        nextProps.getThreads({ streamId: nextProps.streamId });
      }
      if (this.props.title !== nextProps.title) {
        nextProps.navigator.setTitle({
          title: nextProps.title,
        });
      }
    }
  }
  render() {
    const { threadsIds, streamId } = this.props;
    return (
      <ThreadList
        styles={{
          container: {
            backgroundColor: '#ffffff',
          },
          itemStyles: {
            wrapper: {
              borderTopWidth: StyleSheet.hairlineWidth,
              borderTopColor: '#d8d8d8',
            },
            container: {
              backgroundColor: '#ffffff',
            },
            threadContainer: {
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderBottomColor: 'rgba(211, 210,211, 1)',
            },
            title: { color: '#9791a9' },
          },
        }}
        showIcons={true}
        navigator={this.props.navigator}
        items={threadsIds}
        streamId={streamId}
      />
    );
  }
}
