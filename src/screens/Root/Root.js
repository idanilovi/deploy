import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  View,
  Platform,
  NetInfo,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Navigation } from 'react-native-navigation';
import BackgroundImage from '../../components/BackgroundImage';
import {
  fetchCheckToken,
  fetchLoginRequest,
  fetchTokenFromRedirect,
  getSocketConnection,
  getContact,
  getUserSettings,
  getDataFromAsyncStorage,
} from '../../actions';
import global from '../../global';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  loader: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(40, 30, 42, 1)',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapDispatchToProps = {
  fetchCheckToken,
  fetchLoginRequest,
  fetchTokenFromRedirect,
  getSocketConnection,
  getContact,
  getUserSettings,
  getDataFromAsyncStorage,
};

@connect(null, mapDispatchToProps)
export default class Root extends Component {
  static displayName = 'Root';
  static propTypes = {
    fetchCheckToken: PropTypes.func.isRequired,
    fetchLoginRequest: PropTypes.func.isRequired,
    fetchTokenFromRedirect: PropTypes.func.isRequired,
    getSocketConnection: PropTypes.func.isRequired,
    getContact: PropTypes.func.isRequired,
    getUserSettings: PropTypes.func.isRequired,
    getDataFromAsyncStorage: PropTypes.func.isRequired,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarHidden: true,
  };
  constructor() {
    super();
    this.state = { loading: false };
    this.init = this.init.bind(this);
    this.initUser = this.initUser.bind(this);
  }
  componentDidMount() {
    global.navigator = this.props.navigator;
    if (global.timoutId) clearTimeout(global.timoutId);
  }
  componentWillMount() {
    this.setState({ loading: true });
    this.init();
  }
  componentWillUnmount() {
    this.setState({ loading: false });
  }
  async initUser(token, userId) {
    await this.props.getSocketConnection(token);
    await this.props.getContact(userId);
    await this.props.getUserSettings();
    setTimeout(() => Platform.OS === 'ios' && SplashScreen.hide(), 1000);
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'workonflow.Main',
      },
      passProps: {},
      appStyle: {
        orientation: 'portrait',
      },
    });
  }
  // TODO говно ебаное надо рефакторить
  async init() {
    // if (Platform.OS === 'ios') {
    //   console.log('CONNECT');
    //   if (global.timoutId) {
    //     clearTimeout(global.timoutId);
    //   }
    //   NetInfo.isConnected.fetch().then(async isConnected => {
    //     console.log('111111111: ', isConnected);

    //     if (!isConnected) {
    //       console.log('DisCONNECT');
    //       Navigation.startSingleScreenApp({
    //         screen: { screen: 'workonflow.Reload' },
    //         passProps: {},
    //         appStyle: { orientation: 'portrait' },
    //       });
    //     } else {
    //       console.log('!!!!!!!!!: ');
    //       global.timoutId = setTimeout(() => {
    //         Navigation.startSingleScreenApp({
    //           screen: { screen: 'workonflow.Reload' },
    //           passProps: {},
    //           appStyle: { orientation: 'portrait' },
    //         });
    //       }, 4000);
    //     }
    //   });
    // }
    const token = await this.props.getDataFromAsyncStorage('token');
    if (!token) {
      setTimeout(() => Platform.OS === 'ios' && SplashScreen.hide(), 1000);
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.Login',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }
    const checkTokenResponse = await this.props.fetchCheckToken({ token });
    if (checkTokenResponse.result) {
      await this.initUser(token, checkTokenResponse.verified.userId);
      return;
    }
    const email = await this.props.getDataFromAsyncStorage('email');
    const password = await this.props.getDataFromAsyncStorage('password');
    if (!email && !password) {
      setTimeout(() => Platform.OS === 'ios' && SplashScreen.hide(), 1000);
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.Login',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }
    await this.props.fetchLoginRequest({ email, password });
    const teamId = await this.props.getDataFromAsyncStorage('teamId');
    const to = await this.props.getDataFromAsyncStorage('to');
    const userId = await this.props.getDataFromAsyncStorage('userId');
    if (!teamId && !to && !userId) {
      setTimeout(() => Platform.OS === 'ios' && SplashScreen.hide(), 1000);
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.LoginTeams',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }
    await this.props.fetchTokenFromRedirect({ teamId, to, userId });
    const newToken = await this.props.getDataFromAsyncStorage('token');
    if (!newToken) {
      setTimeout(() => Platform.OS === 'ios' && SplashScreen.hide(), 1000);
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.Login',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }
    await this.initUser(newToken, userId);
  }
  renderLoader() {
    if (this.state.loading) {
      return (
        <View
          style={[styles.loader, { width, height, backgroundColor: '#90748B' }]}
        >
          <ActivityIndicator animating={true} color="#ffffff" size="large" />
        </View>
      );
    }
    return null;
  }
  // eslint-disable-next-line class-methods-use-this
  render() {
    return (
      // eslint-disable-next-line global-require
      <BackgroundImage
      // style={{ backgroundColor: '#90748B' }}
      // source={require('../../assets/images/bg.png')}
      >
        {this.renderLoader()}
      </BackgroundImage>
    );
  }
}
