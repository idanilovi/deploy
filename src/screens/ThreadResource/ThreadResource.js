import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  Dimensions,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { setTime, threadByIdSelector } from '../../reducers/threads';
import { setThreadTime } from '../../actions';
import withSafeArea from '../withSafeArea';

const { width, height } = Dimensions.get('window');

class ThreadResource extends PureComponent {
  static displayName = 'ThreadResource';
  static propTypes = {
    threadId: PropTypes.string,
    unit: PropTypes.string,
    thread: PropTypes.shape({
      resource: PropTypes.number,
    }),
    setTime: PropTypes.func.isRequired,
    setThreadTime: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { prevValue: props.thread.resource };
    this.handleChangeText = this.handleChangeText.bind(this);
    this.handleOnSubmitEditing = this.handleOnSubmitEditing.bind(this);
    this.handleOnClose = this.handleOnClose.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
    this.handleOnCancel = this.handleOnCancel.bind(this);
  }
  handleOnSave() {
    const {
      threadId,
      thread: { resource },
    } = this.props;
    this.props.setThreadTime(threadId, resource);
    Navigation.dismissModal();
  }
  handleOnCancel() {
    const { threadId } = this.props;
    const { prevValue } = this.state;
    this.props.setTime(threadId, prevValue);
    Navigation.dismissModal();
  }
  handleChangeText(resource) {
    const { threadId } = this.props;
    this.props.setTime(threadId, resource);
  }
  handleOnSubmitEditing() {
    const {
      threadId,
      thread: { resource },
    } = this.props;
    this.props.setThreadTime(threadId, resource);
    Navigation.dismissModal();
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnClose() {
    this.handleOnCancel();
  }
  render() {
    const {
      thread: { resource },
      unit,
    } = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.handleOnClose}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(40,30,42,1)',
            paddingHorizontal: 18,
            paddingVertical: 18,
          }}
        >
          <View
            style={{
              flex: 0,
              height: 50,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <TouchableOpacity
              style={{
                flex: 0,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.handleOnCancel}
            >
              <Text
                style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}
              >
                Cancel
              </Text>
            </TouchableOpacity>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#ffffff' }}
              >
                Time to completion
              </Text>
            </View>
            <TouchableOpacity
              style={{
                flex: 0,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.handleOnSave}
            >
              <Text
                style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}
              >
                Save
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              backgroundColor: 'transparent',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#ffffff',
                borderRadius: 13,
                width: width - 36,
                height: 68,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <TextInput
                style={{
                  flex: 1,
                  width: width - 36,
                  height: 68,
                  fontSize: 31,
                  fontWeight: '400',
                  color: '#4e4963',
                  paddingVertical: 15,
                  textAlign: 'right',
                  paddingHorizontal: 6,
                }}
                value={resource}
                selectionColor="#000000"
                autoFocus={true}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                returnKeyType="done"
                onChangeText={this.handleChangeText}
                onSubmitEditing={this.handleOnSubmitEditing}
                enablesReturnKeyAutomatically={true}
              />
              <Text
                style={{
                  flex: 1,
                  fontSize: 18,
                  fontWeight: '400',
                  color: '#4e4963',
                }}
              >
                {unit}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  thread: threadByIdSelector(state, ownProps),
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  setTime,
  setThreadTime,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadResource),
);
