import React, { Component } from 'react';
import { NetInfo } from 'react-native';
import hoistNonReactStatics from 'hoist-non-react-statics';
import Toast from 'react-native-root-toast';
import { Navigation } from 'react-native-navigation';
import global from '../global';

export default function withNetworkInformation(WrappedComponent) {
  class WithNetworkInformation extends Component {
    static displayName = `withNetworkInformation(${WrappedComponent.displayName ||
      WrappedComponent.name})`;
    constructor(props) {
      super(props);
      this.handleNetInfoChange = this.handleNetInfoChange.bind(this);
    }
    componentDidMount() {
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this.handleNetInfoChange,
      );
      if (global.offline) {
        if (global.toast && global.toast.length) {
          global.toast.forEach(toast => Toast.hide(toast));
          global.toast = [];
        }
        global.toast.push(
          Toast.show('No connection', {
            duration: 60 * 60 * 1000,
            position: -100,
            animation: true,
            shadow: false,
          }),
        );
      }
    }
    componentWillUnmount() {
      NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this.handleNetInfoChange,
      );
      if (global.toast && global.toast.length) {
        global.toast.forEach(toast => Toast.hide(toast));
        global.toast = [];
      }
    }
    // eslint-disable-next-line class-methods-use-this
    handleNetInfoChange(isConnected) {
      global.offline = !isConnected;
      console.log('OFFLINE: ', global.offline);
      if (isConnected) {
        if (global.toast && global.toast.length) {
          global.toast.forEach(toast => Toast.hide(toast));
          global.toast = [];
        }
        global.offline = false;
        Toast.show('Connection restored', {
          duration: 1000,
          position: -100,
          animation: true,
          shadow: false,
        });
      }
      if (!isConnected) {
        global.offline = true;
        global.toast.push(
          Toast.show('No connection', {
            duration: 60 * 60 * 1000,
            position: -100,
            animation: true,
            shadow: false,
          }),
        );
        // Если приложение запускается в оффлайне запускаем его на экране логинки
        // с уведомлением что нет соединения
        if (WrappedComponent.displayName === 'App' && global.offline) {
          Navigation.startSingleScreenApp({
            screen: {
              screen: 'workonflow.Login',
            },
            passProps: {},
            appStyle: {
              orientation: 'portrait',
            },
          });
        }
      }
    }
    render() {
      return <WrappedComponent {...Object.assign({}, this.props)} />;
    }
  }
  hoistNonReactStatics(WithNetworkInformation, WrappedComponent);
  return WithNetworkInformation;
}
