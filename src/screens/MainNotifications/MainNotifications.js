/* eslint-disable no-underscore-dangle */
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import R from 'ramda';
import { connect } from 'react-redux';
import { FlatList } from 'react-native';
import {
  removeNotifications,
  reversedOrderedNotificationsSelector,
} from '../../reducers/notifications';
import {
  readNotification,
  removeNotify,
  getNotifications,
} from '../../actions';
import NotifyComment from '../../components/Comment/NotifyComment';
// import SplashScreen from 'react-native-splash-screen';

const mapStateToProps = state => ({
  notifications: reversedOrderedNotificationsSelector(state),
  userId: state.userData.userId,
});

const mapDispatchToProps = {
  readNotification,
  removeNotify,
  removeNotifications,
  getNotifications,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class MainNotifications extends Component {
  static displayName = 'MainNotifications';
  static propTypes = {
    notifications: PropTypes.arrayOf(PropTypes.object),
    navigator: PropTypes.shape({
      setStyle: PropTypes.func.isRequired,
    }),
    readNotification: PropTypes.func.isRequired,
    removeNotify: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      notifications: this.props.notifications,
      update: false,
    };
    this.renderItem = this.renderItem.bind(this);
    this.keyExtractor = this.keyExtractor.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }
  componentWillReceiveProps(nextprops) {
    if (
      nextprops.notifications.filter(
        (item, i) => !R.equals(this.props.notifications[i], item),
      ).length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item) {
    return item._id;
  }
  async onRefresh() {
    this.setState({ loading: true });
    await this.props.getNotifications();
    this.setState({ loading: false });
  }
  renderItem({ item }) {
    return (
      <NotifyComment
        key={item._id}
        threadId={R.propOr('', 'threadId', item)}
        streamId={R.propOr('', 'streamId', item)}
        from={R.propOr('', 'from', item)}
        _id={R.propOr('', '_id', item)}
        headerBody={R.propOr('', 'headerBody', item)}
        notifyUrl={R.propOr('', 'notifyUrl', item)}
        fromName={R.propOr('', 'fromName', item)}
        readAt={R.propOr('', 'readAt', item)}
        createdAt={R.propOr('', 'createdAt', item)}
        countMessage={R.propOr('', 'countMessage', item)}
        nameStatus={R.propOr('', 'nameStatus', item)}
        messageNotify={R.propOr('', 'messageNotify', item)}
        colorStatus={R.propOr('', 'colorStatus', item)}
        navigator={this.props.navigator}
        userId={this.props.userId}
        readNotification={this.props.readNotification}
        logo={R.propOr('', 'logo', item)}
      />
    );
  }
  render() {
    return (
      <FlatList
        style={{ flex: 1 }}
        data={this.props.notifications}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
        onRefresh={this.onRefresh}
        refreshing={this.state.loading}
      />
    );
  }
}
