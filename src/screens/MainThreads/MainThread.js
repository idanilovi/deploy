import React, { PureComponent } from 'react';
import moment from 'moment';
import { TouchableHighlight, View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import ThreadAvatarSvg from '../../components/Icon/ThreadAvatarSvg';
import Icon from '../../components/Icon';
import global from '../../global';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    height: 86,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'hsla(0,0%,100%,.3)',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: 'hsla(0,0%,100%,.3)',
    paddingHorizontal: 12,
    margin: 0,
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 18,
  },
  avatarContainer: {
    alignSelf: 'flex-start',
    paddingRight: 10,
    justifyContent: 'center',
    paddingVertical: 20,
  },
  threadCardView: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    alignSelf: 'flex-end',
    paddingVertical: 0,
  },
  countView: {
    height: 20,
    paddingHorizontal: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    alignSelf: 'flex-start',
    borderRadius: 5,
    marginRight: 10,
  },
  title: {
    flex: 1,
    fontSize: 14,
    color: 'rgba(255,255,255,1)',
  },
  streamName: {
    flex: 1,
    fontSize: 14,
    color: 'rgba(255,255,255, 0.33)',
  },
  period: {
    fontWeight: '100',
    fontSize: 14,
    color: 'rgba(255,255,255,0.6)',
    lineHeight: 14,
  },
  deadline: {
    fontWeight: '200',
    fontSize: 14,
    lineHeight: 14,
    color: 'rgba(255,255,255,0.5)',
  },
});

export default class MainThread extends PureComponent {
  static displayName = 'MainThread';
  static propTypes = {
    id: PropTypes.string.isRequired,
    statusColor: PropTypes.string,
    percent: PropTypes.string,
    countMessage: PropTypes.number,
    title: PropTypes.string,
    deadline: PropTypes.array,
    streamName: PropTypes.string,
    priority: PropTypes.string,
    fontWeight: PropTypes.string,
    // navigator: PropTypes.shape({
    //   push: PropTypes.func.isRequired,
    //   setStyle: PropTypes.func.isRequired,
    // }),
  };
  handleOnPress(id) {
    global.tracker.trackEvent('Нажатия на кнопочки', 'thread');
    global.navigator.push({
      screen: 'workonflow.Thread',
      passProps: {
        id,
        threadId: id,
        type: 'Thread',
      },
    });
  }

  renderDeadline(deadline) {
    if (!deadline.length) {
      return null;
    }
    return deadline.filter(_ => _).length === 2 ? (
      <Text style={styles.period}>
        <Text>
          {moment(deadline[0]).calendar(null, {
            sameDay: '[Today]',
            nextDay: 'DD MMM',
            nextWeek: 'DD MMM',
            lastDay: '[Yesterday]',
            lastWeek: 'DD MMM',
            sameElse: 'DD MMM',
          })}
        </Text>-<Text>
          {moment(deadline[1]).calendar(null, {
            sameDay: '[Today]',
            nextDay: 'DD MMM',
            nextWeek: 'DD MMM',
            lastDay: '[Yesterday]',
            lastWeek: 'DD MMM',
            sameElse: 'DD MMM',
          })}
        </Text>
      </Text>
    ) : (
      <Text style={styles.deadline}>
        {' '}
        {moment(deadline[1] || deadline[0]).calendar(null, {
          sameDay: '[Today]',
          nextDay: 'DD MMM',
          nextWeek: 'DD MMM',
          lastDay: '[Yesterday]',
          lastWeek: 'DD MMM',
          sameElse: 'DD MMM',
        })}
      </Text>
    );
  }

  render() {
    return (
      <TouchableHighlight
        underlayColor="rgba(40,30,42,0.7)"
        style={styles.container}
        onPress={() => this.handleOnPress(this.props.id)}
      >
        <View style={styles.mainContainer}>
          <View style={styles.avatarContainer}>
            <ThreadAvatarSvg
              name="threadAvatar"
              width={29}
              height={29}
              fill={this.props.statusColor}
              dasharray={this.props.percent}
              viewBox="0 0 36 36"
            />
          </View>
          <View style={styles.threadCardView}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {this.props.countMessage ? (
                <View style={styles.countView}>
                  <Text
                    style={{
                      flex: 1,
                      fontWeight: this.props.fontWeight,
                    }}
                  >
                    {this.props.countMessage}
                  </Text>
                </View>
              ) : null}
              <Text
                style={[
                  styles.title,
                  {
                    fontWeight: this.props.fontWeight,
                  },
                ]}
                ellipsizeMode="tail"
                numberOfLines={1}
              >
                {this.props.title}
              </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 0 }}>
              <Text
                style={[
                  styles.streamName,
                  {
                    fontWeight: this.props.fontWeight,
                  },
                ]}
                ellipsizeMode="tail"
                numberOfLines={1}
              >
                {this.props.streamName}
              </Text>
              {this.renderDeadline(this.props.deadline)}
              {this.props.priority === 'HIGH' && (
                <View style={{ paddingLeft: 8 }}>
                  <Icon
                    name="HighPriority"
                    width={14}
                    height={14}
                    fill="#e30613"
                    viewBox="0 0 1000 1000"
                  />
                </View>
              )}
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
