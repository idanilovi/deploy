import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { FlatList, Text, Animated } from 'react-native';
import global from '../../global';
import { reversedOrderedNotificationsSelector } from '../../reducers/notifications';
import MainThread from './MainThread';

const userIdSelector = state => R.pathOr(null, ['userData', 'userId'], state);

const threadsSelector = state => R.propOr({}, 'threads', state);
const statusesSelector = state => R.propOr({}, 'statuses', state);
const contactsSelector = state => R.propOr({}, 'contacts', state);
const streamsSelector = state => R.propOr({}, 'streams', state);

const sortByUpdatedAt = R.sortBy(R.prop('updatedAt'));

const userThreadsSelector = createSelector(
  userIdSelector,
  threadsSelector,
  statusesSelector,
  contactsSelector,
  streamsSelector,
  (userId, threads, statuses, contacts, streams) =>
    R.reverse(
      sortByUpdatedAt(
        R.filter(
          ___ => R.path(['status', 'type'], ___) === 'In progress',
          R.map(thread => {
            if (thread) {
              const threadStatus = R.prop(R.prop('status', thread), statuses);
              const threadWithStatus = R.assoc('status', threadStatus, thread);
              const threadResponsible = R.prop(
                R.prop('responsibleUserId', thread),
                contacts,
              );
              const threadWithResponsible = R.assoc(
                'responsible',
                threadResponsible,
                threadWithStatus,
              );
              const threadWithStream = R.assoc(
                'stream',
                R.prop(R.prop('streamId', thread), streams),
                threadWithResponsible,
              );
              return threadWithStream;
            }
            return null;
          }, R.filter(_ => _.id, R.filter(thread => thread.responsibleUserId === userId && thread.streamId, R.values(threads)))),
        ),
      ),
    ),
);

const mapStateToProps = (state, ownProps) => ({
  items: userThreadsSelector(state),
  streams: R.propOr({}, 'streams', state),
  notifications: reversedOrderedNotificationsSelector(state),
});

@connect(mapStateToProps)
export default class MainThreads extends Component {
  static displayName = 'MainThreads';
  static propTypes = {
    threads: PropTypes.object,
    userId: PropTypes.string,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    items: PropTypes.array,
  };
  static navigatorStyle = { navBarHidden: true };
  constructor(props) {
    super(props);
    global.navigator = this.props.navigator;
    this.renderItem = this.renderItem.bind(this);
    this.getPercent = this.getPercent.bind(this);
    this.state = {
      items: this.props.items,
      update: false,
    };
  }

  componentWillReceiveProps(nextprops) {
    if (
      nextprops.items.filter((item, i) => !R.equals(this.props.items[i], item))
        .length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  // eslint-disable-next-line class-methods-use-this
  getPercent(item) {
    if (!item.id && !item._id) return;
    const id = item.id || item._id;
    const threadStatuses = R.pathOr([], ['stream', 'threadStatuses'], item);
    const statusId = R.propOr('', 'id', item.status);
    const indexOfThreadStatus = threadStatuses.indexOf(statusId) + 1;
    const precent = 100 / threadStatuses.length * indexOfThreadStatus;
    return precent;
  }
  keyExtractor(item) {
    return item.id || item._id;
  }
  // eslint-disable-next-line class-methods-use-this
  renderItem({ item }) {
    const countMessage = R.pathOr(
      0,
      [0, 'countMessage'],
      R.filter(notify => notify.threadId === item.id, this.props.notifications),
    );
    return (
      <MainThread
        id={item.id || item._id}
        statusColor={R.pathOr('rgba(255,255,255,1)', ['status', 'color'], item)}
        percent={`${this.getPercent(item) || '0'} , 100`}
        countMessage={countMessage}
        title={item.title}
        deadline={item.deadline}
        streamName={`~ ${R.pathOr('No stream', ['stream', 'name'], item)}`}
        priority={item.priority}
        fontWeight={countMessage ? 'bold' : '100'}
      />
    );
  }
  render() {
    return (
      <Animated.View>
        <FlatList
          initialNumToRender={12}
          data={this.props.items}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      </Animated.View>
    );
  }
}
