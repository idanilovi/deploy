import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';

import Root from './Root';
import Auth from './Auth';
import Main from './Main';

import Login from './Login';
import LoginTeams from './LoginTeams';
import LoginResetPassword from './LoginResetPassword';
import LoginRegistration from './LoginRegistration';
import LoginCheck from './LoginCheck';

import Stream from './Stream';

import Bots from './Bots';

import ImageLightBox from './ImageLightBox';

import Direct from './DirectView';
import Billing from '../components/Billing';
import CustomModal from '../screens/ContactBar/CustomModal';
import Thread from './ThreadView';
import ThreadSettings from './ThreadSettings';

import StreamHeaderButton from './Stream/StreamHeaderButton';
import DirectHeaderButton from './Direct/DirectHeaderButton';
import DirectBackButton from './DirectView/DirectBackButton';
import ThreadHeaderButton from './Thread/ThreadHeaderButton';
import ThreadBackButton from './ThreadView/ThreadBackButton';
import ThreadSettingsButton from './ThreadView/ThreadSettingsButton';

// Modals
import ThreadStatus from './ThreadStatus';
import ThreadStream from './ThreadStream';
import ThreadResponsible from './ThreadResponsible';
import ThreadBudget from './ThreadBudget';
import ThreadResource from './ThreadResource';
import ThreadPoints from './ThreadPoints';
import ThreadContactActions from './ThreadContactActions';

import Email from './Email';

import SidebarDrawer from './SidebarDrawer';
import Channels from './Channels';
import ChannelSettings from './ChannelSettings';
import Reload from './Reload';

import withNetworkInformation from './withNetworkInformation';
// import withSafeArea from './withSafeArea';

const WithNetworkProvider = withNetworkInformation(Provider);

export default function registerScreens(store) {
  // Sidebar Drawer
  Navigation.registerComponent(
    'workonflow.SidebarDrawer',
    () => SidebarDrawer,
    store,
    Provider,
  );
  // LightBoxes
  Navigation.registerComponent(
    'workonflow.ThreadStatus',
    () => ThreadStatus,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.ThreadStream',
    () => ThreadStream,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.ThreadResponsible',
    () => ThreadResponsible,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.ThreadBudget',
    () => ThreadBudget,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.ThreadResource',
    () => ThreadResource,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.ThreadPoints',
    () => ThreadPoints,
    store,
    WithNetworkProvider,
  );
  // Modals
  Navigation.registerComponent(
    'workonflow.ThreadContactActions',
    () => ThreadContactActions,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.Email',
    () => Email,
    store,
    WithNetworkProvider,
  );

  //
  Navigation.registerComponent(
    'StreamHeaderButton',
    () => StreamHeaderButton,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'ThreadBackButton',
    () => ThreadBackButton,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.Root',
    () => Root,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'DirectHeaderButton',
    () => DirectHeaderButton,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'DirectBackButton',
    () => DirectBackButton,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'ThreadHeaderButton',
    () => ThreadHeaderButton,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'ThreadSettingsButton',
    () => ThreadSettingsButton,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Login',
    () => Login,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.LoginTeams',
    () => LoginTeams,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.LoginResetPassword',
    () => LoginResetPassword,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.LoginRegistration',
    () => LoginRegistration,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.LoginCheck',
    () => LoginCheck,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'DirectBackButton',
    () => DirectBackButton,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.Auth',
    () => Auth,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Main',
    () => Main,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.ImageLightBox',
    () => ImageLightBox,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.ThreadSettings',
    () => ThreadSettings,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Stream',
    () => Stream,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Bots',
    () => Bots,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Thread',
    () => Thread,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Direct',
    () => Direct,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Billing',
    () => Billing,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.CustomModal',
    () => CustomModal,
    store,
    WithNetworkProvider,
  );

  Navigation.registerComponent(
    'workonflow.Channels',
    () => Channels,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.ChannelSettings',
    () => ChannelSettings,
    store,
    WithNetworkProvider,
  );
  Navigation.registerComponent(
    'workonflow.Reload',
    () => Reload,
    store,
    WithNetworkProvider,
  );
}
