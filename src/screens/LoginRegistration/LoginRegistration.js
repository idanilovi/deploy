import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  ActivityIndicator,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { AppEventsLogger } from 'react-native-fbsdk';
import FloatingLabelInput from '../../components/FloatingLabelInput';
import Button from '../../components/Button';
import PasswordButton from '../Login/PasswordButton';
import { fetchRegistrationRequest, fetchLoginRequest } from '../../actions';
import backgroundImageSource from '../../assets/images/bg.png';
import withSafeArea from '../withSafeArea';

class LoginRegistration extends Component {
  static displayName = 'LoginRegistration';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    login: PropTypes.shape({
      email: PropTypes.string,
      password: PropTypes.string,
      error: PropTypes.string,
      showPassword: PropTypes.bool,
    }),
    fetchRegistrationRequest: PropTypes.func.isRequired,
    fetchLoginRequest: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarHidden: true,
  };
  constructor() {
    super();
    this.state = {
      showCompanyNameInput: false,
      showPasswordInput: false,
      email: '',
      companyName: '',
      password: '',
      showPassword: false,
      error: false,
      link: '',
      loading: false,
    };
    this.showCompanyNameInput = this.showCompanyNameInput.bind(this);
    this.showPasswordInput = this.showPasswordInput.bind(this);

    this.setEmail = this.setEmail.bind(this);
    this.setCompanyName = this.setCompanyName.bind(this);
    this.setPassword = this.setPassword.bind(this);

    this.checkRegistrationData = this.checkRegistrationData.bind(this);
    this.checkEmailInputValue = this.checkEmailInputValue.bind(this);

    this.handleOnRegistrationPress = this.handleOnRegistrationPress.bind(this);
  }
  showCompanyNameInput() {
    this.setState({ showCompanyNameInput: true });
  }
  showPasswordInput() {
    this.setState({ showPasswordInput: true });
  }
  setEmail(value) {
    this.setState({ email: value, error: false });
  }
  checkEmailInputValue() {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg.test(this.state.email) === false) {
      this.setState({
        error: { message: 'Email is not correct' },
      });
    } else {
      this.setState({ error: false });
    }
  }
  setCompanyName(value) {
    this.setState({ companyName: value });
  }
  setPassword(value) {
    this.setState({ password: value });
  }
  checkRegistrationData() {
    if (this.state.email && this.state.companyName && this.state.password) {
      return true;
    }
    return false;
  }
  handleOnRegistrationPress() {
    if (this.state.error) {
      if (this.emailInput) {
        this.emailInput.focus();
      }
    } else {
      this.setState({ loading: true });
      this.props.fetchRegistrationRequest({
        email: this.state.email,
        name: this.state.name,
        teamName: this.state.companyName,
        password: this.state.password,
        successCallback: res => {
          this.setState({ error: res.error, link: res.link });
          this.props.fetchLoginRequest({
            email: this.state.email,
            password: this.state.password,
            successCallback: () => {
              AppEventsLogger.logEvent('fb_mobile_complete_registration');
              this.props.navigator.push({ screen: 'workonflow.LoginTeams' });
            },
            failureCallback: error => {
              this.props.setError(error);
              if (this.emailInput) {
                this.emailInput.focus();
              }
            },
          });
        },
        failureCallback: error => {
          this.setState({ error });
          if (this.emailInput) {
            this.emailInput.focus();
          }
        },
      });
    }
  }
  renderHeader() {
    return (
      <View
        style={{
          paddingHorizontal: 0,
          paddingTop: Platform.OS === 'ios' ? 42 : 38,
          paddingBottom: 8,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text
          style={{
            fontSize: 17,
            color: '#ffffff',
          }}
        >
          Sign up
        </Text>
      </View>
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            paddingVertical: 16,
            paddingHorizontal: 16,
          }}
        >
          <View style={{ flex: 1, justifyContent: 'flex-start' }}>
            {this.renderHeader()}
            <View style={{ paddingVertical: 10 }}>
              <FloatingLabelInput
                nativeID="LOGIN_FIELD"
                ref={ref => {
                  this.emailInput = ref;
                }}
                error={this.state.error}
                errorMessage={this.state.error.message}
                placeholder="Email"
                keyboardType="email-address"
                value={this.state.email}
                onBlur={this.checkEmailInputValue}
                onChangeTextValue={this.setEmail}
                autoFocus={true}
                returnKeyType="next"
                onSubmitEditing={() => {
                  this.showCompanyNameInput();
                  if (this.companyNameInput) {
                    this.companyNameInput.focus();
                  }
                }}
              />
            </View>
            {this.state.showCompanyNameInput && (
              <View style={{ paddingVertical: 10 }}>
                <FloatingLabelInput
                  ref={ref => {
                    this.companyNameInput = ref;
                  }}
                  placeholder="Company name"
                  keyboardType="default"
                  value={this.state.companyName}
                  onChangeTextValue={this.setCompanyName}
                  autoFocus={true}
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    this.showPasswordInput();
                    if (this.passwordInput) {
                      this.passwordInput.focus();
                    }
                  }}
                />
              </View>
            )}
            {this.state.showPasswordInput && (
              <View style={{ paddingVertical: 10 }}>
                <FloatingLabelInput
                  ref={ref => {
                    this.passwordInput = ref;
                  }}
                  placeholder="Password"
                  keyboardType="default"
                  value={this.state.password}
                  secureTextEntry={!this.state.showPassword}
                  onChangeTextValue={this.setPassword}
                  autoFocus={true}
                  returnKeyType="next"
                  renderButton={() => (
                    <PasswordButton
                      password={this.state.password}
                      showPassword={this.state.showPassword}
                      show={() => {
                        this.setState({ showPassword: true });
                        if (this.passwordInput) {
                          this.passwordInput.focus();
                        }
                      }}
                      hide={() => {
                        this.setState({ showPassword: false });
                        if (this.passwordInput) {
                          this.passwordInput.focus();
                        }
                      }}
                    />
                  )}
                  onSubmitEditing={this.handleOnRegistrationPress}
                />
              </View>
            )}
            {!this.state.showCompanyNameInput && (
              <View style={{ paddingVertical: 10 }}>
                <Button
                  activeOpacity={0.9}
                  title="Enter company name"
                  onPress={this.showCompanyNameInput}
                  styles={{
                    button: {
                      backgroundColor: !this.state.email
                        ? 'rgba(13, 113, 215, 0.7)'
                        : 'rgb(13, 113, 215)',
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 53,
                      borderRadius: 5,
                    },
                    title: {
                      fontSize: 25,
                      fontWeight: '400',
                      color: '#fefefe',
                    },
                  }}
                  disabled={!this.state.email}
                />
              </View>
            )}
            {this.state.showCompanyNameInput &&
              !this.state.showPasswordInput && (
                <View style={{ paddingVertical: 10 }}>
                  <Button
                    activeOpacity={0.9}
                    title="Enter password"
                    onPress={this.showPasswordInput}
                    styles={{
                      button: {
                        backgroundColor: !this.state.companyName
                          ? 'rgba(13, 113, 215, 0.7)'
                          : 'rgb(13, 113, 215)',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 53,
                        borderRadius: 5,
                      },
                      title: {
                        fontSize: 25,
                        fontWeight: '400',
                        color: '#fefefe',
                      },
                    }}
                    disabled={!this.state.companyName}
                  />
                </View>
              )}
            {this.state.showPasswordInput && (
              <View
                style={{
                  paddingVertical: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                {this.state.loading &&
                  !this.state.error && (
                    <ActivityIndicator
                      animating={true}
                      color="#ffffff"
                      size="large"
                      style={{ position: 'absolute', zIndex: 10000 }}
                    />
                  )}
                <Button
                  activeOpacity={0.9}
                  styles={{
                    button: {
                      backgroundColor: !this.checkRegistrationData()
                        ? 'rgba(0, 198, 158, 0.7)'
                        : 'rgb(0, 198, 158)',
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 53,
                      borderRadius: 5,
                    },
                    title: {
                      fontSize: 25,
                      fontWeight: '400',
                      color: '#fefefe',
                    },
                  }}
                  title="Let's start!"
                  onPress={this.handleOnRegistrationPress}
                  disabled={this.state.loading || !this.checkRegistrationData()}
                />
              </View>
            )}
          </View>
          <View style={{ flex: 1, justifyContent: 'flex-end' }}>
            <Button
              activeOpacity={0.9}
              styles={{
                button: {
                  alignItems: 'center',
                  height: 53,
                  justifyContent: 'center',
                },
                title: {
                  fontSize: 17,
                  fontWeight: '400',
                  color: '#ffffff',
                },
              }}
              title="Аlreadу have an account?"
              onPress={() => {
                this.props.navigator.push({ screen: 'workonflow.Login' });
              }}
            />
          </View>
          {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundImageSource: backgroundImageSource,
  safeAreaBackgroundImage: true,
});

const mapDispatchToProps = {
  fetchRegistrationRequest,
  fetchLoginRequest,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(LoginRegistration),
);
