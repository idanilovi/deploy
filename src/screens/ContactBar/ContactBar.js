import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  TouchableHighlight,
  View,
  TouchableOpacity,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import Icons from '../../components/Icon';
import withSafeArea from '../withSafeArea';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    borderTopRightRadius: 16,
  },
  borderBottom: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#d8d8d8',
  },
  cell: {
    paddingHorizontal: 25,
    paddingVertical: 20,
  },
  textLabel: {
    color: '#a8a4b9',
  },
  text: {
    color: '#4e4963',
  },
});

class ContactBar extends Component {
  static displayName = 'ContactBar';
  static propTypes = {
    contact: PropTypes.shape({
      id: PropTypes.string,
    }),
    streams: PropTypes.array,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setStyle: PropTypes.func.isRequired,
    }),
  };
  render() {
    const { contact, streams } = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.borderBottom}>
          <View style={styles.cell}>
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => {
                Navigation.showLightBox({
                  screen: 'workonflow.CustomModal',
                  passProps: {
                    type: 'username',
                    label: R.path(['basicData', 'name'], contact),
                    action: this.props.setContactName,
                    id: R.pathOr('', ['contact', '_id'], this.props),
                  },
                });
              }}
            >
              <Text style={styles.textLabel}>{'User name'}</Text>
              <Text style={styles.text}>
                {R.path(['basicData', 'name'], contact)}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.borderBottom}>
          <View style={styles.cell}>
            <Text style={styles.textLabel}>{'Position'}</Text>
            <Text style={styles.text}>
              {R.path(['hrData', 'position'], contact)}
            </Text>
          </View>
        </View>
        <View style={styles.borderBottom}>
          <View style={[styles.cell, { paddingVertical: 25 }]}>
            <Text style={styles.textLabel}>
              {R.propOr('About team member', 'description', contact)}
            </Text>
          </View>
        </View>
        {R.pathOr([], ['basicData', 'email'], contact).length ? (
          <View style={styles.borderBottom}>
            <View style={styles.cell}>
              <ContactBarItemList
                items={R.pathOr([], ['basicData', 'email'], contact)}
                isCustom={false}
                id={R.propOr('', '_id', contact)}
                type="Email"
                setContactEmail={this.props.setContactEmail}
                navigator={this.props.navigator}
              />
            </View>
          </View>
        ) : null}
        {R.pathOr([], ['basicData', 'phone'], contact).length ? (
          <View style={styles.borderBottom}>
            <View style={styles.cell}>
              <ContactBarItemList
                items={R.pathOr([], ['basicData', 'phone'], contact)}
                type="Phone"
                navigator={this.props.navigator}
              />
            </View>
          </View>
        ) : null}
        <View style={{}}>
          <View>
            <ContactBarItemList
              items={R.propOr([], 'customFields', contact)}
              isCastom={true}
              id={R.propOr('', '_id', contact)}
              type="CustomField"
              setContactEmail={this.props.setContactEmail}
              setContactPhone={this.props.setContactPhone}
              navigator={this.props.navigator}
            />
          </View>
        </View>
        <View style={styles.borderBottom}>
          <TouchableOpacity
            style={{ flex: 1 }}
            onPress={() => {
              Navigation.showLightBox({
                screen: 'workonflow.CustomModal',
                passProps: {
                  type: 'addContact',
                  label: 'Add contact',
                  action: this.props.setContactCustomField,
                  id: R.pathOr('', ['contact', '_id'], this.props),
                },
              });
            }}
          >
            <View
              style={[
                styles.cell,
                { borderTopWidth: 0 },
                { flexDirection: 'row', alignItems: 'center' },
              ]}
            >
              <Icons
                name="Plus"
                width={28}
                height={28}
                fill="#d3bcd7"
                viewBox="0 0 24 24"
              />
              <Text style={[styles.textLabel, { paddingHorizontal: 15 }]}>
                {'Add contact'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.borderBottom}>
          <View>
            <ContactBarItemList
              items={streams}
              type="Stream"
              label="Streams :"
              navigator={this.props.navigator}
              options={{
                action: stream =>
                  R.contains(R.prop('id', contact), R.prop('admins', stream)),
                component: <Icon name="crown" size={16} color="#FFEE58" />,
              }}
              removeFromStream={this.props.removeFromStream}
              userId={this.props.userId}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: '#ffffff',
});

export default connect(mapStateToProps)(withSafeArea(ContactBar));

export class ContactBarItemList extends PureComponent {
  static displayName = 'ContactBarItemList';
  static propTypes = {
    items: PropTypes.array,
    type: PropTypes.string,
    navigator: PropTypes.object.isRequired,
    label: PropTypes.string,
    options: PropTypes.object,
    id: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.renderItems = this.renderItems.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderLabel = this.renderLabel.bind(this);
  }
  renderItem(item, index) {
    const { type, options, id } = this.props;
    const itemProps = {
      onPressAction: null,
      label: null,
      value: null,
      options: null,
    };
    if (type === 'Email') {
      itemProps.label = 'Email';
      itemProps.value = item;
      itemProps.onPressAction = () => {
        Navigation.showLightBox({
          screen: 'workonflow.CustomModal',
          passProps: {
            type: 'email',
            label: item,
            header: 'Email',
            id,
            isCustom: this.props.isCustom,
            action: this.props.setContactEmail,
            userId: this.props.userId,
            index,
          },
        });
      };
    }
    if (type === 'Phone') {
      itemProps.label = 'Phone';
      itemProps.value = item;
      itemProps.onPressAction = () => {
        Navigation.showLightBox({
          screen: 'workonflow.CustomModal',
          passProps: {
            type: 'phone',
            label: item,
            header: 'Phone',
            id,
            isCustom: this.props.isCustom,
            action: this.props.setContactPhone,
            userId: this.props.userId,
            index,
          },
        });
      };
    }
    if (type === 'Stream') {
      itemProps.value = item.name;
      itemProps.onPress = () =>
        this.props.navigator.push({
          screen: 'workonflow.Stream',
          passProps: {
            streamId: item.id,
            type: 'Stream',
            title: item.name,
          },
        });
      itemProps.onPressAction = () =>
        Navigation.showLightBox({
          screen: 'workonflow.CustomModal',
          passProps: {
            type: 'stream',
            label: item.name,
            id: item.id,
            action: this.props.removeFromStream,
            userId: this.props.userId,
            onPress: itemProps.onPress,
          },
        });
      if (options.action(item)) {
        itemProps.options = options;
      }
    }
    if (type === 'CustomField') {
      if (item.type === 'email') {
        itemProps.onPressAction = () => {
          Navigation.showLightBox({
            screen: 'workonflow.CustomModal',
            passProps: {
              type: 'email',
              header: item.label,
              label: item.value,
              id,
              isCustom: true,
              action: this.props.setContactEmail,
              userId: this.props.userId,
              index,
            },
          });
        };
      }
      if (item.type === 'phone') {
        itemProps.onPressAction = () => {
          Navigation.showLightBox({
            screen: 'workonflow.CustomModal',
            passProps: {
              type: 'phone',
              header: item.label,
              label: item.value,
              id,
              isCustom: true,
              action: this.props.setContactPhone,
              userId: this.props.userId,
              index,
            },
          });
        };
      }
      itemProps.label = item.label;
      itemProps.value = item.value;
    }
    return (
      <ContactBarItem
        key={index}
        type={this.props.type}
        onPress={itemProps.onPress}
        onPressAction={itemProps.onPressAction}
        label={itemProps.label}
        value={itemProps.value}
        options={itemProps.options}
      />
    );
  }
  renderItems() {
    const { items } = this.props;
    return items.map((item, index) => this.renderItem(item, index));
  }
  renderLabel() {
    const { label } = this.props;
    if (label) {
      return (
        <Text
          style={[styles.textLabel, { paddingBottom: 15, paddingLeft: 25 }]}
        >
          {label}
        </Text>
      );
    }
    return null;
  }
  render() {
    return (
      <View style={this.props.type === 'Stream' ? { paddingTop: 24 } : {}}>
        {this.renderLabel()}
        {this.renderItems()}
      </View>
    );
  }
}

export class ContactBarItem extends PureComponent {
  static displayName = 'ContactBarItem';
  static propTypes = {
    onPress: PropTypes.func,
    onPressAction: PropTypes.func,
    label: PropTypes.string,
    value: PropTypes.string,
    options: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.renderLabel = this.renderLabel.bind(this);
    this.renderValue = this.renderValue.bind(this);
    this.renderOptions = this.renderOptions.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { onPressAction } = this.props;
    if (onPressAction) {
      onPressAction();
    } else {
      console.log('No action');
    }
  }
  renderLabel() {
    const { label } = this.props;
    if (label) {
      return (
        <Text style={styles.textLabel} ellipsizeMode="tail" numberOfLines={1}>
          {label}
        </Text>
      );
    }
    return null;
  }
  renderValue() {
    const { value } = this.props;
    if (value) {
      return (
        <Text style={styles.text} ellipsizeMode="tail" numberOfLines={1}>
          {this.props.type === 'Stream' ? `~${value}` : value}
        </Text>
      );
    }
    return null;
  }
  renderOptions() {
    const { options } = this.props;
    if (options) {
      return options.component;
    }
    return null;
  }
  render() {
    if (this.props.type === 'Stream') {
      return (
        <TouchableHighlight
          style={[styles.cell, { paddingVertical: 15 }, styles.borderBottom]}
          onPress={this.handleOnPress}
          underlayColor="#e2e2e2"
        >
          <View>
            <View>
              {this.renderLabel()}
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {this.renderValue()}
                {this.renderOptions()}
              </View>
            </View>
          </View>
        </TouchableHighlight>
      );
    } else {
      return (
        <TouchableHighlight
          style={[
            styles.cell,
            this.props.type === 'Email'
              ? { paddingVertical: 0, paddingHorizontal: 0 }
              : { paddingVertical: 15 },
            this.props.type === 'Email' ? {} : styles.borderBottom,
          ]}
          onPress={this.handleOnPress}
          underlayColor="#e2e2e2"
        >
          <View>
            {this.renderLabel()}
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              {this.renderValue()}
              {this.renderOptions()}
            </View>
          </View>
        </TouchableHighlight>
      );
    }
  }
}
