import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingTop: 45,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  cancelText: {
    color: '#0d71d7',
    alignSelf: 'flex-start',
    flex: 1,
  },
  saveView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingHorizontal: 10,
    flex: 1,
  },
  saveText: {
    color: '#0d71d7',
    alignSelf: 'flex-end',
  },
});

export default class ModalHeader extends PureComponent {
  static displayName = 'ModalHeader';
  static propTypes = {
    handleOnCancelPress: PropTypes.func,
    handleOnPress: PropTypes.func,
    label: PropTypes.string,
    type: PropTypes.string,
    showActionButton: PropTypes.bool,
  };
  render() {
    return (
      <View style={styles.container}>
        {this.props.showActionButton && (
          <TouchableOpacity
            style={{ flex: 1, paddingVertical: 5 }}
            onPress={this.props.handleOnCancelPress}
          >
            <Text style={styles.cancelText}>{'Cancel'}</Text>
          </TouchableOpacity>
        )}
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingVertical: 5,
          }}
        >
          <Text
            style={{
              color: '#ffffff',
            }}
          >
            {this.props.label}
          </Text>
        </View>
        {this.props.showActionButton && (
          <View style={styles.saveView}>
            <TouchableOpacity
              style={{ paddingVertical: 5 }}
              onPress={this.props.handleOnPress}
            >
              <Text style={styles.saveText}>
                {this.props.showActionButton ? 'Save' : ''}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}
