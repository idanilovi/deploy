import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { Navigation } from 'react-native-navigation';
import AddContactButtons from './AddContactButtons';
import StreamActions from './StreamActions';
import ModalHeader from './ModalHeader';
import Username from './Username';
import EmailActions from './EmailActions';
import PhoneActions from './PhoneActions';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width,
    height,
    backgroundColor: 'rgba(0,0,0,0.9)',
    alignItems: 'center',
    zIndex: 1,
  },
  containerView: {
    width: width - 80,
    height,
    paddingTop: height - 500,
    zIndex: 10,
  },
});

export default class CustomModal extends PureComponent {
  static displayName = 'CustomModal';
  static propTypes = {
    type: PropTypes.string.isRequired,
    header: PropTypes.string,
    label: PropTypes.string,
    id: PropTypes.string.isRequired,
    isCustom: PropTypes.bool,
    action: PropTypes.action,
    userId: PropTypes.string,
    index: PropTypes.number,
    navigator: PropTypes.object.isRequired,
    onPress: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { value: '', label: '', type: '' };
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnChangeState = this.handleOnChangeState.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.handleOnCancelPress = this.handleOnCancelPress.bind(this);
  }
  handleOnChangeState(data) {
    this.setState(data);
  }
  handleOnPress() {
    if (this.props.type === 'addContact') {
      const { label, value, type } = this.state;
      this.props.action(this.props.id, label, value, type);
    }
    if (this.props.type === 'username') {
      const { value } = this.state;
      this.props.action({ id: this.props.id, name: value });
    }
    if (this.props.type === 'email') {
      const { label } = this.state;
      this.props.action({
        id: this.props.id,
        email: label,
        isCustom: this.props.isCustom,
        index: this.props.index,
      });
    }
    if (this.props.type === 'phone') {
      const { label } = this.state;
      this.props.action({
        id: this.props.id,
        phone: label,
        isCustom: this.props.isCustom,
        index: this.props.index,
      });
    }
    Navigation.dismissLightBox();
  }
  handleOnCancelPress() {
    this.setState({});
    Navigation.dismissLightBox();
  }
  renderContent() {
    if (this.props.type === 'addContact') {
      return (
        <AddContactButtons
          handleOnChangeState={this.handleOnChangeState}
          id={this.props.id}
        />
      );
    }
    if (this.props.type === 'stream') {
      return (
        <StreamActions
          userId={this.props.userId}
          id={this.props.id}
          onPress={this.props.onPress}
          action={this.props.action}
          navigator={this.props.navigator}
          label={this.props.label}
        />
      );
    }
    if (this.props.type === 'username') {
      return (
        <Username
          label={this.props.label}
          handleOnChangeState={this.handleOnChangeState}
        />
      );
    }
    if (this.props.type === 'email') {
      return (
        <EmailActions
          handleOnChangeState={this.handleOnChangeState}
          header={this.props.header}
          label={this.props.label}
          id={this.props.id}
          index={this.props.index}
          isCustom={this.props.isCustom}
          action={this.props.action}
        />
      );
    }

    if (this.props.type === 'phone') {
      return (
        <PhoneActions
          handleOnChangeState={this.handleOnChangeState}
          header={this.props.header}
          label={this.props.label}
          id={this.props.id}
          index={this.props.index}
          isCustom={this.props.isCustom}
          action={this.props.action}
        />
      );
    }
    return null;
  }
  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.handleOnCancelPress}
      >
        <ModalHeader
          handleOnCancelPress={this.handleOnCancelPress}
          handleOnPress={this.handleOnPress}
          label={this.props.label}
          type={this.props.type}
          showActionButton={
            this.props.type === 'username' ||
            this.state.type ||
            this.state.showInput
          }
        />
        <View style={styles.containerView}>{this.renderContent()}</View>
      </TouchableOpacity>
    );
  }
}
