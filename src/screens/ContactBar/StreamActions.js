import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Navigation } from 'react-native-navigation';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    height: 150,
    borderRadius: 14,
    paddingHorizontal: 35,
    paddingVertical: 35,
  },
  text: { color: '#4e4963', fontSize: 18, fontWeight: '400' },
  button: { paddingBottom: 20 },
});

export default class StreamActions extends PureComponent {
  static displayName = 'StreamActions';
  static propTypes = {
    userId: PropTypes.string.isRequired,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    id: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
    label: PropTypes.string,
    onPress: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.handleOnGoToStreamPress = this.handleOnGoToStreamPress.bind(this);
    this.handleOnRemovePress = this.handleOnRemovePress.bind(this);
  }
  handleOnGoToStreamPress() {
    if (this.props.onPress) {
      this.props.onPress();
    }
    Navigation.dismissLightBox();
  }
  handleOnRemovePress() {
    Navigation.dismissLightBox();
    this.props.action({
      id: this.props.id,
      userId: this.props.userId,
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={this.handleOnGoToStreamPress}
          style={styles.button}
        >
          <Text style={styles.text}>Go to Stream</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleOnRemovePress}>
          <Text style={styles.text}>Remove</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
