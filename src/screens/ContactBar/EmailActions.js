import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import Communications from 'react-native-communications';
import { Navigation } from 'react-native-navigation';

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#ffffff',
    height: 80,
    borderRadius: 14,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  buttonsContainer: {
    backgroundColor: '#ffffff',
    height: 170,
    borderRadius: 14,
    paddingHorizontal: 30,
    paddingVertical: 25,
  },
  header: { paddingHorizontal: 4, paddingVertical: 5 },
  input: {
    flex: 1,
    paddingVertical: 20,
    justifyContent: 'center',
    fontWeight: '300',
    paddingTop: 0,
    paddingBottom: 0,
  },
  button: { paddingBottom: 15 },
  buttonText: { fontSize: 18 },
});

export default class EmailActions extends PureComponent {
  static displayName = 'EmailActions';
  static propTypes = {
    handleOnChangeState: PropTypes.func.isRequired,
    header: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    index: PropTypes.number,
    isCustom: PropTypes.bool,
    action: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);

    this.state = {
      showInput: false,
      label: '',
    };

    this.renderInput = this.renderInput.bind(this);
    this.renderButtons = this.renderButtons.bind(this);
    this.handleOnTextChange = this.handleOnTextChange.bind(this);
    this.handelOnEmailToPress = this.handelOnEmailToPress.bind(this);
    this.handleOnEditPress = this.handleOnEditPress.bind(this);
    this.handleOnRemovePress = this.handleOnRemovePress.bind(this);
  }

  handleOnTextChange(text) {
    this.props.handleOnChangeState({ label: text });
    this.setState({ label: text });
  }
  handelOnEmailToPress() {
    Communications.email(this.props.label, null, null, null, null);
  }
  handleOnEditPress() {
    this.props.handleOnChangeState({
      showInput: true,
      label: this.props.label,
    });
    this.setState({ showInput: true, label: this.props.label });
  }
  handleOnRemovePress() {
    this.props.action({
      id: this.props.id,
      email: '',
      index: this.props.index,
      isCustom: this.props.isCustom,
    });
    this.setState({ label: '' });
    Navigation.dismissLightBox();
  }

  renderInput() {
    return (
      <View style={styles.inputContainer}>
        <View style={styles.header}>
          <Text>{this.props.header}</Text>
        </View>
        <TextInput
          onChangeText={this.handleOnTextChange}
          autoFocus={true}
          value={this.state.label || this.props.label}
          placeholderTextColor="#a8a4b9"
          textBreakStrategy="highQuality"
          underlineColorAndroid="#ffffff"
          style={styles.input}
        />
      </View>
    );
  }
  renderButtons() {
    return (
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={this.handelOnEmailToPress}
        >
          <Text style={styles.buttonText}>{'Email to'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={this.handleOnEditPress}
        >
          <Text style={styles.buttonText}>{'Edit'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={this.handleOnRemovePress}
        >
          <Text style={styles.buttonText}>{'Remove'}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return this.state.showInput ? this.renderInput() : this.renderButtons();
  }
}
