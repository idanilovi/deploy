import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    height: 50,
    borderRadius: 14,
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  input: {
    flex: 1,
    paddingVertical: 20,
    justifyContent: 'center',
    fontWeight: '300',
    paddingTop: 0,
    paddingBottom: 0,
  },
});

export default class Username extends PureComponent {
  static displayName = 'Username';
  static propTypes = {
    handleOnChangeState: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.handleOnTextChange = this.handleOnTextChange.bind(this);
  }

  handleOnTextChange(text) {
    this.props.handleOnChangeState({ value: text });
    this.setState({ value: text });
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          onChangeText={this.handleOnTextChange}
          value={this.state.value || this.props.label}
          autoFocus={true}
          placeholderTextColor="#a8a4b9"
          textBreakStrategy="highQuality"
          underlineColorAndroid="#ffffff"
          style={styles.input}
        />
      </View>
    );
  }
}
