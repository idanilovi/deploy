import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
  StyleSheet,
} from 'react-native';
import Icon from '../../components/Icon';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width: width - 80,
    minHeight: 130,
    borderRadius: 10,
    paddingHorizontal: 20,
  },
  inputContainer: { flexDirection: 'column', flex: 1 },
  firstInputView: {
    flex: 1,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#000000',
  },
  secondInputView: { flex: 1 },
  firstInput: {
    flex: 1,
    paddingVertical: 20,
    justifyContent: 'center',
    fontWeight: '300',
    paddingTop: 0,
    paddingBottom: 0,
  },
  secondInput: {
    flex: 1,
    paddingVertical: 20,
    justifyContent: 'center',
  },
  row: { flexDirection: 'row' },
  button: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginHorizontal: 10,
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default class AddContactButtons extends Component {
  static displayName = 'AddContactButtons';
  static propTypes = {
    handleOnChangeState: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      label: '',
      value: '',
      type: '',
      showInput: false,
    };
    this.handleOnAction = this.handleOnAction.bind(this);
    this.renderInput = this.renderInput.bind(this);
    this.renderButtons = this.renderButtons.bind(this);
    this.handleOnLabelChange = this.handleOnLabelChange.bind(this);
    this.handleOnValueChange = this.handleOnValueChange.bind(this);
  }

  handleOnAction(label, type) {
    this.props.handleOnChangeState({ label, type });
    this.setState({ showInput: true, label, type });
  }

  handleOnLabelChange(text) {
    this.props.handleOnChangeState({ label: text });
    this.setState({ label: text });
  }
  handleOnValueChange(text) {
    this.props.handleOnChangeState({ value: text });
    this.setState({ value: text });
  }

  renderInput() {
    return (
      <View style={styles.inputContainer}>
        <View style={styles.firstInputView}>
          <TextInput
            onChangeText={this.handleOnLabelChange}
            value={this.state.label}
            placeholderTextColor="#a8a4b9"
            textBreakStrategy="highQuality"
            underlineColorAndroid="#ffffff"
            style={styles.firstInput}
          />
        </View>
        <View style={styles.secondInputView}>
          <TextInput
            onChangeText={this.handleOnValueChange}
            value={this.state.value}
            autoFocus={true}
            style={styles.secondInput}
            placeholderTextColor="#a8a4b9"
            placeholder={`Enter ${this.state.type}`}
            textBreakStrategy="highQuality"
            underlineColorAndroid="#ffffff"
          />
        </View>
      </View>
    );
  }

  renderButtons() {
    return (
      <View>
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => this.handleOnAction('Email', 'email')}
            style={[
              styles.button,
              {
                backgroundColor: '#bcb471',
              },
            ]}
          >
            <Icon
              name="Email"
              width={53}
              height={36}
              fill="#ffffff"
              viewBox="0 0 32 32"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.handleOnAction('Phone', 'phone')}
            style={[
              styles.button,
              {
                backgroundColor: '#3cbc72',
              },
            ]}
          >
            <Icon
              name="Phone"
              width={44}
              height={25}
              fill="#ffffff"
              viewBox="0 0 139 139"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.handleOnAction('Telegram', 'telegram');
            }}
            style={[
              styles.button,
              {
                backgroundColor: '#139bd0',
              },
            ]}
          >
            <Icon
              name="Telegram"
              width={44}
              height={30}
              fill="#ffffff"
              viewBox="0 0 939 787"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => {
              this.handleOnAction('Facebook', 'facebook');
            }}
            style={[
              styles.button,
              {
                backgroundColor: '#597ac7',
              },
            ]}
          >
            <Icon
              name="Facebook"
              width={41}
              height={24}
              fill="#ffffff"
              viewBox="0 0 56.693 56.693"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.handleOnAction('VK', 'vk')}
            style={[
              styles.button,
              {
                backgroundColor: '#4c75a3',
              },
            ]}
          >
            <Icon
              name="Vk"
              width={54}
              height={22}
              fill="#ffffff"
              viewBox="0 0 24 24"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.handleOnAction('Skype', 'skype')}
            style={[
              styles.button,
              {
                backgroundColor: '#09c6ff',
              },
            ]}
          >
            <Icon
              name="Skype"
              width={60}
              height={24}
              fill="#ffffff"
              viewBox="0 0 56.693 56.693"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => this.handleOnAction('Link', 'link')}
            style={[
              styles.button,
              {
                backgroundColor: '#b457bc',
              },
            ]}
          >
            <Icon
              name="Link"
              width={68}
              height={24}
              fill="#ffffff"
              viewBox="0 0 80 80"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.handleOnAction('Text', 'text')}
            style={[
              styles.button,
              {
                backgroundColor: '#b457bc',
              },
            ]}
          >
            <Icon
              name="Txt"
              width={74}
              height={24}
              fill="#ffffff"
              viewBox="0 0 32 32"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  render() {
    return (
      <View
        style={[
          styles.container,
          { backgroundColor: this.state.showInput ? '#ffffff' : 'transparent' },
        ]}
      >
        {this.state.showInput ? this.renderInput() : this.renderButtons()}
      </View>
    );
  }
}
