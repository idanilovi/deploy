import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import URI from 'urijs';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { StyleSheet, WebView } from 'react-native';
import { setUserData } from '../../reducers/userdata';
import { setToken, getSocketConnection } from '../../actions';

const styles = StyleSheet.create({
  webview: {
    flex: 1,
  },
});

const userDataSelector = state => R.propOr({}, 'userData', state);
const querySelector = (state, ownProps) => R.propOr(null, 'query', ownProps);
const webviewUriSelector = createSelector(querySelector, query => {
  if (query) {
    return `https://login.workonflow.com/${query}`;
  }
  return 'https://login.workonflow.com';
});

const mapStateToProps = (state, ownProps) => ({
  userData: userDataSelector(state),
  url: webviewUriSelector(state, ownProps),
});

const mapDispatchToProps = {
  setToken,
  setUserData,
  getSocketConnection,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class AuthWebView extends Component {
  static propTypes = {
    setUserData: PropTypes.func.isRequired,
    setToken: PropTypes.func.isRequired,
    getSocketConnection: PropTypes.func.isRequired,
    userData: PropTypes.shape({
      token: PropTypes.string,
    }),
    url: PropTypes.string,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
  };
  static navigatorStyle = {
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.onNavigationStateChange = this.onNavigationStateChange.bind(this);
  }
  onNavigationStateChange(event) {
    const parsedUrl = URI.parse(event.url);
    if (parsedUrl.hostname) {
      const parsedQuery = URI.parseQuery(parsedUrl.query);
      if (parsedQuery.token) {
        this.props.setUserData({ token: parsedQuery.token });
        this.props.setToken(parsedQuery.token);
        this.props.getSocketConnection(parsedQuery.token);
        if (this.props.userData.token) {
          this.props.navigator.push({ screen: 'workonflow.Main' });
        }
      }
    }
  }
  render() {
    return (
      <WebView
        source={{ uri: this.props.url }}
        style={styles.webview}
        onNavigationStateChange={this.onNavigationStateChange}
        thirdPartyCookiesEnabled
      />
    );
  }
}
