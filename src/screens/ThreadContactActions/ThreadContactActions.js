import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import {
  setResponsible,
  deleteResponsible,
  deleteRole,
} from '../../reducers/threads';
import {
  setThreadResponsible,
  deleteThreadResponsible,
  deleteThreadRole,
  search,
  setThreadCustomer,
} from '../../actions';
import ContactIcon from '../../components/ContactIcon';
import withSafeArea from '../withSafeArea';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 0,
    borderRadius: 13,
    backgroundColor: '#ffffff',
    paddingVertical: 24,
    width: width - 36,
    zIndex: 10,
  },
  button: {
    paddingHorizontal: 35,
    height: 56,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  buttonText: {
    fontSize: 20,
    color: '#4e4963',
    fontWeight: '400',
  },
});

function throttle(func, ms) {
  let isThrottled = false;
  let savedArgs;
  let savedThis;
  function wrapper() {
    if (isThrottled) {
      savedArgs = arguments;
      savedThis = this;
      return;
    }
    func.apply(this, arguments);
    isThrottled = true;
    setTimeout(function() {
      isThrottled = false;
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedArgs = savedThis = null;
      }
    }, ms);
  }
  return wrapper;
}

class ThreadContactActions extends PureComponent {
  static displayName = 'ThreadContactActions';
  static propTypes = {
    header: PropTypes.string,
    threadId: PropTypes.string,
    userId: PropTypes.string,
    items: PropTypes.array,
    currentItem: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
      color: PropTypes.string,
    }),
    type: PropTypes.string,
    setResponsible: PropTypes.func.isRequired,
    setThreadResponsible: PropTypes.func.isRequired,
    deleteResponsible: PropTypes.func.isRequired,
    deleteThreadResponsible: PropTypes.func.isRequired,
    deleteRole: PropTypes.func.isRequired,
    deleteThreadRole: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      searchCustomer: false,
      text: '',
      contacts: [],
    };
    this.handleOnClose = this.handleOnClose.bind(this);
    this.handleSetResponsible = this.handleSetResponsible.bind(this);
    this.handleDirectMessage = this.handleDirectMessage.bind(this);
    this.handleProfile = this.handleProfile.bind(this);
    this.handleDeleteResponsible = this.handleDeleteResponsible.bind(this);
    this.handleDeleteFollower = this.handleDeleteFollower.bind(this);
    this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.renderButtons = this.renderButtons.bind(this);
    this.search = this.search.bind(this);
    this.hendleOnItemPress = this.hendleOnItemPress.bind(this);
    this._debounceHandleOnChange = throttle(this.search, 300);
  }
  handleDirectMessage() {
    const { currentItem } = this.props;
    Navigation.showModal({
      screen: 'workonflow.Direct',
      passProps: {
        contactId: R.prop('_id', currentItem),
        title: R.path(['basicData', 'name'], currentItem),
        type: 'Direct',
        initialPage: 0,
        customLeftButton: true,
      },
    });
  }
  handleProfile() {
    const { currentItem } = this.props;
    Navigation.showModal({
      screen: 'workonflow.Direct',
      navigatorButtons: {
        leftButtons: [{ id: 'back' }],
      },
      passProps: {
        contactId: R.prop('_id', currentItem),
        title: R.path(['basicData', 'name'], currentItem),
        type: 'Direct',
        initialPage: 1,
        customLeftButton: true,
      },
    });
  }
  handleSetResponsible() {
    const { items, currentItem, threadId, type, header } = this.props;
    // Navigation.dismissModal();
    if (type === 'responsible') {
      Navigation.showModal({
        screen: 'workonflow.ThreadResponsible',
        passProps: {
          items,
          currentItem,
          threadId,
          header,
          type,
        },
      });
    }
    if (type === 'follower') {
      this.props.setResponsible(threadId, R.prop('_id', currentItem));
      this.props.setThreadResponsible({
        threadId,
        userId: R.prop('_id', currentItem),
      });
      Navigation.dismissModal();
    }
  }
  handleDeleteResponsible() {
    const { threadId, currentItem } = this.props;
    this.props.deleteResponsible(threadId, R.prop('_id', currentItem));
    this.props.deleteThreadResponsible({ threadId });
    Navigation.dismissModal();
  }
  handleDeleteFollower() {
    const { threadId, currentItem } = this.props;
    this.props.setThreadCustomer({
      id: threadId,
      customerId: null,
    });
    Navigation.dismissModal();
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnClose() {
    Navigation.dismissModal();
  }
  handleChangeCustomer() {
    this.setState({
      searchCustomer: true,
    });
  }
  handleOnChange(text) {
    this.setState({
      text,
    });
  }
  async search(text) {
    const res = await this.props.search(text);
    this.setState({
      contacts: res,
    });
  }
  hendleOnItemPress(customerId, id) {
    this.props.setThreadCustomer({ id, customerId });
    Navigation.dismissModal();
  }
  renderButtons() {
    const { type } = this.props;
    if (type === 'responsible') {
      return (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleSetResponsible}
            >
              <Text style={styles.buttonText}>Change responsible to</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleDirectMessage}
            >
              <Text style={styles.buttonText}>Direct message</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleProfile}
            >
              <Text style={styles.buttonText}>Go to profile</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleDeleteResponsible}
            >
              <Text style={styles.buttonText}>Remove from responsible</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    if (type === 'follower') {
      return (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleSetResponsible}
            >
              <Text style={styles.buttonText}>Set as responsible</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleDirectMessage}
            >
              <Text style={styles.buttonText}>Direct message</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleProfile}
            >
              <Text style={styles.buttonText}>Go to profile</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleDeleteFollower}
            >
              <Text style={styles.buttonText}>Remove from thread</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    if (type === 'customer') {
      if (this.state.searchCustomer || !this.props.currentItem) {
        return (
          <View style={{ flex: 1, alignItems: 'center' }}>
            <View
              style={{
                // flex: 1,
                borderRadius: 13,
                backgroundColor: '#ffffff',
                // paddingVertical: 24,
                width: width - 36,
                // height: 80,
                zIndex: 10,
              }}
            >
              <TextInput
                style={{ paddingHorizontal: 15, paddingVertical: 15 }}
                onChangeText={text => {
                  this.handleOnChange(text);
                  this._debounceHandleOnChange(text);
                }}
                autoFocus={true}
                value={this.state.text}
                underlineColorAndroid="#ffffff"
                placeholder="Enter contact name"
              />
              <ScrollView
                style={{ maxHeight: 250 }}
                keyboardShouldPersistTaps="always"
              >
                {this.state.contacts.map(contact => (
                  <TouchableOpacity
                    key={contact._id}
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      paddingHorizontal: 15,
                      flex: 1,
                    }}
                    onPress={() =>
                      this.hendleOnItemPress(contact._id, this.props.threadId)
                    }
                  >
                    <View style={{ paddingRight: 5 }}>
                      <ContactIcon
                        show={true}
                        // id={R.propOr(null, '_id', contact)}
                        customer={true}
                        contact={contact}
                        // name={R.pathOr('', ['basicData', 'name'], this.props.contact)}
                        // color={R.propOr('#ffffff', 'color', this.props.contact)}
                        // type={R.propOr(null, 'billingType', this.props.contact)}
                      />
                    </View>
                    <Text>
                      {R.pathOr('', ['_source', 'basicData', 'name'], contact)}
                    </Text>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
          </View>
        );
      }
      return (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleChangeCustomer}
            >
              <Text style={styles.buttonText}>Change customer to</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleDeleteFollower}
            >
              <Text style={styles.buttonText}>Remove from thread</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    return null;
  }
  render() {
    const { header } = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.handleOnClose}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(40,30,42,1)',
            paddingHorizontal: 18,
            paddingVertical: 18,
          }}
        >
          <View
            style={{
              flex: 0,
              height: 50,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#ffffff' }}
              >
                {header || 'Responsible'}
              </Text>
            </View>
          </View>

          {this.renderButtons()}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  setResponsible,
  setThreadResponsible,
  deleteRole,
  deleteResponsible,
  deleteThreadResponsible,
  deleteThreadRole,
  search,
  setThreadCustomer,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadContactActions),
);
