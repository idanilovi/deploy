import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Platform, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import global from '../../global';

export default class ThreadBarHeaderButton extends PureComponent {
  static displayName = 'ThreadBarHeaderButton';
  static propTypes = {
    id: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnPress() {
    global.goToPage(0);
  }
  render() {
    return (
      <TouchableOpacity
        style={{ paddingHorizontal: 6, marginLeft: -12 }}
        onPress={this.handleOnPress}
      >
        <Icon
          name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'}
          size={34}
          color="#ffffff"
        />
      </TouchableOpacity>
    );
  }
}
