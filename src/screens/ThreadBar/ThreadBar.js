import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { createSelector } from 'reselect';
import { StyleSheet, ScrollView, View, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  setThreadStream,
  setThreadStatus,
  setThreadResponsible,
  deleteThreadResponsible,
  setThreadRole,
  setThreadPoints,
  deleteThreadRole,
  setThreadPriority,
  setThreadBudget,
  setThreadTime,
  setThreadDeadline,
  setStreamWidgets,
  getContacts,
  getContact,
} from '../../actions';
import {
  Responsible,
  Roles,
  Status,
  Points,
  Priority,
  Budget,
  Time,
  Stream,
  Deadline,
  Settings,
  Customer,
} from '../../components/ThreadWidgets';
import {
  setStatus,
  setPriority,
  setPoints,
  setResponsible,
  deleteResponsible,
  setBudget,
  setTime,
  setStream,
  setDeadline,
} from '../../reducers/threads';
import { setWidgetSettings } from '../../reducers/streams';
import withSafeArea from '../withSafeArea';

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: 'rgba(43,32,45,1)' },
  scrollContainer: {
    flex: 1,
    flexDirection: 'column',
    // padding: 8,
    borderTopRightRadius: 16,
    backgroundColor: '#ffffff',
  },
});

const threadsSelector = state => R.propOr({}, 'threads', state);
const statusesSelector = state => R.propOr({}, 'statuses', state);
const contactsSelector = state => R.propOr({}, 'contacts', state);
const streamsSelector = state => R.propOr({}, 'streams', state);

const userDataSelector = state => R.propOr({}, 'userData', state);

const filterByBillingType = R.filter(
  contact => R.propOr(null, 'billingType', contact) === 'users' || 'bots',
);

const usersSelector = createSelector(contactsSelector, contacts =>
  filterByBillingType(R.values(contacts)),
);

const visibleStreamsSelector = createSelector(
  userDataSelector,
  streamsSelector,
  (userdata, streams) => {
    if (R.prop('visibleStreams', userdata)) {
      return R.map(
        streamId => R.propOr({}, streamId, streams),
        R.pathOr([], ['visibleStreams', 'seen'], userdata),
      );
    }
    return R.filter(stream => stream.id, R.values(streams));
  },
);

const invisibleStreamsSelector = createSelector(
  userDataSelector,
  streamsSelector,
  (userdata, streams) => {
    if (R.prop('visibleStreams', userdata)) {
      return R.map(
        streamId => R.propOr({}, streamId, streams),
        R.pathOr([], ['visibleStreams', 'unSeen'], userdata),
      );
    }
    return [];
  },
);

const streamsArraySelector = createSelector(
  visibleStreamsSelector,
  invisibleStreamsSelector,
  (visibleStreams, invisibleStreams) =>
    [{ id: undefined, name: 'Reset stream' }]
      .concat(visibleStreams)
      .concat(invisibleStreams),
);

const threadIdSelector = (state, ownProps) => R.propOr(null, 'id', ownProps);

const statusesArraySelector = createSelector(statusesSelector, statuses =>
  R.values(statuses),
);

const threadByIdSelector = createSelector(
  threadIdSelector,
  threadsSelector,
  (threadId, threads) => R.propOr({}, threadId, threads),
);

const threadStreamIdSelector = createSelector(threadByIdSelector, thread =>
  R.prop('streamId', thread),
);

const threadStreamSelector = createSelector(
  threadStreamIdSelector,
  streamsSelector,
  (streamId, streams) => R.propOr({}, streamId, streams),
);

const threadStatusesSelector = createSelector(
  threadStreamSelector,
  statusesSelector,
  (stream, statuses) =>
    R.map(
      statusId => R.propOr({}, statusId, statuses),
      R.propOr([], 'threadStatuses', stream),
    ),
);

const streamWidgetSettingsSelector = createSelector(
  threadStreamSelector,
  stream => R.pathOr({}, ['settings', 'widgets'], stream),
);

const statusesByStreamIdSelector = createSelector(
  threadStreamIdSelector,
  statusesArraySelector,
  (streamId, statuses) =>
    R.filter(status => status.streamId === streamId, statuses),
);

const hydratedThreadByIdSelector = createSelector(
  statusesSelector,
  contactsSelector,
  streamsSelector,
  threadByIdSelector,
  (statuses, contacts, streams, thread) => {
    if (thread) {
      const threadStatus = R.prop(R.prop('status', thread), statuses);
      const threadWithStatus = R.assoc('status', threadStatus, thread);
      const threadResponsible = R.prop(
        R.prop('responsibleUserId', thread),
        contacts,
      );
      const threadWithResponsible = R.assoc(
        'responsibleUser',
        threadResponsible,
        threadWithStatus,
      );
      const threadContacts = R.filter(
        _ => _,
        R.map(
          role => R.propOr(null, role, contacts),
          R.filter(
            _ => _ !== R.propOr([], 'responsibleUserId', thread),
            R.propOr([], 'roles', thread),
          ),
        ),
      );
      const threadRoles = R.filter(
        role => R.propOr(null, 'billingType', role) !== 'contacts',
        threadContacts,
      );
      const threadWithRoles = R.assoc(
        'roles',
        threadRoles,
        threadWithResponsible,
      );
      const threadStream = R.prop(R.prop('streamId', thread), streams);
      const threadWithStream = R.assoc('stream', threadStream, threadWithRoles);
      const threadCustomer = R.filter(
        role =>
          R.propOr(null, 'customerId', thread) === R.propOr('', '_id', role),
        threadContacts,
      );
      const threadWithCustomer = R.assoc(
        'customer',
        threadCustomer,
        threadWithStream,
      );
      return threadWithCustomer;
    }
  },
);

const responsibleContactSelector = createSelector(
  contactsSelector,
  threadsSelector,
  threadIdSelector,
  (contacts, threads, threadId) => {
    const thread = R.propOr({}, threadId, threads);
    return R.propOr({}, thread.responsibleUserId, contacts);
  },
);

const customersContactsSelector = createSelector(contactsSelector, contacts =>
  R.filter(
    contact => R.propOr(null, 'billingType', contact) === 'contacts',
    R.values(contacts),
  ),
);

const rolesContactsSelector = createSelector(
  contactsSelector,
  threadsSelector,
  threadIdSelector,
  responsibleContactSelector,
  (contacts, threads, threadId, responsibleUser) => {
    const thread = R.propOr({}, threadId, threads);
    return R.filter(
      _ =>
        (_.billingType === 'users' || _.billingType === 'bots') &&
        !R.propOr([], 'roles', thread).includes(_._id) &&
        _._id !== responsibleUser._id,
      R.values(contacts),
    );
  },
);

const streamContactsSelector = createSelector(
  contactsSelector,
  streamsSelector,
  threadStreamIdSelector,
  responsibleContactSelector,
  threadsSelector,
  threadIdSelector,
  (contacts, streams, streamId, responsibleUser, threads, threadId) => {
    const stream = R.propOr({}, streamId, streams);
    const thread = R.propOr({}, threadId, threads);
    return R.filter(
      _ =>
        R.propOr([], 'roles', stream).includes(_._id) &&
        _._id !== responsibleUser._id,
      R.values(contacts),
    );
  },
);

class ThreadBar extends Component {
  static displayName = 'ThreadBar';
  static propTypes = {
    thread: PropTypes.object,
    navigator: PropTypes.shape({
      setStyle: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
    }),
    streams: PropTypes.arrayOf(PropTypes.object),
    statuses: PropTypes.arrayOf(PropTypes.object),
    setThreadStreamId: PropTypes.func,
    setThreadStatus: PropTypes.func,
    setStatus: PropTypes.func,
    setThreadResponsible: PropTypes.func,
    responsibleUser: PropTypes.object,
    streamContacts: PropTypes.array,
    rolesContacts: PropTypes.array,
    setThreadRole: PropTypes.func,
    getContacts: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarBackgroundColor: 'rgba(43,32,45,1)',
    navBarTextColor: '#ffffff',
    navBarButtonColor: '#ffffff',
    topBarElevationShadowEnabled: false,
    navBarTitleTextCentered: true,
  };
  constructor(props) {
    super(props);
    this.state = {
      responsibleUser: props.responsibleUser,
      roles: props.thread.roles,
      show: false,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.renderStreamSelect = this.renderStreamSelect.bind(this);
    this.renderStatusWidget = this.renderStatusWidget.bind(this);
    this.renderResponsibleWidget = this.renderResponsibleWidget.bind(this);
    this.renderCustomerWidget = this.renderCustomerWidget.bind(this);
    this.renderPriorityWidget = this.renderPriorityWidget.bind(this);
    this.renderBudgetWidget = this.renderBudgetWidget.bind(this);
    this.renderTimeBudgetWidget = this.renderTimeBudgetWidget.bind(this);
    this.renderPointsWidget = this.renderPointsWidget.bind(this);
    this.renderRoles = this.renderRoles.bind(this);
    this.renderDeadline = this.renderDeadline.bind(this);
  }
  componentDidMount() {
    if (R.prop('customerId', this.props.thread)) {
      this.props.getContact(R.prop('customerId', this.props.thread));
    }
  }
  componentWillUnmount() {
    this.settings = null;
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress') {
      if (event.id === 'edit_widgets') {
        if (this.settings) {
          this.settings.show();
        }
      }
    }
  }
  renderStreamSelect() {
    return (
      <Stream
        placeholder="Set stream"
        currentItem={this.props.thread.stream}
        items={this.props.streams}
        threadId={this.props.thread.id}
      />
    );
  }
  renderStatusWidget() {
    if (R.pathOr(false, ['WorkflowStatus', 'on'], this.props.settings)) {
      return (
        <Status
          placeholder="Set status"
          currentItem={this.props.thread.status}
          items={this.props.statuses}
          threadId={this.props.thread.id}
          onItemPress={statusId => {
            this.props.setStatus(this.props.thread.id, statusId);
            this.props.setThreadStatus(this.props.thread.id, statusId);
          }}
        />
      );
    }
    return null;
  }
  renderResponsibleWidget() {
    if (R.pathOr(false, ['Responsible', 'on'], this.props.settings)) {
      return (
        <Responsible
          currentItem={this.props.responsibleUser}
          items={this.props.streamContacts}
          threadId={this.props.thread.id}
        />
      );
    }
    return null;
  }
  renderCustomerWidget() {
    if (R.pathOr(false, ['Customers', 'on'], this.props.settings)) {
      return (
        <Customer
          threadId={this.props.thread.id}
          currentItem={this.props.thread}
          customer={
            R.filter(
              customer =>
                customer._id === R.prop('customerId', this.props.thread),
              this.props.customersContacts,
            )[0]
          }
        />
      );
    }
    return null;
  }
  renderPriorityWidget() {
    if (R.pathOr(false, ['Priority', 'on'], this.props.settings)) {
      return <Priority threadId={this.props.thread.id} label="Priority" />;
    }
    return null;
  }
  renderBudgetWidget() {
    if (R.pathOr(false, ['Budget', 'on'], this.props.settings)) {
      return (
        <Budget
          threadId={this.props.thread.id}
          label="Budget"
          value={R.propOr(0, 'budget', this.props.thread)}
          unit="₽"
        />
      );
    }
    return null;
  }
  renderTimeBudgetWidget() {
    if (R.pathOr(false, ['TimeBudget', 'on'], this.props.settings)) {
      return (
        <Time
          threadId={this.props.thread.id}
          label="Time"
          value={R.propOr('', 'resource', this.props.thread)}
          unit="Hours"
        />
      );
    }
    return null;
  }
  renderPointsWidget() {
    if (R.pathOr(false, ['TimeBudget', 'on'], this.props.settings)) {
      return (
        <Points
          threadId={this.props.thread.id}
          placeholder="Set points"
          label="Points"
          value={R.propOr(0, 'mainRank', this.props.thread)}
        />
      );
    }
    return null;
  }
  renderRoles() {
    return (
      <Roles
        currentItems={R.uniq(R.propOr([], 'roles', this.props.thread))}
        items={this.props.rolesContacts}
        threadId={this.props.thread.id}
      />
    );
  }
  renderDeadline() {
    if (R.pathOr(false, ['DataTime', 'on'], this.props.settings)) {
      return (
        <Deadline
          threadId={this.props.thread.id}
          label="Deadline"
          value={R.propOr(null, 'deadline', this.props.thread)}
          previousValue={R.propOr(null, 'previousDeadline', this.props.thread)}
          onChange={this.props.setDeadline}
          onSubmit={this.props.setThreadDeadline}
        />
      );
    }
    return null;
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.scrollContainer}
          keyboardShouldPersistTaps="always"
        >
          {this.renderStreamSelect()}
          {this.renderStatusWidget()}
          {this.renderResponsibleWidget()}
          {this.renderCustomerWidget()}
          <View style={{ paddingHorizontal: 15, paddingTop: 15 }}>
            <Text style={{ fontWeight: '400', fontSize: 15, color: '#a8a4b9' }}>
              Parameters
            </Text>
          </View>
          {this.renderDeadline()}
          {this.renderPriorityWidget()}
          {this.renderPointsWidget()}
          {this.renderBudgetWidget()}
          {this.renderTimeBudgetWidget()}
          {this.renderRoles()}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  thread: hydratedThreadByIdSelector(state, ownProps),
  settings: streamWidgetSettingsSelector(state, ownProps),
  streams: streamsArraySelector(state, ownProps),
  statuses: threadStatusesSelector(state, ownProps),
  responsibleUser: responsibleContactSelector(state, ownProps),
  streamContacts: usersSelector(state, ownProps),
  rolesContacts: rolesContactsSelector(state, ownProps),
  customersContacts: customersContactsSelector(state),
  safeAreaBackgroundColor: '#ffffff',
});

const mapDispatchToProps = {
  getContacts,
  setStream,
  setThreadStream,
  setThreadStatus,
  setStatus,
  setThreadResponsible,
  deleteThreadResponsible,
  setThreadRole,
  deleteThreadRole,
  setPoints,
  setThreadPoints,
  setPriority,
  setThreadPriority,
  setBudget,
  setThreadBudget,
  setTime,
  setThreadTime,
  setDeadline,
  setThreadDeadline,
  setWidgetSettings,
  setStreamWidgets,
  getContact,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadBar),
);
