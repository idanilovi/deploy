import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import SidebarList from '../../components/SidebarList';
import { createStream, createUser } from '../../actions';
import { Animated } from 'react-native';

const streamsSelector = state => R.propOr({}, 'streams', state);

const userDataSelector = state => R.propOr({}, 'userData', state);
const userIdSelector = createSelector(userDataSelector, userdata =>
  R.propOr(null, 'userId', userdata),
);

const filterItems = R.filter(_ => R.has('id', _) || R.has('_id', _));

const filterPrivateStreams = (stream, userId) =>
  !stream.isPrivate || R.contains(userId, stream.roles);

const visibleForUserStreamsSelector = createSelector(
  userIdSelector,
  streamsSelector,
  userDataSelector,
  (userId, streams, userdata) =>
    R.filter(
      stream => userdata.isAdmin || filterPrivateStreams(stream, userId),
      streams,
    ),
);

const visibleStreamsSelector = createSelector(
  userDataSelector,
  streamsSelector,
  visibleForUserStreamsSelector,
  (userdata, streams, visibleStreams) => {
    if (R.path(['visibleStreams', 'seen', 'length'], userdata)) {
      return R.map(streamId => {
        const stream = R.propOr({}, streamId, visibleStreams);
        return {
          ...stream,
          ...(R.propOr([], 'roles', stream).includes(
            R.propOr(null, 'userId', userdata),
          )
            ? { itemColor: 'rgba(255,255,255, 1)' }
            : { itemColor: 'rgba(255,255,255,0.6)' }),
        };
      }, R.pathOr([], ['visibleStreams', 'seen'], userdata));
    }
    return R.values(visibleStreams);
  },
);

const invisibleStreamsSelector = createSelector(
  userDataSelector,
  streamsSelector,
  visibleForUserStreamsSelector,
  (userdata, streams, visibleStreams) => {
    return R.map(streamId => {
      const stream = R.propOr({}, streamId, visibleStreams);
      return {
        ...stream,
        ...(R.propOr([], 'roles', stream).includes(
          R.propOr(null, 'userId', userdata),
        )
          ? { itemColor: 'rgba(255,255,255, 1)' }
          : { itemColor: 'rgba(255,255,255,0.6)' }),
      };
    }, R.pathOr([], ['visibleStreams', 'unSeen'], userdata));
  },
);

const otherStreamsSelector = createSelector(
  streamsSelector,
  userDataSelector,
  visibleForUserStreamsSelector,
  (streams, userData, visibleStreams) => {
    const allStreams = R.pathOr(
      [],
      ['visibleStreams', 'seen'],
      userData,
    ).concat(R.pathOr([], ['visibleStreams', 'unSeen'], userData));
    if (allStreams.length === 0) {
      return [];
    }
    return R.filter(
      stream => !allStreams.includes(stream.id),
      R.values(visibleStreams),
    );
  },
);

const sidebarlistStreamsSelector = createSelector(
  visibleStreamsSelector,
  invisibleStreamsSelector,
  otherStreamsSelector,
  (visibleStreams, invisibleStreams, otherStreams) => {
    return R.concat(
      R.concat(visibleStreams, otherStreams),
      invisibleStreams,
    ).concat([{ type: 'inviteButton', text: 'Create stream' }]);
  },
);

const mapStateToProps = state => ({
  items: sidebarlistStreamsSelector(state),
  userId: userIdSelector(state),
});

const mapDispatchToProps = {
  createStream,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class MainStreams extends Component {
  static displayName = 'MainStreams';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setStyle: PropTypes.func.isRequired,
    }),
    hideContacts: PropTypes.func,
    showContacts: PropTypes.func,
    items: PropTypes.array,
  };
  static navigatorStyle = { navBarHidden: true };
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.items,
      update: false,
    };
  }
  componentWillReceiveProps(nextprops) {
    if (
      nextprops.items.filter((item, i) => !R.equals(this.props.items[i], item))
        .length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  render() {
    return (
      <Animated.View>
        <SidebarList
          navigator={this.props.navigator}
          items={this.props.items}
          createStream={this.props.createStream}
          userId={this.props.userId}
          type={'Stream'}
        />
      </Animated.View>
    );
  }
}
