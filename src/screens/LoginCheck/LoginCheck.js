import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, Text, Platform } from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { connect } from 'react-redux';
import Button from '../../components/Button';
import backgroundImageSource from '../../assets/images/bg.png';
import withSafeArea from '../withSafeArea';

class LoginCheck extends Component {
  static displayName = 'LoginCheck';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarHidden: true,
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          paddingVertical: 16,
          paddingHorizontal: 16,
        }}
      >
        <View style={{ flex: 1, justifyContent: 'flex-start' }}>
          <View
            style={{
              paddingHorizontal: 0,
              paddingTop: Platform.OS === 'ios' ? 42 : 38,
              paddingBottom: 8,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text
              style={{
                fontSize: 25,
                color: '#ffffff',
              }}
            >
              Check your email app
            </Text>
          </View>
        </View>
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <Button
            activeOpacity={0.9}
            styles={{
              button: {
                alignItems: 'center',
                height: 53,
                justifyContent: 'center',
              },
              title: {
                fontSize: 17,
                fontWeight: '400',
                color: '#ffffff',
              },
            }}
            title="Back to Log in "
            onPress={() =>
              this.props.navigator.push({ screen: 'workonflow.Login' })
            }
          />
        </View>
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
      </View>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundImageSource: backgroundImageSource,
  safeAreaBackgroundImage: true,
});

export default connect(mapStateToProps)(withSafeArea(LoginCheck));
