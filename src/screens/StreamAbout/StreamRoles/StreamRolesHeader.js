import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  header: { paddingTop: 30, paddingBottom: 15, paddingHorizontal: 16 },
  text: { fontWeight: '400', fontSize: 15, color: '#a8a4b9' },
});

class StreamRolesHeader extends PureComponent {
  static displayName = 'StreamRoles.Header';
  static propTypes = {
    title: PropTypes.string,
  };
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.text}>{this.props.title}</Text>
      </View>
    );
  }
}

export default StreamRolesHeader;
