import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  button: {
    paddingVertical: 16,
    paddingHorizontal: 36,
  },
  label: {
    fontWeight: '400',
    color: '#4e4963',
    fontSize: 20,
  },
});

class StreamRolesButton extends PureComponent {
  static displayName = 'StreamRolesButton';
  static propTypes = {
    label: PropTypes.string,
    onPress: PropTypes.func.isRequired,
  };
  render() {
    const { label, onPress } = this.props;
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.button}>
          <Text style={styles.label}>{label}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default StreamRolesButton;
