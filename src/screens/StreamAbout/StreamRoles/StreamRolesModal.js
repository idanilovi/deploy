import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Dimensions,
  View,
  TouchableWithoutFeedback,
  StyleSheet,
} from 'react-native';
import Modal from 'react-native-modal';
import StreamRoleButton from './StreamRoleButton';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    width,
    height,
  },
  wrapper: {
    backgroundColor: '#ffffff',
    flex: 0,
    borderRadius: 13,
    paddingVertical: 26,
  },
});

class StreamRolesModal extends Component {
  static displayName = 'StreamRolesModal';
  static propTypes = {
    id: PropTypes.string,
    buttonsArray: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        onPress: PropTypes.func.isRequired,
      }),
    ),
    visible: PropTypes.bool,
    onShow: PropTypes.func.isRequired,
    onHide: PropTypes.func.isRequired,
    children: PropTypes.node,
  };
  renderModalButtons() {
    const { buttonsArray, id, onHide } = this.props;
    return buttonsArray.map(({ label, onPress }, index) => (
      <StreamRoleButton
        key={index}
        label={label}
        onPress={() => {
          onHide();
          onPress(id);
        }}
      />
    ));
  }
  render() {
    return (
      <Modal
        isVisible={this.props.visible}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        onBackdropPress={this.props.onHide}
      >
        <TouchableWithoutFeedback
          style={styles.container}
          onPress={this.props.onHide}
        >
          <View style={styles.wrapper}>{this.renderModalButtons()}</View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

export default StreamRolesModal;
