import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    borderBottomColor: 'rgba(211, 210,211, 1)',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  input: {
    height: 70,
    paddingVertical: 20,
    paddingHorizontal: 14,
    fontWeight: '400',
    fontSize: 15,
  },
});

class StreamRolesInput extends PureComponent {
  static displayName = 'StreamRolesInput';
  static propTypes = {
    value: PropTypes.string,
    onChangeText: PropTypes.func.isRequired,
  };
  render() {
    const { value, onChangeText } = this.props;
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Enter name..."
          placeholderTextColor="#a8a4b9"
          autoFocus={true}
          underlineColorAndroid="rgba(255,255,255,0)"
          onChangeText={onChangeText}
          value={value}
        />
      </View>
    );
  }
}

export default StreamRolesInput;
