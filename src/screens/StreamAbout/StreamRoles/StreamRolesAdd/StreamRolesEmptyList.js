import React from 'react';
import { Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  text: {
    color: '#cccccc',
    fontSize: 16,
    paddingVertical: 16,
    paddingHorizontal: 8,
    fontWeight: '300',
  },
});

const StreamRolesEmptyList = () => (
  <Text style={styles.text}>No matches...</Text>
);

StreamRolesEmptyList.displayName = 'StreamRolesEmptyList';

export default StreamRolesEmptyList;
