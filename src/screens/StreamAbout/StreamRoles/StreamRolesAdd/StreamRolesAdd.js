import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import StreamRolesAddButton from './StreamRolesAddButton';
import StreamRolesAddModal from './StreamRolesAddModal';
import StreamRolesAddModalContent from './StreamRolesAddModalContent';

class StreamRolesAdd extends Component {
  static displayName = 'StreamRoles.Add';
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
    show: PropTypes.bool,
    onPress: PropTypes.func,
  };
  constructor() {
    super();
    this.state = { visible: false };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }
  showModal() {
    this.setState({ visible: true });
  }
  hideModal() {
    this.setState({ visible: false });
  }
  render() {
    if (this.props.show) {
      return (
        <View>
          <StreamRolesAddButton
            show={this.props.show}
            onPress={this.showModal}
          />
          <StreamRolesAddModal
            visible={this.state.visible}
            onShow={this.showModal}
            onHide={this.hideModal}
          >
            <StreamRolesAddModalContent
              items={this.props.items}
              onPress={this.props.onPress}
              onShow={this.showModal}
              onHide={this.hideModal}
            />
          </StreamRolesAddModal>
        </View>
      );
    }
    return null;
  }
}

export default StreamRolesAdd;
