import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Modal from 'react-native-modal';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    width,
    height,
  },
  wrapper: {
    backgroundColor: '#ffffff',
    flex: 1,
    borderRadius: 13,
    paddingVertical: 26,
  },
});

class StreamRolesAddModal extends Component {
  static displayName = 'StreamRolesAddModal';
  static propTypes = {
    visible: PropTypes.bool,
    onShow: PropTypes.func.isRequired,
    onHide: PropTypes.func.isRequired,
    children: PropTypes.node,
  };
  render() {
    return (
      <Modal
        isVisible={this.props.visible}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        onBackdropPress={this.props.onHide}
      >
        <TouchableWithoutFeedback
          style={styles.container}
          onPress={this.props.onHide}
        >
          <View style={styles.wrapper}>{this.props.children}</View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

export default StreamRolesAddModal;
