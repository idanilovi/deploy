import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import ContactListItem from '../../../../components/ContactListItem';

class StreamRolesAddButton extends Component {
  static displayName = 'StreamRolesAddButton';
  static propTypes = {
    show: PropTypes.bool,
    onPress: PropTypes.func.isRequired,
  };
  render() {
    if (this.props.show) {
      return (
        <ContactListItem
          showIcon
          placeholder="Add follower"
          style={{
            container: {
              height: 64,
              borderBottomColor: 'rgba(211, 210,211, 1)',
              borderBottomWidth: StyleSheet.hairlineWidth,
            },
            contactListItem: {
              backgroundColor: '#e2e2e2',
              marginBottom: 8,
            },
            contactItemText: {
              fontSize: 18,
              fontWeight: '700',
              color: '#4e4963',
            },
          }}
          onPress={this.props.onPress}
        />
      );
    }
    return null;
  }
}

export default StreamRolesAddButton;
