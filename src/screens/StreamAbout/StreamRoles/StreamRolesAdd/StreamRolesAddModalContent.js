import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FlatList, StyleSheet } from 'react-native';
import ContactListItem from '../../../../components/ContactListItem';
import StreamRolesInput from './StreamRolesInput';
import StreamRolesEmptyList from './StreamRolesEmptyList';

const styles = StyleSheet.create({
  list: {
    flex: 1,
    borderRadius: 13,
    backgroundColor: '#ffffff',
  },
});

const filterContactsByName = (name, items) => {
  if (!name) {
    return { items, value: name };
  }
  const lowerCaseName = name.toLowerCase();
  const filtredContacts = R.filter(
    contact =>
      R.pathOr('', ['basicData', 'name'], contact)
        .toLowerCase()
        .indexOf(lowerCaseName) + 1,
    items,
  );
  return { items: filtredContacts, value: name };
};

class StreamRolesAddModalContent extends Component {
  static displayName = 'StreamRolesAddModalContent';
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
    onPress: PropTypes.func.isRequired,
    onHide: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { items: props.items, value: '' };
    this.renderItem = this.renderItem.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
  }
  handleOnPress(id) {
    if (this.props.onPress) {
      this.props.onHide();
      this.props.onPress(id);
    }
  }
  handleOnChange(text) {
    const { items } = this.props;
    this.setState(filterContactsByName(text, items));
  }
  renderItem({ item }) {
    return (
      <ContactListItem
        // eslint-disable-next-line no-underscore-dangle
        key={item._id}
        showIcon
        style={{
          container: {
            height: 70,
            borderBottomColor: 'rgba(211, 210,211, 1)',
            borderBottomWidth: StyleSheet.hairlineWidth,
            paddingTop: 14,
            paddingBottom: 14,
          },
          contactListItem: {
            backgroundColor: '#e2e2e2',
          },
          contactItemText: {
            color: '#4e4963',
            fontSize: 18,
            fontWeight: '700',
          },
        }}
        contact={item}
        // eslint-disable-next-line no-underscore-dangle
        onPress={() => this.handleOnPress(item._id)}
      />
    );
  }
  render() {
    return (
      <FlatList
        style={styles.list}
        keyExtractor={item => item.id}
        data={this.state.items}
        renderItem={this.renderItem}
        ListHeaderComponent={
          <StreamRolesInput
            value={this.state.value}
            onChangeText={this.handleOnChange}
          />
        }
        ListEmptyComponent={<StreamRolesEmptyList />}
      />
    );
  }
}

export default StreamRolesAddModalContent;
