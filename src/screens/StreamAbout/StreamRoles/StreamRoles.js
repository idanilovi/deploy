import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import StreamRolesHeader from './StreamRolesHeader';
import StreamRolesList from './StreamRolesList';
import StreamRolesAdd from './StreamRolesAdd';
import StreamRolesModal from './StreamRolesModal';

class StreamRoles extends Component {
  static displayName = 'StreamRoles';
  static propTypes = {
    title: PropTypes.string,
    placeholder: PropTypes.string,
    currentItems: PropTypes.arrayOf(PropTypes.object),
    items: PropTypes.arrayOf(PropTypes.object),
    onAdd: PropTypes.func,
    onDelete: PropTypes.func,
    showAddButton: PropTypes.bool,
    buttonsArray: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        onPress: PropTypes.func.isRequired,
      }),
    ),
  };
  constructor() {
    super();
    this.state = { visible: false, current: null };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  showModal(id) {
    this.setState({ visible: true, current: id });
  }
  hideModal() {
    this.setState({ visible: false, current: null });
  }
  handleOnPress(id) {
    this.showModal(id);
  }
  render() {
    return (
      <View>
        <StreamRolesHeader title={this.props.title} />
        <StreamRolesList
          items={this.props.currentItems}
          placeholder="Add admin"
          onPress={this.handleOnPress}
        />
        <StreamRolesAdd
          items={this.props.items}
          show={this.props.showAddButton}
          onPress={this.props.onAdd}
        />
        <StreamRolesModal
          id={this.state.current}
          buttonsArray={this.props.buttonsArray}
          onShow={this.showModal}
          onHide={this.hideModal}
          visible={this.state.visible}
        />
      </View>
    );
  }
}

export default StreamRoles;
