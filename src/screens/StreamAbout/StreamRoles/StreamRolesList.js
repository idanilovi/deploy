import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import ContactListItem from '../../../components/ContactListItem';

class StreamRolesList extends Component {
  static displayName = 'StreamRoles.List';
  static propTypes = {
    items: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
      }),
    ),
    placeholder: PropTypes.string,
    onPress: PropTypes.func,
  };
  render() {
    const { placeholder, items, onPress } = this.props;
    return items.map(item => (
      <ContactListItem
        // eslint-disable-next-line no-underscore-dangle
        key={item._id}
        showIcon
        placeholder={placeholder}
        style={{
          container: {
            height: 64,
            borderBottomColor: 'rgba(211, 210,211, 1)',
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderStyle: 'dashed',
          },
          contactListItem: {
            backgroundColor: '#e2e2e2',
            marginBottom: 8,
          },
          contactItemText: {
            fontSize: 18,
            fontWeight: '700',
            color: '#4e4963',
          },
        }}
        contact={item}
        onPress={() => onPress(item._id)}
      />
    ));
  }
}

export default StreamRolesList;
