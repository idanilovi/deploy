import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, View } from 'react-native';
import {
  mappedStreamAdminsSelector,
  diffBetweenStreamRolesAndAdminsSelector,
} from '../../selectors';
import {
  getStreamDescription,
  setStreamName,
  setStreamResponsibleUser,
  setStreamPrivacy,
  addStreamAdmin,
  deleteStreamAdmin,
  addStreamRole,
  deleteStreamRole,
} from '../../actions';
import {
  setName,
  setPrivacy,
  setResponsibleUser,
  addAdmin,
  deleteAdmin,
  addRole,
  deleteRole,
  streamByIdSelector,
} from '../../reducers/streams';
import { usersArraySelector } from '../../reducers/contacts';
import Description from '../../components/Description';
import StreamRoles from './StreamRoles';
import StreamPrivate from './StreamPrivate';
import StreamName from './StreamName';
import withSafeArea from '../withSafeArea';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});

class StreamAbout extends Component {
  static displayName = 'StreamAbout';
  static propTypes = {
    navigator: PropTypes.object,
    streamId: PropTypes.string,
    title: PropTypes.string,
    stream: PropTypes.object,
    description: PropTypes.object,
    getDescriptionByStreamId: PropTypes.func.isRequired,
    setName: PropTypes.func.isRequired,
    saveName: PropTypes.func.isRequired,
    setResponsibleUser: PropTypes.func.isRequired,
    setPrivacy: PropTypes.func.isRequired,
    addAdmin: PropTypes.func.isRequired,
    deleteAdmin: PropTypes.func.isRequired,
    addRole: PropTypes.func.isRequired,
    deleteRole: PropTypes.func.isRequired,
    roles: PropTypes.arrayOf(PropTypes.object),
    admins: PropTypes.arrayOf(PropTypes.object),
    users: PropTypes.arrayOf(PropTypes.object),
    isPrivate: PropTypes.bool,
  };
  componentDidMount() {
    this.props.getDescriptionByStreamId();
  }
  render() {
    const { stream: { name, isPrivate } = {} } = this.props;
    return (
      <ScrollView style={styles.container}>
        <StreamName
          placeholder="Enter stream name"
          value={name}
          onChangeText={this.props.setName}
          onBlur={this.props.saveName}
        />
        <View style={{ paddingHorizontal: 10 }}>
          <Description
            navigator={this.props.navigator}
            descriptionId={this.props.streamId}
          />
        </View>
        <StreamPrivate
          value={isPrivate}
          label="Private"
          onChange={this.props.setPrivacy}
        />
        <StreamRoles
          showAddButton={false}
          currentItems={this.props.admins}
          title="Admins"
          buttonsArray={[
            { label: 'Delete admin', onPress: this.props.deleteAdmin },
          ]}
        />
        <StreamRoles
          showAddButton={true}
          currentItems={this.props.roles}
          items={this.props.users}
          title="Members"
          placeholder="Add member"
          buttonsArray={[
            { label: 'Make admin', onPress: this.props.addAdmin },
            { label: 'Delete member', onPress: this.props.deleteRole },
          ]}
          onAdd={this.props.addRole}
          onDelete={this.props.deleteRole}
        />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  stream: streamByIdSelector(state, ownProps),
  roles: diffBetweenStreamRolesAndAdminsSelector(state, ownProps),
  admins: mappedStreamAdminsSelector(state, ownProps),
  users: usersArraySelector(state),
  safeAreaBackgroundColor: '#ffffff',
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  getDescriptionByStreamId: () =>
    dispatch(getStreamDescription({ streamId: ownProps.streamId })),
  setName: name => {
    dispatch(setName(ownProps.streamId, name));
  },
  saveName: name => {
    dispatch(setStreamName(ownProps.streamId, name));
  },
  setResponsibleUser: userId => {
    dispatch(setResponsibleUser(ownProps.streamId, userId));
    dispatch(setStreamResponsibleUser(ownProps.streamId, userId));
  },
  setPrivacy: () => {
    dispatch(setPrivacy(ownProps.streamId));
    dispatch(setStreamPrivacy(ownProps.streamId));
  },
  addAdmin: userId => {
    dispatch(addAdmin(ownProps.streamId, userId));
    dispatch(addStreamAdmin(ownProps.streamId, userId));
  },
  deleteAdmin: userId => {
    dispatch(deleteAdmin(ownProps.streamId, userId));
    dispatch(deleteStreamAdmin(ownProps.streamId, userId));
  },
  addRole: userId => {
    dispatch(addRole(ownProps.streamId, userId));
    dispatch(addStreamRole(ownProps.streamId, userId));
  },
  deleteRole: userId => {
    dispatch(deleteRole(ownProps.streamId, userId));
    dispatch(deleteStreamRole(ownProps.streamId, userId));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(StreamAbout),
);
