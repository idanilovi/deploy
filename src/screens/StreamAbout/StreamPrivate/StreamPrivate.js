import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import {
  StyleSheet,
  View,
  TouchableHighlight,
  Switch,
  Text,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'rgba(211, 210,211, 1)',
  },
  wrapper: {
    flexDirection: 'row',
    flex: 1,
    height: 70,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
  },
  label: {
    fontSize: 18,
    fontWeight: '400',
    color: '#4e4963',
  },
});

class StreamPrivate extends PureComponent {
  static displayName = 'StreamPrivate';
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    label: PropTypes.string,
    value: PropTypes.bool,
  };
  render() {
    const { label, value, onChange } = this.props;
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={onChange}
        underlayColor="rgba(242, 242, 245, 1)"
      >
        <View style={styles.wrapper}>
          <Text style={styles.label}>{label}</Text>
          <Switch value={value} onValueChange={onChange} />
        </View>
      </TouchableHighlight>
    );
  }
}

export default StreamPrivate;
