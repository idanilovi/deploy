import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { TextInput, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  input: {
    color: '#c4c2cf',
    fontSize: 22,
    fontWeight: '700',
    paddingHorizontal: 20,
    paddingVertical: 40,
  },
});

class StreamName extends Component {
  static displayName = 'StreamName';
  static propTypes = {
    value: PropTypes.string,
    placeholder: PropTypes.string,
    onChangeText: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
  };
  constructor() {
    super();
    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.handleOnChangeText = this.handleOnChangeText.bind(this);
  }
  handleOnBlur() {
    if (this.props.onBlur) {
      this.props.onBlur(this.props.value);
    }
  }
  handleOnChangeText(text) {
    if (this.props.onChangeText) {
      this.props.onChangeText(text);
    }
  }
  render() {
    const { value, placeholder } = this.props;
    return (
      <TextInput
        style={styles.input}
        multiline={true}
        placeholder={placeholder}
        placeholderTextColor="#c4c2cf"
        textBreakStrategy="highQuality"
        numberOfLines={3}
        underlineColorAndroid="transparent"
        onBlur={this.handleOnBlur}
        onChangeText={this.handleOnChangeText}
        value={value}
      />
    );
  }
}

export default StreamName;
