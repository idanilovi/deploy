import React, { PureComponent } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class StreamHeaderButton extends PureComponent {
  static displayName = 'StreamHeaderButton';
  // eslint-disable-next-line class-methods-use-this
  render() {
    return <Icon name="more-vert" size={28} color="#ffffff" />;
  }
}
