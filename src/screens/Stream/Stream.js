import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import StreamBoard from '../StreamBoard';
import StreamChat from '../StreamChat';
import StreamList from '../StreamList';
import StreamAbout from '../StreamAbout';
import StreamTabBar from './StreamTabBar';
import Loader from '../../components/Loader';
import global from '../../global';

class Stream extends Component {
  static displayName = 'Stream';
  static propTypes = {
    navigator: PropTypes.object,
    title: PropTypes.string,
    streamId: PropTypes.string,
    type: PropTypes.string,
  };
  static navigatorStyle = {
    navBarTextfontWeight: '300',
    navBarBackgroundColor: 'rgba(43,32,45,1)',
    navBarTextColor: '#ffffff',
    navBarButtonColor: '#ffffff',
    topBarElevationShadowEnabled: false,
    navBarTitleTextCentered: true,
  };
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'stream-header-button',
        component: 'StreamHeaderButton',
        passProps: {},
      },
    ],
  };
  constructor(props) {
    super(props);
    this.state = { visible: true, loading: true };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.handleOnChangeTab = this.handleOnChangeTab.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  componentWillMount() {
    global.tracker.trackScreenView('Chat');
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      this.setState({ visible: true });
    }
    if (event.id === 'willDisappear') {
      this.setState({ visible: false });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnChangeTab({ i }) {
    if (i === 0) {
      global.tracker.trackScreenView('Chat');
    }
    if (i === 1) {
      global.tracker.trackScreenView('Board');
    }
    if (i === 2) {
      global.tracker.trackScreenView('List');
    }
    if (i === 3) {
      global.tracker.trackScreenView('About');
    }
  }
  render() {
    return (
      <Loader
        type="stream"
        streamId={this.props.streamId}
        color="rgba(43,32,45,1)"
      >
        <ScrollableTabView
          initialPage={
            this.props.isChat
              ? 0
              : this.props.initialPage
                ? this.props.initialPage
                : 1
          }
          tabBarPosition="top"
          renderTabBar={() => <StreamTabBar />}
          onChangeTab={this.handleOnChangeTab}
          prerenderingSiblingsNumber={Infinity}
          tabBarBackgroundColor="#ffffff"
          tabBarInactiveTextColor="#BDBDBD"
          tabBarActiveTextColor="#212121"
          contentProps={{
            keyboardShouldPersistTaps: 'always',
            style: { flex: this.state.visible ? 1 : 0 },
          }}
        >
          <StreamChat
            tabLabel="Chat"
            navigator={this.props.navigator}
            streamId={this.props.streamId}
            title={this.props.title}
            type={this.props.type}
          />
          <StreamBoard
            tabLabel="Board"
            navigator={this.props.navigator}
            streamId={this.props.streamId}
            title={this.props.title}
          />
          <StreamList
            tabLabel="List"
            navigator={this.props.navigator}
            streamId={this.props.streamId}
            title={this.props.title}
          />
          <StreamAbout
            tabLabel="About"
            navigator={this.props.navigator}
            streamId={this.props.streamId}
            title={this.props.title}
          />
        </ScrollableTabView>
      </Loader>
    );
  }
}

export default Stream;
