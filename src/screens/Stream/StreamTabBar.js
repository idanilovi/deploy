import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(43,32,45,1)',
  },
  tab: {
    height: 44,
    paddingHorizontal: 24,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  tabs: {
    backgroundColor: '#ffffff',
    height: 44,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(216,216,216,1)',
  },
});

export default class StreamTabBar extends Component {
  static displayName = 'StreamTabBar';
  static propTypes = {
    tabs: PropTypes.array,
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    scrollValue: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.icons = [];
    this.getTabs = this.getTabs.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
  }
  getTabs() {
    return this.props.tabs;
  }
  renderTabs() {
    return this.props.tabs.map((tab, i) => (
      <TouchableOpacity
        key={tab}
        onPress={() => this.props.goToPage(i)}
        style={styles.tab}
      >
        <Text
          style={{
            color: this.props.activeTab === i ? '#696285' : '#a9a5ba',
            fontWeight: '100',
            fontSize: 18,
          }}
        >
          {tab}
        </Text>
      </TouchableOpacity>
    ));
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.tabs}>{this.renderTabs()}</View>
      </View>
    );
  }
}
