import R from 'ramda';
import React, { PureComponent } from 'react';
import { WebView } from 'react-native';
import PropTypes from 'prop-types';

export default class WebViewPreview extends PureComponent {
  static displayName = 'WebViewPreview';
  static propTypes = {
    path: PropTypes.string,
  };
  render() {
    const { path } = this.props;
    return (
      <WebView
        source={{ uri: path }}
        style={{ flex: 1 }}
        {...R.dissoc('url', this.props)}
      />
    );
  }
}
