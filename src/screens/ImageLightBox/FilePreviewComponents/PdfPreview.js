import React, { PureComponent } from 'react';
import Pdf from 'react-native-pdf';
import PropTypes from 'prop-types';
import { View, StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  document: {
    flex: 1,
    width: Dimensions.get('window').width,
  },
});

export default class PdfPreview extends PureComponent {
  static displayName = 'PdfPreview';
  static propTypes = {
    path: PropTypes.string,
  };
  render() {
    const { path } = this.props;
    return (
      <View style={styles.container}>
        <Pdf source={{ uri: path }} style={styles.document} />
      </View>
    );
  }
}
