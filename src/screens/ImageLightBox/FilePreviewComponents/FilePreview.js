import R from 'ramda';
import React, { PureComponent } from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import OpenFile from 'react-native-doc-viewer';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  icon: {
    borderRadius: 15,
    backgroundColor: '#0d71d7',
    width: 164,
    height: 164,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filename: {
    color: '#ffffff',
    fontWeight: '200',
    fontSize: 36,
  },
});

function getFileExtensionFromFileName(filename) {
  if (filename) {
    return filename
      .split('.')
      .pop()
      .toUpperCase();
  }
  return '';
}

export default class FilePreview extends PureComponent {
  static displayName = 'FilePreview';
  static propTypes = {
    filename: PropTypes.string,
    path: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { filename, path } = this.props;
    if (Platform.OS === 'ios') {
      console.log('not-handled');
    } else {
      OpenFile.openDoc(
        [
          {
            url: path,
            fileName: filename,
            fileType: getFileExtensionFromFileName(filename),
            cache: true,
          },
        ],
        (error, url) => {
          if (error) {
            console.error(error);
          } else {
            console.log('handled', url);
          }
        },
      );
    }
  }
  render() {
    const { filename } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={this.handleOnPress}>
        <View style={styles.icon}>
          <Text style={styles.filename}>
            {getFileExtensionFromFileName(filename)}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
