import React, { PureComponent } from 'react';
import SvgUri from 'react-native-svg-uri';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
});

export default class SvgPreview extends PureComponent {
  static displayName = 'SvgPreview';
  static propTypes = {
    path: PropTypes.string,
  };
  render() {
    const { path } = this.props;
    return (
      <View style={styles.container}>
        <SvgUri
          width="200"
          height="200"
          source={{
            uri: path,
          }}
        />
      </View>
    );
  }
}
