import React, { PureComponent } from 'react';
import { Platform, Image } from 'react-native';
import PhotoView from 'react-native-photo-view';
import PropTypes from 'prop-types';

export default class ImagePreview extends PureComponent {
  static displayName = 'ImagePreview';
  static propTypes = {
    path: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnError = this.handleOnError.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnError() {
    return 'handled';
  }
  render() {
    const { path } = this.props;
    if (Platform.OS === 'ios') {
      return (
        <Image
          source={{ uri: path }}
          style={{ flex: 1 }}
          resizeMode="contain"
          {...this.props}
        />
      );
    }
    return (
      <PhotoView
        source={{ uri: path }}
        minimumZoomScale={1}
        maximumZoomScale={3}
        style={{ flex: 1, backgroundColor: 'rgba(40,30,42,1)' }}
        onError={this.handleOnError}
        {...this.props}
      />
    );
  }
}
