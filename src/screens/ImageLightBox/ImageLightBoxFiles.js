import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Platform, View } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {
  ImagePreview,
  WebViewPreview,
  SvgPreview,
  FilePreview,
  PdfPreview,
} from './FilePreviewComponents';
import withLoader from '../withLoader';

const WebViewPreviewWithLoader = withLoader(WebViewPreview);
const ImagePreviewWithLoader = withLoader(ImagePreview);

function getFileExtensionFromFileName(filename) {
  if (filename) {
    return filename
      .split('.')
      .pop()
      .toUpperCase();
  }
  return '';
}
export default class ImageLightBoxFiles extends PureComponent {
  static displayName = 'ImageLightBoxFiles';
  static propTypes = {
    files: PropTypes.array,
    currentFileIndex: PropTypes.number,
    onChangeTab: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.state = { currentFileIndex: props.currentFileIndex };
    this.renderFiles = this.renderFiles.bind(this);
    this.handleOnChangeTab = this.handleOnChangeTab.bind(this);
  }
  handleOnChangeTab({ i }) {
    if (this.props.onChangeTab) {
      this.props.onChangeTab({ i });
    } else {
      this.setState({ currentIndex: i });
    }
  }
  renderFiles() {
    return this.props.files.filter(_ => _).map(file => {
      const { type, path, id, filename } = file;
      const filePath = Platform.OS === 'ios' ? path : `file://${path}`;
      if (Platform.OS === 'ios') {
        if (
          type ===
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          type === 'application/msword' ||
          type === 'text/markdown' ||
          type ===
            'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          type === 'application/pdf'
        ) {
          return (
            <WebViewPreviewWithLoader tabLabel={id} key={id} path={filePath} />
          );
        }
        if (
          type === 'image/png' ||
          type === 'image/jpeg' ||
          type === 'image/gif'
        ) {
          return (
            <ImagePreviewWithLoader tabLabel={id} key={id} path={filePath} />
          );
        }
        if (type === 'multipart/form-data') {
          try {
            const ext = getFileExtensionFromFileName(filename);
            if (ext === 'CDR') {
              return (
                <FilePreview
                  tabLabel={id}
                  key={id}
                  filename={filename}
                  path={filePath}
                />
              );
            }
            return (
              <ImagePreviewWithLoader tabLabel={id} key={id} path={filePath} />
            );
          } catch (error) {
            return (
              <FilePreview
                tabLabel={id}
                key={id}
                filename={filename}
                path={filePath}
              />
            );
          }
        }
        if (type === 'image/svg+xml') {
          return <SvgPreview tabLabel={id} key={id} path={filePath} />;
        }
        if (type === 'application/pdf') {
          return <PdfPreview tabLabel={id} key={id} path={filePath} />;
        }
        return (
          <FilePreview
            key={id}
            tabLabel={id}
            filename={filename}
            path={filePath}
          />
        );
      }
      if (
        type === 'image/png' ||
        type === 'image/jpeg' ||
        type === 'image/gif'
      ) {
        return (
          <ImagePreviewWithLoader
            tabLabel={id}
            key={id}
            path={filePath}
            filename={filename}
          />
        );
      }
      if (
        type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
        type === 'application/msword' ||
        type === 'text/markdown' ||
        type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      ) {
        return (
          <FilePreview
            tabLabel={id}
            key={id}
            filename={filename}
            path={filePath}
          />
        );
      }
      if (type === 'multipart/form-data') {
        const fileExtension = getFileExtensionFromFileName(filename);
        if (['jpg', 'jpeg', 'png', 'gif'].indexOf(fileExtension) !== -1) {
          return (
            <ImagePreviewWithLoader
              tabLabel={id}
              key={id}
              path={filePath}
              filename={filename}
            />
          );
        }
        return (
          <FilePreview
            tabLabel={id}
            key={id}
            filename={filename}
            path={filePath}
          />
        );
      }
      if (type === 'image/svg+xml') {
        return (
          <WebViewPreviewWithLoader tabLabel={id} key={id} path={filePath} />
        );
      }
      if (type === 'application/pdf') {
        return <PdfPreview tabLabel={id} key={id} path={filePath} />;
      }
      return (
        <FilePreview
          tabLabel={id}
          key={id}
          filename={filename}
          path={filePath}
        />
      );
    });
  }
  render() {
    return (
      <ScrollableTabView
        initialPage={this.props.currentFileIndex}
        renderTabBar={() => <View />}
        onChangeTab={this.handleOnChangeTab}
        prerenderingSiblingsNumber={0}
      >
        {this.renderFiles()}
      </ScrollableTabView>
    );
  }
}
