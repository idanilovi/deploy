import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  CameraRoll,
  Dimensions,
  View,
  Platform,
  Text,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import Share from 'react-native-share';
import RNFetchBlob from 'react-native-fetch-blob';
import SvgIcon from '../../components/Icon';
import ImageLightBoxFiles from './ImageLightBoxFiles';
import withSafeArea from '../withSafeArea';

const { width, height } = Dimensions.get('window');

const currentFileIndexSelector = (state, ownProps) =>
  R.findIndex(R.propEq('id', R.prop('fileId', ownProps)))(
    R.prop('filesContext', ownProps),
  );

class ImageLightBox extends Component {
  static displayName = 'ImageLightBox';
  static propTypes = {
    url: PropTypes.string,
    filename: PropTypes.string,
    navigator: PropTypes.shape({
      dismissAllModals: PropTypes.func.isRequired,
      dismissModal: PropTypes.func.isRequired,
      push: PropTypes.func.isRequired,
    }),
    streamId: PropTypes.string,
    threadId: PropTypes.string,
    view: PropTypes.string,
    currentFileIndex: PropTypes.number,
    filesContext: PropTypes.array,
    createdAt: PropTypes.number,
    path: PropTypes.string,
    type: PropTypes.string,
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      show: false,
      currentIndex: props.currentFileIndex,
      progress: '',
      donebuttonclicked: false,
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);

    this.handleOnClose = this.handleOnClose.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
    this.handleOnShare = this.handleOnShare.bind(this);
    this.handleOnGoToThreadPress = this.handleOnGoToThreadPress.bind(this);
    this.handleOnChangeTab = this.handleOnChangeTab.bind(this);

    this.renderHeader = this.renderHeader.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderLoader = this.renderLoader.bind(this);
    this.renderModal = this.renderModal.bind(this);
  }
  showModal() {
    this.setState({ show: true });
  }
  hideModal() {
    this.setState({ show: false });
  }
  handleOnClose() {
    this.props.navigator.dismissModal({
      animationType: 'none',
    });
  }
  handleOnSave() {
    if (Platform.OS === 'ios') {
      CameraRoll.saveToCameraRoll(
        `file://${R.pathOr(
          this.props.path,
          [this.state.currentIndex, 'path'],
          this.props.filesContext,
        )}`,
      )
        .then(Alert.alert('Success', 'Photo added to camera roll!'))
        .catch(err => console.error(err));
    } else {
      const srcPath = R.pathOr(
        this.props.path,
        [this.state.currentIndex, 'path'],
        this.props.filesContext,
      );
      const destPath = `${RNFetchBlob.fs.dirs.DownloadDir}/${R.pathOr(
        this.props.filename,
        [this.state.currentIndex, 'filename'],
        this.props.filesContext,
      )}`;
      RNFetchBlob.fs
        .cp(srcPath, destPath)
        .then(Alert.alert('Success', 'File saved to Download directory!!!'))
        .catch(() => console.log());
    }
  }
  handleOnShare() {
    const { type, path, filesContext } = this.props;
    const { currentIndex } = this.state;
    const filePath = R.pathOr(path, [currentIndex, 'path'], filesContext);
    const fileType = R.pathOr(type, [currentIndex, 'type'], filesContext);
    RNFetchBlob.fs
      .readFile(filePath, 'base64')
      .then(fileData => {
        Share.open({
          message: '',
          type: fileType,
          url: `data:${fileType};base64,${fileData}`,
        });
      })
      .catch(error => console.error(error));
  }
  handleOnGoToThreadPress() {
    if (
      R.pathOr(
        this.props.threadId,
        [this.state.currentIndex, 'threadId'],
        this.props.filesContext,
      )
    ) {
      this.hideModal();
      this.props.navigator.push({
        screen: 'workonflow.Thread',
        passProps: {
          id: R.pathOr(
            this.props.threadId,
            [this.state.currentIndex, 'threadId'],
            this.props.filesContext,
          ),
          type: 'Thread',
        },
      });
    }
  }
  handleOnChangeTab({ i }) {
    this.setState({ currentIndex: i });
  }
  renderLoader() {
    if (this.state.loading) {
      return (
        <View
          style={[
            {
              position: 'absolute',
              top: 0,
              left: 0,
              backgroundColor: 'rgba(40,30,42,1)',
              justifyContent: 'center',
              alignItems: 'center',
            },
            { width, height },
          ]}
        >
          <ActivityIndicator animating={true} color="#ffffff" size="large" />
        </View>
      );
    }
    return null;
  }
  renderHeader() {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingLeft: 7,
          paddingRight: 8,
          paddingVertical: 8,
        }}
      >
        <View style={{ flex: 0 }}>
          <TouchableOpacity onPress={this.handleOnClose}>
            <SvgIcon
              name="ArrowLeft"
              width={27}
              height={27}
              viewBox="0 0 32 32"
              fill="#ffffff"
            />
          </TouchableOpacity>
        </View>
        <View
          style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}
        >
          <Text
            style={{
              fontWeight: '400',
              fontSize: 17,
              color: 'rgba(255,255,255, 1)',
            }}
          >
            {`${this.state.currentIndex + 1} / ${
              this.props.filesContext.filter(_ => _).length
            }`}
          </Text>
        </View>
        <View style={{ flex: 0 }}>
          <TouchableOpacity onPress={this.showModal}>
            <Icon name="more-vert" size={28} color="#ffffff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderFooter() {
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 20,
          paddingVertical: 20,
        }}
      >
        <View style={{ flex: 1 }}>
          <View style={{ paddingBottom: 2.5 }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 17,
                color: 'rgba(255,255,255, 1)',
              }}
              ellipsizeMode="tail"
              numberOfLines={1}
            >
              {R.pathOr(
                this.props.filename,
                [this.state.currentIndex, 'filename'],
                this.props.filesContext,
              )}
            </Text>
          </View>
          <View style={{ paddingTop: 2.5 }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 17,
                color: 'rgba(255,255,255, 0.5)',
              }}
            >
              {moment(
                R.pathOr(
                  this.props.createdAt,
                  [this.state.currentIndex, 'createdAt'],
                  this.props.filesContext,
                ),
              ).format('D.MM.YY h:mm')}
            </Text>
          </View>
        </View>
        <View style={{ flex: 0, justifyContent: 'flex-end' }}>
          <TouchableOpacity onPress={this.handleOnSave}>
            <SvgIcon
              name="Download"
              viewBox="0 0 141.732 141.732"
              width={22}
              height={22}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderModal() {
    return (
      <Modal
        isVisible={this.state.show}
        style={{
          paddingVertical: 20,
          paddingHorizontal: 20,
        }}
        useNativeDriver={true}
        onBackdropPress={this.hideModal}
        onBackButtonPress={this.hideModal}
      >
        <View
          style={{
            position: 'absolute',
            backgroundColor: '#ffffff',
            paddingVertical: 28,
            paddingHorizontal: 35,
            borderRadius: 13,
            flexDirection: 'column',
            width: Dimensions.get('window').width - 40,
          }}
        >
          <TouchableOpacity
            style={{ paddingVertical: 20 }}
            onPress={this.handleOnGoToThreadPress}
          >
            <Text
              style={{
                fontWeight: '400',
                fontSize: 20,
                color: this.props.threadId ? '#4e4963' : '#a9a5ba',
              }}
            >
              Go to thread
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ paddingVertical: 20 }}
            onPress={this.handleOnShare}
          >
            <Text
              style={{
                fontWeight: '400',
                fontSize: 20,
                color: '#4e4963',
              }}
            >
              Share
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(40,30,42,1)',
        }}
      >
        {this.renderHeader()}
        <ImageLightBoxFiles
          files={this.props.filesContext}
          currentFileIndex={this.props.currentFileIndex}
          onChangeTab={this.handleOnChangeTab}
        />
        {this.renderFooter()}
        {this.renderLoader()}
        {this.renderModal()}
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  currentFileIndex: currentFileIndexSelector(state, ownProps),
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

export default connect(mapStateToProps)(withSafeArea(ImageLightBox));
