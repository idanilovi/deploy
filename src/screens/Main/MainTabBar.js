import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import Icon from '../../components/Icon';
import global from '../../global';

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  tabs: {
    backgroundColor: 'rgb(88, 68, 82)',
    height: 62,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...ifIphoneX({
      height: 78,
      paddingBottom: 16,
    }),
  },
});

export default class MainTabBar extends PureComponent {
  static displayName = 'MainTabBar';
  static propTypes = {
    tabs: PropTypes.array,
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    scrollValue: PropTypes.object,
    unreadNotifications: PropTypes.bool,
  };
  constructor(props) {
    super(props);
    this.renderTabs = this.renderTabs.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.state = {
      activeTab: this.props.activeTab,
      unreadNotifications: this.props.unreadNotifications,
    };
  }
  shouldComponentUpdate(nextProps) {
    return (
      nextProps.activeTab !== this.props.activeTab ||
      nextProps.unreadNotifications !== this.props.unreadNotifications
    );
  }
  handleOnPress(index) {
    if (index === 0) {
      global.tracker.trackEvent('Нажатия на кнопочки', 'main streams');
    }
    if (index === 1) {
      global.tracker.trackEvent('Нажатия на кнопочки', 'main directs');
    }
    if (index === 2) {
      global.tracker.trackEvent('Нажатия на кнопочки', 'main notify');
    }
    if (index === 3) {
      global.tracker.trackEvent('Нажатия на кнопочки', 'main streams');
    }
    this.props.goToPage(index);
  }
  renderTabs() {
    return this.props.tabs.map((tab, index) => (
      <TouchableOpacity
        key={tab}
        onPress={() => this.handleOnPress(index)}
        style={styles.tab}
      >
        {this.renderIcon(index)}
      </TouchableOpacity>
    ));
  }
  renderIcon(index) {
    if (index === 0) {
      return (
        <Icon
          name="MainDirects"
          width={44}
          height={44}
          fill={
            this.props.activeTab === index
              ? 'rgb(255,255,255)'
              : 'rgb(129, 124, 131)'
          }
          viewBox="0 0 4721 4721"
        />
      );
    }
    if (index === 1) {
      return (
        <Icon
          name="MainStreams"
          width={44}
          height={44}
          fill={
            this.props.activeTab === index
              ? 'rgb(255,255,255)'
              : 'rgb(129, 124, 131)'
          }
          viewBox="0 0 4721 4721"
        />
      );
    }
    if (index === 2) {
      if (this.props.unreadNotifications) {
        return (
          <Icon
            name="MainNotificationsUnread"
            width={44}
            height={44}
            fill={
              this.props.activeTab === index
                ? 'rgb(255,255,255)'
                : 'rgb(129, 124, 131)'
            }
            viewBox="0 0 1472 1472"
          />
        );
      }
      return (
        <Icon
          name="MainNotifications"
          width={44}
          height={44}
          fill={
            this.props.activeTab === index
              ? 'rgb(255,255,255)'
              : 'rgb(129, 124, 131)'
          }
          viewBox="0 0 4721 4721"
        />
      );
    }
    if (index === 3) {
      return (
        <Icon
          name="MainThreads"
          width={44}
          height={44}
          fill={
            this.props.activeTab === index
              ? 'rgb(255,255,255)'
              : 'rgba(255,255,255, 0.3)'
          }
          viewBox="0 0 4721 4721"
        />
      );
    }
    if (index === 4) {
      return (
        <Icon
          name="Draft"
          width={30}
          height={30}
          stroke={
            this.props.activeTab === index
              ? 'rgb(255,255,255)'
              : 'rgba(255,255,255, 0.3)'
          }
          viewBox="0 0 24 24"
        />
      );
    }
    return null;
  }
  render() {
    return <View style={styles.tabs}>{this.renderTabs()}</View>;
  }
}
