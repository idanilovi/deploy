import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';
import {
  Animated,
  BackHandler,
  Dimensions,
  View,
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import SplashScreen from 'react-native-splash-screen';
import Drawer from 'react-native-drawer';
import SidebarDrawer from '../SidebarDrawer';
import MainStreams from '../MainStreams';
import MainNotifications from '../MainNotifications';
import MainThreads from '../MainThreads';
import MainDrafts from '../MainDrafts';
import MainDirects from '../MainDirects';
import MainHeader from '../../components/MainHeader';
import MainTabBar from './MainTabBar';
import { setWidth, setHeight } from '../../reducers/ui/background';
import {
  createStream,
  createUser,
  setContactCustomField,
  removeFromStream,
  setContactName,
  setContactEmail,
  setContactPhone,
  createThread,
} from '../../actions';
import { notificationsArraySelector } from '../../reducers/notifications';
import global from '../../global';
// import withSafeArea from '../withSafeArea';
// import backgroundImageSource from '../../assets/images/bg_workonflow.png';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  AView: {
    width,
    height,
    backgroundColor: 'rgb(88, 68, 82)',
    flex: 1,
  },
  imgBackground: {
    position: 'absolute',
    height,
    top: 0,
  },
});

const userSelector = state =>
  R.prop(R.path(['userData', 'userId'], state), R.prop('contacts', state));

const userDataSelector = state => R.prop('userData', state);

const backgroundParamsSelector = state =>
  R.pathOr({}, ['ui', 'background'], state);

const imgUrlSelector = createSelector(
  userSelector,
  userDataSelector,
  (user, userData) =>
    R.propOr(
      '',
      'url',
      R.propOr(
        {},
        R.pathOr('', ['background', 'fileId'], user),
        R.propOr({}, 'backgrounds', userData),
      ),
    ),
);

const userBackgroundSelector = createSelector(userSelector, user =>
  R.propOr({}, 'background', user),
);

const unreadNotificationsBooleanSelector = createSelector(
  notificationsArraySelector,
  notifications =>
    !R.isEmpty(R.filter(notification => !notification.readAt, notifications)),
);

class Main extends Component {
  static displayName = 'Main';
  static propTypes = {
    user: PropTypes.object,
    navigator: PropTypes.object,
    imgUrl: PropTypes.string,
    background: PropTypes.object,
    setWidth: PropTypes.func.isRequired,
    setHeight: PropTypes.func.isRequired,
    createStream: PropTypes.func,
    createUser: PropTypes.func,
    unreadNotifications: PropTypes.bool,
    imgWidth: PropTypes.number,
  };
  static navigatorStyle = {
    navBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      imgLeft: 0,
      tabIndex: 2,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.handleOnScroll = this.handleOnScroll.bind(this);
    this.setImagePosition = this.setImagePosition.bind(this);
    this.addBackHandler = this.addBackHandler.bind(this);
    this.removeBackHandler = this.removeBackHandler.bind(this);
    this.backButtonHandler = this.backButtonHandler.bind(this);
    this.renderBackgroundImage = this.renderBackgroundImage.bind(this);
    this.handleOnChangeTab = this.handleOnChangeTab.bind(this);
    this.drawer = React.createRef();
    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
    this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
  }
  componentDidMount() {
    clearTimeout(global.timoutId);
    global.tracker.trackScreenView('Main');
    if (this.props.imgUrl) {
      Image.getSize(
        this.props.imgUrl,
        (imgWidth, imgHeight) => {
          this.props.setWidth(imgWidth);
          this.props.setHeight(imgHeight);
        },
        error => {
          console.error(error.message);
        },
      );
    }
    if (Platform.OS === 'ios') {
      SplashScreen.hide();
    }
  }
  addBackHandler() {
    BackHandler.addEventListener('hardwareBackPress', this.backButtonHandler);
  }
  removeBackHandler() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.backButtonHandler,
    );
  }
  // eslint-disable-next-line class-methods-use-this
  backButtonHandler() {
    BackHandler.exitApp();
    return false;
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      this.setState({ visible: true });
      this.addBackHandler();
    }
    if (event.id === 'willDisappear') {
      this.setState({ visible: false });
      this.removeBackHandler();
    }
  }
  setImagePosition(scrollValue) {
    const {
      backgroundParams: { imgWidth },
    } = this.props;
    const imgLeft = imgWidth / 4 * scrollValue;
    if (imgWidth >= width * 4) {
      this.setState({ imgLeft });
    } else {
      this.setState({ imgLeft: imgLeft * imgWidth / (width * 4) });
    }
  }
  handleOnScroll(scrollValue) {
    if (scrollValue >= 0 && scrollValue <= 3) {
      this.setImagePosition(scrollValue);
    }
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnChangeTab({ i }) {
    this.setState({ tabIndex: i });
    // if (i === 0) global.tracker.trackScreenView('MainDirects');
    // if (i === 1) global.tracker.trackScreenView('MainStreams');
    // if (i === 2) global.tracker.trackScreenView('MainNotify');
    // if (i === 3) global.tracker.trackScreenView('MainThreads');
  }
  handleOpenDrawer() {
    this.drawer.current.open();
  }
  handleCloseDrawer() {
    this.drawer.current.close();
  }
  renderBackgroundImage() {
    const {
      backgroundParams: { imgWidth },
    } = this.props;
    // if (this.props.imgUrl) {
    return (
      <ImageBackground
        source={
          this.props.imgUrl || require('../../assets/images/bg_workonflow.png')
        }
        style={[styles.imgBackground, { width: imgWidth || width }]}
        blurRadius={this.props.background.blur || 0}
        resizeMode="cover"
        resizeMethod="scale"
      >
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(53,40,56,0.6)',
          }}
        />
      </ImageBackground>
    );
  }
  render() {
    return (
      <Drawer
        content={
          <SidebarDrawer
            scrollableTabViewRef={this.a}
            navigator={this.props.navigator}
            closeDrawer={this.handleCloseDrawer}
          />
        }
        type="overlay"
        styles={{ drawerStyles: { marginBottom: 20 } }}
        ref={this.drawer}
        tapToClose={true}
        panCloseMask={0.2}
      >
        <Animated.View style={styles.AView}>
          <MainHeader
            navigator={this.props.navigator}
            teamName={R.propOr('', 'teamName', this.props.userData)}
            userId={R.propOr(null, '_id', this.props.user)}
            userName={R.pathOr('', ['basicData', 'name'], this.props.user)}
            userPosition={R.pathOr('', ['hrData', 'position'], this.props.user)}
            userColor={R.propOr('#000000', 'color', this.props.user)}
            createStreamAction={this.props.createStream}
            createUserAction={this.props.createUser}
            isAdmin={R.propOr(false, 'isAdmin', this.props.user)}
            tabIndex={this.state.tabIndex}
            openDrawer={this.handleOpenDrawer}
            scrollable={this.a}
          />
          <ScrollableTabView
            ref={component => (this.a = component)}
            initialPage={2}
            tabBarPosition="overlayBottom"
            renderTabBar={() =>
              this.state.tabIndex !== 4 ? (
                <MainTabBar
                  unreadNotifications={this.props.unreadNotifications}
                />
              ) : (
                <View />
              )
            }
            onScroll={this.handleOnScroll}
            onChangeTab={this.handleOnChangeTab}
            prerenderingSiblingsNumber={Infinity}
          >
            <MainDirects
              key="MainDirects"
              tabLabel="MainDirects"
              navigator={this.props.navigator}
            />
            <MainStreams
              key="MainStreams"
              tabLabel="MainStreams"
              navigator={this.props.navigator}
            />
            <MainNotifications
              key="MainNotifications"
              tabLabel="MainNotifications"
              navigator={this.props.navigator}
            />
            <MainThreads
              key="MainThreads"
              tabLabel="MainThreads"
              navigator={this.props.navigator}
            />
            <MainDrafts
              key="MainDrafts"
              tabLabel="MainDrafts"
              user={this.props.user}
              userId={R.prop('_id', this.props.user)}
              setContactCustomField={this.props.setContactCustomField}
              removeFromStream={this.props.removeFromStream}
              setContactName={this.props.setContactName}
              setContactEmail={this.props.setContactEmail}
              setContactPhone={this.props.setContactPhone}
              createThread={this.props.createThread}
              navigator={this.props.navigator}
            />
          </ScrollableTabView>
        </Animated.View>
      </Drawer>
    );
  }
}

const mapStateToProps = state => ({
  imgUrl: imgUrlSelector(state),
  background: userBackgroundSelector(state),
  backgroundImageUrl: state.userData.backgroundImage,
  user: userSelector(state),
  userData: userDataSelector(state),
  streams: state.streams,
  contacts: state.contacts,
  backgroundParams: backgroundParamsSelector(state),
  unreadNotifications: unreadNotificationsBooleanSelector(state),
  // safeAreaBackgroundColor: 'rgb(88, 68, 82)',
  // safeAreaBackgroundImageSource: backgroundImageSource,
  // safeAreaBackgroundImage: true,
});

const mapDispatchToProps = {
  setHeight,
  setWidth,
  createStream,
  createUser,
  createThread,
  setContactCustomField,
  removeFromStream,
  setContactName,
  setContactEmail,
  setContactPhone,
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);

// export class BgImage extends PureComponent {
//   render() {
//     return (
//       <ImageBackground
//         source={require('../../assets/images/bg_workonflow.png')}
//         style={[styles.imgBackground, { width }]}
//         blurRadius={0}
//         resizeMode="cover"
//         resizeMethod="scale"
//       />
//     );
//   }
// }
