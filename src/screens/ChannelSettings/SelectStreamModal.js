import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  Switch,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import { streamsArraySelector } from '../../reducers/streams';

class SelectStreamModal extends Component {
  static displayName = 'SelectStreamModal';
  static propTypes = {
    streams: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
      }),
    ),
  };
  static defaultProps = {
    visibleInStreams: [],
    isVisibleToAll: false,
  };
  constructor() {
    super();
    this.renderItem = this.renderItem.bind(this);
  }
  renderItem({ item: { id, name } = {} }) {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onChange(id);
          if (!this.props.showCheckboxes) {
            this.props.onHide();
          }
        }}
      >
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            height: 70,
            borderBottomColor: '#a9a5ba',
            borderBottomWidth: StyleSheet.hairlineWidth,
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingHorizontal: 24,
          }}
        >
          {this.props.showCheckboxes ? (
            <CheckBox
              isChecked={this.props.visibleInStreams.indexOf(id) !== -1}
              onClick={() => this.props.onChange(id)}
            />
          ) : null}
          <Text
            style={{
              flex: 0,
              color: '#4e4963',
              fontWeight: '400',
              fontSize: 18,
              paddingLeft: 17,
            }}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {`~${name}`}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <Modal
        isVisible={this.props.visible}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        onBackdropPress={this.props.onHide}
      >
        <FlatList
          style={{ backgroundColor: '#ffffff', borderRadius: 13 }}
          ListHeaderComponent={
            this.props.showHeader ? (
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  height: 70,
                  borderBottomColor: '#a9a5ba',
                  borderBottomWidth: StyleSheet.hairlineWidth,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 24,
                }}
              >
                <Text
                  style={{
                    color: '#4e4963',
                    fontWeight: '400',
                    fontSize: 18,
                  }}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  Default for all
                </Text>
                <Switch
                  value={this.props.isVisibleToAll}
                  onValueChange={this.props.onValueChange}
                />
              </View>
            ) : null
          }
          data={this.props.streams}
          keyExtractor={(item, index) => item.id || item._id || index}
          renderItem={this.renderItem}
        />
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  streams: streamsArraySelector(state),
});

export default connect(mapStateToProps)(SelectStreamModal);
