import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import FloatingLabelInput from './FloatingLabelInput';
import { channelByIdAndTypeSelector } from '../../../../selectors';
import { streamsSelector } from '../../../../reducers/streams';
import SelectStreamModal from '../../SelectStreamModal';
import {
  createTelegramChannel,
  updateTelegramChannel,
  deleteTelegramChannel,
} from '../../../../actions';
import Header from '../../Header';
import Content from '../../Content';
import DeleteButton from '../../DeleteButton';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(50, 41, 58)',
  },
});

class MessangerForm extends Component {
  static displayName = 'MessangerForm';
  static propTypes = {
    channel: PropTypes.object,
  };
  constructor(props) {
    super(props);
    const { channel: { name = '', token = '', streamId = '' } = {} } = props;
    this.state = {
      visible: false,
      name,
      token,
      streamId,
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleOnNameChange = name => this.handleOnChange('name', name);
    this.handleOnTokenChange = token => this.handleOnChange('token', token);
    this.handleOnSave = this.handleOnSave.bind(this);
    this.handleOnStreamChange = id => this.handleOnChange('streamId', id);
  }
  showModal() {
    this.setState({ visible: true });
  }
  hideModal() {
    this.setState({ visible: false });
  }
  handleOnChange(key, value) {
    this.setState({ [key]: value });
  }
  handleOnSave() {
    const { name, token, streamId } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    const id = this.props.id || this.props._id;
    if (id) {
      this.props.updateAccount(
        Object.assign({ id }, { name, token, streamId }),
      );
    } else {
      this.props.createAccount({ name, token, streamId });
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Header>
          <Header.Button title="Cancel" onPress={this.props.onCancel} />
          <Header.Title title="Settings" />
          <Header.Button title="Connect" onPress={this.handleOnSave} />
        </Header>
        <Content>
          <View>
            <FloatingLabelInput
              label="Messanger channel name"
              onChangeText={this.handleOnNameChange}
              value={this.state.name}
            />
            <FloatingLabelInput
              label="Bot token"
              onChangeText={this.handleOnTokenChange}
              value={this.state.token}
            />
            <TouchableOpacity
              onPress={this.showModal}
              style={{
                paddingHorizontal: 30,
                paddingVertical: 18,
                height: 70,
                borderBottomColor: '#a9a5ba',
                borderBottomWidth: StyleSheet.hairlineWidth,
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#a9a5ba' }}
              >
                Streams with outgoing call privileges
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  color: '#4e4963',
                }}
              >
                {`~ ${R.pathOr(
                  '',
                  ['streams', this.state.streamId, 'name'],
                  this.props,
                )}`}
              </Text>
            </TouchableOpacity>
            <SelectStreamModal
              visible={this.state.visible}
              showCheckboxes={false}
              showHeader={false}
              onChange={this.handleOnStreamChange}
              // isVisibleToAll={this.props.channel.isVisibleToAll}
              // visibleInStreams={this.props.channel.visibleInStreams}
              onShow={this.showModal}
              onHide={this.hideModal}
            />
          </View>
          {this.props.type !== 'admins' && (
            <DeleteButton
              title="Delete channel"
              onPress={this.props.deleteTelegram}
            />
          )}
        </Content>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  channel: channelByIdAndTypeSelector(state, ownProps),
  streams: streamsSelector(state),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  createAccount: options => dispatch(createTelegramChannel(options)),
  updateAccount: options => dispatch(updateTelegramChannel(options)),
  deleteTelegram: () => {
    dispatch(deleteTelegramChannel({ id: ownProps.id }));
    Navigation.dismissModal({ animationType: 'slide-down' });
  },
  onCancel: () => {
    Navigation.dismissModal({ animationType: 'slide-down' });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MessangerForm);
