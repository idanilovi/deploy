import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import FloatingLabelInput from './FloatingLabelInput';
import { channelByIdAndTypeSelector } from '../../../../selectors';
import SelectStreamModal from '../../SelectStreamModal';
import {
  createMailChannel,
  updateMailAccount,
  deleteMailChannel,
} from '../../../../actions';
import Header from '../../Header';
import Content from '../../Content';
import DeleteButton from '../../DeleteButton';
import { streamsSelector } from '../../../../reducers/streams';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(50, 41, 58)',
  },
});

class MailForm extends Component {
  static displayName = 'MailForm';
  static propTypes = {
    channel: PropTypes.object,
  };
  constructor(props) {
    super(props);
    const {
      channel: {
        name = '',
        email = '',
        password = '',
        visibleInStreams = [],
        isVisibleToAll = false,
      } = {},
    } = props;
    this.state = {
      visible: false,
      name,
      email,
      password,
      visibleInStreams,
      isVisibleToAll,
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleOnNameChange = name => this.handleOnChange('name', name);
    this.handleOnPasswordChange = password =>
      this.handleOnChange('password', password);
    this.handleOnEmailChange = email => this.handleOnChange('email', email);
    this.handleOnCheckItem = this.handleOnCheckItem.bind(this);
    this.handleOnValueChange = this.handleOnValueChange.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
  }
  showModal() {
    this.setState({ visible: true });
  }
  hideModal() {
    this.setState({ visible: false });
  }
  handleOnChange(key, value) {
    this.setState({ [key]: value });
  }
  handleOnCheckItem(id) {
    const { visibleInStreams } = this.state;
    const b = [];
    const a = new Set(visibleInStreams);
    if (a.has(id)) {
      a.delete(id);
    } else {
      a.add(id);
    }
    a.forEach(_ => b.push(_));
    this.setState({ visibleInStreams: b });
  }
  handleOnValueChange() {
    this.setState({ isVisibleToAll: !this.state.isVisibleToAll });
  }
  handleOnSave() {
    const {
      name,
      password,
      email,
      visibleInStreams,
      isVisibleToAll,
    } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    const id = this.props.id || this.props._id;
    if (id) {
      this.props.updateAccount(
        Object.assign(
          { id },
          {
            name,
            password,
            email,
            visibleInStreams,
            isVisibleToAll,
          },
        ),
      );
    } else {
      this.props.createAccount({
        name,
        password,
        email,
        visibleInStreams,
        isVisibleToAll,
      });
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Header>
          <Header.Button title="Cancel" onPress={this.props.onCancel} />
          <Header.Title title="Settings" />
          <Header.Button title="Connect" onPress={this.handleOnSave} />
        </Header>
        <Content>
          <View>
            <FloatingLabelInput
              label="Email channel name"
              onChangeText={this.handleOnNameChange}
              value={this.state.name}
            />
            <FloatingLabelInput
              label="Email"
              onChangeText={this.handleOnEmailChange}
              value={this.state.email}
            />
            <FloatingLabelInput
              label="Password"
              onChangeText={this.handleOnPasswordChange}
              value={this.state.password}
            />
            <TouchableOpacity
              onPress={this.showModal}
              style={{
                paddingHorizontal: 30,
                paddingVertical: 18,
                minHeight: 70,
                borderBottomColor: '#a9a5ba',
                borderBottomWidth: StyleSheet.hairlineWidth,
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#a9a5ba' }}
              >
                Streams with outgoing call privileges
              </Text>
              {R.propOr([], 'visibleInStreams', this.state).map(id => {
                return (
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: '400',
                      color: '#4e4963',
                    }}
                  >
                    {`~ ${R.pathOr('', ['streams', id, 'name'], this.props)}`}
                  </Text>
                );
              })}
            </TouchableOpacity>
            <SelectStreamModal
              visible={this.state.visible}
              showCheckboxes={true}
              showHeader={true}
              isVisibleToAll={this.state.isVisibleToAll}
              visibleInStreams={this.state.visibleInStreams}
              onChange={this.handleOnCheckItem}
              onValueChange={this.handleOnValueChange}
              onShow={this.showModal}
              onHide={this.hideModal}
            />
          </View>
          {this.props.type !== 'admins' && (
            <DeleteButton
              title="Delete channel"
              onPress={this.props.deleteMail}
            />
          )}
        </Content>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  channel: channelByIdAndTypeSelector(state, ownProps),
  streams: streamsSelector(state),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  createAccount: options => dispatch(createMailChannel(options)),
  updateAccount: options => dispatch(updateMailAccount(options)),
  deleteMail: () => {
    dispatch(deleteMailChannel({ id: ownProps.id }));
    Navigation.dismissModal({ animationType: 'slide-down' });
  },
  onCancel: () => {
    Navigation.dismissModal({ animationType: 'slide-down' });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MailForm);
