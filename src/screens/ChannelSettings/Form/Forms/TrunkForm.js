import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import FloatingLabelInput from './FloatingLabelInput';
import { channelByIdAndTypeSelector } from '../../../../selectors';
import { streamsSelector } from '../../../../reducers/streams';
import SelectStreamModal from '../../SelectStreamModal';
import { createTrunk, updateTrunk, deleteTrunk } from '../../../../actions';
import Header from '../../Header';
import Content from '../../Content';
import DeleteButton from '../../DeleteButton';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(50, 41, 58)',
  },
});

class TrunkForm extends Component {
  static displayName = 'TrunkForm';
  static propTypes = {
    channel: PropTypes.shape({
      name: PropTypes.string,
      password: PropTypes.string,
      sip: PropTypes.shape({
        params: PropTypes.shape({
          realm: PropTypes.string,
          username: PropTypes.string,
        }),
      }),
      phoneNumber: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      isVisibleToAll: PropTypes.bool,
    }),
  };
  constructor(props) {
    super(props);
    const {
      channel: {
        name = '',
        password = '',
        sip: { params: { realm = '', username = '' } = {} } = {},
        phoneNumber = '',
        redirect = '',
      } = {},
    } = props;
    this.state = {
      visible: false,
      name,
      password,
      realm,
      username,
      phoneNumber,
      redirect,
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleOnNameChange = name => this.handleOnChange('name', name);
    this.handleOnPasswordChange = password =>
      this.handleOnChange('password', password);
    this.handleOnRealmChange = realm => this.handleOnChange('realm', realm);
    this.handleOnUserNameChange = username =>
      this.handleOnChange('username', username);
    this.handleOnPhoneNumberChange = phoneNumber =>
      this.handleOnChange('phoneNumber', phoneNumber);
    this.handleOnRedirectChange = id => this.handleOnChange('redirect', id);
    this.handleOnSave = this.handleOnSave.bind(this);
  }
  showModal() {
    this.setState({ visible: true });
  }
  hideModal() {
    this.setState({ visible: false });
  }
  handleOnChange(key, value) {
    this.setState({ [key]: value });
  }
  handleOnSave() {
    const {
      name,
      password,
      realm,
      username,
      phoneNumber,
      redirect,
    } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    const id = this.props.id || this.props._id;
    if (id) {
      this.props.updateAccount(
        Object.assign(
          { id },
          {
            updateTrunk: {
              name,
              password,
              realm,
              username,
              phoneNumber,
              redirect,
            },
          },
        ),
      );
    } else {
      this.props.createAccount({
        name,
        password,
        realm,
        username,
        phoneNumber,
        redirect,
      });
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Header>
          <Header.Button title="Cancel" onPress={this.props.onCancel} />
          <Header.Title title="Settings" />
          <Header.Button title="Connect" onPress={this.handleOnSave} />
        </Header>
        <Content>
          <View>
            <FloatingLabelInput
              label="Phone channel name"
              onChangeText={this.handleOnNameChange}
              value={this.state.name}
            />
            <FloatingLabelInput
              label="Password"
              onChangeText={this.handleOnPasswordChange}
              value={this.state.password}
            />
            <FloatingLabelInput
              label="Realm"
              onChangeText={this.handleOnRealmChange}
              value={this.state.realm}
            />
            <FloatingLabelInput
              label="Username"
              onChangeText={this.handleOnUserNameChange}
              value={this.state.username}
            />
            <FloatingLabelInput
              label="Phone number"
              onChangeText={this.handleOnPhoneNumberChange}
              value={this.state.phoneNumber}
            />
            <TouchableOpacity
              onPress={this.showModal}
              style={{
                paddingHorizontal: 30,
                paddingVertical: 18,
                height: 70,
                borderBottomColor: '#a9a5ba',
                borderBottomWidth: StyleSheet.hairlineWidth,
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#a9a5ba' }}
              >
                Streams with outgoing call privileges
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  color: '#4e4963',
                }}
              >
                {`~ ${R.pathOr(
                  '',
                  ['streams', this.state.redirect, 'name'],
                  this.props,
                )}`}
              </Text>
            </TouchableOpacity>
            <SelectStreamModal
              visible={this.state.visible}
              showCheckboxes={false}
              showHeader={false}
              onChange={this.handleOnRedirectChange}
              onShow={this.showModal}
              onHide={this.hideModal}
            />
          </View>
          {this.props.type !== 'admins' && (
            <DeleteButton
              title="Delete channel"
              onPress={this.props.deleteTrunk}
            />
          )}
        </Content>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  channel: channelByIdAndTypeSelector(state, ownProps),
  streams: streamsSelector(state),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  createAccount: options => dispatch(createTrunk(options)),
  updateAccount: options => dispatch(updateTrunk(options)),
  deleteTrunk: () => {
    dispatch(deleteTrunk({ trunkId: ownProps.id }));
    Navigation.dismissModal({ animationType: 'slide-down' });
  },
  onCancel: () => {
    Navigation.dismissModal({ animationType: 'slide-down' });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TrunkForm);
