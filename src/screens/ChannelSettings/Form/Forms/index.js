import MailForm from './MailForm';
import TrunkForm from './TrunkForm';
import MessangerForm from './MessangerForm';
import AdminsForm from './AdminsForm';

export { MailForm, TrunkForm, MessangerForm, AdminsForm };
