import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import FloatingLabelInput from './FloatingLabelInput';
import { contactsSelector } from '../../../../selectors';
import SelectStreamModal from '../../SelectStreamModal';
import ContactIcon from '../../../../components/ContactIcon';
import Header from '../../Header';
import Content from '../../Content';
import DeleteButton from '../../DeleteButton';

import {
  createMailChannel,
  updateMailAccount,
  setChannelAdmin,
} from '../../../../actions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(50, 41, 58)',
  },
});

class AdminsForm extends Component {
  static displayName = 'AdminsForm';
  static propTypes = {
    contacts: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      contacts: [],
      name: '',
      currentContact: {},
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleOnNameChange = name => this.handleOnChange('name', name);
  }
  showModal() {
    this.setState({ visible: true });
  }
  hideModal() {
    this.setState({ visible: false });
  }
  handleOnChange(key, value) {
    this.setState({ [key]: value });
    if (!value) {
      this.setState({ contacts: [], value });
      return;
    }
    const lowerCaseName = value.toLowerCase();
    const filtredContacts = R.filter(
      contact =>
        R.pathOr('', ['basicData', 'name'], contact)
          .toLowerCase()
          .indexOf(lowerCaseName) + 1,
      R.filter(_ => _.billingType === 'users', R.values(this.props.contacts)),
    );
    this.setState({ contacts: filtredContacts, value });
  }
  handleOnSave() {
    const { name, contacts, currentContact } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    if (this.props.id || this.props._id) {
      this.props.updateAccount({
        name,
        password,
        email,
        visibleInStreams,
        isVisibleToAll,
      });
    } else {
      this.props.createAccount({
        name,
        password,
        email,
        visibleInStreams,
        isVisibleToAll,
      });
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Header>
          <Header.Button title="Cancel" onPress={this.props.onCancel} />
          <Header.Title title="Settings" />
          <Header.Button title="Connect" onPress={this.handleOnSave} />
        </Header>
        <Content>
          <FloatingLabelInput
            label="Enter name"
            onChangeText={this.handleOnNameChange}
            value={this.state.name}
          />
          <ScrollView>
            {R.map(
              contact => (
                <TouchableOpacity
                  key={contact._id}
                  style={{ height: 50, flexDirection: 'row' }}
                  onPress={() => this.props.setAdmin({ userId: contact._id })}
                >
                  <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                    <ContactIcon
                      show={true}
                      id={R.propOr(null, '_id', contact)}
                    />
                  </View>
                  <View style={{ paddingHorizontal: 10, paddingVertical: 15 }}>
                    <Text>{R.pathOr('', ['basicData', 'name'], contact)}</Text>
                  </View>
                </TouchableOpacity>
              ),
              this.state.contacts,
            )}
          </ScrollView>
        </Content>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  contacts: contactsSelector(state, ownProps),
});

const mapDispatchToProps = dispatch => ({
  createAccount: options => dispatch(createMailChannel(options)),
  updateAccount: options => dispatch(updateMailAccount(options)),
  setAdmin: options => dispatch(setChannelAdmin(options)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminsForm);
