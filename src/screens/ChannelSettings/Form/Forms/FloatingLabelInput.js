import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TextInput, StyleSheet } from 'react-native';

class FloatingLabelInput extends Component {
  static displayName = 'FloatingLabelInput';
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
  };
  constructor() {
    super();
    this.state = {
      isFocused: false,
    };
    this.renderLabel = this.renderLabel.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }
  handleFocus() {
    this.setState({ isFocused: true });
  }
  handleBlur() {
    this.setState({ isFocused: false });
  }
  renderLabel() {
    return (
      <Text
        style={{
          position: 'absolute',
          backgroundColor: 'transparent',
          left: 34,
          top: !this.state.isFocused && !this.props.value ? 18 : 10,
          fontSize: !this.state.isFocused && !this.props.value ? 20 : 14,
          fontWeight: '400',
          color: '#a9a5ba',
        }}
      >
        {this.props.label}
      </Text>
    );
  }
  render() {
    return (
      <View
        style={{
          paddingHorizontal: 30,
          paddingVertical: 18,
          height: 70,
          borderBottomColor: '#a9a5ba',
          borderBottomWidth: StyleSheet.hairlineWidth,
        }}
      >
        {this.renderLabel()}
        <TextInput
          {...this.props}
          style={{
            height: 48,
            fontSize: 20,
            fontWeight: '400',
            color: '#4e4963',
          }}
          underlineColorAndroid="transparent"
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
        />
      </View>
    );
  }
}

export default FloatingLabelInput;
