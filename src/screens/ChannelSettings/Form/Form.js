import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MailForm, TrunkForm, MessangerForm, AdminsForm } from './Forms';
import global from '../../../global';

// TODO перенести все из внутреннего стейта форм в стор редакса
class Form extends Component {
  static displayName = 'Form';
  static propTypes = {
    id: PropTypes.string,
    type: PropTypes.string,
  };
  constructor() {
    super();
    this.form = React.createRef();
  }
  componentDidMount() {
    global.form = this.form;
  }
  render() {
    const { id, type } = this.props;
    if (type === 'mails') {
      return <MailForm id={id} type={type} ref={this.form} />;
    }
    if (type === 'trunks') {
      return <TrunkForm id={id} type={type} ref={this.form} />;
    }
    if (type === 'messangers') {
      return <MessangerForm id={id} type={type} ref={this.form} />;
    }
    if (type === 'admins') {
      return <AdminsForm id={id} type={type} ref={this.form} />;
    }
    return null;
  }
}

export default Form;
