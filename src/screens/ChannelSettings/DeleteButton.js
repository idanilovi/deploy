import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';

const styles = StyleSheet.create({
  button: {
    flex: 0,
    height: 90,
    paddingHorizontal: 30,
    paddingVertical: 35,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#a9a5ba',
  },
  title: { fontSize: 20, color: '#0d71d7', fontWeight: '400', paddingLeft: 8 },
});

class DeleteButton extends PureComponent {
  static displayName = 'DeleteButton';
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func.isRequired,
  };
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={styles.button}>
          <Icon name="ios-trash" size={20} color="#0d71d7" />
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default DeleteButton;
