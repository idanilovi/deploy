import PropTypes from 'prop-types';
import React, { Component } from 'react';
// import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';

// import { Navigation } from 'react-native-navigation';
// import Header from './Header';
// import Content from './Content';
import Form from './Form';
// import DeleteButton from './DeleteButton';
// import { channelByIdAndTypeSelector } from '../../selectors';
// import global from '../../global';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...ifIphoneX({
      paddingTop: 34,
      backgroundColor: 'rgb(50, 41, 58)',
    }),
  },
});

class ChannelSettings extends Component {
  static displayName = 'ChannelSettings';
  static propTypes = {
    id: PropTypes.string,
    type: PropTypes.string,
    navigator: PropTypes.object,
    onCancel: PropTypes.func,
  };
  static navigatorStyle = {
    navBarHidden: true,
  };
  render() {
    return (
      // <View style={styles.container}>
      //   <Header>
      //     <Header.Button title="Cancel" onPress={this.props.onCancel} />
      //     <Header.Title title="Settings" />
      //     <Header.Button title="Connect" onPress={console.log} />
      //   </Header>
      //   <Content>
      <View style={styles.container}>
        <Form id={this.props._id || this.props.id} type={this.props.type} />
      </View>
      //     {this.props.type !== 'admins' && (
      //       <DeleteButton title="Delete channel" onPress={console.log} />
      //     )}
      //   </Content>
      // </View>
    );
  }
}

// const mapStateToProps = (state, ownProps) => ({
//   channel: channelByIdAndTypeSelector(state, ownProps),
// });
// const mapDispatchToProps = (dispatch, ownProps) => ({
//   onCancel: () => {
//     Navigation.dismissModal({ animationType: 'slide-down' });
//   },
// });

// export default connect(mapStateToProps, mapDispatchToProps)(ChannelSettings);
export default ChannelSettings;
//
