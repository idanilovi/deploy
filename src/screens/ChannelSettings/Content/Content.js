import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'column',
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    backgroundColor: '#fff',
  },
});

class Content extends Component {
  static displayName = 'Content';
  static propTypes = {
    children: PropTypes.node,
  };
  render() {
    return (
      <ScrollView style={styles.content}>{this.props.children}</ScrollView>
    );
  }
}

export default Content;
