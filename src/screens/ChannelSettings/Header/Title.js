import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  title: {
    fontSize: 15,
    color: '#ffffff',
    fontWeight: '400',
  },
});

const Title = ({ title }) => <Text style={styles.title}>{title}</Text>;

Title.displayName = 'Header.Title';
Title.propTypes = {
  title: PropTypes.string,
};

export default Title;
