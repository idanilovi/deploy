import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  button: {},
  title: { fontSize: 15, color: '#0d71d7', fontWeight: '400' },
});

const Button = ({ title, onPress }) => (
  <TouchableOpacity style={styles.button} onPress={onPress}>
    <Text style={styles.title}>{title}</Text>
  </TouchableOpacity>
);

Button.displayName = 'Header.Button';
Button.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func.isRequired,
};

export default Button;
