import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import Button from './Button';
import Title from './Title';

const styles = StyleSheet.create({
  header: {
    flex: 0,
    height: 60,
    paddingHorizontal: 15,
    paddingVertical: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

class Header extends Component {
  static displayName = 'Header';
  static propTypes = {
    children: PropTypes.node,
  };
  static Button = Button;
  static Title = Title;
  render() {
    return <View style={styles.header}>{this.props.children}</View>;
  }
}

export default Header;
