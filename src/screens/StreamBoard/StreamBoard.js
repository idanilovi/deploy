import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import Kanban from '../../components/Kanban';
import { getThreads } from '../../actions';
import withSafeArea from '../withSafeArea';

const threadsSelector = state => R.propOr({}, 'threads', state);
const streamIdSelector = (state, ownProps) =>
  R.propOr(null, 'streamId', ownProps);
const streamTitleSelector = (state, ownProps) =>
  R.propOr(null, 'title', ownProps);

const statusesSelector = createSelector(
  streamIdSelector,
  state => state.statuses,
  (streamId, statuses) =>
    R.filter(status => status.streamId === streamId, R.values(statuses)),
);
const threadsIdsByStreamIdSelector = createSelector(
  streamIdSelector,
  threadsSelector,
  (streamId, threads) =>
    R.map(
      thread => R.prop('id', thread),
      R.filter(
        thread => R.prop('streamId', thread) === streamId,
        R.values(threads),
      ),
    ),
);

class StreamBoard extends Component {
  static displayName = 'StreamBoard';
  static propTypes = {
    threadsIds: PropTypes.array,
    streamId: PropTypes.string,
    title: PropTypes.string,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      toggleTabs: PropTypes.func.isRequired,
      setTitle: PropTypes.func.isRequired,
      setStyle: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
      resetTo: PropTypes.func.isRequired,
    }),
    getThreads: PropTypes.func.isRequired,
  };
  static navigatorStyle = { tabBarHidden: true };
  componentWillMount() {
    if (this.props.title) {
      this.props.navigator.setTitle({
        title: this.props.title,
      });
    }
  }
  componentDidMount() {
    if (this.props.streamId) {
      this.props.getThreads({ streamId: this.props.streamId });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.streamId !== nextProps.streamId) {
      if (nextProps.streamId) {
        nextProps.getThreads({ streamId: nextProps.streamId });
      }
      if (this.props.title !== nextProps.title) {
        nextProps.navigator.setTitle({
          title: nextProps.title,
        });
      }
    }
  }
  render() {
    const { threadsIds, streamId } = this.props;
    return (
      <Kanban
        navigator={this.props.navigator}
        threads={threadsIds}
        streamId={streamId}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  threadsIds: threadsIdsByStreamIdSelector(state, ownProps),
  streamId: streamIdSelector(state, ownProps),
  title: streamTitleSelector(state, ownProps),
  statuses: statusesSelector(state, ownProps),
  safeAreaBackgroundColor: '#f3eff2',
});

const mapDispatchToProps = {
  getThreads,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(StreamBoard),
);
