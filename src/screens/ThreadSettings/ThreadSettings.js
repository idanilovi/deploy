import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import {
  Dimensions,
  Switch,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import { setStreamWidgets } from '../../actions';
import { setWidgetSettings } from '../../reducers/streams';
import withSafeArea from '../withSafeArea';

const streamWidgetSettingsSelector = createSelector(
  (state, ownProps) => R.propOr(null, 'streamId', ownProps),
  state => R.propOr({}, 'streams', state),
  (streamId, streams) =>
    R.pathOr({}, ['settings', 'widgets'], R.propOr({}, streamId, streams)),
);

class ThreadSettings extends Component {
  static displayName = 'ThreadSettings';
  static propTypes = {
    streamId: PropTypes.string,
    settings: PropTypes.object,
    onChange: PropTypes.func,
    onSave: PropTypes.func,
    navigator: PropTypes.shape({
      dismissModal: PropTypes.func.isRequired,
    }),
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { show: false, prevSettings: props.settings };

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.toggle = this.toggle.bind(this);

    // this.renderButtons = this.renderButtons.bind(this);
    this.renderSettingsItems = this.renderSettingsItems.bind(this);
    this.renderSettingsItem = this.renderSettingsItem.bind(this);
    this.renderModalContent = this.renderModalContent.bind(this);

    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnCancel = this.handleOnCancel.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);

    this.getNameForSettingsType = this.getNameForSettingsType.bind(this);
  }
  show() {
    this.setState({ show: true });
  }
  hide() {
    this.props.navigator.dismissModal();
  }
  toggle() {
    this.setState({ show: !this.state.show });
  }
  handleOnChange(type) {
    if (this.props.onChange) {
      const widgets = R.assoc(
        type,
        { type, on: !R.pathOr(false, [type, 'on'], this.props.settings) },
        this.props.settings,
      );
      this.props.onChange(this.props.streamId, widgets);
    }
  }
  handleOnSave() {
    if (this.props.onSave) {
      this.props.onSave(this.props.streamId, this.props.settings);
    }
    this.hide();
  }
  handleOnCancel() {
    if (this.props.onChange) {
      if (!R.equals(this.props.settings, this.state.prevSettings)) {
        this.props.onChange(this.props.streamId, this.state.prevSettings);
      }
    }
    this.hide();
  }
  // eslint-disable-next-line class-methods-use-this
  getNameForSettingsType(type) {
    if (type === 'WorkflowStatus') {
      return 'Select status';
    }
    if (type === 'Customers') {
      return 'Customer';
    }
    if (type === 'DataTime') {
      return 'Deadline';
    }
    if (type === 'TimeBudget') {
      return 'Time to completion';
    }
    return type;
  }
  renderSettingsItems() {
    const settingsMap = {
      Main: ['WorkflowStatus', 'Responsible', 'Customers'],
      Time: ['DataTime', 'Priority'],
      Resources: ['TimeBudget', 'Budget', 'Points'],
    };
    return R.map(
      key => (
        <View key={key}>
          <View style={{ paddingHorizontal: 20, paddingTop: 28 }}>
            <Text style={{ color: '#a8a4b9', fontSize: 15, fontWeight: '400' }}>
              {key}
            </Text>
          </View>
          {R.map(
            value =>
              this.renderSettingsItem(
                R.propOr(
                  { type: value, on: false },
                  value,
                  this.props.settings,
                ),
              ),
            R.propOr([], key, settingsMap),
          )}
        </View>
      ),
      R.keys(settingsMap),
    );
  }
  renderModalContent() {
    return this.renderSettingsItems();
  }
  renderSettingsItem({ type, on }) {
    return (
      <View
        key={type}
        style={{
          height: 70,
          borderBottomWidth: StyleSheet.hairlineWidth,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
        }}
      >
        <Text style={{ color: '#4e4963', fontSize: 20, fontWeight: '400' }}>
          {this.getNameForSettingsType(type)}
        </Text>
        <Switch value={on} onValueChange={() => this.handleOnChange(type)} />
      </View>
    );
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(40,30,42,1)',
          paddingHorizontal: 18,
          paddingVertical: 18,
        }}
      >
        <View
          style={{
            flex: 0,
            height: 50,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <TouchableOpacity
            style={{
              flex: 0,
              height: 48,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={this.handleOnCancel}
          >
            <Text style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}>
              Cancel
            </Text>
          </TouchableOpacity>
          <View
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          >
            <Text style={{ fontSize: 15, fontWeight: '400', color: '#ffffff' }}>
              Thread fields
            </Text>
          </View>
          <TouchableOpacity
            style={{
              flex: 0,
              height: 48,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={this.handleOnSave}
          >
            <Text style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}>
              Save
            </Text>
          </TouchableOpacity>
        </View>
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: '#ffffff',
            borderRadius: 13,
          }}
        >
          <View
            style={{
              paddingTop: 4,
              paddingBottom: 12,
            }}
          >
            {this.renderModalContent()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  settings: streamWidgetSettingsSelector(state, ownProps),
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  onChange: setWidgetSettings,
  onSave: setStreamWidgets,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadSettings),
);
