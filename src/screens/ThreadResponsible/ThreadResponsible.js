import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  Dimensions,
  FlatList,
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { setResponsible, setRole } from '../../reducers/threads';
import { setThreadResponsible, setThreadRole } from '../../actions';
import ContactListItem from '../../components/ContactListItem';
import withSafeArea from '../withSafeArea';

const { width, height } = Dimensions.get('window');

const filterContactsByName = (name, contacts) => {
  if (!name) {
    return { contacts, value: name };
  }
  const lowerCaseName = name.toLowerCase();
  const filtredContacts = R.filter(
    contact =>
      R.pathOr('', ['basicData', 'name'], contact)
        .toLowerCase()
        .indexOf(lowerCaseName) + 1,
    contacts,
  );
  return { contacts: filtredContacts, value: name };
};

class ThreadResponsible extends PureComponent {
  static displayName = 'ThreadResponsible';
  static propTypes = {
    threadId: PropTypes.string,
    type: PropTypes.string,
    items: PropTypes.array,
    header: PropTypes.string,
    currentItem: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
      color: PropTypes.string,
    }),
    setResponsible: PropTypes.func.isRequired,
    setThreadResponsible: PropTypes.func.isRequired,
    setRole: PropTypes.func.isRequired,
    setThreadRole: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { contacts: props.items, value: '' };
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnClose = this.handleOnClose.bind(this);
    this.handleOnFilter = this.handleOnFilter.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }
  handleOnPress(userId) {
    const { threadId, type } = this.props;
    if (type === 'responsible') {
      this.props.setResponsible(threadId, userId);
      this.props.setThreadResponsible({ threadId, userId });
    }
    if (type === 'follower') {
      this.props.setRole(threadId, userId);
      this.props.setThreadRole({ threadId, userId });
    }
    Navigation.dismissAllModals();
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnClose() {
    Navigation.dismissAllModals();
  }
  handleOnFilter(text) {
    const { items } = this.props;
    this.setState(filterContactsByName(text, items));
  }
  renderItem({ item }) {
    return (
      <ContactListItem
        // eslint-disable-next-line no-underscore-dangle
        key={item._id}
        showIcon
        style={{
          container: {
            height: 70,
            borderBottomColor: 'rgba(211, 210,211, 1)',
            borderBottomWidth: StyleSheet.hairlineWidth,
            paddingTop: 14,
            paddingBottom: 14,
          },
          contactListItem: {
            backgroundColor: '#e2e2e2',
          },
          contactItemText: {
            color: '#4e4963',
            fontSize: 18,
            fontWeight: '700',
          },
        }}
        contact={item}
        // eslint-disable-next-line no-underscore-dangle
        onPress={() => this.handleOnPress(item._id)}
      />
    );
  }
  render() {
    const { header } = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.handleOnClose}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(40,30,42,1)',
            paddingHorizontal: 18,
            paddingVertical: 18,
          }}
        >
          <View
            style={{
              flex: 0,
              height: 50,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#ffffff' }}
              >
                {header || 'Responsible'}
              </Text>
            </View>
          </View>
          <FlatList
            style={{
              flex: 1,
              borderRadius: 13,
              backgroundColor: '#ffffff',
            }}
            keyExtractor={item => item.id}
            data={this.state.contacts}
            renderItem={this.renderItem}
            ListHeaderComponent={
              <View
                style={{
                  borderBottomColor: 'rgba(211, 210,211, 1)',
                  borderBottomWidth: StyleSheet.hairlineWidth,
                }}
              >
                <TextInput
                  style={{
                    height: 70,
                    paddingVertical: 20,
                    paddingHorizontal: 14,
                    fontWeight: '400',
                    fontSize: 15,
                  }}
                  placeholder="Enter name..."
                  placeholderTextColor="#a8a4b9"
                  autoFocus={true}
                  underlineColorAndroid="rgba(255,255,255,0)"
                  onChangeText={this.handleOnFilter}
                  value={this.state.value}
                />
              </View>
            }
            ListEmptyComponent={
              <Text
                style={{
                  color: '#cccccc',
                  fontSize: 16,
                  paddingVertical: 16,
                  paddingHorizontal: 8,
                  fontWeight: '300',
                }}
              >
                No matches...
              </Text>
            }
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  setResponsible,
  setThreadResponsible,
  setRole,
  setThreadRole,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadResponsible),
);
