import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import {
  FlatList,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Animated,
} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import global from '../../global';
import { reversedOrderedNotificationsSelector } from '../../reducers/notifications';
import ThreadCard from '../../components/ThreadCard';
import Icons from '../../components/Icon';
import ContactBar from '../ContactBar';

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tab: {
    height: 44,
    paddingHorizontal: 24,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  tabs: {
    backgroundColor: '#ffffff',
    height: 44,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(216,216,216,1)',
  },
  mainDraftContainer: {
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    ...ifIphoneX({ paddingBottom: 36 }),
  },
  header: { paddingBottom: 20 },
  button: {
    justifyContent: 'center',
    backgroundColor: 'transparent',
    // height: 66,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'hsla(0,0%,100%,.3)',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: 'hsla(0,0%,100%,.3)',
    alignItems: 'center',
  },
  buttonView: {
    borderColor: 'rgb(211,188,215)',
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    height: 45,
    width: width - 40,
    flexDirection: 'row',
  },
  iconPlus: { justifyContent: 'center' },
  headerTitle: {
    alignItems: 'center',
    flex: 1,
  },
  headerTitleMargin: { marginLeft: -30 },
  text: { color: 'rgb(211,188,215)', fontSize: 16 },
  draftView: { height: height - 205, paddingBottom: 10 },
  contacBarView: {
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
});
const userIdSelector = state => R.pathOr(null, ['userData', 'userId'], state);

const threadsSelector = state => R.propOr({}, 'threads', state);
const statusesSelector = state => R.propOr({}, 'statuses', state);
const contactsSelector = state => R.propOr({}, 'contacts', state);
const streamsSelector = state => R.propOr({}, 'streams', state);

const sortByUpdatedAt = R.sortBy(R.prop('updatedAt'));

const userThreadsSelector = createSelector(
  userIdSelector,
  threadsSelector,
  statusesSelector,
  contactsSelector,
  streamsSelector,
  (userId, threads, statuses, contacts, streams) =>
    R.reverse(
      sortByUpdatedAt(
        R.map(thread => {
          if (thread) {
            const threadStatus = R.prop(R.prop('status', thread), statuses);
            const threadWithStatus = R.assoc('status', threadStatus, thread);
            const threadResponsible = R.prop(
              R.prop('responsibleUserId', thread),
              contacts,
            );
            const threadWithResponsible = R.assoc(
              'responsible',
              threadResponsible,
              threadWithStatus,
            );
            const threadWithStream = R.assoc(
              'stream',
              R.prop(R.prop('streamId', thread), streams),
              threadWithResponsible,
            );
            return threadWithStream;
          }
          return null;
        }, R.filter(_ => _.id, R.filter(thread => thread.authorId === userId && !thread.streamId, R.values(threads)))),
      ),
    ),
);

const mapStateToProps = (state, ownProps) => ({
  items: userThreadsSelector(state),
  streams: R.propOr({}, 'streams', state),
  notifications: reversedOrderedNotificationsSelector(state),
});

@connect(mapStateToProps)
export default class MainDrafts extends Component {
  static displayName = 'MainDrafts';
  static propTypes = {
    threads: PropTypes.object,
    userId: PropTypes.string,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
      setOnNavigatorEvent: PropTypes.func.isRequired,
    }),
    items: PropTypes.array,
  };
  static navigatorStyle = {
    navBarTextfontWeight: '300',
    navBarBackgroundColor: 'rgba(38,204,138,1)',
    navBarTextColor: '#ffffff',
    navBarButtonColor: '#ffffff',
    topBarElevationShadowEnabled: false,
    navBarTitleTextCentered: true,
  };
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'stream-header-button',
        component: 'StreamHeaderButton',
        passProps: {},
      },
    ],
  };
  constructor(props) {
    super(props);
    global.navigator = this.props.navigator;
    this.renderItem = this.renderItem.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.handleOnChangeTab = this.handleOnChangeTab.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.state = {
      items: this.props.items,
      update: false,
      // visible: true,
      tab: 0,
    };
  }

  componentWillReceiveProps(nextprops) {
    if (
      nextprops.items.filter((item, i) => !R.equals(this.props.items[i], item))
        .length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  // shouldComponentUpdate(nextProps, nextState) {
  //   if (nextState.update) {
  //     this.setState({
  //       update: false,
  //     });
  //     return true;
  //   }
  //   // if (this.state.tab !== nextState.tab) return true;
  //   return false;
  // }
  // eslint-disable-next-line class-methods-use-this
  onNavigatorEvent() {}
  // eslint-disable-next-line class-methods-use-this
  handleOnChangeTab({ i }) {
    this.setState({ tab: i });
    if (i === 0) global.tracker.trackScreenView('Chat');
    if (i === 1) global.tracker.trackScreenView('Board');
    if (i === 2) global.tracker.trackScreenView('List');
  }
  keyExtractor(item) {
    return item.id || item._id;
  }

  handleOnPress() {
    this.props.createThread(
      {
        statusId: '',
        title: '',
        streamId: '',
        roles: [this.props.userId],
        authorId: this.props.userId,
      },
      id =>
        this.props.navigator.push({
          screen: 'workonflow.Thread',
          title: '',
          passProps: {
            id,
            threadId: id,
            type: 'Thread',
            focusOnTitle: true,
          },
        }),
    );
  }
  // eslint-disable-next-line class-methods-use-this
  renderItem({ item }) {
    return (
      <ThreadCard id={item.id} thread={item} navigator={this.props.navigator} />
    );
  }
  render() {
    return (
      <Animated.View style={{ flex: 1 }}>
        <ScrollableTabView
          initialPage={0}
          tabBarPosition="top"
          renderTabBar={() => <DraftTabBar />}
          onChangeTab={this.handleOnChangeTab}
          prerenderingSiblingsNumber={Infinity}
          tabBarBackgroundColor="#ffffff"
          tabBarInactiveTextColor="#BDBDBD"
          tabBarActiveTextColor="#212121"
          locked={true}
          contentProps={{
            keyboardShouldPersistTaps: 'always',
            style: {
              flex: 1,
              backgroundColor: 'rgb(243, 239, 241)',
            },
          }}
        >
          <View style={styles.mainDraftContainer}>
            <View style={styles.header}>
              <TouchableOpacity
                style={styles.button}
                onPress={this.handleOnPress}
              >
                <View style={styles.buttonView}>
                  <View style={styles.iconPlus}>
                    <Icons
                      name="Plus"
                      width={28}
                      height={28}
                      fill="rgb(211,188,215)"
                      viewBox="0 0 24 24"
                    />
                  </View>
                  <View style={styles.headerTitle}>
                    <View style={styles.headerTitleMargin}>
                      <Text style={styles.text}>{'Create draft'}</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.draftView}>
              <FlatList
                initialNumToRender={12}
                data={this.props.items}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
              />
            </View>
          </View>
          <View>
            <View style={styles.contacBarView}>
              <ContactBar
                tabLabel="ContactBar"
                navigator={this.props.navigator}
                contact={this.props.user}
                streams={R.values(this.props.streams)}
                setContactCustomField={this.props.setContactCustomField}
                removeFromStream={this.props.removeFromStream}
                setContactName={this.props.setContactName}
                setContactEmail={this.props.setContactEmail}
                setContactPhone={this.props.setContactPhone}
                userId={this.props.userId}
              />
            </View>
          </View>
        </ScrollableTabView>
      </Animated.View>
    );
  }
}

export class DraftTabBar extends Component {
  static displayName = 'DraftTabBar';
  static propTypes = {
    tabs: PropTypes.array,
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    scrollValue: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.icons = [];
    this.getTabs = this.getTabs.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
    this.props.scrollValue.addListener(this.setAnimationValue.bind(this));
  }
  // eslint-disable-next-line class-methods-use-this
  setAnimationValue() {}
  getTabs() {
    return this.props.tabs;
  }
  renderTabs() {
    return ['Drafts', 'Profile'].map((tab, i) => (
      <TouchableOpacity
        key={tab}
        onPress={() => this.props.goToPage(i)}
        style={styles.tab}
      >
        <Text
          style={{
            color: this.props.activeTab === i ? '#696285' : '#a9a5ba',
            fontWeight: '100',
            fontSize: 18,
          }}
        >
          {tab}
        </Text>
      </TouchableOpacity>
    ));
  }
  render() {
    return (
      <View style={{ backgroundColor: 'rgba(38,204,138,1)' }}>
        <View style={styles.tabs}>{this.renderTabs()}</View>
      </View>
    );
  }
}
