import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Platform,
  Dimensions,
  View,
  Text,
  TouchableWithoutFeedback,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Slider from 'react-native-slider';
import { Navigation } from 'react-native-navigation';
import { setPoints, threadByIdSelector } from '../../reducers/threads';
import { setThreadPoints } from '../../actions';
import withSafeArea from '../withSafeArea';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  slider: { flex: 1 },
  thumb: {
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#aaa6ba',
  },
  thumbIOS: {
    width: 28,
    height: 28,
    borderRadius: 14,
  },
  thumbAndroid: {
    width: 24,
    height: 24,
    borderRadius: 12,
  },
  track: {
    borderRadius: 5,
    height: 9,
  },
});

class ThreadPoints extends Component {
  static displayName = 'ThreadPoints';
  static propTypes = {
    threadId: PropTypes.string,
    unit: PropTypes.string,
    thread: PropTypes.shape({
      mainRank: PropTypes.number,
    }),
    setPoints: PropTypes.func.isRequired,
    setThreadPoints: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = { prevValue: props.thread.mainRank };
    this.items = [0, 0.5, 1, 2, 3, 4, 8, 16, 24, 32, 40, 80, 160];
    this.handleOnValueChange = this.handleOnValueChange.bind(this);
    this.handleOnSlidingComplete = this.handleOnSlidingComplete.bind(this);
    this.handleOnClose = this.handleOnClose.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
    this.handleOnCancel = this.handleOnCancel.bind(this);
  }
  getIndexForValue() {
    const {
      thread: { mainRank = 0 },
    } = this.props;
    return R.propOr([], 'items', this).indexOf(mainRank);
  }
  handleOnSave() {
    const {
      threadId,
      thread: { mainRank },
    } = this.props;
    this.props.setThreadPoints(threadId, mainRank);
    Navigation.dismissModal();
  }
  handleOnCancel() {
    const { threadId } = this.props;
    const { prevValue } = this.state;
    this.props.setPoints(threadId, prevValue);
    Navigation.dismissModal();
  }
  handleOnValueChange(index) {
    const { threadId } = this.props;
    this.props.setPoints(threadId, this.items[index]);
  }
  handleOnSlidingComplete() {
    const {
      threadId,
      thread: { mainRank },
    } = this.props;
    this.props.setThreadPoints(threadId, mainRank);
  }
  handleOnClose() {
    this.handleOnCancel();
  }
  render() {
    const {
      thread: { mainRank },
    } = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.handleOnClose}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(40,30,42,1)',
            paddingHorizontal: 18,
            paddingVertical: 18,
          }}
        >
          <View
            style={{
              flex: 0,
              height: 50,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <TouchableOpacity
              style={{
                flex: 0,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.handleOnCancel}
            >
              <Text
                style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}
              >
                Cancel
              </Text>
            </TouchableOpacity>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{ fontSize: 15, fontWeight: '400', color: '#ffffff' }}
              >
                Points
              </Text>
            </View>
            <TouchableOpacity
              style={{
                flex: 0,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.handleOnSave}
            >
              <Text
                style={{ color: '#0d71d7', fontSize: 18, fontWeight: '400' }}
              >
                Save
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              backgroundColor: 'transparent',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                backgroundColor: '#ffffff',
                borderRadius: 13,
                width: width - 36,
                height: 108,
                paddingVertical: 16,
                paddingHorizontal: 16,
              }}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Text
                  style={{
                    fontSize: 36,
                    color: '#4e4963',
                    fontWeight: '400',
                  }}
                >
                  {mainRank}
                </Text>
              </View>
              <Slider
                style={styles.slider}
                value={this.getIndexForValue()}
                step={1}
                maximumValue={12}
                minimumValue={0}
                onValueChange={this.handleOnValueChange}
                minimumTrackTintColor="#0d71d7"
                maximumTrackTintColor="#dcdbe0"
                trackStyle={styles.track}
                thumbStyle={[
                  styles.thumb,
                  Platform.OS === 'ios' ? styles.thumbIOS : styles.thumbAndroid,
                ]}
              />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  thread: threadByIdSelector(state, ownProps),
  safeAreaBackgroundColor: 'rgba(40,30,42,1)',
});

const mapDispatchToProps = {
  setPoints,
  setThreadPoints,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(ThreadPoints),
);
