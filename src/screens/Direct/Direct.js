import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import DirectChat from '../../components/DirectChat';
import withSafeArea from '../withSafeArea';

class Direct extends Component {
  static displayName = 'Direct';
  static propTypes = {
    contactId: PropTypes.string,
  };
  render() {
    return <DirectChat contactId={this.props.contactId} />;
  }
}

const mapStateToProps = () => ({
  safeAreaBackgroundColor: '#ffffff',
});

export default connect(mapStateToProps)(withSafeArea(Direct));
