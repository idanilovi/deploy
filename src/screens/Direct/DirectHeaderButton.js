import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Icon from '../../components/Icon';
import global from '../../global';

const styles = StyleSheet.create({
  buttonContainer: {
    width: 48,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default class DirectHeaderButton extends PureComponent {
  static displayName = 'DirectHeaderButton';
  static propTypes = {
    id: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnPress() {
    if (R.pathOr(null, ['directTabBar', 'goToPage'], global)) {
      global.directTabBar.goToPage(1);
    }
  }
  render() {
    return (
      <TouchableOpacity
        style={[styles.buttonContainer]}
        onPress={this.handleOnPress}
      >
        <Icon
          name="BusinessCard"
          height={24}
          width={24}
          fill="#ffffff"
          viewBox="0 0 24 24"
        />
      </TouchableOpacity>
    );
  }
}
