import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  View,
  Text,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import FloatingLabelInput from '../../components/FloatingLabelInput';
import Button from '../../components/Button';
import { setEmail, loginSelector } from '../../reducers/ui/login';
import { fetchRecoverPassword } from '../../actions';
import backgroundImageSource from '../../assets/images/bg.png';
import withSafeArea from '../withSafeArea';

class LoginResetPassword extends Component {
  static displayName = 'LoginResetPassword';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    login: PropTypes.shape({
      email: PropTypes.string,
      password: PropTypes.string,
      error: PropTypes.string,
      showPassword: PropTypes.bool,
    }),
    setEmail: PropTypes.func.isRequired,
    fetchRecoverPassword: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    tabBarHidden: true,
    navBarHidden: true,
  };
  constructor() {
    super();
    this.state = { error: false };
    this.handleOnResetPress = this.handleOnResetPress.bind(this);
    this.checkEmailInputValue = this.checkEmailInputValue.bind(this);
  }
  checkEmailInputValue() {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg.test(this.state.email) === false) {
      this.setState({
        error: { error: 'Email is not correct' },
      });
    } else {
      this.setState({ error: false });
    }
  }
  handleOnResetPress() {
    this.props.fetchRecoverPassword({
      email: this.props.login.email,
      successCallback: () =>
        this.props.navigator.push({ screen: 'workonflow.LoginCheck' }),
      failureCallback: error => this.setState({ error }),
    });
  }
  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            paddingVertical: 16,
            paddingHorizontal: 16,
          }}
        >
          <View style={{ flex: 1, justifyContent: 'flex-start' }}>
            <View
              style={{
                paddingHorizontal: 0,
                paddingTop: Platform.OS === 'ios' ? 42 : 38,
                paddingBottom: 8,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text
                style={{
                  fontSize: 17,
                  color: '#ffffff',
                }}
              >
                Forgot your password?
              </Text>
            </View>
            <View style={{ paddingVertical: 10 }}>
              <FloatingLabelInput
                error={this.state.error}
                errorMessage={this.state.error.error}
                nativeID="LOGIN_FIELD"
                placeholder="Email: "
                keyboardType="email-address"
                value={this.props.login.email}
                onChangeTextValue={this.props.setEmail}
                autoFocus={true}
                returnKeyType="next"
                onBlur={this.checkEmailInputValue}
                onSubmitEditing={this.handleOnResetPress}
              />
            </View>
            <View style={{ paddingVertical: 10 }}>
              <Button
                activeOpacity={0.9}
                styles={{
                  button: {
                    backgroundColor: 'rgb(0, 198, 158)',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 53,
                    borderRadius: 5,
                  },
                  title: {
                    fontSize: 25,
                    fontWeight: '400',
                    color: '#fefefe',
                  },
                }}
                title="Reset password"
                onPress={this.handleOnResetPress}
              />
            </View>
          </View>
          <View style={{ flex: 1, justifyContent: 'flex-end' }}>
            <Button
              activeOpacity={0.9}
              styles={{
                button: {
                  alignItems: 'center',
                  height: 53,
                  justifyContent: 'center',
                },
                title: {
                  fontSize: 17,
                  fontWeight: '400',
                  color: '#ffffff',
                },
              }}
              title="Back to Log in"
              onPress={() =>
                this.props.navigator.push({ screen: 'workonflow.Login' })
              }
            />
          </View>
          {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => ({
  login: loginSelector(state),
  safeAreaBackgroundImageSource: backgroundImageSource,
  safeAreaBackgroundImage: true,
});

const mapDispatchToProps = {
  setEmail,
  fetchRecoverPassword,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(LoginResetPassword),
);
