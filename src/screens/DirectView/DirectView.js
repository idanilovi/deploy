import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { View, Platform } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Loader from '../../components/Loader';
import {
  getContact,
  setContactCustomField,
  removeFromStream,
  setContactName,
  setContactEmail,
  setContactPhone,
} from '../../actions';
import Direct from '../Direct';
import ContactBar from '../ContactBar';
import global from '../../global';

const userIdSelector = (state, ownProps) =>
  R.pathOr(R.prop('userId', ownProps), ['userData', 'userId'], state);

const contactIdSelector = (state, ownProps) =>
  R.propOr(null, 'contactId', ownProps);

const contactsSelector = state => R.propOr({}, 'contacts', state);

const contactColorSelector = createSelector(
  contactsSelector,
  contactIdSelector,
  (contacts, contactId) =>
    R.propOr('#ffffff', 'color', R.propOr({}, contactId, contacts)),
);

const streamsSelector = state => R.prop('streams', state);

const streamsWithContactInRolesSelector = createSelector(
  contactIdSelector,
  streamsSelector,
  (contactId, streams) =>
    R.filter(
      stream => R.contains(contactId, R.propOr([], 'roles', stream)),
      R.values(streams),
    ),
);

const contactByIdSelector = createSelector(
  contactIdSelector,
  contactsSelector,
  (contactId, contacts) => R.propOr({}, contactId, contacts),
);

const mapStateToProps = (state, ownProps) => ({
  contactId: contactIdSelector(state, ownProps),
  userId: userIdSelector(state, ownProps),
  contactColor: contactColorSelector(state, ownProps),
  contact: contactByIdSelector(state, ownProps),
  streams: streamsWithContactInRolesSelector(state, ownProps),
});

const mapDispatchToProps = {
  getContact,
  setContactCustomField,
  removeFromStream,
  setContactName,
  setContactEmail,
  setContactPhone,
};
@connect(mapStateToProps, mapDispatchToProps)
export default class DirectView extends Component {
  static displayName = 'DirectView';
  static propTypes = {
    navigator: PropTypes.object,
    contact: PropTypes.object,
    streams: PropTypes.array,
    initialPage: PropTypes.number,
    customLeftButton: PropTypes.bool,
    contactId: PropTypes.string,
    title: PropTypes.string,
    getContact: PropTypes.func.isRequired,
  };
  static navigatorStyle = {
    navBarTextfontWeight: '300',
    navBarBackgroundColor: 'rgba(43,32,45,1)',
    navBarTextColor: '#ffffff',
    navBarButtonColor: '#ffffff',
    topBarElevationShadowEnabled: false,
    navBarTitleTextCentered: true,
  };
  constructor(props) {
    super(props);
    this.state = { visible: true, loading: true };
    this.handleOnChangeTab = this.handleOnChangeTab.bind(this);
    this.renderTabBar = this.renderTabBar.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  componentDidMount() {
    const { initialPage, customLeftButton, contactId, title } = this.props;
    this.props.getContact(contactId);
    global.tracker.trackScreenView('Direct');
    this.props.navigator.setTitle({ title });
    if (initialPage === 0) {
      this.props.navigator.setButtons({
        rightButtons: [
          {
            id: 'direct-header-button',
            component: 'DirectHeaderButton',
            passProps: {
              id: contactId,
            },
          },
        ],
        leftButtons: [
          Platform.OS === 'ios' && customLeftButton
            ? {
                id: 'direct-back-button',
                component: 'DirectBackButton',
                passProps: {
                  customLeftButton,
                },
              }
            : { id: 'back' },
        ],
      });
    }
    if (initialPage) {
      this.props.navigator.setButtons({
        rightButtons: [],
        leftButtons: [
          Platform.OS === 'ios'
            ? {
                id: 'direct-back-button',
                component: 'DirectBackButton',
                passProps: {},
              }
            : { id: 'back' },
        ],
      });
    }
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      this.setState({ visible: true });
    }
    if (event.id === 'willDisappear') {
      this.setState({ visible: false });
    }
  }
  handleOnChangeTab({ i }) {
    const { customLeftButton } = this.props;
    if (i === 0) {
      this.props.navigator.setButtons({
        rightButtons: [
          {
            id: 'direct-header-button',
            component: 'DirectHeaderButton',
            passProps: {
              id: this.props.contactId,
            },
          },
        ],
        leftButtons: [
          Platform.OS === 'ios' && customLeftButton
            ? {
                id: 'direct-back-button',
                component: 'DirectBackButton',
                passProps: {
                  customLeftButton,
                },
              }
            : { id: 'back' },
        ],
      });
    }
    if (i === 1) {
      this.props.navigator.setButtons({
        rightButtons: [],
        leftButtons: [
          Platform.OS === 'ios'
            ? {
                id: 'direct-back-button',
                component: 'DirectBackButton',
                passProps: {},
              }
            : { id: 'back' },
        ],
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  renderTabBar() {
    return <View />;
  }
  render() {
    return (
      <Loader
        type="direct"
        contactId={this.props.contactId}
        color="rgba(43,32,45,1)"
      >
        <ScrollableTabView
          initialPage={this.props.initialPage || 0}
          ref={ref => {
            global.directTabBar = ref;
          }}
          renderTabBar={this.renderTabBar}
          onChangeTab={this.handleOnChangeTab}
          prerenderingSiblingsNumber={Infinity}
          contentProps={{
            keyboardShouldPersistTaps: 'always',
            style: {
              flex: this.state.visible ? 1 : 0,
              backgroundColor: 'rgba(43, 32, 45, 1)',
            },
          }}
        >
          <Direct
            tabLabel="Direct"
            navigator={this.props.navigator}
            title={this.props.title}
            contactId={this.props.contactId}
          />
          <ContactBar
            tabLabel="ContactBar"
            navigator={this.props.navigator}
            contact={this.props.contact}
            streams={this.props.streams}
            setContactCustomField={this.props.setContactCustomField}
            removeFromStream={this.props.removeFromStream}
            setContactName={this.props.setContactName}
            setContactEmail={this.props.setContactEmail}
            setContactPhone={this.props.setContactPhone}
            userId={this.props.userId}
          />
        </ScrollableTabView>
      </Loader>
    );
  }
}
