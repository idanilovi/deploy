import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Platform, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { Navigation } from 'react-native-navigation';
import global from '../../global';

const styles = StyleSheet.create({
  buttonContainer: {
    margin: 0,
    padding: 0,
    width: 34,
    height: 34,
    marginLeft: -6,
  },
});

export default class DirectBackButton extends PureComponent {
  static displayName = 'DirectBackButton';
  static propTypes = {
    id: PropTypes.string,
    customLeftButton: PropTypes.bool,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnPress() {
    const { customLeftButton } = this.props;
    if (customLeftButton) {
      Navigation.dismissModal();
    } else if (R.pathOr(null, ['directTabBar', 'goToPage'], global)) {
      global.directTabBar.goToPage(0);
    }
  }
  // eslint-disable-next-line class-methods-use-this
  renderIcon() {
    return Platform.OS === 'ios' ? (
      <Icon name="ios-arrow-back" size={34} color="#ffffff" />
    ) : (
      <Icon name="md-arrow-back" size={34} color="#ffffff" />
    );
  }
  render() {
    return (
      <TouchableOpacity
        style={[styles.buttonContainer]}
        onPress={this.handleOnPress}
      >
        {this.renderIcon()}
      </TouchableOpacity>
    );
  }
}
