import { createLogger } from 'redux-logger';
import { applyMiddleware, compose, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import moment from 'moment';
// import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer from './reducers';
import registerScreens from './screens';
import global from './global';
import * as Actions from './actions';
import getConnection from './helpers/getConnection';

moment.locale('ru');

const loggerMiddleware = createLogger({
  // eslint-disable-next-line no-underscore-dangle
  // eslint-disable-next-line no-undef
  predicate: () => __DEV__ === true,
});

const presistConfig = {
  key: 'root',
  storage,
};

function configureStore() {
  return createStore(
    persistReducer(presistConfig, rootReducer),
    compose(applyMiddleware(thunkMiddleware, loggerMiddleware)),
  );
}

const store = configureStore();

console.disableYellowBox = true;
console.ignoredYellowBox = ['Setting a timer'];

// global.tracker = new GoogleAnalyticsTracker('UA-110690036-1');
global.tracker = {
  trackScreenView: () => {},
  trackEvent: () => {},
};

async function initApplication() {
  if (global.timoutId) {
    clearTimeout(global.timoutId);
  }
  global.isConnected = await getConnection();

  if (!global.isConnected) {
    Navigation.startSingleScreenApp({
      screen: { screen: 'workonflow.Reload' },
      passProps: {},
      appStyle: { orientation: 'portrait' },
      navigatorStyle: {
        header: '#302938',
      },
    });
  } else {
    global.timoutId = setTimeout(() => {
      Navigation.startSingleScreenApp({
        screen: { screen: 'workonflow.Reload' },
        passProps: {},
        appStyle: { orientation: 'portrait' },
      });
    }, 18000);
    store.dispatch(Actions.getFilesFromCache());
    if (Platform.OS === 'ios') {
      Navigation.startSingleScreenApp({
        screen: { screen: 'workonflow.Root' },
        passProps: {},
        appStyle: { orientation: 'portrait' },
      });
      return;
    }
    const token = await store.dispatch(
      Actions.getDataFromAsyncStorage('token'),
    );
    if (!token) {
      Navigation.startSingleScreenApp({
        screen: { screen: 'workonflow.Login' },
        passProps: {},
        appStyle: { orientation: 'portrait' },
      });
      return;
    }
    const checkTokenResponse = await store.dispatch(
      Actions.fetchCheckToken({ token }),
    );
    if (checkTokenResponse.result) {
      await store.dispatch(Actions.getSocketConnection(token));
      store.dispatch(Actions.getContact(checkTokenResponse.verified.userId));
      await store.dispatch(Actions.getUserSettings());
      Navigation.startSingleScreenApp({
        screen: { screen: 'workonflow.Main' },
        drawer: {
          left: {
            screen: 'workonflow.SidebarDrawer',
            passProps: {},
            disableOpenGesture: false,
          },
          style: {
            drawerShadow: true,
            contentOverlayColor: 'rgba(0,0,0,0.25)',
            leftDrawerWidth: 50,
            rightDrawerWidth: 50,
          },
          type: 'MMDrawer',
          animationType: 'slide',
          disableOpenGesture: true,
        },
        passProps: {},
        appStyle: { orientation: 'portrait' },
      });
      return;
    }
    const email = await store.dispatch(
      Actions.getDataFromAsyncStorage('email'),
    );
    const password = await store.dispatch(
      Actions.getDataFromAsyncStorage('password'),
    );
    if (!email && !password) {
      Navigation.startSingleScreenApp({
        screen: { screen: 'workonflow.Login' },
        passProps: {},
        appStyle: { orientation: 'portrait' },
      });
      return;
    }
    await store.dispatch(Actions.fetchLoginRequest({ email, password }));
    const teamId = await store.dispatch(
      Actions.getDataFromAsyncStorage('teamId'),
    );
    const to = await store.dispatch(Actions.getDataFromAsyncStorage('to'));
    const userId = await store.dispatch(
      Actions.getDataFromAsyncStorage('userId'),
    );
    if (!teamId && !to && !userId) {
      Navigation.startSingleScreenApp({
        screen: { screen: 'workonflow.LoginTeams' },
        passProps: {},
        appStyle: { orientation: 'portrait' },
      });
      return;
    }
    await store.dispatch(
      Actions.fetchTokenFromRedirect({ teamId, to, userId }),
    );
    const newToken = await store.dispatch(
      Actions.getDataFromAsyncStorage('token'),
    );
    if (!newToken) {
      Navigation.startSingleScreenApp({
        screen: { screen: 'workonflow.Login' },
        passProps: {},
        appStyle: { orientation: 'portrait' },
      });
      return;
    }
    await store.dispatch(Actions.getSocketConnection(newToken));
    store.dispatch(Actions.getContact(userId));
    store.dispatch(Actions.getUserSettings());
    Navigation.startSingleScreenApp({
      screen: { screen: 'workonflow.Main' },
      drawer: {
        left: {
          screen: 'workonflow.SidebarDrawer',
          passProps: {},
          disableOpenGesture: false,
        },
        style: {
          drawerShadow: true,
          contentOverlayColor: 'rgba(0,0,0,0.25)',
          leftDrawerWidth: 50,
          rightDrawerWidth: 50,
        },
        type: 'MMDrawer',
        animationType: 'slide',
        disableOpenGesture: true,
      },
      passProps: {},
      appStyle: { orientation: 'portrait' },
    });
  }
}

persistStore(store, null, () => {
  registerScreens(store);
  initApplication();
});
