import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import ListItem from './ListItem';

function getItemColor(index) {
  if (index % 3 === 0) {
    return '#9d80e5';
  }
  if (index % 3 === 1 % 3) {
    return '#3fd195';
  }
  if (index % 3 === 2 % 3) {
    return '#80c3e5';
  }
  return '#ffffff';
}

export default class List extends Component {
  static propTypes = {
    items: PropTypes.array,
    onItemPress: PropTypes.func,
    renderItem: PropTypes.func,
    ListHeaderComponent: PropTypes.node,
  };
  constructor() {
    super();
    this.renderItem = this.renderItem.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item) {
    // eslint-disable-next-line no-underscore-dangle
    return item._id;
  }
  renderItem({ item, index }) {
    if (this.props.renderItem) {
      return this.props.renderItem({ item });
    }
    return (
      <ListItem
        item={item}
        color={getItemColor(index)}
        onItemPress={() => {
          this.props.onItemPress({
            teamId: item.teamId,
            to: item.url,
            userId: item.userId,
            teamName: item.teamName || item.teamId,
          });
        }}
      />
    );
  }
  render() {
    return (
      <FlatList
        data={this.props.items}
        ListHeaderComponent={this.props.ListHeaderComponent || null}
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
      />
    );
  }
}
