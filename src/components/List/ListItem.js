import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';

const styles = StyleSheet.create({
  touchableContainer: {
    paddingLeft: 4,
    borderRadius: 4,
    marginBottom: 4,
  },
  container: {
    borderRadius: 4,
    height: 48,
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
  },
  textWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftText: {
    fontSize: 18,
    fontWeight: '200',
    color: '#212121',
    flexShrink: 1,
  },
  rightText: {
    fontSize: 14,
    fontWeight: '200',
    color: '#212121',
  },
});

export default class ListItem extends PureComponent {
  static propTypes = {
    onItemPress: PropTypes.func,
    item: PropTypes.shape({
      teamId: PropTypes.string,
      url: PropTypes.string,
      teamName: PropTypes.string,
    }),
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    if (this.props.onItemPress) {
      this.props.onItemPress();
    }
  }
  render() {
    return (
      <TouchableOpacity
        style={[
          styles.touchableContainer,
          { backgroundColor: this.props.color },
        ]}
        onPress={this.handleOnPress}
      >
        <View style={styles.container}>
          <View style={styles.textWrapper}>
            <Text
              style={styles.leftText}
              numberOfLines={1}
              ellipsizeMode="tail"
              textBreakStrategy="highQuality"
            >
              {this.props.item.teamName || this.props.item.teamId}
            </Text>
            <Text
              style={styles.rightText}
              numberOfLines={1}
              ellipsizeMode="tail"
              textBreakStrategy="highQuality"
            >
              {this.props.item.url}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
