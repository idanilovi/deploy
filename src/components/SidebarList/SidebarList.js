/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  FlatList,
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import SidebarListItem from '../SidebarListItem';
import Icons from '../Icon';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
  createButton: {
    flex: 0,
    backgroundColor: 'transparent',
    borderRadius: 18,
    height: 32,
    width: 32,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  addIcon: {
    backgroundColor: 'transparent',
    color: '#ffffff',
  },
  checkmark: {
    backgroundColor: 'transparent',
    color: '#000000',
    paddingVertical: 18,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    // height: 144,
  },
  view: {
    flex: 1,
    position: 'absolute',
    width: 330,
    paddingHorizontal: 8,
    paddingVertical: 8,
    borderRadius: 10,
    backgroundColor: '#ffffff',
  },
  button: {
    padding: 20,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '400',
  },
  inputView: {
    flexDirection: 'row',
    borderRadius: 10,
  },
  textInput: {
    width: 280,
    borderRadius: 10,
  },
});
export default class SidebarList extends Component {
  static displayName = 'SidebarList';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    items: PropTypes.array,
  };
  constructor(props) {
    super(props);
    this.keyExtractor = this.keyExtractor.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnChangeText = this.handleOnChangeText.bind(this);
    this.handleCreateStreamButton = this.handleCreateStreamButton.bind(this);
    this.handleCreateUserButton = this.handleCreateUserButton.bind(this);
    this.handleSaveButton = this.handleSaveButton.bind(this);
    this.hide = this.hide.bind(this);
    this.state = {
      items: this.props.items,
      update: false,
      show: false,
      text: '',
      showStreamInput: false,
      showUserInput: false,
    };
  }
  componentWillReceiveProps(nextprops) {
    if (
      nextprops.items.filter((item, i) => !R.equals(this.props.items[i], item))
        .length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    if (this.state.show !== nextState.show) return true;

    return false;
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item, index) {
    return item._id || item.id || `${item.type}${index}${this.props.type}`;
  }
  handleOnPress(text) {
    if (text === 'Hire bot') {
      this.props.navigator.push({
        screen: 'workonflow.Bots',
        title: 'Botstore',
      });
      return;
    }
    this.setState({
      show: true,
      showUserInput: text === 'Invite user',
      showStreamInput: text === 'Create stream',
    });
  }
  hide() {
    this.setState({ show: false });
  }
  handleCreateUserButton() {
    this.hide();
    this.props.createUser(this.state.text);
  }
  async handleCreateStreamButton() {
    this.hide();
    const data = await this.props.createStream({
      userId: this.props.userId,
      title: this.state.text,
      navigator: this.props.navigator,
    });
    if (data) {
      this.props.navigator.push({
        screen: 'workonflow.Stream',
        passProps: {
          streamId: R.prop('id', data),
          type: 'Stream',
          title: R.prop('name', data),
          initialPage: 3,
        },
      });
    }
  }
  handleOnChangeText(text) {
    this.setState({ text });
  }
  renderItem({ item, index }) {
    if (item.type === 'inviteButton') {
      return (
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'transparent',
            height: 66,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: 'hsla(0,0%,100%,.3)',
            borderTopWidth: StyleSheet.hairlineWidth,
            borderTopColor: 'hsla(0,0%,100%,.3)',
            alignItems: 'center',
          }}
          onPress={() => this.handleOnPress(item.text)}
        >
          <View
            style={{
              borderColor: '#ffffff',
              borderWidth: 1,
              borderRadius: 10,
              paddingHorizontal: 10,
              paddingVertical: 10,
              height: 45,
              width: width - 40,
              flexDirection: 'row',
            }}
          >
            <View style={{ justifyContent: 'center' }}>
              <Icons
                name="Plus"
                width={28}
                height={28}
                fill="#ffffff"
                viewBox="0 0 24 24"
              />
            </View>
            <View
              style={{
                alignItems: 'center',
                flex: 1,
              }}
            >
              <View style={{ marginLeft: -30 }}>
                <Text style={{ color: '#ffffff', fontSize: 16 }}>
                  {item.text}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    }
    if (item.id || item._id) {
      return (
        <SidebarListItem
          id={item.id || item._id}
          item={item}
          navigator={this.props.navigator}
        />
      );
    }
    return null;
  }
  handleSaveButton() {
    if (this.state.showUserInput) {
      this.handleCreateUserButton();
      return;
    }
    if (this.state.showStreamInput) {
      this.handleCreateStreamButton();
      return;
    }
  }
  renderModal() {
    if (this.state.show) {
      return (
        <Modal
          key="add_modal"
          isVisible={this.state.show}
          style={styles.modal}
          useNativeDriver={true}
          onBackdropPress={this.hide}
          onBackButtonPress={this.hide}
          backdropOpacity={0.9}
          // avoidKeyboard={true}
        >
          <View style={{ position: 'absolute', top: 0, flexDirection: 'row' }}>
            <TouchableOpacity
              style={{ alignItems: 'flex-start', flex: 1 }}
              onPress={this.hide}
            >
              <Text style={{ color: '#0d71d7' }}>{'Close'}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ alignItems: 'flex-end' }}
              onPress={this.handleSaveButton}
            >
              <Text style={{ color: '#0d71d7' }}>{'Save'}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.view}>
            {this.state.showStreamInput && (
              <View style={styles.inputView}>
                <TextInput
                  style={styles.textInput}
                  autoFocus={true}
                  onChangeText={this.handleOnChangeText}
                  placeholder="type stream title"
                />
              </View>
            )}
            {this.state.showUserInput && (
              <View style={styles.inputView}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={this.handleOnChangeText}
                  autoFocus={true}
                  placeholder="type invite email"
                />
              </View>
            )}
          </View>
        </Modal>
      );
    }
    return null;
  }
  render() {
    return [
      <FlatList
        initialNumToRender={12}
        data={this.props.items}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
      />,
      this.renderModal(),
    ];
  }
}
