import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Animated, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
});

export default class FloatingLabel extends Component {
  static propTypes = {
    visible: PropTypes.bool,
    children: PropTypes.node,
  };
  constructor(props) {
    super(props);
    let initialPadding = 9;
    let initialOpacity = 0;
    if (this.props.visible) {
      initialPadding = 5;
      initialOpacity = 1;
    }
    this.state = {
      paddingAnim: new Animated.Value(initialPadding),
      opacityAnim: new Animated.Value(initialOpacity),
    };
  }
  componentWillReceiveProps(nextProps) {
    Animated.timing(this.state.paddingAnim, {
      toValue: nextProps.visible ? 0 : 9,
      duration: 200,
    }).start();
    Animated.timing(this.state.opacityAnim, {
      toValue: nextProps.visible ? 1 : 0,
      duration: 200,
    }).start();
  }
  render() {
    return (
      <Animated.View
        style={[
          styles.container,
          {
            paddingTop: this.state.paddingAnim,
            opacity: this.state.opacityAnim,
          },
        ]}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}
