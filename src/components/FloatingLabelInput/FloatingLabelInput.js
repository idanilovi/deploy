import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Text, View, TextInput, Platform, StyleSheet } from 'react-native';
import TextFieldWrapper from './TextFieldWrapper';
import FloatingLabel from './FloatingLabel';

const styles = StyleSheet.create({
  container: {
    height: 53,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    borderRadius: 5,
    paddingVertical: Platform.OS === 'ios' ? 4 : 2,
    paddingHorizontal: 10,
    flexDirection: 'row',
  },
  containerFocused: {
    paddingVertical: Platform.OS === 'ios' ? 4 : 2,
    backgroundColor: '#fefefe',
  },
  containerUnfocused: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    position: 'relative',
  },
  label: {
    height: 15,
    fontSize: Platform.OS === 'ios' ? 15 : 12,
    fontWeight: '400',
    color: '#a9a5ba',
  },
  input: {
    height: Platform.OS === 'ios' ? 20 : 60,
    fontSize: Platform.OS === 'ios' ? 20 : 17,
    fontWeight: '400',
    color: '#fefefe',
  },
});

export default class FloatLabelTextField extends PureComponent {
  static displayName = 'FloatLabelTextField';
  static propTypes = {
    styles: PropTypes.object,
    nativeID: PropTypes.string,
    maxLength: PropTypes.number,
    value: PropTypes.string,
    defaultValue: PropTypes.string,
    placeholder: PropTypes.string,
    leftPadding: PropTypes.number,
    renderButton: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChangeTextValue: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      text: this.props.value,
    };
    this.handleOnFocus = this.handleOnFocus.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.handleOnChangeText = this.handleOnChangeText.bind(this);
    this.renderButton = this.renderButton.bind(this);

    this.focus = this.focus.bind(this);
    this.blur = this.blur.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.value && nextProps.value !== this.state.text) {
      this.setState({ text: nextProps.value });
    }
  }
  focus() {
    if (this.input) {
      this.input.focus();
    }
  }
  blur() {
    if (this.input) {
      this.input.blur();
    }
  }
  handleOnChangeText(value) {
    this.setState({ text: value }, () => {
      if (this.props.onChangeTextValue) {
        this.props.onChangeTextValue(value);
      }
    });
  }
  handleOnFocus() {
    this.setState({ focused: true }, () => {
      if (this.props.onFocus) {
        this.props.onFocus();
      }
    });
  }
  handleOnBlur() {
    this.setState({ focused: false }, () => {
      if (this.props.onBlur) {
        this.props.onBlur();
      }
    });
  }
  renderButton() {
    if (this.props.renderButton) {
      return this.props.renderButton();
    }
    return null;
  }
  render() {
    const showFloatingLabel = this.state.focused || this.state.text;
    return (
      <View
        style={[
          styles.container,
          showFloatingLabel ? styles.containerFocused : {},
          this.state.focused ? {} : styles.containerUnfocused,
        ]}
      >
        <View style={styles.wrapper}>
          <FloatingLabel visible={showFloatingLabel}>
            <Text
              style={[
                styles.label,
                this.state.focused ? {} : { color: '#ffffff' },
                this.props.error ? { color: '#ee0000' } : {},
              ]}
            >
              {this.props.error
                ? this.props.errorMessage
                : this.props.placeholder}
            </Text>
          </FloatingLabel>
          <TextFieldWrapper withValue={showFloatingLabel}>
            <TextInput
              {...this.props}
              ref={ref => {
                this.input = ref;
              }}
              placeholder={this.props.placeholder}
              nativeID={this.props.nativeID || null}
              underlineColorAndroid="transparent"
              style={[
                styles.input,
                this.state.text ? { color: '#4e4963' } : {},
                this.state.focused ? {} : { color: '#ffffff' },
              ]}
              placeholderTextColor={
                this.state.focused ? 'transparent' : '#fefefe'
              }
              defaultValue={this.props.defaultValue}
              value={this.state.text}
              maxLength={this.props.maxLength}
              onFocus={this.handleOnFocus}
              onBlur={this.handleOnBlur}
              onChangeText={this.handleOnChangeText}
              selectionColor={showFloatingLabel ? '#4e4963' : '#fefefe'}
            />
          </TextFieldWrapper>
        </View>
        {this.renderButton()}
      </View>
    );
  }
}
