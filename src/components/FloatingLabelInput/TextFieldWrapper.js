import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Animated } from 'react-native';

export default class TextFieldHolder extends Component {
  static displayName = 'TextFieldHolder';
  static propTypes = {
    withValue: PropTypes.bool,
    children: PropTypes.node,
  };
  constructor(props) {
    super(props);
    this.state = {
      marginAnim: new Animated.Value(this.props.withValue ? 14 : 0),
    };
  }
  componentWillReceiveProps(nextProps) {
    Animated.timing(this.state.marginAnim, {
      toValue: nextProps.withValue ? 14 : 0,
      duration: 200,
    }).start();
  }
  render() {
    return (
      <Animated.View style={{ marginTop: this.state.marginAnim }}>
        {this.props.children}
      </Animated.View>
    );
  }
}
