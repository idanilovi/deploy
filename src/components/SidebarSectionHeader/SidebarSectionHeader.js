import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 32,
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: 'transparent',
  },
  text: {
    fontSize: 18,
    fontStyle: 'normal',
    fontWeight: '600',
    color: '#ffffff',
  },
});

export default class SidebarSectionHeader extends PureComponent {
  static displayName = 'SidebarSectionHeader'
  static propTypes = {
    title: PropTypes.string,
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {this.props.title}
        </Text>
      </View>
    );
  }
}
