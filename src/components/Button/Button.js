import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
  button: {
    flex: 0,
    borderRadius: 8,
    paddingVertical: 8,
    paddingHorizontal: 20,
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    fontWeight: '300',
    color: '#ffffff',
  },
});

export default class Button extends PureComponent {
  static displayName = 'Button';
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    styles: PropTypes.object,
    disabled: PropTypes.bool,
    renderIcon: PropTypes.func,
    activeOpacity: PropTypes.number,
  };
  static defaultProps = {
    styles: {},
    disabled: false,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }
  handleOnPress() {
    if (this.props.onPress) {
      this.props.onPress();
    }
    return null;
  }
  renderIcon() {
    if (this.props.renderIcon) {
      return this.props.renderIcon();
    }
    return null;
  }
  render() {
    return (
      <TouchableOpacity
        activeOpacity={this.props.activeOpacity || 0.2}
        nativeID={this.props.nativeID || null}
        disabled={this.props.disabled}
        style={[
          styles.button,
          this.props.styles.button,
          this.props.disabled && this.props.styles.disabled,
        ]}
        onPress={this.handleOnPress}
      >
        {this.renderIcon()}
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <Text style={[styles.title, this.props.styles.title]}>
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
