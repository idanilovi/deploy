import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Platform } from 'react-native';
import { CustomSegmentedControl } from 'react-native-custom-segmented-control';

export default class SegmentedTabs extends Component {
  static displayName = 'SegmentedTabs';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    streamId: PropTypes.string,
    title: PropTypes.string,
    selected: PropTypes.number,
  };
  constructor(props) {
    super(props);
    this.onSelectedWillChange = this.onSelectedWillChange.bind(this);
    this.onSelectedDidChange = this.onSelectedDidChange.bind(this);
  }
  onSelectedWillChange(event) {}
  onSelectedDidChange(event) {
    let pushScreen = null;
    switch (event.nativeEvent.selected) {
      case 0: {
        pushScreen = 'workonflow.StreamBoard';
        break;
      }
      case 1: {
        pushScreen = 'workonflow.StreamChat';
        break;
      }
      case 3: {
        pushScreen = 'workonflow.StreamList';
        break;
      }
      default: {
        pushScreen = 'workonflow.StreamList';
        break;
      }
    }
    this.props.navigator.push({
      screen: pushScreen,
      passProps: {
        streamId: this.props.streamId,
        type: 'Stream',
        title: this.props.title,
        selectedTab: event.nativeEvent.selected,
      },
      animated: false,
      backButtonHidden: true,
      navigatorButtons: {
        leftButtons: [
          {
            title: '< Back',
            id: 'back',
            testID: 'e2e_rules',
            disabled: false,
            disableIconTint: true,
          },
        ],
      },
    });
  }
  render() {
    if (Platform.OS === 'ios') {
      return (
        <CustomSegmentedControl
          style={{
            flex: 0,
            backgroundColor: 'transparent',
            padding: 24,
          }}
          textValues={['Board', 'Chat', 'List']}
          selected={this.props.selected}
          segmentedStyle={{
            selectedLineHeight: 2,
            fontSize: 24,
            fontWeight: 'bold',
            segmentBackgroundColor: 'transparent',
            segmentTextColor: '#7a92a5',
            segmentHighlightTextColor: '#7a92a599',
            selectedLineColor: '#00adf5',
            selectedLineAlign: 'bottom',
            selectedLineMode: 'text',
            selectedTextColor: 'black',
            selectedLinePaddingWidth: 30,
            segmentFontFamily: 'system-font-bold',
          }}
          animation={{
            duration: 0.7,
            damping: 0.5,
            animationType: 'default',
            initialDampingVelocity: 0.4,
          }}
          onSelectedWillChange={this.onSelectedWillChange}
          onSelectedDidChange={this.onSelectedDidChange}
        />
      );
    }
    return null;
  }
}
