/* eslint-disable global-require */
import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  Text,
  View,
  Platform,
} from 'react-native';
import UserStatusIcon from '../UserStatusIcon';
import Icon from '../Icon';
import global from '../../global';

const styles = StyleSheet.create({
  item: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    alignItems: 'flex-start',
    height: 66,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'hsla(0,0%,100%,.3)',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: 'hsla(0,0%,100%,.3)',
  },
  itemText: {
    fontSize: 17,
    fontWeight: '100',
    color: '#ffffff',
  },
});

export default class SidebarListItem extends Component {
  static displayName = 'SidebarListItem';
  static propTypes = {
    item: PropTypes.object,
    id: PropTypes.string,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
  };
  constructor(props) {
    super(props);
    this.state = { disabled: false };
    this.preventButtonOnPress = this.preventButtonOnPress.bind(this);
    this.renderTitle = this.renderTitle.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.state = {
      item: this.props.item,
      id: this.props.id,
      update: false,
    };
  }
  componentWillReceiveProps(nextprops) {
    if (!R.equals(this.props.item, nextprops.item)) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  preventButtonOnPress() {
    this.setState({ disabled: true });
    return setTimeout(() => this.setState({ disabled: false }), 200);
  }
  handleOnPress() {
    global.tracker.trackEvent('Нажатия на кнопочки', 'stream');
    // TODO блокирует переход по стримам,
    // делалось чтобы не обрабатывались повторные нажатия
    // this.preventButtonOnPress();
    const { item } = this.props;
    if (Object.prototype.hasOwnProperty.call(item, 'name')) {
      this.props.navigator.push({
        screen: 'workonflow.Stream',
        passProps: {
          streamId: R.prop('id', item),
          type: 'Stream',
          title: R.prop('name', item),
        },
      });
    }
    if (Object.prototype.hasOwnProperty.call(item, 'basicData')) {
      this.props.navigator.push({
        screen: 'workonflow.Direct',
        passProps: {
          contactId: R.prop('_id', item),
          title: R.path(['basicData', 'name'], item),
          type: 'Direct',
        },
      });
    }
  }
  renderTitle() {
    const { item } = this.props;
    if (Object.prototype.hasOwnProperty.call(item, 'name')) {
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}
        >
          <View style={{ paddingHorizontal: 12 }}>
            <Icon
              name="MainStreams"
              width={37}
              height={37}
              fill={R.propOr('#ffffff', 'itemColor', item)}
              viewBox="0 0 4721 4721"
            />
          </View>
          <Text
            style={[
              styles.itemText,
              { color: R.propOr('#ffffff', 'itemColor', item) },
            ]}
            ellipsizeMode="tail"
            numberOfLines={1}
          >
            {item.name}
          </Text>
        </View>
      );
    }
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <UserStatusIcon
          status={R.prop('status', item)}
          billingType={R.prop('billingType', item)}
        />
        <Text style={styles.itemText} ellipsizeMode="tail" numberOfLines={1}>
          {R.path(['basicData', 'name'], item)}
        </Text>
      </View>
    );
  }
  render() {
    return (
      <TouchableHighlight
        // key={this.props.item.id || this.props.item._id}
        style={styles.item}
        underlayColor="rgba(40,30,42,0.7)"
        disabled={this.state.disabled}
        onPress={this.handleOnPress}
      >
        {this.renderTitle()}
      </TouchableHighlight>
    );
  }
}
