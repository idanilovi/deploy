import React, { PureComponent } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import ContactIcon from '../ContactIcon';
import ContactInfo from '../ContactInfo';

const styles = StyleSheet.create({
  header: {
    flex: 0,
    height: 140,
    backgroundColor: 'rgba(0, 206, 121, 1)',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingTop: 30,
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 30,
    ...ifIphoneX(
      {
        paddingTop: 64,
        height: 174,
      },
      {
        paddingTop: 20,
      },
    ),
  },
  wrapper: {
    flex: 0,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  icon: { paddingBottom: 13 },
});

class SidebarHeader extends PureComponent {
  static displayName = 'SidebarHeader';
  static propTypes = {
    id: PropTypes.string,
    color: PropTypes.string,
    isAdmin: PropTypes.bool,
  };
  render() {
    const { id, isAdmin, color } = this.props;
    return (
      <View style={[styles.header, { backgroundColor: color }]}>
        <ContactInfo id={id} />
        <View style={styles.wrapper}>
          {isAdmin ? (
            <Icon
              name="ios-construct"
              size={16}
              color="#ffffff"
              style={styles.icon}
            />
          ) : null}
          <ContactIcon
            styles={{
              container: {
                width: 52,
                height: 52,
                borderRadius: 26,
                backgroundColor: 'rgba(70, 63,85, 1)',
              },
              text: {
                fontSize: 23,
                color: '#ffffff',
              },
            }}
            id={id}
            show={true}
            showAdminIcon={true}
          />
        </View>
      </View>
    );
  }
}

export default SidebarHeader;
