import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableHighlight, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  title: {
    fontSize: 18,
    color: '#4e4963',
    fontWeight: '400',
  },
});

class SidebarButton extends PureComponent {
  static displayName = 'SidebarButton';
  static propTypes = {
    show: PropTypes.bool,
    title: PropTypes.string,
    onPress: PropTypes.func,
  };
  render() {
    const { show = true, title, onPress } = this.props;
    if (show) {
      return (
        <TouchableHighlight
          style={styles.button}
          onPress={onPress}
          underlayColor="#f5f5f5"
        >
          <Text style={styles.title}>{title}</Text>
        </TouchableHighlight>
      );
    }
    return null;
  }
}

export default SidebarButton;
