import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});

class SidebarContent extends Component {
  static displayName = 'SidebarContent';
  static propTypes = {
    children: PropTypes.node,
  };
  render() {
    return (
      <ScrollView style={styles.content}>{this.props.children}</ScrollView>
    );
  }
}

export default SidebarContent;
