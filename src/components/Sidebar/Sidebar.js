import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import SidebarButton from './SidebarButton';
import SidebarContent from './SidebarContent';
import SidebarHeader from './SidebarHeader';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  sidebar: {
    width: width * 0.8,
    height,
  },
});

class Sidebar extends Component {
  static displayName = 'Sidebar';
  static propTypes = { children: PropTypes.node };
  static Header = SidebarHeader;
  static Content = SidebarContent;
  static Button = SidebarButton;
  render() {
    return <View style={styles.sidebar}>{this.props.children}</View>;
  }
}

export default Sidebar;
