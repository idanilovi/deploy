import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Dimensions, ImageBackground, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  image: { flex: 1, width, height, backgroundColor: 'rgb(232,204,215)' },
});

export default class BackgroundImage extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    source: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  };
  render() {
    return (
      <ImageBackground
        source={this.props.source}
        style={styles.image}
        resizeMode="cover"
      >
        {this.props.children}
      </ImageBackground>
    );
  }
}
