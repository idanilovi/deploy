import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { ThreadListEntry } from '../../components';

export default class ThreadList extends Component {
  static displayName = 'ThreadList';
  static propTypes = {
    navigator: PropTypes.object,
    items: PropTypes.array,
    showIcons: PropTypes.bool,
  };
  static defaultProps = {
    styles: {},
  };
  constructor(props) {
    super(props);
    this.renderItem = this.renderItem.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item, index) {
    return item || index;
  }
  renderItem({ item }) {
    return (
      <ThreadListEntry
        key={item}
        id={item}
        styles={this.props.styles.itemStyles}
        navigator={this.props.navigator}
        showIcons={this.props.showIcons}
      />
    );
  }
  render() {
    return (
      <FlatList
        style={[
          { backgroundColor: '#f3eff2', flex: 1 },
          this.props.styles.container,
        ]}
        data={R.reverse(this.props.items)}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
      />
    );
  }
}
