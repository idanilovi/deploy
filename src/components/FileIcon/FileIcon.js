import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import ThreadAvatarSvg from '../Icon/ThreadAvatarSvg';

const styles = StyleSheet.create({
  fileIcon: {
    width: 50,
    height: 50,
    borderRadius: 3,
    backgroundColor: '#0099ff',
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fileIconText: {
    fontSize: 26,
    fontWeight: '200',
    color: '#ffffff',
    paddingTop: 5,
  },
});

export default class FileIcon extends PureComponent {
  static displayName = 'FileIcon';
  static propTypes = {
    filename: PropTypes.string,
    styles: PropTypes.shape({
      fileIcon: PropTypes.object,
      fileIconText: PropTypes.object,
    }),
  };
  static defaultProps = {
    styles: {},
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
    this.renderFileExtension = this.renderFileExtension.bind(this);
    this.download = this.download.bind(this);
  }
  renderFileExtension() {
    return (
      R.prop('filename', this.props)
        .split('.')
        .reverse()[0]
        .toUpperCase() || '?'
    );
  }
  download() {
    this.setState({
      loading: true,
    });
    this.props.getFilesUrl({ id: this.props.fileId, force: true });
  }
  render() {
    if (this.props.download) {
      return (
        <View style={[styles.fileIcon, this.props.styles.fileIcon]}>
          <TouchableOpacity
            style={{
              width: 45,
              height: 45,
              borderWidth: 1,
              borderRadius: 50,
              borderColor: '#0099ff',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={this.download}
          >
            <Text style={[styles.fileIconText, this.props.styles.fileIconText]}>
              {'^'}
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <View style={[styles.fileIcon, this.props.styles.fileIcon]}>
        {
          <Text style={[styles.fileIconText, this.props.styles.fileIconText]}>
            {this.renderFileExtension()}
          </Text>
        }
        {this.state.loading && (
          <ThreadAvatarSvg
            name="threadAvatar"
            width={29}
            height={29}
            fill={'#0099ff'}
            dasharray={'60, 100'}
            viewBox="0 0 36 36"
          />
        )}
      </View>
    );
  }
}
