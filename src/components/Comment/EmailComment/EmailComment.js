import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from '../../Icon';
import { TimeBlock } from '../CommentBlocks';
import CommentAttachments from '../CommentAttachments';
import ContactIcon from '../../ContactIcon';

const commentContactSelector = (state, ownProps) =>
  R.prop(R.prop('from', ownProps), R.prop('contacts', state));

const commentThreadSelector = (state, ownProps) =>
  R.prop(R.prop('threadId', ownProps), R.prop('threads', state));

const mapStateToProps = (state, ownProps) => ({
  thread: commentThreadSelector(state, ownProps),
  contact: commentContactSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class EmailComment extends Component {
  static displayName = 'EmailComment';
  static propTypes = {
    contact: PropTypes.object,
    showAvatar: PropTypes.bool,
    metadata: PropTypes.object,
    createdAt: PropTypes.number,
    att: PropTypes.arrayOf(PropTypes.object),
    thread: PropTypes.shape({
      title: PropTypes.string,
    }),
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderAttachments = this.renderAttachments.bind(this);
    this.renderComment = this.renderComment.bind(this);
    this.renderSubject = this.renderSubject.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
  }
  handleOnPress() {
    const {
      metadata: { to, from, subject },
      att,
      thread: { title } = {},
    } = this.props;
    const { data: { html, text } = {} } = att
      .filter(_ => _.type === 'mail')
      .shift();
    Navigation.showModal({
      screen: 'workonflow.Email',
      passProps: {
        to,
        from,
        subject,
        html,
        text,
        title,
      },
    });
  }
  renderHeader() {
    return (
      <Text
        // selectable={true}
        style={{
          color: '#4e4963',
          fontWeight: '300',
          fontSize: 15,
          alignItems: 'flex-start',
          paddingBottom: 7,
          paddingHorizontal: 0,
        }}
      >
        <Text
          style={{
            color: this.props.isUserMessage ? '#00c69e' : '#4e4963',
            fontWeight: '300',
          }}
        >
          {R.pathOr('', ['basicData', 'name'], this.props.contact)}
        </Text>
        {this.props.viewType !== 'Thread' && (
          <Text
            style={{ color: '#c4c2cf', fontWeight: '200', fontStyle: 'italic' }}
          >
            {` send email in ${R.propOr('', 'title', this.props.thread)}`}
          </Text>
        )}
      </Text>
    );
  }
  renderSubject() {
    return (
      <Text
        style={{
          fontSize: 15,
          fontWeight: '300',
          color: '#4e4963',
        }}
      >
        {R.prop('subject', this.props.metadata)}
      </Text>
    );
  }
  renderAttachments() {
    return (
      <CommentAttachments
        viewType={this.props.viewType}
        attachments={R.filter(
          att => att.type !== 'image' && att.type !== 'linkPreview',
          R.propOr([], 'att', this.props),
        )}
        filesContext={this.props.filesContext}
        isUserMessage={this.props.isUserMessage}
        onLongPress={this.props.onLongPress}
      />
    );
  }
  renderComment() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          borderRadius: 10,
          backgroundColor: '#f3eff3',
        }}
      >
        <View
          style={{
            borderBottomLeftRadius: 10,
            borderTopLeftRadius: 10,
            width: 6,
            backgroundColor: '#f9e400',
          }}
        />
        <TouchableWithoutFeedback
          style={{ flex: 1 }}
          onPress={this.handleOnPress}
        >
          <View style={{ flex: 1, paddingVertical: 8, paddingHorizontal: 8 }}>
            {this.renderSubject()}
            {this.renderAttachments()}
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'flex-end',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <Icon
                name="Mail"
                fill="#bfbccc"
                width={14}
                height={14}
                viewBox="0 0 32 32"
              />
              <TimeBlock
                style={{
                  paddingLeft: 8,
                  color: '#c4c2cf',
                  fontWeight: '200',
                }}
                format="hh:mm"
                value={this.props.createdAt}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
  renderAvatar() {
    return (
      <ContactIcon
        show={true}
        id={R.propOr(null, '_id', this.props.contact)}
        // name={R.pathOr('', ['basicData', 'name'], this.props.contact)}
        // color={R.propOr('#ffffff', 'color', this.props.contact)}
        // type={R.propOr(null, 'billingType', this.props.contact)}
      />
    );
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ paddingRight: 8 }}>{this.renderAvatar()}</View>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          {this.renderHeader()}
          {this.renderComment()}
        </View>
      </View>
    );
  }
}
