import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Icon from '../../Icon';
import { TimeBlock } from '../CommentBlocks';
import CommentAttachments from '../CommentAttachments';
import ContactIcon from '../../ContactIcon';

const styles = StyleSheet.create({
  comment: {
    flexDirection: 'column',
    borderLeftColor: '#f9e400',
    borderLeftWidth: 4,
    borderStyle: 'solid',
    padding: 4,
  },
  subject: { fontSize: 14, fontWeight: '600' },
});

const commentContactSelector = (state, ownProps) =>
  R.prop(R.prop('from', ownProps), R.prop('contacts', state));

const commentThreadSelector = (state, ownProps) =>
  R.prop(R.prop('threadId', ownProps), R.prop('threads', state));

const mapStateToProps = (state, ownProps) => ({
  thread: commentThreadSelector(state, ownProps),
  contact: commentContactSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class TelegramComment extends Component {
  static displayName = 'TelegramComment';
  static propTypes = {
    att: PropTypes.array,
    contact: PropTypes.object,
    showAvatar: PropTypes.bool,
    metadata: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.renderAvatar = this.renderAvatar.bind(this);
    this.renderComment = this.renderComment.bind(this);
    this.renderSubject = this.renderSubject.bind(this);
    this.renderAttachments = this.renderAttachments.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
  }
  renderHeader() {
    return (
      <Text
        style={{
          color: '#4e4963',
          fontWeight: '300',
          fontSize: 15,
          alignItems: 'flex-start',
          paddingBottom: 7,
          paddingHorizontal: 0,
        }}
      >
        <Text
          style={{
            color: this.props.isUserMessage ? '#00c69e' : '#4e4963',
            fontWeight: '300',
          }}
        >
          {R.pathOr('', ['basicData', 'name'], this.props.contact)}
        </Text>
        {this.props.viewType !== 'Thread' && (
          <Text
            style={{ color: '#c4c2cf', fontWeight: '200', fontStyle: 'italic' }}
          >
            {` send telegram message in ${R.propOr(
              '',
              'title',
              this.props.thread,
            )}`}
          </Text>
        )}
      </Text>
    );
  }
  renderSubject() {
    return (
      <Text style={styles.subject}>
        {R.prop('subject', this.props.metadata)}
      </Text>
    );
  }
  renderAttachments() {
    return (
      <CommentAttachments
        viewType={this.props.viewType}
        attachments={R.filter(
          att => att.type !== 'image' && att.type !== 'linkPreview',
          R.propOr([], 'att', this.props),
        )}
        filesContext={this.props.filesContext}
        isUserMessage={this.props.isUserMessage}
        onLongPress={this.props.onLongPress}
      />
    );
  }
  renderComment() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          borderRadius: 10,
          backgroundColor: '#f3eff3',
        }}
      >
        <View
          style={{
            borderBottomLeftRadius: 10,
            borderTopLeftRadius: 10,
            width: 6,
            backgroundColor: '#f9e400',
          }}
        />
        <View style={{ flex: 1, paddingVertical: 8, paddingHorizontal: 8 }}>
          {this.renderSubject()}
          {this.renderAttachments()}
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'flex-end',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Icon
              name="Telegram"
              fill="#bfbccc"
              width={14}
              height={14}
              viewBox="0 0 939 787"
            />
            <TimeBlock
              style={{
                paddingLeft: 8,
                color: '#c4c2cf',
                fontWeight: '200',
              }}
              format="hh:mm"
              value={this.props.createdAt}
            />
          </View>
        </View>
      </View>
    );
  }
  renderAvatar() {
    return (
      <ContactIcon
        show={true}
        id={R.propOr(null, '_id', this.props.contact)}
        // name={R.path(['basicData', 'name'], this.props.contact)}
        // color={R.prop('color', this.props.contact)}
        // type={R.propOr(null, 'billingType', this.props.contact)}
      />
    );
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ paddingRight: 8 }}>{this.renderAvatar()}</View>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          {this.renderHeader()}
          {this.renderComment()}
        </View>
      </View>
    );
  }
}
