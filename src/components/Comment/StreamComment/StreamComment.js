import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import ContactIcon from '../../ContactIcon';
import { TimeBlock } from '../CommentBlocks';
import CommentAttachments from '../CommentAttachments';

const commentContactSelector = (state, ownProps) =>
  R.prop(R.prop('from', ownProps), R.prop('contacts', state));

const mapStateToProps = (state, ownProps) => ({
  contact: commentContactSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class StreamComment extends Component {
  static displayName = 'StreamComment';
  static propTypes = {
    att: PropTypes.array,
    contact: PropTypes.object,
    showAvatar: PropTypes.bool,
    onLongPress: PropTypes.func,
    isUserMessage: PropTypes.bool,
    isSameUser: PropTypes.shape({
      isSameTimePrev: PropTypes.bool,
      isSameUserPrev: PropTypes.bool,
    }),
    navigator: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.renderAvatar = this.renderAvatar.bind(this);
    this.renderComment = this.renderComment.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
  }
  renderHeader() {
    const { isSameUserPrev, isSameTimePrev } = this.props.isSameUser;
    if (isSameUserPrev && isSameTimePrev) {
      return null;
    }
    return (
      <Text
        selectable={true}
        style={{
          color: this.props.isUserMessage ? '#00c69e' : '#4e4963',
          fontWeight: '300',
          fontSize: 15,
          alignItems: 'flex-start',
          // paddingTop: 4,
          paddingBottom: 7,
          paddingHorizontal: 0,
        }}
      >
        {R.pathOr('', ['basicData', 'name'], this.props.contact)}
      </Text>
    );
  }
  renderComment() {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          borderRadius: 10,
          backgroundColor: '#f3eff3',
          paddingVertical: 8,
          paddingHorizontal: 8,
          flexWrap: 'wrap',
        }}
      >
        <CommentAttachments
          viewType={this.props.viewType}
          filesContext={this.props.filesContext}
          attachments={R.filter(
            att => att.type !== 'image' && att.type !== 'linkPreview',
            R.propOr([], 'att', this.props),
          )}
          isUserMessage={this.props.isUserMessage}
          onLongPress={this.props.onLongPress}
          navigator={this.props.navigator}
        />
        <View
          style={{
            alignSelf: 'flex-end',
            marginLeft: 'auto',
            marginRight: 0,
          }}
        >
          <TimeBlock
            style={{
              color: '#c4c2cf',
              fontWeight: '200',
              paddingLeft: 4,
            }}
            format="HH:mm"
            value={this.props.createdAt}
          />
        </View>
      </View>
    );
  }
  renderAvatar() {
    const { isSameUserPrev, isSameTimePrev } = this.props.isSameUser;
    if (isSameUserPrev && isSameTimePrev) {
      return (
        <View
          style={{
            width: 29,
            height: 29,
          }}
        />
      );
    }
    return (
      <ContactIcon
        show
        id={R.propOr(null, '_id', this.props.contact)}
        // name={R.pathOr('', ['basicData', 'name'], this.props.contact)}
        // color={R.propOr('#ffffff', 'color', this.props.contact)}
        // type={R.propOr(null, 'billingType', this.props.contact)}
      />
    );
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'nowrap' }}>
        <View style={{ paddingRight: 8 }}>{this.renderAvatar()}</View>
        <View style={{ flexGrow: 0, flexShrink: 1, flexDirection: 'column' }}>
          {this.renderHeader()}
          {this.renderComment()}
        </View>
      </View>
    );
  }
}
