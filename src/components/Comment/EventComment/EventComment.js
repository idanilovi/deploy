import PropTypes from 'prop-types';
import R from 'ramda';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { InlineBlock, TimeBlock } from '../CommentBlocks';
import {
  contactsSelector,
  statusesSelector,
  streamsSelector,
  threadsSelector,
} from '../../../selectors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  event: {
    fontSize: 15,
    color: '#c4c2cf',
    fontWeight: '200',
    fontStyle: 'italic',
  },
});

function mapStateToProps(state, ownProps) {
  return {
    ...ownProps,
    streams: streamsSelector(state),
    contacts: contactsSelector(state),
    statuses: statusesSelector(state),
    threads: threadsSelector(state),
  };
}

@connect(mapStateToProps)
export default class EventComment extends Component {
  static displayName = 'EventComment';
  static propTypes = {
    createdAt: PropTypes.number,
    channel: PropTypes.string,
    contacts: PropTypes.object,
    metadata: PropTypes.shape({
      deadline: PropTypes.array,
      title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      userId: PropTypes.string,
      statusId: PropTypes.string,
      responsibleUserId: PropTypes.string,
      roleId: PropTypes.string,
    }),
    statuses: PropTypes.object,
    streams: PropTypes.object,
    streamId: PropTypes.string,
    task: PropTypes.bool,
    threadId: PropTypes.string,
    threads: PropTypes.object,
    type: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.renderTime = this.renderTime.bind(this);
    this.renderEventBlock = this.renderEventBlock.bind(this);
  }
  renderEventBlock() {
    const {
      type,
      streams,
      metadata,
      contacts,
      statuses,
      threads,
      channel,
    } = this.props;
    if (type === 'setStatus') {
      const { userId, statusId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const status = R.propOr({}, statusId, statuses); // eslint-disable-line no-shadow
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" set status " />
          <InlineBlock
            value={R.prop('name', status)}
            style={{
              color: R.propOr('#000000', 'color', status),
              fontWeight: '200',
              fontStyle: 'italic',
              fontSize: 15,
            }}
          />
        </Text>
      );
    }
    if (type === 'removeRole') {
      const { userId, roleId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const role = R.propOr({}, roleId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" delete " />
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], role)}
          />
          <InlineBlock style={styles.event} value=" from followers " />
        </Text>
      );
    }
    if (type === 'removeResponsible') {
      const { userId, roleId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const role = R.propOr({}, roleId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" delete " />
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], role)}
          />
          <InlineBlock style={styles.event} value=" from responsible " />
        </Text>
      );
    }
    if (type === 'setResponsibleUser') {
      const { userId, responsibleUserId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const responsibleUser = R.propOr({}, responsibleUserId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" set " />
          <InlineBlock
            style={styles.event}
            value={R.propOr('', ['basicData', 'name'], responsibleUser)}
          />
          <InlineBlock style={styles.event} value=" as responsible user " />
        </Text>
      );
    }
    if (type === 'removeResponsibleUser') {
      const { userId, responsibleUserId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const responsibleUser = R.propOr({}, responsibleUserId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" remove " />
          <InlineBlock
            style={styles.event}
            value={R.propOr('', ['basicData', 'name'], responsibleUser)}
          />
          <InlineBlock style={styles.event} value=" from responsible user " />
        </Text>
      );
    }
    if (type === 'setTitle') {
      const { userId, title } = metadata;
      const user = R.propOr({}, userId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" set thread title to " />
          <InlineBlock style={styles.event} value={`"${title}"`} />
        </Text>
      );
    }
    if (type === 'setRole') {
      const { userId, roleId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const role = R.propOr({}, roleId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" add " />
          <InlineBlock
            style={styles.event}
            value={R.propOr('', ['basicData', 'name'], role)}
          />
          <InlineBlock style={styles.event} value=" to followers " />
        </Text>
      );
    }
    if (type === 'setDeadline') {
      const { userId, deadline } = metadata;
      const text = deadline[0] == null ? ' set deadline ' : ' set period ';
      const user = R.propOr({}, userId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value={text} />
          <TimeBlock format="DD MMMM hh:mm" value={deadline} />
        </Text>
      );
    }
    if (type === 'create') {
      const { userId, threadId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const thread = R.propOr({}, threadId, threads);
      if (thread) {
        return (
          <Text selectable={true} style={styles.container}>
            <InlineBlock
              style={styles.event}
              value={R.pathOr('', ['basicData', 'name'], user)}
            />
            <InlineBlock style={styles.event} value=" create thread " />
            <InlineBlock
              style={styles.event}
              value={R.propOr('', 'title', thread)}
            />
          </Text>
        );
      }
      return null;
    }
    if (type === 'setRank') {
      const { userId, mainRank } = metadata;
      const user = R.propOr({}, userId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" set points to " />
          <InlineBlock style={styles.event} value={mainRank} />
        </Text>
      );
    }
    if (type === 'setBudget') {
      const { userId, budget } = metadata;
      const user = R.propOr({}, userId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" set budget to " />
          <InlineBlock style={styles.event} value={`${budget} ₽`} />
        </Text>
      );
    }
    if (type === 'setPriority') {
      const { userId, priority } = metadata;
      const user = R.propOr({}, userId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" set priority to " />
          <InlineBlock style={styles.event} value={priority} />
        </Text>
      );
    }
    if (type === 'setResource') {
      const { userId, resource } = metadata;
      const user = R.propOr({}, userId, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" set resource to " />
          <InlineBlock style={styles.event} value={resource} />
          <InlineBlock style={styles.event} value=" hours " />
        </Text>
      );
    }
    if (type === 'addChild') {
      const { userId, childId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const childThread = R.propOr({}, childId, threads);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" create subthread " />
          <InlineBlock
            style={styles.event}
            value={R.prop('title', childThread)}
          />
        </Text>
      );
    }
    if (type === 'removeChild') {
      const { userId, childId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const childThread = R.propOr({}, childId, threads);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" remove subtask " />
          <InlineBlock
            style={styles.event}
            value={R.prop('title', childThread)}
          />
        </Text>
      );
    }
    // Начало блока комментов стрима
    if (type === 'giveAdmin') {
      const { userId, user } = metadata;
      const who = R.propOr({}, userId, contacts);
      const whom = R.propOr({}, user, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], who)}
          />
          <InlineBlock
            style={styles.event}
            value=" gave administrator rights to "
          />
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], whom)}
          />
        </Text>
      );
    }
    if (type === 'revokeAdmin') {
      const { userId, user } = metadata;
      const who = R.propOr({}, userId, contacts);
      const whom = R.propOr({}, user, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], who)}
          />
          <InlineBlock
            style={styles.event}
            value=" took administrator rights from "
          />
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], whom)}
          />
        </Text>
      );
    }
    if (type === 'setStreamRole') {
      const { userId, user } = metadata;
      const who = R.propOr({}, userId, contacts);
      const whom = R.propOr({}, user, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], who)}
          />
          <InlineBlock style={styles.event} value=" invited " />
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], whom)}
          />
          <InlineBlock style={styles.event} value=" to this stream " />
        </Text>
      );
    }
    if (type === 'deleteStreamRole') {
      const { userId, user } = metadata;
      const who = R.propOr({}, userId, contacts);
      const whom = R.propOr({}, user, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], who)}
          />
          <InlineBlock style={styles.event} value=" removed " />
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], whom)}
          />
          <InlineBlock style={styles.event} value=" from this stream " />
        </Text>
      );
    }
    if (type === 'mailAccountConnection') {
      const { status } = metadata;
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock style={styles.event} value="email: " />
          <InlineBlock style={styles.event} value={channel} />
          <InlineBlock
            style={styles.event}
            value={
              status
                ? ' successfully connected '
                : ' disconnected, please check '
            }
          />
        </Text>
      );
    }
    if (type === 'askAccessToStream') {
      const { userId, streamId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const stream = R.propOr({}, streamId, streams);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock
            style={styles.event}
            value=" send request access to stream "
          />
          <InlineBlock style={styles.event} value={R.prop('name', stream)} />
        </Text>
      );
    }
    if (type === 'askAccessToThread') {
      const { userId, threadId } = metadata; // eslint-disable-line no-shadow
      const user = R.propOr({}, userId, contacts);
      const thread = R.propOr({}, threadId, threads);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock
            style={styles.event}
            value=" send request access to thread in "
          />
          <InlineBlock style={styles.event} value={R.prop('title', thread)} />
        </Text>
      );
    }
    if (type === 'accessDenied') {
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock style={styles.event} value=" Access denied " />
        </Text>
      );
    }
    if (type === 'setStream') {
      const { userId, streamId, prevStreamId } = metadata;
      const user = R.propOr({}, userId, contacts);
      const stream = R.propOr({}, streamId, streams);
      const prevStream = R.propOr({}, prevStreamId, streams);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock style={styles.event} value=" change stream from " />
          <InlineBlock
            style={styles.event}
            value={R.prop('name', prevStream)}
          />
          <InlineBlock style={styles.event} value=" to " />
          <InlineBlock style={styles.event} value={R.prop('name', stream)} />
        </Text>
      );
    }
    if (type === 'createContact') {
      const { userId, id, billingType } = metadata;
      const user = R.propOr({}, userId, contacts);
      const contact = R.propOr({}, id, contacts);
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], user)}
          />
          <InlineBlock
            style={styles.event}
            value={
              billingType === 'users' ? ' create user ' : ' create customer '
            }
          />
          <InlineBlock
            style={styles.event}
            value={R.pathOr('', ['basicData', 'name'], contact)}
          />
        </Text>
      );
    }
    if (type === 'connectionUrl') {
      const { userIds, livechatId, url } = metadata;
      return (
        <Text selectable={true} style={styles.container}>
          <InlineBlock style={styles.event} value="connected " />
          <InlineBlock style={styles.event} value={url} />
          <InlineBlock style={styles.event} value=" via livechat for onpbx" />
        </Text>
      );
    }
    return null;
  }
  renderTime() {
    return (
      <TimeBlock
        style={{
          fontSize: 15,
          color: '#c4c2cf',
          fontWeight: '200',
          fontStyle: 'italic',
        }}
        format="HH:MM"
        value={this.props.createdAt}
      />
    );
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View
          style={{
            justifyContent: 'center',
            width: 44,
            alignSelf: 'flex-start',
          }}
        >
          {this.renderTime()}
        </View>
        <View style={{ flex: 1 }}>{this.renderEventBlock()}</View>
      </View>
    );
  }
}
