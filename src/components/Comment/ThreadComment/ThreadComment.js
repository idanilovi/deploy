import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import CommentAttachments from '../CommentAttachments';
import { TimeBlock } from '../CommentBlocks';
import ContactIcon from '../../ContactIcon';

const commentContactSelector = (state, ownProps) =>
  R.prop(R.path(['metadata', 'userId'], ownProps), R.prop('contacts', state));

const commentThreadSelector = (state, ownProps) =>
  R.prop(R.prop('threadId', ownProps), R.prop('threads', state));

const mapStateToProps = (state, ownProps) => ({
  contact: commentContactSelector(state, ownProps),
  thread: commentThreadSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class ThreadComment extends Component {
  static displayName = 'ThreadComment';
  static propTypes = {
    att: PropTypes.array,
    contact: PropTypes.object,
    thread: PropTypes.object,
    inThread: PropTypes.bool,
    isUserMessage: PropTypes.bool,
    isSameUser: PropTypes.shape({
      isSameTimePrev: PropTypes.bool,
      isSameUserPrev: PropTypes.bool,
    }),
    navigator: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderComment = this.renderComment.bind(this);
    this.renderAvatar = this.renderAvatar.bind(this);
  }
  renderHeader() {
    const { isSameUserPrev, isSameTimePrev } = this.props.isSameUser;
    if (isSameUserPrev && isSameTimePrev) {
      return null;
    }
    return (
      <Text
        selectable={true}
        style={{
          color: '#4e4963',
          fontWeight: '300',
          fontSize: 15,
          alignItems: 'flex-start',
          paddingBottom: 7,
          paddingHorizontal: 0,
        }}
      >
        <Text
          selectable={true}
          style={{
            color: this.props.isUserMessage ? '#00c69e' : '#4e4963',
            fontWeight: '300',
          }}
        >
          {R.pathOr('', ['basicData', 'name'], this.props.contact)}
        </Text>
        <Text
          selectable={true}
          style={{ color: '#c4c2cf', fontWeight: '200', fontStyle: 'italic' }}
        >
          {` add comment in ${R.propOr('', 'title', this.props.thread)}`}
        </Text>
      </Text>
    );
  }
  renderComment() {
    return (
      <View
        style={{
          flexGrow: 0,
          flexShrink: 1,
          flexDirection: 'row',
          borderRadius: 10,
          backgroundColor: '#f3eff3',
          justifyContent: 'space-between',
          flexWrap: 'wrap',
        }}
      >
        <View
          style={{
            borderBottomLeftRadius: 10,
            borderTopLeftRadius: 10,
            width: 6,
            backgroundColor: '#8aceea',
          }}
        />
        <View style={{ flex: 1, paddingVertical: 8, paddingHorizontal: 8 }}>
          <CommentAttachments
            viewType={this.props.viewType}
            filesContext={this.props.filesContext}
            attachments={R.filter(
              att => att.type !== 'image' && att.type !== 'linkPreview',
              R.propOr([], 'att', this.props),
            )}
            // isUserMessage={this.props.isUserMessage}
            onLongPress={this.props.onLongPress}
            navigator={this.props.navigator}
          />
          <TimeBlock
            style={{
              alignSelf: 'flex-end',
              color: '#c4c2cf',
              fontWeight: '200',
            }}
            format="hh:mm"
            value={this.props.createdAt}
          />
        </View>
      </View>
    );
  }
  renderAvatar() {
    const { isSameUserPrev, isSameTimePrev } = this.props.isSameUser;
    if (isSameUserPrev && isSameTimePrev) {
      return (
        <View
          style={{
            width: 29,
            height: 29,
          }}
        />
      );
    }
    return (
      <ContactIcon
        show={true}
        id={R.propOr(null, '_id', this.props.contact)}
        // name={R.path(['basicData', 'name'], this.props.contact)}
        // color={R.prop('color', this.props.contact)}
        // type={R.propOr(null, 'billingType', this.props.contact)}
      />
    );
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ paddingRight: 8 }}>{this.renderAvatar()}</View>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          {this.renderHeader()}
          {this.renderComment()}
        </View>
      </View>
    );
  }
}
