import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import URI from 'urijs';
import {
  View,
  // Text,
  StyleSheet,
  ActionSheetIOS,
  Clipboard,
  Platform,
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import EventComment from './EventComment';
import StreamComment from './StreamComment';
import ThreadComment from './ThreadComment';
import NotifyComment from './NotifyComment';
import EmailComment from './EmailComment';
import CallComment from './CallComment';
import LiveChatComment from './LiveChatComment';
import TelegramComment from './TelegramComment';
import DirectComment from './DirectComment';
import BotComment from './BotComment';
import CommentAttachments from './CommentAttachments';
import { getContact } from '../../actions';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    paddingHorizontal: 8,
  },
});

const mapDispatchToProps = {
  getContact,
};

@connect(null, mapDispatchToProps)
export default class Comment extends Component {
  static displayName = 'Comment';
  static propTypes = {
    comment: PropTypes.object,
    type: PropTypes.string,
    navigator: PropTypes.object,
    readNotification: PropTypes.func,
    isSameUser: PropTypes.shape({
      isSameUserPrev: PropTypes.bool,
      isSameUserNext: PropTypes.bool,
      isSameTimePrev: PropTypes.bool,
      isSameTimeNext: PropTypes.bool,
    }),
    getContact: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.renderComment = this.renderComment.bind(this);
    this.renderActionSheet = this.renderActionSheet.bind(this);
    this.renderImages = this.renderImages.bind(this);
    this.getAdditionalStyles = this.getAdditionalStyles.bind(this);
    this.handleOnLongPress = this.handleOnLongPress.bind(this);
    this.handleActionSheetPress = this.handleActionSheetPress.bind(this);
  }
  componentDidMount() {
    if (R.isEmpty(R.propOr({}, 'user', this.props.comment))) {
      const contactId = R.propOr('', 'from', this.props.comment);
      if (contactId) {
        this.props.getContact(contactId);
      }
    }
  }
  handleOnLongPress() {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: ['Copy text', 'Cancel'],
          cancelButtonIndex: 1,
        },
        this.handleActionSheetPress,
      );
    } else {
      this.ActionSheet.show();
    }
  }
  handleActionSheetPress(index) {
    switch (index) {
      case 0: {
        const text = this.props.comment.att
          .map(attachment => {
            if (attachment.type === 'text') {
              return attachment.data.text;
            }
            if (attachment.type === 'mail') {
              return attachment.data.text;
            }
            return '';
          })
          .join();
        Clipboard.setString(text);
        break;
      }
      default: {
        break;
      }
    }
  }
  renderComment() {
    if (this.props.type === 'Direct') {
      return (
        <DirectComment
          {...this.props.comment}
          type={this.props.type}
          filesContext={this.props.filesContext}
          viewType={this.props.type}
          isUserMessage={this.props.isUserMessage}
          onLongPress={this.handleOnLongPress}
          isSameUser={this.props.isSameUser}
        />
      );
    }
    if (this.props.type === 'notify') {
      return (
        <NotifyComment
          threadId={R.propOr('', 'threadId', this.props.comment)}
          streamId={R.propOr('', 'streamId', this.props.comment)}
          from={R.propOr('', 'from', this.props.comment)}
          _id={R.propOr('', '_id', this.props.comment)}
          headerBody={R.propOr('', 'headerBody', this.props.comment)}
          notifyUrl={R.propOr('', 'notifyUrl', this.props.comment)}
          fromName={R.propOr('', 'fromName', this.props.comment)}
          readAt={R.propOr('', 'readAt', this.props.comment)}
          createdAt={R.propOr('', 'createdAt', this.props.comment)}
          countMessage={R.propOr('', 'countMessage', this.props.comment)}
          messageNotify={R.propOr('', 'messageNotify', this.props.comment)}
          colorStatus={R.propOr('', 'colorStatus', this.props.comment)}
          navigator={this.props.navigator}
          userId={this.props.userId}
        />
      );
    }
    if (this.props.comment.type) {
      if (this.props.comment.type === 'workonflow-bot') {
        return (
          <BotComment
            {...this.props.comment}
            filesContext={this.props.filesContext}
            viewType={this.props.type}
            inThread={
              !!(
                this.props.type === 'Stream' &&
                R.prop('threadId', this.props.comment)
              )
            }
            isUserMessage={this.props.isUserMessage}
            onLongPress={this.handleOnLongPress}
          />
        );
      }
      if (this.props.comment.type === 'mail') {
        return (
          <EmailComment
            filesContext={this.props.filesContext}
            {...this.props.comment}
            viewType={this.props.type}
            inThread={
              !!(
                this.props.type === 'Stream' &&
                R.prop('threadId', this.props.comment)
              )
            }
            isUserMessage={this.props.isUserMessage}
            onLongPress={this.handleOnLongPress}
          />
        );
      }
      if (this.props.comment.type === 'call') {
        return (
          <CallComment
            filesContext={this.props.filesContext}
            {...this.props.comment}
            viewType={this.props.type}
            inThread={
              !!(
                this.props.type === 'Stream' &&
                R.prop('threadId', this.props.comment)
              )
            }
            isUserMessage={this.props.isUserMessage}
          />
        );
      }
      if (this.props.comment.type === 'livechat') {
        return (
          <LiveChatComment
            filesContext={this.props.filesContext}
            {...this.props.comment}
            viewType={this.props.type}
            isUserMessage={this.props.isUserMessage}
            onLongPress={this.handleOnLongPress}
          />
        );
      }
      if (this.props.comment.type === 'telegram') {
        return (
          <TelegramComment
            filesContext={this.props.filesContext}
            {...this.props.comment}
            viewType={this.props.type}
            isUserMessage={this.props.isUserMessage}
            onLongPress={this.handleOnLongPress}
          />
        );
      }
      return <EventComment {...this.props.comment} />;
    }
    if (
      this.props.type === 'Stream' &&
      R.prop('threadId', this.props.comment)
    ) {
      return (
        <ThreadComment
          filesContext={this.props.filesContext}
          {...this.props.comment}
          viewType={this.props.type}
          onLongPress={this.handleOnLongPress}
          isSameUser={this.props.isSameUser}
          isUserMessage={this.props.isUserMessage}
          navigator={this.props.navigator}
        />
      );
    }
    if (this.props.type === 'Thread') {
      return (
        <StreamComment
          filesContext={this.props.filesContext}
          {...this.props.comment}
          viewType={this.props.type}
          showAvatar={true}
          isUserMessage={this.props.isUserMessage}
          isSameUser={this.props.isSameUser}
          onLongPress={this.handleOnLongPress}
          navigator={this.props.navigator}
        />
      );
    }
    return (
      <StreamComment
        filesContext={this.props.filesContext}
        {...this.props.comment}
        type={this.props.type}
        viewType={this.props.type}
        isUserMessage={this.props.isUserMessage}
        isSameUser={this.props.isSameUser}
        onLongPress={this.handleOnLongPress}
        navigator={this.props.navigator}
      />
    );
  }
  getAdditionalStyles() {
    if (
      this.props.type === 'Direct' ||
      (this.props.type === 'Stream' && !this.props.comment.type) ||
      (this.props.type === 'Thread' && !this.props.comment.type)
    ) {
      if (this.props.isSameUser) {
        // TODO
        const {
          isSameUserPrev,
          isSameUserNext,
          isSameTimePrev,
          isSameTimeNext,
        } = this.props.isSameUser;
        return {
          paddingVertical: 0,
          paddingTop:
            isSameTimePrev && isSameUserPrev ? 2 : isSameUserPrev ? 3.5 : 8,
          paddingBottom:
            isSameTimeNext && isSameUserNext ? 2 : isSameUserNext ? 3.5 : 8,
        };
      }
      return { paddingVertical: 16 };
    }
    if (this.props.type === 'notify') {
      return { paddingVertical: 0, paddingHorizontal: 0 };
    }
    return {};
  }
  renderImages() {
    return (
      <View
        style={[
          this.props.type === 'Direct' && this.props.isUserMessage
            ? { alignSelf: 'flex-end' }
            : { marginLeft: 37 },
        ]}
      >
        <CommentAttachments
          viewType={this.props.type}
          filesContext={this.props.filesContext}
          isThreadComment={
            this.props.type === 'Stream' &&
            !!R.prop('threadId', this.props.comment)
          }
          attachments={R.filter(
            att => att.type === 'image' || att.type === 'linkPreview',
            R.propOr([], 'att', this.props.comment),
          )}
          createdAt={R.prop('createdAt', this.props.comment)}
          navigator={this.props.navigator}
        />
      </View>
    );
  }
  renderActionSheet() {
    if (Platform.OS === 'android') {
      return (
        <ActionSheet
          ref={node => {
            this.ActionSheet = node;
          }}
          options={['Copy', 'Cancel']}
          cancelButtonIndex={1}
          onPress={this.handleActionSheetPress}
        />
      );
    }
    return null;
  }
  render() {
    return (
      <View style={[styles.container, this.getAdditionalStyles()]}>
        {this.renderComment()}
        {this.renderActionSheet()}
        {this.renderImages()}
      </View>
    );
  }
}
