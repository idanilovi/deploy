import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import Sound from 'react-native-sound';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { getFiles } from '../../../../actions';

const fileByIdSelector = (state, ownProps) =>
  R.propOr(null, R.prop('fileId', ownProps), R.prop('files', state));

const styles = StyleSheet.create({
  fileblock: {
    flex: 1,
    flexDirection: 'row',
    maxWidth: 200,
  },
});

const mapStateToProps = (state, ownProps) => ({
  id: ownProps.fileId,
  file: fileByIdSelector(state, ownProps),
});

const mapDispatchToProps = {
  getFiles,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class AudioBlock extends PureComponent {
  static displayName = 'AudioBlock';
  static propTypes = {
    id: PropTypes.string,
    getFiles: PropTypes.func.isRequired,
    isUserMessage: PropTypes.bool,
  };
  constructor() {
    super();
    this.state = { playing: false };
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.handleOnPlay = this.handleOnPlay.bind(this);
    this.handleOnPause = this.handleOnPause.bind(this);
  }
  componentDidMount() {
    if (this.props.id) {
      this.props.getFiles({ id: this.props.id });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.id !== nextProps.id) {
      if (nextProps.id) {
        nextProps.getFiles({ id: nextProps.id });
      }
    }
  }
  play(uri) {
    return new Promise((resolve, reject) => {
      this.sound = new Sound(uri, '', error => {
        if (error) {
          console.error(error);
          reject(error);
          return;
        }
        this.sound.play(() => {
          resolve();
          this.sound.release();
        });
      });
    });
  }
  pause() {
    this.sound.pause();
  }
  handleOnPlay() {
    this.setState({ playing: true });
    this.play(this.props.file.url);
  }
  handleOnPause() {
    this.setState({ playing: false });
    this.pause();
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={this.state.playing ? this.handleOnPause : this.handleOnPlay}
        >
          <Icon
            name={this.state.playing ? 'md-square' : 'md-play'}
            size={32}
            color={this.props.isUserMessage ? '#ffffff' : '#0d71d7'}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
