import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import { Text } from 'react-native';

function TimeBlock({ value, format, style }) {
  return (
    <Text style={[{ fontWeight: '300' }, style]}>
      {// TODO разобраться нахуй так сделали
      Array.isArray(value)
        ? value[0] == null
          ? ` to ${moment(value[1]).format(format)} `
          : ` from ${moment(value[0]).format(format)} to ${moment(
              value[1],
            ).format(format)} `
        : moment(value).format(format)}
    </Text>
  );
}

TimeBlock.displayName = 'TimeBlock';
TimeBlock.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.number,
  ]),
  format: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
};

export default TimeBlock;
