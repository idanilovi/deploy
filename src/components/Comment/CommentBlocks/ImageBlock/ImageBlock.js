import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import React, { PureComponent } from 'react';
import {
  TouchableOpacity,
  View,
  ImageBackground,
  StyleSheet,
  Platform,
  Text,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Navigation } from 'react-native-navigation';
import SVGImage from 'react-native-svg-image';
import { getFilesUrl } from '../../../../actions';
import { TimeBlock } from '../../CommentBlocks';

const filesSelector = state => R.propOr({}, 'files', state);
const fileIdSelector = (state, ownProps) => R.propOr(null, 'fileId', ownProps);

const fileByIdSelector = createSelector(
  filesSelector,
  fileIdSelector,
  (files, fileId) => R.propOr({}, fileId, files),
);

const mapStateToProps = (state, ownProps) => ({
  ...fileByIdSelector(state, ownProps),
});

const mapDispatchToProps = {
  getFilesUrl,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class ImageBlock extends PureComponent {
  static displayName = 'ImageBlock';
  static propTypes = {
    file: PropTypes.object,
    filename: PropTypes.string,
    fileId: PropTypes.string,
    loading: PropTypes.bool,
    url: PropTypes.string,
    urlPreview: PropTypes.string,
    getFilesUrl: PropTypes.func.isRequired,
    isThreadComment: PropTypes.bool,
    createdAt: PropTypes.number,
    dontOpenModal: PropTypes.bool,
    path: PropTypes.string,
    previewPath: PropTypes.string,
    type: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.download = this.download.bind(this);
    this.renderGradient = this.renderGradient.bind(this);
    this.renderTime = this.renderTime.bind(this);
    this.renderImage = this.renderImage.bind(this);
  }
  componentDidMount() {
    if (this.props.fileId) {
      this.props.getFilesUrl({ id: this.props.fileId });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.fileId !== nextProps.fileId) {
      if (nextProps.fileId) {
        nextProps.getFilesUrl({ id: nextProps.fileId });
      }
    }
  }
  handleOnPress() {
    if (!this.props.dontOpenModal) {
      Navigation.showModal({
        screen: 'workonflow.ImageLightBox',
        passProps: {
          size: this.props.size,
          filename: this.props.filename,
          fileId: this.props.fileId,
          path: this.props.path,
          previewPath: this.props.previewPath,
          type: this.props.type,
          url: this.props.url,
          createdAt: this.props.createdAt,
          threadId: this.props.threadId,
          streamId: this.props.streamId,
          authorId: this.props.authorId,
          viewType: this.props.viewType,
          filesContext:
            this.props.filesContext ||
            [].concat({
              size: this.props.size,
              filename: this.props.filename,
              fileId: this.props.fileId,
              id: this.props.id,
              path: this.props.path,
              previewPath: this.props.previewPath,
              type: this.props.type,
            }),
        },
        animationType: 'none',
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  renderGradient() {
    return (
      <LinearGradient
        colors={[
          'rgba(0, 0, 0, 0)',
          'rgba(0, 0, 0, 0.2)',
          'rgba(0, 0, 0, 0.25)',
          'rgba(0, 0, 0, 0.27)',
          'rgba(0, 0, 0, 0.29)',
        ]}
        locations={[0, 0.6, 0.83, 0.9, 0.92]}
        start={{ x: 0.0, y: 0.65 }}
        end={{ x: 0.0, y: 1.0 }}
        style={{
          width: 275,
          height: 235,
          borderRadius: 10,
          overflow: 'hidden',
        }}
      />
    );
  }
  renderTime() {
    if (this.props.createdAt) {
      return (
        <TimeBlock
          style={{
            color: '#ffffff',
            backgroundColor: 'transparent',
            fontWeight: '200',
            fontSize: 15,
            paddingLeft: 4,
            position: 'absolute',
            bottom: 16,
            right: 18,
          }}
          format="HH:mm"
          value={this.props.createdAt}
        />
      );
    }
    return null;
  }
  renderImage() {
    const { previewPath, path, type } = this.props;
    const filePath = previewPath || path;
    if (type === 'image/svg+xml') {
      return (
        <View style={{ flex: 1 }}>
          <SVGImage
            style={{ width: 275, height: 235 }}
            source={{
              uri: Platform.OS === 'ios' ? filePath : `file://${filePath}`,
              isStatic: true,
            }}
          />
          {this.renderGradient()}
        </View>
      );
    }
    if (filePath) {
      return (
        <ImageBackground
          source={{
            uri: Platform.OS === 'ios' ? filePath : `file://${filePath}`,
            isStatic: true,
          }}
          style={{
            width: 275,
            height: 235,
            borderRadius: 10,
            overflow: 'hidden',
          }}
          resizeMode="cover"
          resizeMethod="auto"
        >
          {this.renderGradient()}
        </ImageBackground>
      );
    }
    return this.renderGradient();
  }
  download() {
    this.props.getFilesUrl({ id: this.props.fileId, force: true });
  }
  render() {
    // if (this.props.download) {
    //   return (
    //     <View
    //       style={{
    //         width: 275,
    //         height: 235,
    //         borderRadius: 10,
    //         overflow: 'hidden',
    //         marginTop: 2,
    //         borderWidth: StyleSheet.hairlineWidth,
    //         borderColor: '#ede9eb',
    //         justifyContent: 'center',
    //         alignItems: 'center',
    //       }}
    //     >
    //       <TouchableOpacity
    //         style={{
    //           width: 50,
    //           height: 50,
    //           borderWidth: 1,
    //           borderColor: '#0000',
    //           borderRadius: 50,
    //           alignItems: 'center',
    //           justifyContent: 'center',
    //         }}
    //         onPress={this.download}
    //       >
    //         <Text style={{ fontSize: 20, color: '#000' }}>{'^'}</Text>
    //       </TouchableOpacity>
    //       {this.renderTime()}
    //     </View>
    //   );
    // }
    return (
      <TouchableOpacity onPress={this.handleOnPress}>
        <View
          style={{
            width: 275,
            height: 235,
            borderRadius: 10,
            overflow: 'hidden',
            marginTop: 2,
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: '#ede9eb',
          }}
        >
          {this.renderImage()}
          {this.renderTime()}
        </View>
      </TouchableOpacity>
    );
  }
}
