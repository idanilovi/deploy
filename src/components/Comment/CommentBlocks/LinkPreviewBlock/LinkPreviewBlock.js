import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Text, View, Image, Dimensions, StyleSheet, Linking, TouchableOpacity } from 'react-native';
import Hyperlink from 'react-native-hyperlink'
import TimeBlock from '../TimeBlock';

const { width } = Dimensions.get('window');

function extractHostname(url) {
  let hostname = '';
  //find & remove protocol (http, ftp, etc.) and get hostname
  if (url.indexOf("://") > -1) {
    hostname = url.split('/')[2];
  } else {
    hostname = url.split('/')[0];
  }

  //find & remove port number
  hostname = hostname.split(':')[0];
  //find & remove "?"
  hostname = hostname.split('?')[0];

  return hostname;
}

// function extractRootDomain(url) {
//   let domain = extractHostname(url)
//   const splitArr = domain.split('.')
//   const arrLen = splitArr.length

//   //extracting the root domain here
//   //if there is a subdomain 
//   if (arrLen > 2) {
//     domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
//     //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
//     if (splitArr[arrLen - 1].length == 2 && splitArr[arrLen - 1].length == 2) {
//       //this is using a ccTLD
//       domain = splitArr[arrLen - 3] + '.' + domain;
//     }
//   }
//   return domain;
// }

export default class LinkPreviewBlock extends PureComponent {
  static displayName = 'LinkPreviewBlock';
  static propTypes = {
    title: PropTypes.string,
    imgUrl: PropTypes.string,
    description: PropTypes.string,
    url: PropTypes.string,
    blockProps: PropTypes.object,
  };

  render() {
    return (
      <TouchableOpacity
        style={{
          // flex: 1,
          width: width - 55,
          // height: 400,
          backgroundColor: '#f3eff3',
          borderRadius: 10,
          overflow: 'hidden',
          marginTop: 2,
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: '#ede9eb',
          flexDirection: 'row',

        }}
        onPress={() => Linking.openURL(this.props.url)}
      >
        {this.props.type !== 'Direct' && this.props.isThreadComment && <View
          style={{
            borderBottomLeftRadius: 10,
            borderTopLeftRadius: 10,
            width: 6,
            backgroundColor: '#8aceea',
          }}
        />}
        <View style={{ flex: 1,alignSelf: 'flex-end', paddingVertical: 8, paddingHorizontal: 8 }}>
          {this.props.imgUrl && (
            <Image
              source={{
                uri: this.props.imgUrl,
                cache: 'force-cache',
              }}
              style={{
                width: width - 55,
                height: 235,
                borderTopLeftRadius: this.props.isThreadComment ? 0 : 10,
                borderTopRightRadius: 10,
                marginLeft: this.props.type !== 'Direct' ? -8 : 0,
                marginTop: -8,
                overflow: 'hidden',
              }}
              resizeMode="cover"
              resizeMethod="auto"
            />
          )}
          {this.props.title && <Text style={{
            fontSize: 16,
            fontWeight: 'bold',
            color: '#544f68',
            paddingHorizontal: 10,
            paddingVertical: 10,
          }}>
            {this.props.title.trim()}
          </Text>}
         {this.props.description && <Text style={{
            fontSize: 16,
            color: '#544f68',
            paddingHorizontal: 10,
            paddingVertical: 10,
          }}>
            {this.props.description}
          </Text>}
          <Hyperlink
            linkStyle={{
              color: '#a8a4b9',
              fontSize: 14,
            }}
            linkText={ url => extractHostname(url) }
          >
            <Text style={{
              paddingHorizontal: 10,
              paddingVertical: 10,
            }}>
              {this.props.url}
            </Text>
          </Hyperlink>
        </View>
      </TouchableOpacity>
    );
  }
}
