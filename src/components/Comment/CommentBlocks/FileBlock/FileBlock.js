import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import filesize from 'filesize';
import { Navigation } from 'react-native-navigation';
import { getFilesUrl } from '../../../../actions';
import FileIcon from '../../../FileIcon';

const fileByIdSelector = (state, ownProps) =>
  R.propOr(null, R.prop('fileId', ownProps), R.prop('files', state));

function mapStateToProps(state, ownProps) {
  return {
    id: ownProps.fileId,
    file: fileByIdSelector(state, ownProps),
  };
}

const mapDispatchToProps = {
  getFilesUrl,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class FileBlock extends Component {
  static displayName = 'FileBlock';
  static propTypes = {
    id: PropTypes.string,
    fileId: PropTypes.string,
    file: PropTypes.object,
    getFilesUrl: PropTypes.func.isRequired,
    isUserMessage: PropTypes.bool,
  };
  static defaultProps = {
    styles: {},
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  componentDidMount() {
    this.props.getFilesUrl({ id: this.props.fileId });
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.fileId !== nextProps.fileId) {
      if (nextProps.fileId) {
        nextProps.getFilesUrl({ id: nextProps.fileId });
      }
    }
  }
  handleOnPress() {
    Navigation.showModal({
      screen: 'workonflow.ImageLightBox',
      passProps: {
        url: this.props.url,
        filename: this.props.filename,
        createdAt: this.props.createdAt,
        size: this.props.size,
        fileId: this.props.fileId,
        threadId: this.props.threadId,
        streamId: this.props.streamId,
        authorId: this.props.authorId,
        viewType: this.props.viewType,
        filesContext: this.props.filesContext || [].concat(this.props.file),
      },
      animationType: 'none',
    });
  }
  render() {
    const { file, isUserMessage } = this.props;
    if (file) {
      if (file.download) {
        return (
          <View
            style={[
              {
                width: 200,
                flexDirection: 'column',
                paddingVertical: 13.5,
              },
              this.props.styles.container,
            ]}
          >
            <View style={{ flexDirection: 'row' }}>
              <FileIcon
                filename={R.propOr('', 'filename', file)}
                download={file.download}
                fileId={this.props.fileId}
                getFilesUrl={this.props.getFilesUrl}
                styles={
                  isUserMessage && {
                    fileIcon: { backgroundColor: '#ffffff' },
                    fileIconText: { color: '#0099ff' },
                  }
                }
              />
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                  paddingHorizontal: 12,
                  flex: 1,
                  flexWrap: 'nowrap',
                }}
              >
                <Text
                  style={{
                    fontWeight: '200',
                    fontSize: 17,
                    color: isUserMessage ? '#ffffff' : '#656565',
                  }}
                  ellipsizeMode="tail"
                  numberOfLines={1}
                >
                  {file.filename}
                </Text>
                <Text
                  style={{
                    fontWeight: '200',
                    fontSize: 17,
                    color: isUserMessage ? '#ffffff' : '#9d9d9d',
                  }}
                  ellipsizeMode="tail"
                  numberOfLines={1}
                >
                  {filesize(Number(R.propOr(0, 'size', file)), { round: 1 })}
                </Text>
              </View>
            </View>
          </View>
        );
      }
      return (
        <TouchableOpacity
          style={[
            {
              width: 200,
              flexDirection: 'column',
              paddingVertical: 13.5,
            },
            this.props.styles.container,
          ]}
          onPress={this.handleOnPress}
        >
          <View style={{ flexDirection: 'row' }}>
            <FileIcon
              filename={R.propOr('', 'filename', file)}
              styles={
                isUserMessage && {
                  fileIcon: { backgroundColor: '#ffffff' },
                  fileIconText: { color: '#0099ff' },
                }
              }
            />
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                paddingHorizontal: 12,
                flex: 1,
                flexWrap: 'nowrap',
              }}
            >
              <Text
                style={{
                  fontWeight: '200',
                  fontSize: 17,
                  color: isUserMessage ? '#ffffff' : '#656565',
                }}
                ellipsizeMode="tail"
                numberOfLines={1}
              >
                {file.filename}
              </Text>
              <Text
                style={{
                  fontWeight: '200',
                  fontSize: 17,
                  color: isUserMessage ? '#ffffff' : '#9d9d9d',
                }}
                ellipsizeMode="tail"
                numberOfLines={1}
              >
                {filesize(Number(R.propOr(0, 'size', file)), { round: 1 })}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    }
    return null;
  }
}
