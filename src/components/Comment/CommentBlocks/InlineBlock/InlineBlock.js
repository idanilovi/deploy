import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import TextParser from './TextParser';

export default class InlineBlock extends PureComponent {
  static displayName = 'InlineBlock';
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    // style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    blockProps: PropTypes.object,
    shouldBeParsed: PropTypes.bool,
    navigator: PropTypes.object,
    dontRenderThreadLinks: PropTypes.bool,
  };
  render() {
    return (
      <TextParser
        value={this.props.value}
        style={this.props.style}
        blockProps={this.props.blockProps}
        triger={['@']}
        shouldBeParsed={this.props.shouldBeParsed}
        type={this.props.type}
        isUserMessage={this.props.isUserMessage}
        navigator={this.props.navigator}
        dontRenderThreadLinks={this.props.dontRenderThreadLinks}
      />
    );
  }
}
