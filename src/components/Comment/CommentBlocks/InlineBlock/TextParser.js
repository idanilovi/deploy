import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Text, StyleSheet } from 'react-native';
import React, { PureComponent } from 'react';
import Hyperlink from 'react-native-hyperlink';
import URI from 'urijs';
import Parser from 'simple-text-parser';
import { contactsSelector } from '../../../../reducers/contacts';
import { userIdSelector } from '../../../../reducers/userdata';
import ThreadLink from '../../../Description/ThreadLink';

const styles = StyleSheet.create({
  container: {
    flex: 0,
    flexWrap: 'wrap',
    color: '#696285',
    fontSize: 18,
  },
});

const mapStateToProps = state => ({
  userId: userIdSelector(state),
  contacts: contactsSelector(state),
});

class TextParser extends PureComponent {
  static displayName = 'TextParser';
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    // style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    blockProps: PropTypes.object,
    contacts: PropTypes.object,
    userId: PropTypes.string,
    navigator: PropTypes.object,
    dontRenderThreadLinks: PropTypes.bool,
    shouldBeParsed: PropTypes.bool,
    type: PropTypes.string,
    isUserMessage: PropTypes.bool,
  };
  constructor(props) {
    super(props);
    this.renderText = this.renderText.bind(this);
    this.renderComment = this.renderComment.bind(this);
  }
  renderText() {
    const parser = new Parser();
    parser.addRule(/\@[\S]+@/gi, (tag, cleanTag) => {
      const { userId, contacts } = this.props;
      const id = tag.slice(1, -1);
      const name =
        contacts &&
        contacts[id] &&
        contacts[id].basicData &&
        contacts[id].basicData.name;
      const text = name ? `${name}` : tag;
      return {
        type: 'mention',
        text,
        tag: cleanTag,
        style: {
          fontWeight: '200',
          color: id === userId ? '#00c69e' : '#0d71d7',
        },
      };
    });
    parser.addRule(/https:\/\/beta.workonflow\.com.*\/(>|%3e)\w{24}/i, tag => {
      const pathname = new URI(tag).pathname();
      const id = pathname
        .split('/')
        .pop()
        .slice(3);
      return {
        type: 'thread_link',
        id,
      };
    });
    return parser.toTree(this.props.value);
  }
  renderComment(_, i) {
    const { style, blockProps, navigator, dontRenderThreadLinks } = this.props;
    if (_.type === 'text') {
      return (
        <Text key={i} style={[{ fontWeight: '300' }, style]} {...blockProps}>
          {_.text}
        </Text>
      );
    }
    if (_.type === 'mention') {
      return (
        <Text key={i} style={[{ fontWeight: '300' }, style]}>
          <Text style={[{ fontWeight: '300' }, _.style]}>{_.text}</Text>
        </Text>
      );
    }
    if (_.type === 'thread_link') {
      if (dontRenderThreadLinks) {
        return <Text key={i} />;
      }
      return <ThreadLink key={i} id={_.id} navigator={navigator} />;
    }
    return null;
  }
  render() {
    if (this.props.shouldBeParsed) {
      return (
        <Hyperlink
          linkDefault={true}
          linkStyle={[
            {
              color:
                this.props.type === 'Direct'
                  ? !this.props.isUserMessage && '#2c83dc'
                  : '#2c83dc',
              fontSize: 20,
            },
            this.props.style,
          ]}
        >
          <Text
            style={[{ fontWeight: '300' }, styles.container]}
            {...this.props.blockProps}
          >
            {this.renderText().map(this.renderComment)}
          </Text>
        </Hyperlink>
      );
    }
    return (
      <Text
        style={[{ fontWeight: '300' }, styles.container]}
        {...this.props.blockProps}
      >
        {this.renderText().map(this.renderComment)}
      </Text>
    );
  }
}

export default connect(mapStateToProps)(TextParser);
