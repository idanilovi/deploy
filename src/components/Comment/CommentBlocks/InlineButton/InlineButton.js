import PropTypes from 'prop-types';
import { Button } from 'react-native';
import React, { Component } from 'react';

export default class InlineButton extends Component {
  static displayName = 'InlineButton';
  static propTypes = {
    blockStyle: PropTypes.object,
    onPress: PropTypes.func.isRequired,
    value: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    this.props.onPress();
  }
  render() {
    const { value, blockStyle } = this.props;
    return (
      <Button
        style={blockStyle}
        color="#03a4ec"
        onPress={this.handleOnPress}
        title={value}
      />
    );
  }
}
