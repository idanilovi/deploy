import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Text } from 'react-native';

export default class MailBlock extends PureComponent {
  static displayName = 'MailBlock';
  static propTypes = {
    text: PropTypes.string,
    blockProps: PropTypes.object,
  };
  render() {
    return (
      <Text
        ellipsizeMode="tail"
        numberOfLines={1}
        style={{ color: this.props.isUserMessage ? '#ffffff' : '#4e4963' }}
        {...this.props.blockProps}
      >
        {this.props.text}
      </Text>
    );
  }
}
