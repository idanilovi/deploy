import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import URI from 'urijs';
import { Text } from 'react-native';
import {
  InlineBlock,
  FileBlock,
  ImageBlock,
  MailBlock,
  AudioBlock,
  LinkPreviewBlock,
} from '../CommentBlocks';
import ThreadLink from '../../Description/ThreadLink';

export default class CommentAttachments extends PureComponent {
  static displayName = 'CommentAttachments';
  static propTypes = {
    attachments: PropTypes.arrayOf(PropTypes.object),
    createdAt: PropTypes.number,
    isThreadComment: PropTypes.bool,
    navigator: PropTypes.object,
  };
  render() {
    return this.props.attachments.map((attachment, index) => {
      if (attachment.type === 'text') {
        return (
          <InlineBlock
            key={index}
            value={R.path(['data', 'text'], attachment)}
            style={[
              { fontWeight: '200' },
              this.props.isUserMessage && this.props.type === 'Direct'
                ? { color: '#ffffff' }
                : {},
            ]}
            shouldBeParsed={true}
            blockProps={{
              onLongPress: this.props.onLongPress,
              mentionColor:
                this.props.type === 'Direct' ? '#ffffff' : '#0d71d7',
            }}
            type={this.props.type}
            navigator={this.props.navigator}
          />
        );
      }
      if (attachment.type === 'image') {
        return (
          <ImageBlock
            key={index}
            viewType={this.props.viewType}
            filesContext={this.props.filesContext}
            fileId={R.path(['data', 'id'], attachment)}
            isThreadComment={this.props.isThreadComment}
            createdAt={this.props.createdAt}
          />
        );
      }
      if (attachment.type === 'file') {
        return (
          <FileBlock
            key={index}
            viewType={this.props.viewType}
            filesContext={this.props.filesContext}
            isUserMessage={
              this.props.isUserMessage && this.props.type === 'Direct'
            }
            fileId={R.path(['data', 'id'], attachment)}
            size={R.path(['data', 'size'], attachment)}
          />
        );
      }
      if (attachment.type === 'audio') {
        return (
          <AudioBlock
            key={index}
            isUserMessage={
              this.props.isUserMessage && this.props.type === 'Direct'
            }
            fileId={R.path(['data', 'id'], attachment)}
          />
        );
      }
      if (attachment.type === 'mail') {
        return (
          <MailBlock
            text={R.path(['data', 'text'], attachment)}
            html={R.path(['data', 'html'], attachment)}
            isUserMessage={this.props.isUserMessage}
            blockProps={{
              onLongPress: this.props.onLongPress,
            }}
          />
        );
      }
      if (attachment.type === 'linkPreview') {
        const uri = new URI(attachment.data.url);
        if (uri.domain() === 'workonflow.com') {
          return null;
        }
        return (
          <LinkPreviewBlock
            title={R.pathOr(
              R.pathOr(
                R.path(['data', 'metaTitle'], attachment),
                ['data', 'title'],
                attachment,
              ),
              ['data', 'h1'],
              attachment,
            )}
            imgUrl={R.pathOr(
              R.path(['data', 'image'], attachment),
              ['data', 'metaImage'],
              attachment,
            )}
            description={R.path(['data', 'metaDecription'], attachment)}
            url={R.pathOr(
              R.path(['data', 'metaUrl'], attachment),
              ['data', 'url'],
              attachment,
            )}
            isThreadComment={this.props.isThreadComment}
            type={this.props.type}
          />
        );
      }
      return null;
    });
  }
}
