import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Dimensions, View } from 'react-native';
import ContactIcon from '../../ContactIcon';
import { TimeBlock } from '../CommentBlocks';
import CommentAttachments from '../CommentAttachments';

const { width } = Dimensions.get('window');

const commentContactSelector = (state, ownProps) =>
  R.prop(R.prop('from', ownProps), R.prop('contacts', state));

const mapStateToProps = (state, ownProps) => ({
  contact: commentContactSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class DirectComment extends Component {
  static displayName = 'DirectComment';
  static propTypes = {
    att: PropTypes.array,
    contact: PropTypes.object,
    showAvatar: PropTypes.bool,
    onLongPress: PropTypes.func,
    isSameUser: PropTypes.shape({
      isSameTimePrev: PropTypes.bool,
      isSameUserPrev: PropTypes.bool,
    }),
  };
  constructor(props) {
    super(props);
    this.renderAvatar = this.renderAvatar.bind(this);
    this.renderComment = this.renderComment.bind(this);
  }
  renderComment() {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          borderRadius: 10,
          backgroundColor: this.props.isUserMessage ? '#4a98f9' : '#f3eff3',
          paddingVertical: 8,
          paddingHorizontal: 8,
          flexWrap: 'wrap',
        }}
      >
        <CommentAttachments
          type="Direct"
          viewType={this.props.viewType}
          filesContext={this.props.filesContext}
          attachments={R.filter(
            att => att.type !== 'image' && att.type !== 'linkPreview',
            this.props.att,
          )}
          isUserMessage={this.props.isUserMessage}
          onLongPress={this.props.onLongPress}
        />
        <View
          style={{
            alignSelf: 'flex-end',
            marginLeft: 'auto',
            marginRight: 0,
            alignItems: 'center',
          }}
        >
          <TimeBlock
            style={{
              color: this.props.isUserMessage ? '#ffffff' : '#c4c2cf',
              fontWeight: '200',
              paddingLeft: 4,
            }}
            format="HH:mm"
            value={this.props.createdAt}
          />
        </View>
      </View>
    );
  }
  renderAvatar() {
    if (!this.props.isUserMessage) {
      const { isSameUserPrev, isSameTimePrev } = this.props.isSameUser;
      if (isSameUserPrev && isSameTimePrev) {
        return (
          <View
            style={{
              width: 29,
              height: 29,
            }}
          />
        );
      }
      return (
        <ContactIcon
          show={true}
          id={R.propOr(null, '_id', this.props.contact)}
          // name={R.pathOr('', ['basicData', 'name'], this.props.contact)}
          // color={R.propOr('#ffffff', 'color', this.props.contact)}
          // type={R.propOr(null, 'billingType', this.props.contact)}
        />
      );
    }
    return null;
  }
  render() {
    return (
      <View
        style={[
          { flex: 1, flexDirection: 'row', maxWidth: width - 100 },
          this.props.isUserMessage
            ? { alignSelf: 'flex-end', maxWidth: width - 60 }
            : { alignSelf: 'flex-start', maxWidth: width - 60 },
        ]}
      >
        <View style={{ paddingRight: 8 }}>{this.renderAvatar()}</View>
        <View style={{ flexGrow: 0, flexShrink: 1, flexDirection: 'column' }}>
          {this.renderComment()}
        </View>
      </View>
    );
  }
}
