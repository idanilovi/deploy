import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Text, TouchableHighlight, View, StyleSheet } from 'react-native';
import { TimeBlock, InlineBlock } from '../CommentBlocks';
import ContactIcon from '../../ContactIcon';
import global from '../../../global';
import ThreadAvatarSvg from '../../Icon/ThreadAvatarSvg';
import { getThread, readNotification } from '../../../actions';

const commentContactSelector = (state, ownProps) =>
  R.prop(R.prop('from', ownProps), R.prop('contacts', state));

const threadStatusPercentSelector = (state, ownProps) => {
  const threadId = R.propOr('', 'threadId', ownProps);
  if (!threadId) return;
  const streamId = R.propOr('', 'streamId', ownProps);
  const threadStatuses = R.pathOr(
    [],
    ['streams', streamId, 'threadStatuses'],
    state,
  );
  const statusId = R.pathOr(
    '',
    [threadId, 'status'],
    R.propOr({}, 'threads', state),
  );
  const indexOfThreadStatus = threadStatuses.indexOf(statusId) + 1;
  const precent = 100 / threadStatuses.length * indexOfThreadStatus;
  return precent;
};
const statusTypeSelector = (state, ownProps) => {
  const threadId = R.propOr('', 'threadId', ownProps);
  if (!threadId) return;
  const thread = R.pathOr({}, ['threads', threadId], state);
  const status = R.pathOr(
    {},
    ['statuses', R.propOr('', 'status', thread)],
    state,
  );
  return {
    statusColor: R.propOr(null, 'color', status),
    statusType: R.propOr(null, 'type', status),
  };
};

const getBotIdForLogo = logo => {
  switch (logo) {
    case 'GIF': {
      return '5a8581db17bf50d16513e6c1';
    }
    case 'PST': {
      return '5a8547c04eb6b66121809805';
    }
    case 'TST': {
      return '5a819c28842780addaaeef99';
    }
    case 'RMD': {
      return '5a8548934eb6b66121809806';
    }
    case 'F*K': {
      return '5964833c81d3f80014976053';
    }
    default: {
      return null;
    }
  }
};

const mapStateToProps = (state, ownProps) => ({
  contact: commentContactSelector(state, ownProps),
  precent: threadStatusPercentSelector(state, ownProps),
  status: statusTypeSelector(state, ownProps),
  threads: state.threads,
});

const mapDispatchToProps = {
  getThread,
  readNotification,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class NotifyComment extends PureComponent {
  static displayName = 'NotifyComment';
  static propTypes = {
    _id: PropTypes.string,
    threadId: PropTypes.string,
    streamId: PropTypes.string,
    from: PropTypes.string,
    fromName: PropTypes.string,
    headerBody: PropTypes.string,
    createdAt: PropTypes.number,
    messageNotify: PropTypes.string,
    colorContact: PropTypes.string,
    readAt: PropTypes.number,
    countMessage: PropTypes.number,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    readNotification: PropTypes.func.isRequired,
    getThread: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderAvatar = this.renderAvatar.bind(this);
    this.renderComment = this.renderComment.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderHeaderBody = this.renderHeaderBody.bind(this);
    this.renderBody = this.renderBody.bind(this);
    this.renderBodyFrom = this.renderBodyFrom.bind(this);
    this.renderBodyMessage = this.renderBodyMessage.bind(this);
    this.renderCounter = this.renderCounter.bind(this);
  }
  handleOnPress() {
    global.tracker.trackEvent('Нажатия на кнопочки', 'notify');
    // eslint-disable-next-line no-underscore-dangle
    this.props.readNotification({ commentId: this.props._id });
    if (this.props.threadId) {
      this.props.navigator.push({
        screen: 'workonflow.Thread',
        passProps: {
          id: this.props.threadId,
          type: 'Thread',
          title: this.props.headerBody,
          toScroll: true,
        },
      });
    } else if (this.props.streamId) {
      this.props.navigator.push({
        screen: 'workonflow.Stream',
        passProps: {
          isChat: true,
          streamId: this.props.streamId,
          type: 'Stream',
          title: this.props.headerBody,
        },
      });
    } else if (this.props.from) {
      const url = this.props.notifyUrl;
      this.props.navigator.push({
        screen: 'workonflow.Direct',
        passProps: {
          contactId: url.substring(url.indexOf('@') + 1, url.indexOf('@') + 25),
          type: 'Direct',
          title: this.props.fromName,
        },
      });
    }
  }
  renderHeaderBody() {
    return this.props.headerBody ? (
      <Text
        style={{
          flex: 1,
          fontSize: 14,
          backgroundColor: 'transparent',
          fontWeight: !this.props.readAt ? 'bold' : '100',
          color: 'rgba(255,255,255, 1)',
        }}
        ellipsizeMode="tail"
        numberOfLines={1}
      >
        {this.props.headerBody}
      </Text>
    ) : (
      <Text
        style={{
          flex: 1,
          fontSize: 14,
          backgroundColor: 'transparent',
          fontWeight: !this.props.readAt ? 'bold' : '100',
          color: 'rgba(255,255,255, 0.5)',
        }}
        ellipsizeMode="tail"
        numberOfLines={1}
      >
        {this.props.fromName}
      </Text>
    );
  }
  renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          flexWrap: 'nowrap',
        }}
      >
        {this.renderCounter()}
        <View style={{ flex: 1 }}>{this.renderHeaderBody()}</View>
        <TimeBlock
          style={{
            flex: 0,
            color: 'rgba(255,255,255, 0.33)',
            backgroundColor: 'transparent',
            fontWeight: '100',
            fontSize: 14,
          }}
          format="HH:mm"
          value={this.props.createdAt}
        />
      </View>
    );
  }
  renderCounter() {
    const { countMessage } = this.props;
    if (countMessage > 0) {
      return (
        <View style={{ paddingRight: 10 }}>
          <View
            style={{
              height: 20,
              paddingHorizontal: 8,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#ffffff',
              alignSelf: 'flex-start',
              borderRadius: 5,
            }}
          >
            <Text
              style={{
                fontWeight: !this.props.readAt ? 'bold' : '400',
                fontSize: 12,
                color: '#241c2e',
              }}
            >
              {countMessage}
            </Text>
          </View>
        </View>
      );
    }
    return null;
  }
  renderBodyFrom() {
    const isYou = this.props.from === this.props.userId;
    return (
      <Text
        style={{
          color: 'rgba(255,255,255,0.5)',
          alignSelf: 'flex-start',
          fontWeight: !this.props.readAt ? 'bold' : '100',
          fontSize: 14,
          backgroundColor: 'transparent',
        }}
      >
        {isYou ? 'You:' : this.props.headerBody && `${this.props.fromName}:`}
      </Text>
    );
  }
  renderBodyMessage() {
    const { headerBody, messageNotify, nameStatus, colorStatus } = this.props;
    return (
      <Text>
        <InlineBlock
          dontRenderThreadLinks={true}
          blockProps={{
            notification: true,
            mentionColor: headerBody
              ? 'rgba(255,255,255, 0.33)'
              : 'rgba(255,255,255,1)',
          }}
          value={` ${messageNotify}`}
          style={[
            {
              fontWeight: !this.props.readAt ? 'bold' : '200',
              fontSize: 14,
              backgroundColor: 'transparent',
              color: 'rgba(255,255,255,1)',
            },
            headerBody
              ? {
                  color: 'rgba(255,255,255, 0.33)',
                }
              : {},
          ]}
        />
        {nameStatus ? (
          <InlineBlock
            blockProps={{ notification: true }}
            value={` ${nameStatus}`}
            style={{
              fontWeight: !this.props.readAt ? 'bold' : '200',
              fontSize: 14,
              backgroundColor: 'transparent',
              color: colorStatus || 'rgba(255,255,255,1)',
            }}
          />
        ) : null}
      </Text>
    );
  }
  renderBody() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <Text
          numberOfLines={1}
          elipsisMod="tail"
          style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: 'transparent',
          }}
        >
          {this.renderBodyFrom()}
          {this.renderBodyMessage()}
        </Text>
      </View>
    );
  }
  renderComment() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          paddingVertical: 20,
          paddingLeft: 8,
          paddingRight: 16,
          justifyContent: 'center',
          alignItems: 'stretch',
        }}
      >
        {this.renderHeader()}
        {this.renderBody()}
      </View>
    );
  }
  renderAvatar() {
    return (
      <View
        style={{
          flex: 0,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 28,
          paddingBottom: 13,
          paddingLeft: 13,
          paddingRight: 9,
        }}
      >
        {this.props.threadId ? (
          <View>
            <ThreadAvatarSvg
              name="threadAvatar"
              width={29}
              height={29}
              fill={R.prop('statusColor', this.props.status)}
              dasharray={
                R.prop('statusType', this.props.status) === 'Done'
                  ? '100, 100'
                  : R.propOr('10', 'precent', this.props) + ', 100'
              }
              viewBox="0 0 36 36"
            />
          </View>
        ) : (
          <ContactIcon
            show
            styles={{
              container: {
                width: 33,
                height: 33,
                borderRadius: 16.5,
              },
            }}
            id={
              getBotIdForLogo(this.props.logo) ||
              this.props.from ||
              this.props.userId
            }
          />
        )}
      </View>
    );
  }
  render() {
    return (
      <TouchableHighlight
        underlayColor="rgba(40,30,42,0.5)"
        style={[
          {
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'transparent',
            alignItems: 'flex-start',
            flexDirection: 'row',
            height: 86,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: 'hsla(0,0%,100%,.3)',
            borderTopWidth: StyleSheet.hairlineWidth,
            borderTopColor: 'hsla(0,0%,100%,.3)',
          },
          !this.props.readAt ? { backgroundColor: 'rgba(40,30,42,0.33)' } : {},
        ]}
        onPress={this.handleOnPress}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'transparent',
            alignItems: 'flex-start',
            flexDirection: 'row',
          }}
        >
          {this.renderAvatar()}
          {this.renderComment()}
        </View>
      </TouchableHighlight>
    );
  }
}
