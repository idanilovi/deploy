import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import { contactSelector } from '../../reducers/contacts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 52,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 15,
    fontWeight: '400',
    color: '#ffffff',
  },
  opacity: {
    opacity: 0.5,
  },
});

class ContactInfo extends Component {
  static displayName = 'ContactInfo';
  static propTypes = {
    name: PropTypes.string,
    position: PropTypes.string,
  };
  shouldComponentUpdate(nextProps) {
    if (this.props.name !== nextProps.name) {
      return true;
    }
    if (this.props.position !== nextProps.position) {
      return true;
    }
    return false;
  }
  render() {
    const { name, position } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{name}</Text>
        <Text style={[styles.text, styles.opacity]}>{position}</Text>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const contact = contactSelector(state, ownProps);
  const { basicData: { name } = {}, hrData: { position } = {} } = contact;
  return {
    name,
    position,
  };
};

export default connect(mapStateToProps)(ContactInfo);
