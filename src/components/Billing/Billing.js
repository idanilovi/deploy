import R from 'ramda';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  TouchableHighlight,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  ScrollView,
  StyleSheet,
  TextInput,
} from 'react-native';
import Modal from 'react-native-modalbox';
import ContactIcon from '../ContactIcon';
import CreateCardForm from './CreateCardForm';
import setCreditCardActive from '../../actions/setCreditCardActive';
import Icon from '../Icon';
import { createCreditCards } from '../../actions';
import withSafeArea from '../../screens/withSafeArea';

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'rgba(0,0,0, 0.6)',
    flex: 1,
    flexDirection: 'column',
  },
  cell: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#000000',
    // borderBottomColor: 'hsla(0,0%,100%,.9)',
    // justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  header: {
    justifyContent: 'center',
    // paddingVertical: 20,
    flexDirection: 'row',
  },
  headerText: { color: '#000000', fontSize: 22 },
  containerBody: { flexDirection: 'row', justifyContent: 'center' },
  body: {
    // paddingHorizontal: 20,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    // flex: 5,
    // flex:1,
    height: height,
    width: width,
    borderRadius: 14,
  },
  scrollView: {
    flex: 1,
  },
  text: { color: '#4e4963' },
  textBold: { fontWeight: 'bold', color: '#4e4963' },
  paymentValueRow: {
    flexDirection: 'row',
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  botRow: {
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nextPayment: {
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    paddingHorizontal: 15,
    paddingVertical: 10,
    // alignItems: 'center',
    // justifyContent: 'center',
    borderColor: 'red',
    borderWidth: 1,
    borderRadius: 5,
  },
  cardView: {
    width: 100,
    height: 60,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderRadius: 5,
    marginHorizontal: 8,
  },
  activeCard: {
    borderColor: '#2ad18c',
  },
  addCardButton: {
    width: 100,
    height: 60,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 5,
    marginHorizontal: 8,
  },
  dateInput: {
    height: 25,
    width: 50,
    borderRadius: 3,
    paddingTop: 0,
    paddingBottom: 0,
    backgroundColor: '#ffffff',
  },
  cardForm: {
    backgroundColor: '#7ecab0',
    borderColor: '#ffffff',
    borderRadius: 10,
    flex: 1,
    height: 180,
    paddingHorizontal: 20,
  },
  cardNumberInput: {
    height: 25,
    borderColor: '#eeeeee',
    borderWidth: 1,
    paddingTop: 0,
    paddingBottom: 0,
    backgroundColor: '#ffffff',
    borderRadius: 3,
  },
});

const botsArraySelector = state =>
  R.filter(
    bot => bot,
    R.map(
      contact =>
        R.propOr('', 'billingType', contact) === 'bots' &&
        R.propOr(false, 'botId', contact) && {
          ...contact,
          price:
            R.propOr('', 'botId', contact) === '5a9814df67f71216107e9f75'
              ? 30
              : 0,
        },
      R.values(R.propOr({}, 'contacts', state)),
    ),
  );
const usersCountSelector = state =>
  R.filter(
    contact =>
      R.propOr('', 'billingType', contact) === 'users' ||
      (R.propOr('', 'billingType', contact) === 'bots' &&
        R.propOr(false, 'botId', contact)),
    R.values(R.propOr({}, 'contacts', state)),
  ).length;

const cardsSelector = state => R.values(R.propOr({}, 'cards', state));
const paymentHistorySelector = state =>
  R.values(R.propOr({}, 'paymentsHistory', state));

class Billing extends Component {
  static displayName = 'Billing';
  static defaultProps = {
    paymentHistory: [],
    bots: [],
    cards: [],
  };
  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  };
  constructor(props, defaultProps) {
    super(props, defaultProps);
    this.state = {
      bots: [],
      freeUsers: 0,
      openCreateCardForm: false,
    };
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnCreateCardPress = this.handleOnCreateCardPress.bind(this);
    this.handleOnPressCard = this.handleOnPressCard.bind(this);
  }
  // botPrice(id) {
  //   switch (id) {
  //     case '5a9814df67f71216107e9f75':
  //       return 0;
  //       break;
  //     default:
  //       return 30;
  //       break;
  //     // case '5a1d803540b006001a8951bb': return 0;
  //   }
  // }
  componentDidMount() {
    this.setState({
      freeUsers: this.props.users > 5 ? 5 : this.props.users,
      botsPrice: R.sum(R.map(bot => bot.price, this.props.bots)),
    });
  }

  handleOnPress() {
    this.props.navigator.dismissModal();
  }
  handleOnPressCard(id) {
    this.props.setCreditCardActive(id);
  }
  handleOnCreateCardPress() {
    this.setState({ openCreateCardForm: !this.state.openCreateCardForm });
  }
  render() {
    return (
      <View style={styles.container}>
        <Modal
          style={{
            backgroundColor: '#ffffff',
          }}
          swipeToClose={true}
          isOpen={true}
          onClosed={this.handleOnPress}
          swipeArea={200}
          swipeThreshold={200}
          // bounces={false}
          // onOpened={this.onOpen}
          // onClosingState={this.onClosingState}>
        >
          <View style={styles.header}>
            <Text style={styles.headerText}>{'˯'} </Text>
          </View>
          <View style={styles.containerBody}>
            <View style={styles.body}>
              <ScrollView style={styles.scrollView}>
                <View style={styles.cell}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={[
                        styles.text,
                        { flex: 1, alignSelf: 'flex-start', color: '#a8a4b9' },
                      ]}
                    >
                      {'Users'}
                    </Text>
                    <TouchableOpacity
                      style={{ alignSelf: 'flex-start' }}
                      onPress={this.handleOnPress}
                    >
                      <Text>{'Close'}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.paymentValueRow}>
                    <Text
                      style={[
                        styles.textBold,
                        { flex: 1, alignSelf: 'flex-start' },
                      ]}
                    >
                      {'Free'}
                    </Text>
                    <Text style={[styles.text, { flex: 1 }]}>{`${
                      this.state.freeUsers
                    }/5`}</Text>
                    <Text
                      style={[
                        styles.text,
                        { alignSelf: 'flex-end', paddingHorizontal: 10 },
                      ]}
                    >
                      {'$0'}
                    </Text>
                  </View>
                  <View style={styles.paymentValueRow}>
                    <Text
                      style={[
                        styles.textBold,
                        { flex: 1, alignSelf: 'flex-start' },
                      ]}
                    >
                      {'Paid'}
                    </Text>
                    <Text style={[styles.text, { flex: 1 }]}>{`${this.props
                      .users - this.state.freeUsers} x $10`}</Text>
                    <Text
                      style={[
                        styles.text,
                        { alignSelf: 'flex-end', paddingHorizontal: 10 },
                      ]}
                    >
                      {`$${(this.props.users - this.state.freeUsers) * 10}`}
                    </Text>
                  </View>
                </View>
                <View style={styles.cell}>
                  <Text
                    style={[
                      styles.text,
                      { color: '#a8a4b9', paddingVertical: 10 },
                    ]}
                  >
                    {'Bots'}
                  </Text>
                  {R.map(
                    bot => (
                      <View key={bot.id} style={styles.botRow}>
                        <View style={{ flex: 1, alignSelf: 'flex-start' }}>
                          <ContactIcon
                            show={true}
                            id={R.propOr(null, '_id', bot)}
                          />
                        </View>
                        <Text
                          style={[
                            styles.textBold,
                            {
                              flex: 4,
                            },
                          ]}
                        >
                          {R.pathOr('Bot Name', ['basicData', 'name'], bot)}
                        </Text>
                        <Text
                          style={[
                            styles.text,
                            {
                              paddingHorizontal: 10,
                              // flex: 1,
                              alignSelf: 'flex-end',
                            },
                          ]}
                        >
                          {`$${R.propOr(0, 'price', bot)}`}
                        </Text>
                      </View>
                    ),
                    this.props.bots,
                  )}
                </View>
                <View
                  style={[
                    styles.cell,
                    { flexDirection: 'column', borderBottomWidth: 0 },
                  ]}
                >
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      paddingBottom: 15,
                    }}
                  >
                    <Text style={styles.textBold}>{'Mounthly total'}</Text>
                    <Text style={[styles.textBold, { fontSize: 18 }]}>{`$${(this
                      .props.users -
                      this.state.freeUsers) *
                      10 +
                      this.state.botsPrice}`}</Text>
                  </View>
                  <View style={styles.nextPayment}>
                    <Text
                      style={[
                        styles.text,
                        { flex: 1, alignSelf: 'flex-start' },
                      ]}
                    >
                      {'Next payment due: '}
                    </Text>
                    <Text
                      style={[styles.textBold, { alignSelf: 'flex-start' }]}
                    >
                      {moment(this.props.registerDate)
                        .add(
                          1 +
                            Math.abs(
                              Math.ceil(
                                moment(this.props.registerDate).diff(
                                  moment(Date.now()),
                                  'month',
                                  true,
                                ),
                              ),
                            ),
                          'month',
                        )
                        .format('MMM DD, YYYY')}
                    </Text>
                  </View>
                </View>
                <View
                  style={[
                    styles.cell,
                    {
                      backgroundColor: '#f3eff2',
                      borderBottomWidth: 0,
                      flexDirection: 'column',
                    },
                  ]}
                >
                  <Text
                    style={[
                      styles.text,
                      {
                        paddingBottom: 10,
                        paddingHorizontal: 8,
                        color: '#a8a4b9',
                      },
                    ]}
                  >
                    {'My cards'}
                  </Text>
                  <ScrollView style={{ paddingBottom: 15 }} horizontal={true}>
                    <View style={{ flexDirection: 'row' }}>
                      {R.map(
                        card => (
                          <TouchableOpacity
                            key={card.id}
                            onPress={() => {
                              !card.isActive && this.handleOnPressCard(card.id);
                            }}
                          >
                            <View
                              style={[
                                styles.cardView,
                                card.isActive ? styles.activeCard : {},
                              ]}
                            >
                              <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'center',
                                }}
                              >
                                {card.isActive && (
                                  <View
                                    style={{
                                      flex: 1,
                                      paddingHorizontal: 10,
                                      paddingVertical: 2,
                                    }}
                                  >
                                    <Icon
                                      name="CheckIcon"
                                      width={20}
                                      height={20}
                                      fill="#2ad18c"
                                      viewBox="0 0 20 20"
                                    />
                                  </View>
                                )}
                                <Text
                                  style={{
                                    alignSelf: 'flex-end',
                                    paddingHorizontal: 10,
                                  }}
                                >
                                  {card.lastFour}
                                </Text>
                              </View>
                              <View
                                style={{
                                  flex: 1,
                                  justifyContent: 'space-between',
                                  alignSelf: 'flex-end',
                                }}
                              >
                                {card.type === 'MasterCard' && (
                                  <Icon
                                    name="MasterCard"
                                    // fill="#000000"
                                    width={40}
                                    height={40}
                                    viewBox={'0 0 60 60'}
                                  />
                                )}
                                {card.type === 'Visa' && (
                                  <Icon
                                    name="Visa"
                                    fill="#051d77"
                                    width={40}
                                    height={40}
                                    viewBox="-815 823.1 56.7 56.7"
                                  />
                                )}
                              </View>
                              {/* <Text>{card.expires}</Text> */}
                            </View>
                          </TouchableOpacity>
                        ),
                        this.props.cards,
                      )}

                      <TouchableOpacity
                        onPress={this.handleOnCreateCardPress}
                        style={styles.addCardButton}
                      >
                        <Text style={{ fontSize: 30 }}>{'+'}</Text>
                      </TouchableOpacity>
                    </View>
                  </ScrollView>
                  {this.state.openCreateCardForm && (
                    <CreateCardForm
                      styles={styles}
                      createCreditCards={this.props.createCreditCards}
                    />
                  )}
                </View>
                <Text
                  style={{
                    paddingHorizontal: 20,
                    paddingVertical: 15,
                  }}
                >
                  {'Payment history'}
                </Text>
                <View
                  style={{
                    flexDirection: 'column',
                  }}
                >
                  {R.map(
                    payment => (
                      <View
                        key={payment._id}
                        style={{
                          borderBottomWidth: StyleSheet.hairlineWidth,
                          borderBottomColor: '#000000',
                          paddingVertical: 10,
                        }}
                      >
                        <View
                          style={{
                            flexDirection: 'row',
                            paddingHorizontal: 20,
                          }}
                        >
                          <Text
                            style={[
                              styles.textBold,
                              { flex: 1, alignSelf: 'flex-start' },
                            ]}
                          >
                            {moment(payment.paidAt).format('MMM DD, YYYY')}
                          </Text>
                          <Text
                            style={[styles.text, { alignSelf: 'flex-end' }]}
                          >
                            {`$${payment.sum}`}
                          </Text>
                        </View>
                        <View style={{ paddingHorizontal: 20 }}>
                          <Text style={styles.text}>
                            {'Monthly subscription fee'}
                          </Text>
                        </View>
                      </View>
                    ),
                    this.props.paymentHistory,
                  )}
                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  bots: botsArraySelector(state),
  users: usersCountSelector(state),
  cards: cardsSelector(state),
  paymentHistory: paymentHistorySelector(state),
  registerDate: R.propOr('', 'registerDate', state.userData),
  paid: R.propOr('', 'paid', state.userData),
  safeAreaBackgroundColor: '#ffffff',
});

const mapDispatchToProps = () => ({
  setCreditCardActive,
  createCreditCards,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withSafeArea(Billing),
);
