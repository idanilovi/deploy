import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  NativeModules,
} from 'react-native';

export default class CreateCardForm extends Component {
  static displayName = 'CreateCardForm';
  constructor(props) {
    super(props);
    this.state = {
      cardNumber: '',
      month: '',
      year: '',
      vcc: '',
    };

    this.handleOnChangeCardNumber = this.handleOnChangeCardNumber.bind(this);
    this.handleOnChangevcc = this.handleOnChangevcc.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
  }

  handleOnChangeCardNumber(text) {
    this.setState({ cardNumber: text });
  }
  handleOnChangevcc(text) {
    this.setState({ vcc: text });
  }
  handleOnPress() {
    if (
      !this.state.cardNumber ||
      !this.state.vcc ||
      !this.state.month ||
      !this.state.year
    )
      return;
    NativeModules.CardPay.createCardCryptogram(
      this.state.cardNumber,
      this.state.cvv,
      `${this.state.month}.${this.state.year}`,
      'publicId12345',
      crypto => this.props.createCreditCards(crypto),
    );
  }

  render() {
    return (
      <View
        style={[
          this.props.styles.cell,
          { borderBottomWidth: 0, paddingHorizontal: 10 },
        ]}
      >
        <View style={this.props.styles.cardForm}>
          <Text style={{ paddingVertical: 15, color: '#ffffff' }}>
            {'CREDIT CARD'}
          </Text>
          <Text
            style={{
              fontSize: 10,
              color: '#ffffff',
              paddingBottom: 5,
            }}
          >
            {'CARD NUMBER'}
          </Text>
          <TextInput
            style={this.props.styles.cardNumberInput}
            onChangeText={this.handleOnChangeCardNumber}
            value={this.state.cardNumber}
            underlineColorAndroid="#ffffff"
            keyboardType="numeric"
            placeholder="CARD NUMBER"
            maxLength={19}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingVertical: 10,
            }}
          >
            <View style={{ marginRight: 5 }}>
              <Text style={{ color: '#ffffff', fontSize: 10 }}>{'MONTH'}</Text>
              <TextInput
                style={this.props.styles.dateInput}
                onChangeText={t => this.setState({ month: t })}
                value={this.state.month}
                underlineColorAndroid="#ffffff"
                keyboardType="numeric"
                placeholder="MM"
                maxLength={2}
              />
            </View>
            <Text
              style={{
                fontSize: 18,
                color: '#ffffff',
                paddingTop: 10,
              }}
            >
              {'/'}
            </Text>
            <View style={{ marginLeft: 5 }}>
              <Text style={{ color: '#ffffff', fontSize: 10 }}>{'YEAR'}</Text>
              <TextInput
                style={this.props.styles.dateInput}
                onChangeText={t => this.setState({ year: t })}
                value={this.state.year}
                underlineColorAndroid="#ffffff"
                keyboardType="numeric"
                placeholder="YY"
                maxLength={2}
              />
            </View>
            <View style={{ paddingHorizontal: 40 }}>
              <Text style={{ color: '#ffffff', fontSize: 10 }}>{'CVC'}</Text>
              <TextInput
                style={this.props.styles.dateInput}
                onChangeText={this.handleOnChangevcc}
                value={this.state.vcc}
                underlineColorAndroid="#ffffff"
                keyboardType="numeric"
                placeholder="CVC"
                maxLength={3}
              />
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={{
            backgroundColor: '#696285',
            flex: 1,
            height: 35,
            marginTop: 10,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 3,
          }}
          onPress={this.handleOnPress}
        >
          <Text style={{ color: '#ffffff' }}>{'Add card'}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
