import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, Button } from 'react-native';
import Comment from '../Comment';

export default class CommentGroup extends Component {
  static displayName = 'CommentGroup';
  static propTypes = {
    comments: PropTypes.array,
    someProp: PropTypes.bool,
    numberOfShownComments: PropTypes.number,
    type: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.state = { show: false };
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderComments = this.renderComments.bind(this);
    this.renderHiddenComments = this.renderHiddenComments.bind(this);
    this.renderCommentGroup = this.renderCommentGroup.bind(this);
  }
  handleOnPress() {
    this.setState({ show: true });
  }
  renderComments(comments) {
    const { type } = this.props;
    return comments.map(comment => (
      <Comment type={type} key={comment.id} comment={comment} />
    ));
  }
  renderHiddenComments(comments) {
    const { show } = this.state;
    if (show) {
      return this.renderComments(comments);
    }
    return (
      <View style={{ alignSelf: 'flex-start', paddingLeft: 46 }}>
        <Button title="show more" onPress={this.handleOnPress} />
      </View>
    );
  }
  renderCommentGroup() {
    const { comments, numberOfShownComments } = this.props;
    if (comments.length > numberOfShownComments) {
      const displayedComments = comments.slice(0, numberOfShownComments);
      const hiddenComments = comments.slice(
        numberOfShownComments,
        comments.length,
      );
      return (
        <View>
          {this.renderComments(displayedComments)}
          {this.renderHiddenComments(hiddenComments)}
        </View>
      );
    }
    return <View>{this.renderComments(comments)}</View>;
  }
  render() {
    return this.renderCommentGroup();
  }
}
