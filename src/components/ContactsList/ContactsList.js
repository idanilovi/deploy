import R from 'ramda';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text } from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
});

const contactsArraySelector = state =>
  R.filter(
    contact => contact.billingType === 'users',
    R.values(R.propOr({}, 'contacts', state)),
  );

const paramsUserIdSelector = (state, ownProps) =>
  R.path(['navigation', 'state', 'params', 'userId'], ownProps);

function mapStateToProps(state, ownProps) {
  return {
    contacts: contactsArraySelector(state),
    userId: paramsUserIdSelector(state, ownProps),
    myId: state.userData.userId,
  };
}

@connect(mapStateToProps)
export default class ContactList extends Component {
  static displayName = 'ContactList';
  static propTypes = {
    contacts: PropTypes.array,
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
    }),
  };
  constructor(props) {
    super(props);
    this.renderContacts = this.renderContacts.bind(this);
  }
  renderContacts() {
    const { contacts } = this.props;
    const { navigate } = this.props.navigation;
    return contacts.map(contact => (
      <Text
        key={contact._id}
        onPress={() => {
          navigate('DirectContact', {
            id: contact._id,
            type: 'Direct',
            title: 'Direct',
          });
        }}
      >
        {R.pathOr('', ['basicData', 'name'], contact)}
      </Text>
    ));
  }
  render() {
    return (
      <ScrollView style={styles.container}>{this.renderContacts()}</ScrollView>
    );
  }
}
