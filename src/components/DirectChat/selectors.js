import R from 'ramda';
import moment from 'moment';
import uuid from 'uuid';
import { createSelector } from 'reselect';
import { filesSelector } from '../../selectors';

const sortByCreatedAt = R.sortBy(R.prop('createdAt'));

export const userIdSelector = state =>
  R.pathOr(null, ['userData', 'userId'], state);

const contactIdSelector = (state, ownProps) =>
  R.propOr(null, 'contactId', ownProps);

const commentsSelector = state => R.propOr({}, 'comments', state);

const contactsSelector = state => R.propOr({}, 'contacts', state);

const contactSelector = createSelector(
  contactsSelector,
  contactIdSelector,
  (contacts, contactId) => R.propOr({}, contactId, contacts),
);

function addTimeIdToComment(comment) {
  const date = moment(comment.createdAt);
  return R.assoc(
    'timeId',
    `${date.year()}/${date.month()}/${date.date()}`,
    comment,
  );
}

function groupByTimeId(comment) {
  return comment.timeId;
}

const hydratedCommentsSelector = createSelector(
  commentsSelector,
  contactsSelector,
  (comments, contacts) =>
    R.reverse(
      sortByCreatedAt(
        R.map(
          comment =>
            R.assoc(
              'user',
              R.propOr({}, R.prop('from', comment), contacts),
              comment,
            ),
          R.values(comments),
        ),
      ),
    ),
);

const filesForCurrentDirectSelector = createSelector(
  userIdSelector,
  contactSelector,
  hydratedCommentsSelector,
  filesSelector,
  (userId, contact, comments, files) => {
    const billingType = R.prop('billingType', contact);
    const contactEmail = R.path(['basicData', 'email', 0], contact);
    const originUserIds = R.propOr([], 'originContactIds', contact);
    const filteredComments =
      billingType !== 'contacts'
        ? R.filter(
            comment =>
              (comment.to.includes(contact._id) && comment.from === userId) ||
              (comment.to.includes(userId) && comment.from === contact._id),
            comments,
          )
        : R.filter(
            comment =>
              comment.to.includes(contact._id) ||
              comment.from === contact._id ||
              originUserIds.includes(comment.from) ||
              (contactEmail && comment.metadata.to === contactEmail) ||
              (contactEmail && comment.metadata.from === contactEmail),
            comments,
          );
    return R.map(
      att => R.propOr({}, R.prop('id', att), files),
      R.flatten(
        R.map(comment => {
          return R.map(
            att => ({ id: R.path(['data', 'id'], att), type: att.type }),
            R.filter(
              att => ['image', 'file'].indexOf(att.type) !== -1,
              R.propOr([], 'att', comment),
            ),
          );
        }, filteredComments),
      ),
    );
  },
);

const filterFiles = files => R.filter(file => !!file.id, files);

export const filesForCurrentViewSelector = createSelector(
  filesForCurrentDirectSelector,
  directFiles => filterFiles(directFiles),
);

const directCommentsSelector = createSelector(
  userIdSelector,
  contactSelector,
  hydratedCommentsSelector,
  (userId, contact, comments) => {
    const billingType = R.prop('billingType', contact);
    const contactEmail = R.path(['basicData', 'email', 0], contact);
    const originUserIds = R.propOr([], 'originContactIds', contact);
    const filteredComments =
      billingType !== 'contacts'
        ? R.filter(
            comment =>
              (comment.to.includes(contact._id) && comment.from === userId) ||
              (comment.to.includes(userId) && comment.from === contact._id),
            comments,
          )
        : R.filter(
            comment =>
              comment.to.includes(contact._id) ||
              comment.from === contact._id ||
              originUserIds.includes(comment.from) ||
              (contactEmail && comment.metadata.to === contactEmail) ||
              (contactEmail && comment.metadata.from === contactEmail),
            comments,
          );
    return R.groupBy(
      groupByTimeId,
      R.map(
        addTimeIdToComment,
        R.reverse(R.filter(comment => !comment.threadId, filteredComments)),
      ),
    );
  },
);

const reverseFlattenFilteredArray = R.compose(
  R.reverse,
  R.filter(_ => _),
  R.flatten,
  R.map(_ => [{ _id: _.title, header: true, title: _.title }].concat(_.data)),
);

export const sectionCommentsSelector = createSelector(
  directCommentsSelector,
  directComments => {
    const direct = Object.keys(directComments).map(key => ({
      title: key,
      data: directComments[key],
    }));
    return reverseFlattenFilteredArray(direct);
  },
);

export const stickyHeadersIndex = createSelector(
  sectionCommentsSelector,
  comments =>
    R.map(comment => comment.header && comments.indexOf(comment), comments),
);

export const userSelector = createSelector(
  contactsSelector,
  userIdSelector,
  (contacts, userId) => R.propOr({}, userId, contacts),
);

export const isSame = (prev, current, next) => {
  const currentDate = Date.now();
  return {
    isSameUserPrev:
      R.path(['user', '_id'], current) === R.path(['user', '_id'], prev),
    isSameUserNext:
      R.path(['user', '_id'], current) === R.path(['user', '_id'], next),
    isSameTimePrev:
      Math.abs(
        R.propOr(currentDate, 'createdAt', current) -
          R.propOr(currentDate, 'createdAt', prev),
      ) <=
      15 * 60 * 1000,
    isSameTimeNext:
      Math.abs(
        R.propOr(currentDate, 'createdAt', next) -
          R.propOr(currentDate, 'createdAt', current),
      ) <=
      15 * 60 * 1000,
  };
};
