import R from 'ramda';
import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {
  Animated,
  Platform,
  Clipboard,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import { getComments, getFiles } from '../../actions';
import Comment from '../Comment';
import InputToolbar from '../../components/InputToolbar';
import {
  sectionCommentsSelector,
  userIdSelector,
  userSelector,
  stickyHeadersIndex,
  filesForCurrentViewSelector,
  isSame,
} from './selectors';

const { width, height } = Dimensions.get('window');

const mapStateToProps = (state, ownProps) => ({
  comments: sectionCommentsSelector(state, ownProps),
  userId: userIdSelector(state),
  user: userSelector(state),
  contacts: state.contacts,
  streams: state.streams,
  stickyHeadersIndex: stickyHeadersIndex(state, ownProps),
  files: filesForCurrentViewSelector(state, ownProps),
});

const mapDispatchToProps = {
  getComments,
  getFiles,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class DirectChat extends Component {
  static displayName = 'DirectChat';
  static defaultProps = {
    contactId: '',
    styles: {},
  };
  static propTypes = {
    getFiles: PropTypes.func.isRequired,
    getComments: PropTypes.func.isRequired,
    contactId: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    comments: PropTypes.array,
    dontRenderInputToolbar: PropTypes.bool,
    navigator: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.isCloseToTop = this.isCloseToTop.bind(this);
    this.handleOnLoadEarlier = this.handleOnLoadEarlier.bind(this);
    this.handleOnLongPress = this.handleOnLongPress.bind(this);
    this.renderInputToolbar = this.renderInputToolbar.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.keyExtractor = this.keyExtractor.bind(this);
    this.state = { isLoadingEarlier: false, sticky: [] };
    this.state = {
      comments: this.props.comments,
      update: false,
    };
  }

  componentWillReceiveProps(nextprops) {
    if (
      nextprops.comments.filter(
        (item, i) => !R.equals(this.props.comments[i], item),
      ).length
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  handleOnLoadEarlier() {
    this.setState({ isLoadingEarlier: true });
    if (this.props.userId && this.props.contactId) {
      this.props.getComments(
        {
          to: [this.props.userId],
          from: [this.props.contactId],
          skip: this.props.comments.length,
          limit: 30,
        },
        () => this.setState({ isLoadingEarlier: false }),
      );
    }
  }

  renderInputToolbar() {
    if (this.props.dontRenderInputToolbar) {
      return null;
    }
    return (
      <InputToolbar
        type="Direct"
        contactId={this.props.contactId}
        refCommentList={this.refCommentList}
      />
    );
  }

  // eslint-disable-next-line class-methods-use-this
  handleOnLongPress(context, currentMessage) {
    if (currentMessage) {
      if (
        !currentMessage.type ||
        ['mail', 'telegram', 'livechat'].indexOf(currentMessage.type) !== -1
      ) {
        const options = ['Copy Text', 'Cancel'];
        const cancelButtonIndex = options.length - 1;
        context.actionSheet().showActionSheetWithOptions(
          {
            options,
            cancelButtonIndex,
          },
          buttonIndex => {
            switch (buttonIndex) {
              case 0: {
                const text = currentMessage.att
                  .map(attachment => {
                    if (attachment.type === 'text') {
                      return attachment.data.text;
                    }
                    if (attachment.type === 'mail') {
                      return attachment.data.text;
                    }
                    return '';
                  })
                  .join();
                Clipboard.setString(text);
                break;
              }
              default: {
                break;
              }
            }
          },
        );
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  isCloseToTop({ layoutMeasurement, contentOffset, contentSize }) {
    const paddingToTop = 80;
    return (
      contentSize.height - layoutMeasurement.height - paddingToTop <=
      contentOffset.y
    );
  }

  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item) {
    return item._id;
  }

  renderItem({ item, index }) {
    if (!item) return null;
    return item.header ? (
      <View
        style={{
          flex: 0,
          backgroundColor: 'rgba(78, 73, 99, 0.3)',
          borderRadius: 10,
          paddingVertical: 7,
          marginVertical: 8,
          paddingHorizontal: 10,
          alignSelf: 'center',
        }}
      >
        <Text
          style={{
            fontWeight: '500',
            fontSize: 15,
            color: '#ffffff',
          }}
        >
          {moment(item.title.split('/')).calendar(null, {
            sameDay: '[Today]',
            nextDay: 'DD MMM',
            nextWeek: 'DD MMM',
            lastDay: '[Yesterday]',
            lastWeek: 'DD MMM',
            sameElse: 'DD MMM',
          })}
        </Text>
      </View>
    ) : (
      <Comment
        comment={item}
        type="Direct"
        filesContext={this.props.files}
        isUserMessage={
          item.metadata.userId === this.props.userId ||
          item.from === this.props.userId
        }
        isSameUser={isSame(
          this.props.comments[index + 1],
          item,
          this.props.comments[index - 1],
        )}
        navigator={this.props.navigator}
      />
    );
  }

  render() {
    return (
      <Animated.View
        style={{
          flex: 1,
          width,
          backgroundColor: '#ffffff',
        }}
      >
        <FlatList
          keyExtractor={this.keyExtractor}
          data={this.props.comments}
          renderItem={this.renderItem}
          inverted={true}
          onEndReachedThreshold={0.5}
          onEndReached={this.handleOnLoadEarlier}
        />
        {this.renderInputToolbar()}
        {Platform.OS === 'ios' && <KeyboardSpacer />}
      </Animated.View>
    );
  }
}
