import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import React, { PureComponent } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { PagerDotIndicator, IndicatorViewPager } from 'rn-viewpager';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
// import Carousel from 'react-native-snap-carousel';
import Carousel from 'react-native-carousel-control';
import ThreadCard from '../ThreadCard';
import { createThread } from '../../actions';
import global from '../../global';
import { userIdSelector } from '../../reducers/userdata';

const statusesSelector = state => state && state.statuses;
const streamsSelector = state => state && state.streams;
const threadsSelector = state => state && state.threads;

const sortByCreatedAt = R.sortBy(R.prop('createdAt'));
const sortByPosition = R.sortBy(R.prop('position'));

const streamIdSelector = (state, ownProps) =>
  R.pathOr(
    R.propOr(null, 'streamId', ownProps),
    ['navigation', 'state', 'params', 'id'],
    ownProps,
  );

const streamByIdSelector = createSelector(
  streamsSelector,
  streamIdSelector,
  (streams, streamId) => R.propOr({}, streamId, streams),
);
const OrderedStatusesSelector = createSelector(
  streamByIdSelector,
  statusesSelector,
  (stream, statuses) =>
    R.propOr([], 'threadStatuses', stream).map(id => statuses[id] || {}),
);
const threadByStreamSelector = createSelector(
  threadsSelector,
  streamIdSelector,
  (threads, streamId) =>
    R.filter(thread => thread.streamId === streamId, R.values(threads)),
);

const threadsByStatusSelector = createSelector(
  OrderedStatusesSelector,
  threadByStreamSelector,
  (statuses, tasks) =>
    R.groupBy(
      task => task.status || 'other',
      // TODO нахуй сортировать по createdAt?
      sortByPosition(sortByCreatedAt(tasks)),
    ),
);

function mapStateToProps(state, ownProps) {
  const streamId = streamIdSelector(state, ownProps);
  const stream = streamByIdSelector(state, ownProps);
  const threads = threadsByStatusSelector(state, ownProps);
  const statuses = OrderedStatusesSelector(state, ownProps);
  const userId = userIdSelector(state);
  return {
    streamId,
    threads,
    stream,
    statuses,
    userId,
  };
}
const mapDispatchToProps = {
  createThread,
};

const { width, height } = Dimensions.get('window');
const itemWidth = width - 30;

const styles = StyleSheet.create({
  header: {
    marginVertical: 5,
    width: itemWidth,
    borderRadius: 5,
  },
  title: {
    textAlign: 'center',
    fontWeight: '200',
    fontSize: 25,
    paddingVertical: 12,
    color: '#a9a5ba',
  },
  board: {
    width: itemWidth,
    alignItems: 'center',
    height: height - 124,
  },
  list: {
    flex: 1,
  },
  dot: {
    // backgroundColor: '#000000',
    width: 24,
    height: 6,
    margin: 1,
  },
  createButton: {
    backgroundColor: '#01c69e',
    position: 'absolute',
    bottom: 15,
    right: 15,
    borderColor: '#ede9eb',
    borderWidth: 1,
    borderRadius: 100,
    height: 48,
    width: 48,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addIcon: {
    backgroundColor: 'transparent',
    color: '#ffffff',
  },
});
@connect(mapStateToProps, mapDispatchToProps)
export default class Kanban extends PureComponent {
  static displayName = 'Kanban';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    createThread: PropTypes.func.isRequired,
    streamId: PropTypes.string,
    userId: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.state = { currentPage: 0 };
    this.renderList = this.renderList.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderIndicator = this.renderIndicator.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.curPage = this.curPage.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item) {
    return item.id;
  }
  renderItem({ item }) {
    return (
      <ThreadCard
        key={item.id}
        thread={item}
        navigator={this.props.navigator}
      />
    );
  }
  handleOnPress(statusId) {
    global.tracker.trackEvent('Нажатия на кнопочки', 'create thread');
    this.props.createThread(
      {
        statusId,
        title: '',
        streamId: this.props.streamId,
        roles: [this.props.userId],
        authorId: this.props.userId,
      },
      id =>
        this.props.navigator.push({
          screen: 'workonflow.Thread',
          title: '',
          passProps: {
            id,
            threadId: id,
            type: 'Thread',
            focusOnTitle: true,
          },
        }),
    );
  }
  renderCreateButton(statusId) {
    if (statusId) {
      return (
        <TouchableOpacity
          onPress={() => this.handleOnPress(statusId)}
          style={styles.createButton}
        >
          <Icon style={styles.addIcon} name="plus" size={24} />
        </TouchableOpacity>
      );
    }
    return null;
  }
  renderList() {
    return this.props.statuses.map((item, index) => (
      <View key={item.id} style={styles.board}>
        {this.renderHeader(item)}
        <FlatList
          style={styles.list}
          data={R.propOr([], item.id, this.props.threads)}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      </View>
    ));
  }
  // eslint-disable-next-line class-methods-use-this
  renderHeader(title) {
    return (
      <View style={styles.header}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          {R.pathOr('', ['name'], title)}
        </Text>
        <View
          style={{
            backgroundColor: R.propOr('#000000', 'color', title),
            height: 8,
            width: width - 30,
            borderRadius: 4,
            marginBottom: 4,
          }}
        />
      </View>
    );
  }
  renderIndicator(count) {
    return (
      <PagerDotIndicator
        dotStyle={styles.dot}
        selectedDotStyle={{
          width: 23,
          height: 5,
          margin: 1,
          backgroundColor: R.pathOr(
            0,
            [this.state.currentPage, 'color'],
            this.props.statuses,
          ),
        }}
        pageCount={count}
      />
    );
  }

  curPage(state) {
    this.setState({ currentPage: state.position });
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#f3eff2',
          overflow: 'hidden',
        }}
      >
        <IndicatorViewPager
          style={{ flex: 1 }}
          initialPage={0}
          indicator={this.renderIndicator(this.props.statuses.length)}
          onPageSelected={this.curPage}
        >
          {this.renderList()}
        </IndicatorViewPager>
        {this.renderCreateButton(R.path(['0', 'id'], this.props.statuses))}
      </View>
    );
  }
}
