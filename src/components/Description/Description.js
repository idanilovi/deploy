import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import ProsemirrorRenderer from './ProsemirrorRenderer';

const descriptionsSelector = state => R.propOr({}, 'descriptions', state);
const descriptionIdSelector = (state, ownProps) =>
  R.propOr(null, 'descriptionId', ownProps);

const descriptionByIdSelector = createSelector(
  descriptionsSelector,
  descriptionIdSelector,
  (descriptions, descriptionId) => R.propOr({}, descriptionId, descriptions),
);

const mapStateToProps = (state, ownProps) => ({
  description: descriptionByIdSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class Description extends PureComponent {
  static displayName = 'Description';
  static propTypes = {
    description: PropTypes.shape({
      content: PropTypes.object,
    }),
    navigator: PropTypes.object,
  };
  render() {
    return ProsemirrorRenderer(
      this.props.description.content,
      this.props.navigator,
    );
  }
}
