import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from '../../Icon';

const styles = StyleSheet.create({
  container: {
    flex: 0,
    paddingTop: 12,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
});

const ThreadLinkIcon = () => (
  <View style={styles.container}>
    <Icon
      name="External"
      width={14}
      height={14}
      viewBox="0 0 24 24"
      fill="none"
      stroke="rgb(168, 164, 185)"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
    />
  </View>
);

ThreadLinkIcon.displayName = 'ThreadLinkIcon';

export default ThreadLinkIcon;
