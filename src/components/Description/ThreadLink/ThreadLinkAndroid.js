import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    fontWeight: '600',
  },
});

export default class ThreadLinkAndroid extends PureComponent {
  static displayName = 'ThreadLinkAndroid';
  static propTypes = {
    threadTitle: PropTypes.string,
    streamTitle: PropTypes.string,
    statusTitle: PropTypes.string,
    statusColor: PropTypes.string,
    onPress: PropTypes.func.isRequired,
  };
  render() {
    const {
      threadTitle,
      streamTitle,
      statusTitle,
      statusColor,
      onPress,
    } = this.props;
    return (
      <Text style={styles.container} onPress={onPress}>
        {` "${threadTitle}" thread in "${streamTitle}" stream in `}
        <Text style={{ color: statusColor }}>{`"${statusTitle}" `}</Text>
        <Text>status </Text>
      </Text>
    );
  }
}
