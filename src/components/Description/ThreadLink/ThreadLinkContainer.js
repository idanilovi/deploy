import React, { PureComponent } from 'react';
import { TouchableHighlight, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    minWidth: '100%',
    height: 70,
    width: '100%',
    borderRadius: 10,
    borderColor: '#b8b8b8',
    backgroundColor: '#ffffff',
    borderWidth: StyleSheet.hairlineWidth,
    paddingLeft: 2,
    paddingRight: 14,
    flexDirection: 'row',
    marginVertical: 8,
  },
});

export default class ThreadLinkContainer extends PureComponent {
  static displayName = 'ThreadLinkContainer';
  static propTypes = {
    children: PropTypes.node,
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    onPress: PropTypes.func.isRequired,
  };
  render() {
    return (
      <TouchableHighlight
        onPress={this.props.onPress}
        underlayColor="transparent"
      >
        <View style={styles.container}>{this.props.children}</View>
      </TouchableHighlight>
    );
  }
}
