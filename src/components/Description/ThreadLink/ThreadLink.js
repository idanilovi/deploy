import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Platform } from 'react-native';
import ThreadLinkContainer from './ThreadLinkContainer';
import ThreadLinkStatusIcon from './ThreadLinkStatusIcon';
import ThreadLinkText from './ThreadLinkText';
import ThreadLinkIcon from './ThreadLinkIcon';
import ThreadLinkAndroid from './ThreadLinkAndroid';
import { hydratedThreadSelector } from '../../../selectors';

const mapStateToProps = (state, ownProps) => ({
  thread: hydratedThreadSelector(state, ownProps),
});

class ThreadLink extends Component {
  static displayName = 'ThreadLink';
  static propTypes = {
    id: PropTypes.string,
    thread: PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      stream: PropTypes.shape({
        name: PropTypes.string,
      }),
      progress: PropTypes.number,
    }),
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    description: PropTypes.bool,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { navigator, id } = this.props;
    navigator.push({
      screen: 'workonflow.Thread',
      passProps: { id, type: 'Thread' },
    });
  }
  render() {
    const {
      thread: { title, stream, status, progress },
      description,
    } = this.props;
    if (Platform.OS === 'ios' || description) {
      return (
        <ThreadLinkContainer onPress={this.handleOnPress}>
          <ThreadLinkStatusIcon color={status.color} progress={progress} />
          <ThreadLinkText threadTitle={title} streamTitle={stream.name} />
          <ThreadLinkIcon />
        </ThreadLinkContainer>
      );
    }
    return (
      <ThreadLinkAndroid
        statusTitle={status.name}
        statusColor={status.color}
        threadTitle={title}
        streamTitle={stream.name}
        onPress={this.handleOnPress}
      />
    );
  }
}

export default connect(mapStateToProps)(ThreadLink);
