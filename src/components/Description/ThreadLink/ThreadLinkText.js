import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  thread: {
    fontSize: 15,
    color: '#4e4963',
    fontWeight: '400',
    paddingVertical: 7,
  },
  stream: {
    fontSize: 12,
    color: '#4e4963',
    opacity: 0.5,
    fontWeight: '400',
    paddingVertical: 7,
  },
});

export default class ThreadLinkText extends PureComponent {
  static displayName = 'ThreadLinkText';
  static propTypes = {
    threadTitle: PropTypes.string,
    streamTitle: PropTypes.string,
  };
  render() {
    const { threadTitle, streamTitle } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.thread} numberOfLines={1} ellipsizeMode="tail">
          {threadTitle}
        </Text>
        <Text style={styles.stream} numberOfLines={1} ellipsizeMode="tail">
          {streamTitle}
        </Text>
      </View>
    );
  }
}
