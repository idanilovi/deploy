import React, { PureComponent } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Icon from '../../Icon/ThreadAvatarSvg';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingTop: 14,
    paddingBottom: 16,
  },
});

export default class ThreadLinkStatusIcon extends PureComponent {
  static displayName = 'ThreadLinkStatusIcon';
  static propTypes = {
    color: PropTypes.string,
    progress: PropTypes.number,
  };
  render() {
    const { color, progress } = this.props;
    return (
      <View style={styles.container}>
        <Icon
          name="threadAvatar"
          width={40}
          height={40}
          fill={color}
          dasharray={`${progress}, 100`}
          viewBox="0 0 36 36"
        />
      </View>
    );
  }
}
