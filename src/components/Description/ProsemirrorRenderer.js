import React from 'react';
import PropTypes from 'prop-types';
import { Linking, StyleSheet, View, Text } from 'react-native';
import CheckBox from 'react-native-check-box';
import ThreadListEntry from '../ThreadListEntry';
import { ImageBlock, FileBlock } from '../Comment/CommentBlocks';
import ThreadLink from './ThreadLink';

const stylesMapper = ({ type }) => {
  if (type === 'strong') {
    return { fontWeight: '600' };
  }
  if (type === 'em') {
    return { fontWeight: '300', fontStyle: 'italic' };
  }
  if (type === 'underline') {
    return { textDecorationLine: 'underline' };
  }
  if (type === 'strike') {
    return { textDecorationLine: 'line-through' };
  }
  if (type === 'link') {
    return { textDecorationLine: 'underline', color: '#337ab7' };
  }
  return { fontWeight: '300' };
};

const propsMapper = ({ type, attrs }) => {
  if (type === 'link') {
    return {
      selectable: true,
      onPress: () => {
        Linking.openURL(attrs.href);
      },
    };
  }
  return { selectable: true };
};

const Renderer = (
  {
    type,
    content = [],
    attrs = {},
    text = '',
    marks = [],
    index = 0,
    prevType = '',
    isChecked = false,
  },
  navigator,
) => {
  const RendererWithNavigator = content => Renderer(content, navigator);
  if (type) {
    if (type === 'doc') {
      if (content.length) {
        return (
          <View
            style={{
              paddingHorizontal: 16,
              paddingVertical: 16,
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderBottomColor: 'rgba(40,30,42,.95)',
              flexDirection: 'column',
            }}
          >
            {content.map(RendererWithNavigator)}
          </View>
        );
      }
      return <View />;
    }
    if (type === 'paragraph') {
      if (content.length) {
        return content.map(_ =>
          RendererWithNavigator(Object.assign({}, _, { isChecked })),
        );
      }
      return <View />;
    }
    if (type === 'text') {
      if (text) {
        return (
          <Text
            style={Object.assign(
              { fontSize: 16, fontWeight: '300' },
              ...marks.map(stylesMapper),
              isChecked ? { textDecorationLine: 'line-through' } : {},
            )}
            {...Object.assign({ selectable: true }, ...marks.map(propsMapper))}
          >
            {index ? `${index}. ${text}` : text}
          </Text>
        );
      }
      return null;
    }
    if (type === 'ordered_check_list') {
      if (content.length) {
        return content.map(RendererWithNavigator);
      }
      return null;
    }
    if (type === 'check_list_item') {
      if (content.length) {
        return (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              flexWrap: 'wrap',
              paddingVertical: 4,
            }}
          >
            <CheckBox disabled={true} isChecked={attrs.isCheck} />
            {content.map(_ =>
              RendererWithNavigator(
                Object.assign({}, _, {
                  isChecked: attrs.isCheck,
                }),
              ),
            )}
          </View>
        );
      }
      return <CheckBox disabled={true} checked={attrs.isCheck} />;
    }
    if (type === 'file') {
      return (
        <FileBlock
          fileId={attrs.id}
          styles={{ container: { padding: 8 } }}
          dontOpenModal={true}
        />
      );
    }
    if (type === 'list_item') {
      if (attrs.id) {
        return (
          <ThreadListEntry
            id={attrs.id}
            styles={{
              container: { backgroundColor: '#f4f4f6', marginVertical: 4 },
              title: { color: '#4e4963' },
            }}
            navigator={navigator}
          />
        );
      }
      if (prevType === 'ordered_number_list') {
        return (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
            <View style={{ alignSelf: 'flex-start' }}>
              <Text style={{ fontSize: 16, fontWeight: '300' }}>
                {`${index + 1}. `}
              </Text>
            </View>
            <View style={{ paddingLeft: 8 }}>
              {content.map(_ =>
                RendererWithNavigator(
                  Object.assign({}, _, {
                    prevType: 'list_item',
                  }),
                ),
              )}
            </View>
          </View>
        );
      }
      if (prevType === 'ordered_list') {
        return (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
            <View style={{ alignSelf: 'flex-start' }}>
              <Text
                style={{ fontSize: 16, fontWeight: '300', color: '#b2afc1' }}
              >
                &bull;
              </Text>
            </View>
            <View style={{ paddingLeft: 8 }}>
              {content.map(_ =>
                RendererWithNavigator(
                  Object.assign({}, _, {
                    prevType: 'list_item',
                  }),
                ),
              )}
            </View>
          </View>
        );
      }
      return content.map(_ =>
        RendererWithNavigator(Object.assign({}, _, { prevType: 'list_item' })),
      );
    }
    if (type === 'image') {
      return <ImageBlock fileId={attrs.id} />;
    }
    if (type === 'horizontal_rule') {
      return (
        <View
          style={{
            marginVertical: 8,
            height: StyleSheet.hairlineWidth,
            backgroundColor: 'rgba(40,30,42,.95)',
          }}
        />
      );
    }
    if (type === 'ordered_list') {
      if (content.length) {
        if (prevType === 'list_item') {
          return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
              {content.map((_, index) =>
                RendererWithNavigator(
                  Object.assign({}, _, {
                    prevType: 'ordered_list',
                    index,
                  }),
                ),
              )}
            </View>
          );
        }
        return (
          <View style={{ flex: 1, flexDirection: 'column' }}>
            {content.map((_, index) =>
              RendererWithNavigator(
                Object.assign({}, _, {
                  prevType: 'ordered_list',
                  index,
                }),
              ),
            )}
          </View>
        );
      }
      return null;
    }
    if (type === 'ordered_number_list') {
      if (content.length) {
        if (prevType === 'list_item') {
          return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
              {content.map((_, index) =>
                RendererWithNavigator(
                  Object.assign({}, _, {
                    prevType: 'ordered_number_list',
                    index,
                  }),
                ),
              )}
            </View>
          );
        }
        return (
          <View style={{ flex: 1, flexDirection: 'column' }}>
            {content.map((_, index) =>
              RendererWithNavigator(
                Object.assign({}, _, {
                  prevType: 'ordered_number_list',
                  index,
                }),
              ),
            )}
          </View>
        );
      }
    }
    if (type === 'link_thread') {
      return (
        <ThreadLink id={attrs.id} navigator={navigator} description={true} />
      );
    }
    return null;
  }
  return null;
};

Renderer.displayName = 'Renderer';
Renderer.propTypes = {
  type: PropTypes.string,
  content: PropTypes.array,
  attrs: PropTypes.object,
  text: PropTypes.string,
  marks: PropTypes.array,
  index: PropTypes.number,
  prevType: PropTypes.string,
  isChecked: PropTypes.bool,
};

export default (content, navigator) => {
  if (content) {
    return Renderer(content, navigator);
  }
  return null;
};
