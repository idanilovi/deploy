import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import React, { Component } from 'react';
import { createSelector } from 'reselect';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import ContactIcon from '../ContactIcon';
import global from '../../global';
import { threadsSelector } from '../../reducers/threads';
import { contactsSelector } from '../../reducers/contacts';
import { statusesSelector } from '../../reducers/statuses';
import { streamsSelector } from '../../reducers/streams';
import Icons from '../Icon';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: width - 30,
    height: 131,
    borderRadius: 7,
    backgroundColor: '#fefefe',
    marginBottom: 14,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
  },
  footer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleWrapper: {
    flex: 1,
    paddingRight: 20,
  },
  title: {
    fontSize: 16,
    fontWeight: '500',
    color: '#696285',
  },
  text: {
    fontSize: 14,
    fontWeight: '200',
    color: '#a8a4b9',
  },
  priorityWrapper: {
    flex: 0,
  },
  rocketWrapper: {
    flex: 0,
  },
  priorityIcon: {
    width: 20,
    height: 20,
  },
  deadline: {
    flex: 4,
    paddingVertical: 4,
  },
});

function threadHydrator(threads, statuses, streams, contacts) {
  const memo = new Map();
  function hydrator(thread) {
    if (!thread) return {};
    if (memo.has(thread.id)) return memo.get(thread.id);
    const newChildren = (thread.children || []).map(child => threads[child]);
    const withStream = R.assoc('streamId', streams[thread.streamId], thread);
    const withStatus = R.assoc('status', statuses[thread.status], withStream);
    const withMasterStatus = R.assoc(
      'masterStatus',
      statuses[thread.masterStatus],
      withStatus,
    );
    const withChildren = R.assoc('children', newChildren, withMasterStatus);
    const responsibleUser = R.prop(thread.responsibleUserId, contacts);
    const withResponsible = R.assoc(
      'responsibleUser',
      responsibleUser,
      withChildren,
    );
    const threadRoles = R.pick(thread.roles || [], contacts);
    const withRoles = R.assoc('roles', R.values(threadRoles), withResponsible);
    memo.set(thread.id, withRoles);
    return withRoles;
  }
  return hydrator;
}

const threadHydratorSelector = createSelector(
  threadsSelector,
  statusesSelector,
  streamsSelector,
  contactsSelector,
  threadHydrator,
);

const mapStateToProps = (state, ownProps) => ({
  task: threadHydratorSelector(state)(ownProps.thread),
});

@connect(mapStateToProps)
export default class ThreadCard extends Component {
  static displayName = 'ThreadCard';
  static propTypes = {
    navigator: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    task: PropTypes.object,
    thread: PropTypes.shape({
      id: PropTypes.string,
    }),
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderDeadline = this.renderDeadline.bind(this);
    this.renderPriority = this.renderPriority.bind(this);
    this.renderRank = this.renderRank.bind(this);
    this.renderResponsible = this.renderResponsible.bind(this);
    this.renderTitle = this.renderTitle.bind(this);
    this.renderRocket = this.renderRocket.bind(this);
    this.state = {
      update: false,
    };
  }

  componentWillReceiveProps(nextprops) {
    if (
      !R.equals(this.props.thread, nextprops.thread) ||
      !R.equals(this.props.task, nextprops.task)
    ) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }
    return false;
  }
  handleOnPress() {
    global.tracker.trackEvent('Нажатия на кнопочки', 'thread');
    this.props.navigator.push({
      screen: 'workonflow.Thread',
      title: '',
      passProps: {
        id: this.props.thread.id,
        threadId: this.props.thread.id,
        type: 'Thread',
      },
    });
  }
  renderDeadline() {
    const firstDate = R.pathOr(null, ['deadline', '0'], this.props.task);
    const secondDate = R.pathOr(null, ['deadline', '1'], this.props.task);
    if (firstDate && secondDate) {
      return (
        <View style={styles.deadline}>
          <Text style={styles.text}>
            {`${moment(firstDate).format('DD MMM')} - ${moment(
              secondDate,
            ).format('DD MMM')}`}
          </Text>
        </View>
      );
    }
    if (!firstDate && secondDate) {
      return (
        <View style={styles.deadline}>
          <Text style={styles.text}>{moment(secondDate).format('DD MMM')}</Text>
        </View>
      );
    }
    return null;
  }
  renderRank() {
    // console.log('CHILDREN: ', this.props.task.countChildren);
    const childrenCount = this.props.task.countChildren;
    const rank = R.propOr(null, 'mainRank', this.props.task);
    if (rank > 0) {
      return (
        <View style={{ paddingVertical: 4 }}>
          <Text style={styles.text}>{`pt. ${rank}`}</Text>
        </View>
      );
    }
    return (
      <View style={{ paddingVertical: 4 }}>
        <Text style={styles.text}>
          {childrenCount ? `> ${childrenCount}` : ''}
        </Text>
      </View>
    );

    return null;
  }
  renderResponsible() {
    return (
      <View style={{ flex: 1 }}>
        <ContactIcon
          styles={{
            container: {
              width: 28,
              height: 28,
              borderRadius: 14,
              borderStyle: 'dashed',
            },
            text: {
              fontSize: 12,
            },
          }}
          show={true}
          id={R.pathOr('', ['responsibleUser', '_id'], this.props.task)}
          // name={R.pathOr(
          //   ' ',
          //   ['responsibleUser', 'basicData', 'name'],
          //   this.props.task,
          // )}
          // color={R.pathOr('', ['responsibleUser', 'color'], this.props.task)}
          // type={R.pathOr(
          //   null,
          //   ['responsibleUser', 'billingType'],
          //   this.props.task,
          // )}
        />
      </View>
    );
  }
  renderPriority() {
    if (R.propOr('', 'priority', this.props.task) === 'HIGH') {
      return (
        <View style={styles.priorityWrapper}>
          <Image
            // eslint-disable-next-line global-require
            source={require('../../assets/images/fire.png')}
            style={styles.priorityIcon}
          />
        </View>
      );
    }
    return null;
  }
  renderTitle() {
    return (
      <View style={styles.titleWrapper}>
        <Text ellipsizeMode="tail" numberOfLines={2} style={styles.title}>
          {R.propOr('', 'title', this.props.task)}
        </Text>
      </View>
    );
  }
  renderRocket() {
    if (!R.pathOr(false, ['task', 'streamId', 'id'], this.props)) {
      return (
        <View style={styles.rocketWrapper}>
          <Icons
            name="Rocket"
            width={24}
            height={24}
            fill="#aba7bb"
            viewBox="-30 25 50 50"
          />
        </View>
      );
    }
    return null;
  }
  render() {
    return (
      <TouchableOpacity onPress={this.handleOnPress}>
        <View style={styles.container}>
          <View style={styles.header}>
            {this.renderTitle()}
            {this.renderRocket()}
            {this.renderPriority()}
          </View>
          <View style={styles.footer}>
            {this.renderResponsible()}
            {this.renderDeadline()}
            {this.renderRank()}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
