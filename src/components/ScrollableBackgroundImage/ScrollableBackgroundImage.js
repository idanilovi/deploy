import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Animated, Dimensions, View } from 'react-native';
import { setWidth, setHeight } from '../../reducers/ui/background';

const { width, height } = Dimensions.get('window');

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  setHeight,
  setWidth,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class ScrollableBackgroundImage extends Component {
  static displayName = 'ScrollableBackgroundImage';
  static propTypes = {
    children: PropTypes.node,
    source: PropTypes.string,
    backgroundColor: PropTypes.string,
    blur: PropTypes.number,
    opacity: PropTypes.number,
  };
  constructor() {
    super();
    this.handleOnScroll = this.handleOnScroll.bind(this);
  }
  componentDidMount() {
    if (this.props.source.uri) {
      Image.getSize(
        this.props.source.uri,
        (imgWidth, imgHeight) => {
          this.props.setWidth(imgWidth);
          this.props.setHeight(imgHeight);
        },
        error => {
          console.error(error.message);
        },
      );
    }
  }
  setImagePosition(scrollValue) {
    const { backgroundParams: { imgWidth } } = this.props;
    const imgLeft = imgWidth / 3 * scrollValue;
    if (imgWidth >= width * 3) {
      this.setState({ imgLeft });
    } else {
      this.setState({ imgLeft: imgLeft * imgWidth / (width * 3) });
    }
  }
  handleOnScroll(scrollValue) {
    if (scrollValue >= 0 && scrollValue <= 2) {
      this.setImagePosition(scrollValue);
    }
  }
  render() {
    throw Error({ message: 'Этот комопнент не дописан!!!' });
    const { backgroundParams: { imgWidth } } = this.props;
    return (
      <Animated.View
        style={{
          flex: this.state.visible ? 1 : 0,
          width,
          height,
          backgroundColor: this.props.backgroundColor,
        }}
      >
        <Animated.Image
          source={this.props.source}
          style={{
            position: 'absolute',
            width: imgWidth,
            height,
            top: 0,
            left: -this.state.imgLeft,
          }}
          blurRadius={this.props.blur}
          resizeMode="cover"
          resizeMethod="scale"
        >
          <View
            style={{
              flex: 1,
              backgroundColor: 'rgba(40,30,42,0.95)',
              opacity: this.props.opacity / 100,
            }}
          />
        </Animated.Image>
        {this.props.children}
      </Animated.View>
    );
  }
}
