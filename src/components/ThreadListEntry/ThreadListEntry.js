import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import ContactIcon from '../ContactIcon';
import { getThread } from '../../actions';
import global from '../../global';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 4,
    // paddingBottom: 4,
    // paddingHorizontal: 12,
  },
  thread: {
    flex: 1,
    height: 74,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 14,
    // paddingLeft: 12,
    // backgroundColor: 'rgba(40,30,42,1)',
    // backgroundColor: 'red',
    borderRadius: 8,
    alignItems: 'flex-start',
  },
  title: {
    color: '#696285',
    fontSize: 20,
    width: width - 110,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingBottom: 4,
  },
  status: {
    fontSize: 16,
    fontWeight: '300',
    fontStyle: 'normal',
    paddingBottom: 2,
  },
});

const threadsSelector = state => state && state.threads;
const statusesSelector = state => state && state.statuses;
const contactsSelector = state => state && state.contacts;
const threadIdSelector = (state, ownProps) => R.prop('id', ownProps);

const threadByIdSelector = createSelector(
  threadsSelector,
  threadIdSelector,
  (threads, threadId) => R.prop(threadId, threads),
);

const hydratedThreadByIdSelector = createSelector(
  statusesSelector,
  contactsSelector,
  threadByIdSelector,
  (statuses, contacts, thread) => {
    if (thread) {
      const threadStatus = R.prop(R.prop('status', thread), statuses);
      const threadWithStatus = R.assoc('status', threadStatus, thread);
      const threadResponsible = R.prop(
        R.prop('responsibleUserId', thread),
        contacts,
      );
      const threadWithResponsible = R.assoc(
        'responsible',
        threadResponsible,
        threadWithStatus,
      );
      return threadWithResponsible;
    }
    return null;
  },
);

const mapStateToProps = (state, ownProps) => ({
  id: ownProps.id,
  thread: hydratedThreadByIdSelector(state, ownProps),
});

const mapDispatchToProps = {
  getThread,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class ThreadListEntry extends Component {
  static displayName = 'ThreadListEntry';
  static propTypes = {
    id: PropTypes.string,
    navigator: PropTypes.shape({
      toggleTabs: PropTypes.func.isRequired,
      push: PropTypes.func.isRequired,
    }),
    thread: PropTypes.object,
    showIcons: PropTypes.bool,
    getThread: PropTypes.func.isRequired,
  };
  static defaultProps = {
    styles: {},
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  componentDidMount() {
    if (this.props.id) {
      this.props.getThread(this.props.id);
    }
  }
  handleOnPress() {
    global.tracker.trackEvent('Нажатия на кнопочки', 'thread');
    this.props.navigator.push({
      screen: 'workonflow.Thread',
      passProps: {
        id: this.props.id,
        type: 'Thread',
      },
    });
  }
  render() {
    return (
      <TouchableOpacity
        style={[styles.container, this.props.styles.threadContainer]}
        onPress={this.handleOnPress}
      >
        <View style={[styles.thread, this.props.styles.container]}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
            }}
          >
            <Text style={[styles.title]} numberOfLines={1} ellipsizeMode="tail">
              {R.propOr('', 'title', this.props.thread)}
            </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={[
                styles.status,
                {
                  color: R.pathOr(
                    '#000000',
                    ['status', 'color'],
                    this.props.thread,
                  ),
                },
              ]}
            >
              {R.pathOr('', ['status', 'name'], this.props.thread)}
            </Text>
          </View>
          <View
            style={{
              alignItems: 'flex-end',
              paddingHorizontal: 8,
              marginLeft: 0,
            }}
          >
            <ContactIcon
              show={
                this.props.showIcons &&
                !!R.propOr(false, 'responsible', this.props.thread)
              }
              id={R.pathOr('', ['responsible', '_id'], this.props.thread)}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
