import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Picker,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import Modal from 'react-native-root-modal';

const styles = StyleSheet.create({
  field: {
    flex: 1,
    overflow: 'hidden',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pickerModal: {
    bottom: 0,
    left: 0,
    right: 0,
  },
  picker: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: '#ccc',
    backgroundColor: '#fff',
  },
  value: {
    alignItems: 'center',
    width: 200,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    height: 30,
    justifyContent: 'center',
  },
  hidePicker: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 5,
  },
  hidePickerText: {
    fontSize: 12,
    color: '#333',
  },
});

export default class ItemPicker extends Component {
  static displayName = 'ItemPicker';
  static propTypes = {
    id: PropTypes.string,
    item: PropTypes.object,
    items: PropTypes.arrayOf(PropTypes.object),
    onValueChange: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { picker: false, value: null };
    this.openPicker = this.openPicker.bind(this);
    this.closePicker = this.closePicker.bind(this);
    this.renderPickerItems = this.renderPickerItems.bind(this);
    this.handleOnValueChange = this.handleOnValueChange.bind(this);
  }
  openPicker() {
    this.setState({ picker: true });
  }
  closePicker() {
    this.setState({ picker: false });
  }
  handleOnValueChange(itemValue, itemIndex) {
    this.props.onValueChange(this.props.id, itemValue);
  }
  renderPickerItems() {
    return R.map(
      item => (
        <Picker.Item key={item.id} label={`~ ${item.name}`} value={item.id} />
      ),
      this.props.items,
    );
  }
  render() {
    if (Platform.OS === 'ios') {
      return (
        <View style={styles.field}>
          <TouchableOpacity onPress={this.openPicker}>
            <View style={styles.value}>
              <Text>{R.prop('name', this.props.item)}</Text>
            </View>
          </TouchableOpacity>
          <Modal style={styles.pickerModal} visible={this.state.picker}>
            <Picker
              style={styles.picker}
              selectedValue={R.propOr('', 'id', this.props.item)}
              onValueChange={this.handleOnValueChange}
            >
              {this.renderPickerItems()}
            </Picker>
            <TouchableOpacity
              style={styles.hidePicker}
              onPress={this.closePicker}
            >
              <Text style={styles.hidePickerText}>DONE</Text>
            </TouchableOpacity>
          </Modal>
        </View>
      );
    }
    return (
      <Picker
        style={styles.picker}
        selectedValue={R.propOr('', 'id', this.props.item)}
        onValueChange={this.handleOnValueChange}
      >
        {this.renderPickerItems()}
      </Picker>
    );
  }
}
