import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 8,
    paddingHorizontal: 8,
    backgroundColor: 'transparent',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 8,
  },
  headerText: {
    fontSize: 18,
    fontWeight: '300',
    color: '#ffffff',
  },
  button: {},
  buttonText: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: '200',
  },
  emptyText: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '300',
  },
});

export default class Header extends Component {
  static displayName = 'Header';
  static propTypes = {
    containerStyles: PropTypes.object,
    headerText: PropTypes.string,
    buttonText: PropTypes.string,
    emptyText: PropTypes.string,
    HeaderComponent: PropTypes.node,
    ButtonComponent: PropTypes.node,
    EmptyComponent: PropTypes.node,
    showEmpty: PropTypes.bool,
    showButton: PropTypes.bool,
    showHeader: PropTypes.bool,
    onButtonPress: PropTypes.func,
    styles: PropTypes.object,
  };
  static defaultProps = {
    styles: {},
  };
  constructor(props) {
    super(props);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderButton = this.renderButton.bind(this);
    this.renderEmpty = this.renderEmpty.bind(this);
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { onButtonPress } = this.props;
    if (onButtonPress) {
      onButtonPress();
    }
    return null;
  }
  renderEmpty() {
    const { showEmpty, EmptyComponent } = this.props;
    if (showEmpty) {
      if (EmptyComponent) {
        return EmptyComponent;
      }
      return <Text style={styles.emptyText}>{this.props.emptyText}</Text>;
    }
    return null;
  }
  renderHeader() {
    const { showHeader = true, HeaderComponent } = this.props;
    if (showHeader) {
      if (HeaderComponent) {
        return HeaderComponent;
      }
      return (
        <Text style={[styles.headerText, this.props.styles.headerText]}>
          {this.props.headerText}
        </Text>
      );
    }
    return null;
  }
  renderButton() {
    const { showButton = true, ButtonComponent } = this.props;
    if (showButton) {
      if (ButtonComponent) {
        return ButtonComponent;
      }
      return (
        <TouchableOpacity onPress={this.handleOnPress}>
          <Text style={styles.buttonText}>{this.props.buttonText}</Text>
        </TouchableOpacity>
      );
    }
    return null;
  }
  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.containerStyles,
          this.props.styles.container,
        ]}
      >
        <View style={[styles.header, this.props.styles.header]}>
          {this.renderHeader()}
          {this.renderButton()}
        </View>
        <View>{this.renderEmpty()}</View>
      </View>
    );
  }
}
