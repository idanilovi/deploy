import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import Icons from '../Icon';
import MainCreateButton from '../MainCreateButton';
import { createThread } from '../../actions';

const styles = StyleSheet.create({
  container: {
    height: 60,
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 12,
    marginTop: 0,
    backgroundColor: 'rgb(88, 68, 82)',
    ...ifIphoneX(
      {
        height: 94,
        paddingTop: 34,
      },
      {
        paddingTop: 20,
        height: 80,
      },
    ),
  },
  button: {
    flex: 0,
    height: 48,
    width: 48,
    borderRadius: 4,
    backgroundColor: 'transparent',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

class MainHeader extends Component {
  static displayName = 'MainHeader';
  static propTypes = {
    navigator: PropTypes.shape({
      toggleDrawer: PropTypes.func.isRequired,
    }),
    createThread: PropTypes.func,
    openDrawer: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.handleShowDrawer = this.handleShowDrawer.bind(this);
    this.getHeaderText = this.getHeaderText.bind(this);
    this.handleDraftBackButton = this.handleDraftBackButton.bind(this);
    this.state = {
      teamName: props.teamName,
      userId: props.userId,
      userName: props.userName,
      userColor: props.userColor,
      isAdmin: props.isAdmin,
      openModal: false,
      tabIndex: props.tabIndex,
    };
  }
  componentWillReceiveProps(nextprops) {
    if (
      nextprops.teamName !== this.props.teamName ||
      nextprops.userId !== this.props.userId ||
      nextprops.userName !== this.props.userName ||
      nextprops.userColor !== this.props.userColor ||
      nextprops.isAdmin !== this.props.isAdmin ||
      nextprops.tabIndex !== this.props.tabIndex ||
      nextprops.scrollable !== this.props.scrollable
    ) {
      this.setState({
        teamName: nextprops.teamName,
        userId: nextprops.userId,
        userName: nextprops.userName,
        userColor: nextprops.userColor,
        isAdmin: nextprops.isAdmin,
        tabIndex: nextprops.tabIndex,
        update: true,
      });
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({ update: false });
      return true;
    }
    return false;
  }
  handleShowDrawer() {
    this.props.openDrawer();
  }
  handleDraftBackButton() {
    R.propOr(
      () => console.log('goToPage is not a function'),
      'goToPage',
      this.props.scrollable,
    )(3);
  }
  // eslint-disable-next-line class-methods-use-this
  getHeaderText(index) {
    if (index === 0) return 'Team';
    if (index === 1) return 'Streams';
    if (index === 2) return 'Conversations';
    if (index === 3) return 'My threads';
    if (index === 4) return this.state.userName;
    return null;
  }
  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.tabIndex === 4 && {
            backgroundColor: 'rgb(38,204,138)',
          },
        ]}
      >
        {this.props.tabIndex !== 4 ? (
          <Icon.Button
            name="ios-menu"
            underlayColor="#4e4963"
            backgroundColor="transparent"
            onPress={this.handleShowDrawer}
          />
        ) : (
          <TouchableOpacity onPress={this.handleDraftBackButton}>
            <Icons
              name="ArrowLeft"
              width={24}
              height={24}
              fill="rgba(255, 255, 255, 1)"
              viewBox="0 0 32 32"
            />
          </TouchableOpacity>
        )}
        <View
          style={{
            flex: 1,
            alignItems: 'center',
          }}
        >
          <Text style={{ color: '#ffffff' }}>
            {this.getHeaderText(this.props.tabIndex)}
          </Text>
        </View>
        {this.props.tabIndex !== 4 && (
          <MainCreateButton
            userId={this.state.userId}
            navigator={this.props.navigator}
            createThread={this.props.createThread}
          />
        )}
      </View>
    );
  }
}

const mapDispatchToProps = {
  createThread,
};

export default connect(null, mapDispatchToProps)(MainHeader);
