import PropTypes from 'prop-types';
import React from 'react';
import { Text } from 'react-native';
import moment from 'moment';

const TimeSeparator = ({ value }) => (
  <Text style={{ textAlign: 'center', fontWeight: '600', padding: 10 }}>
    {moment(value.split('/')).calendar(null, {
      sameDay: '[Today]',
      nextDay: '[Tomorrow]',
      nextWeek: 'dddd',
      lastDay: '[Yesterday]',
      lastWeek: '[Last] dddd',
      sameElse: 'DD/MM/YYYY',
    })}
  </Text>
);

TimeSeparator.displayName = 'TimeSeparator';
TimeSeparator.propTypes = {
  value: PropTypes.string,
};

export default TimeSeparator;
