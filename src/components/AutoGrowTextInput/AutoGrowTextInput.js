import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Platform, TextInput } from 'react-native';

export default class AutoGrowTextInput extends PureComponent {
  static displayName = 'AutoGrowTextInput';
  static propTypes = {
    style: PropTypes.number,
    value: PropTypes.string,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChangeText: PropTypes.func.isRequired,
    underlineColorAndroid: PropTypes.string,
    placeholder: PropTypes.string,
    placeholderTextColor: PropTypes.string,
    blurOnSubmit: PropTypes.bool,
  };
  constructor(props) {
    super(props);
    this.state = { height: 0 };
    this.getValue = this.getValue.bind(this);
    this.focus = this.focus.bind(this);
    this.clear = this.clear.bind(this);
    this.updateHeight = this.updateHeight.bind(this);
    this.handleOnFocus = this.handleOnFocus.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnChangeText = this.handleOnChangeText.bind(this);
    this.handleOnContentSizeChange = this.handleOnContentSizeChange.bind(this);
  }
  focus() {
    this.input.focus();
  }
  clear() {
    this.input.clear();
  }
  getValue() {
    if (typeof this.props.value !== 'string') {
      return String(this.props.value);
    }
    return this.props.value;
  }
  updateHeight(height) {
    this.setState({ height });
  }
  handleOnChangeText(text) {
    this.props.onChangeText(text);
  }
  handleOnChange({ nativeEvent }) {
    if (nativeEvent.contentSize) {
      this.updateHeight(nativeEvent.contentSize.height);
    }
  }
  handleOnFocus() {
    if (this.props.onFocus) {
      this.props.onFocus();
    }
  }
  handleOnBlur() {
    if (this.props.onBlur) {
      this.props.onBlur();
    }
  }
  handleOnContentSizeChange({ nativeEvent }) {
    this.setState({ height: nativeEvent.contentSize.height });
  }
  render() {
    return (
      <TextInput
        ref={ref => {
          this.input = ref;
        }}
        autoGrow={true}
        multiline={true}
        autoCorrect={true}
        blurOnSubmit={this.props.blurOnSubmit || false}
        placeholder={this.props.placeholder || ''}
        placeholderTextColor={this.props.placeholderTextColor || '#a8a4b9'}
        textBreakStrategy="highQuality"
        numberOfLines={10}
        underlineColorAndroid={this.props.underlineColorAndroid}
        style={{
          flex: 1,
          fontWeight: '300',
          fontSize: 16,
          paddingTop: 0,
          paddingBottom: 0,
          height: Math.max(35, this.state.height),
        }}
        minHeight={Platform.OS === 'ios' ? 20 : 40}
        maxHeight={210}
        onBlur={this.handleOnBlur}
        onFocus={this.handleOnFocus}
        // onChange={this.handleOnChange}
        onChangeText={this.handleOnChangeText}
        onContentSizeChange={this.handleOnContentSizeChange}
        value={this.getValue()}
      />
    );
  }
}
