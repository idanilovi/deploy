import R from 'ramda';
import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import React, { Component } from 'react';
import { SectionList, Platform } from 'react-native';
import CommentsGroup from '../CommentsGroup';
import Comment from '../Comment';
import TimeSeparator from '../TimeSeparator';

const sortByCreatedAt = R.sortBy(R.prop('createdAt'));

const commentsSelector = state => R.propOr({}, 'comments', state);

const streamIdSelector = (state, ownProps) =>
  R.pathOr(
    R.prop('streamId', ownProps),
    ['navigation', 'state', 'params', 'id'],
    ownProps,
  );

const typeSelector = (state, ownProps) =>
  R.pathOr(
    R.prop('type', ownProps),
    ['navigation', 'state', 'params', 'type'],
    ownProps,
  );

const orderedStreamCommentsSelector = createSelector(
  commentsSelector,
  streamIdSelector,
  (comments, streamId) => {
    const filteredComments = R.filter(
      comment => comment.streamId === streamId,
      R.values(comments),
    );
    return sortByCreatedAt(filteredComments);
  },
);
const threadIdSelector = (state, ownProps) =>
  R.pathOr(
    R.prop('threadId', ownProps),
    ['navigation', 'state', 'params', 'id'],
    ownProps,
  );

const orderedThreadCommentsSelector = createSelector(
  commentsSelector,
  threadIdSelector,
  (comments, threadId) => {
    const filteredComments = R.filter(
      comment => comment.threadId === threadId,
      R.values(comments),
    );
    return sortByCreatedAt(filteredComments);
  },
);

const contactsSelector = state => R.prop('contacts', state);

const orderedDirectCommentsSelector = createSelector(
  state => R.path(['userData', 'userId'], state),
  (state, ownProps) =>
    R.pathOr(
      R.prop('contactId', ownProps),
      ['navigation', 'state', 'params', 'id'],
      ownProps,
    ),
  commentsSelector,
  contactsSelector,
  (userId, contactId, comments, contacts) => {
    const contact = R.propOr({}, 'contactId', contacts);
    const billingType = R.prop('billingType', contact);
    const contactEmail = R.path(['basicData', 'email', 0], contact);
    const originUserIds = R.propOr([], 'originContactIds', contact);
    const filteredComments =
      billingType !== 'contacts'
        ? R.filter(
            comment =>
              (comment.to.includes(contactId) && comment.from === userId) ||
              (comment.to.includes(userId) && comment.from === contactId),
            R.values(comments),
          )
        : R.filter(
            comment =>
              comment.to.includes(contactId) ||
              comment.from === contactId ||
              originUserIds.includes(comment.from) ||
              (contactEmail && comment.metadata.to === contactEmail) ||
              (contactEmail && comment.metadata.from === contactEmail),
            R.values(comments),
          );
    return sortByCreatedAt(filteredComments);
  },
);

function addTimeIdToComment(comment) {
  const date = moment(comment.createdAt);
  return R.assoc(
    'timeId',
    `${date.year()}/${date.month()}/${date.date()}`,
    comment,
  );
}

function groupByTimeId(comment) {
  return comment.timeId;
}

function groupEventComments(comments) {
  if (!Array.isArray(comments) || comments.length === 0) {
    return comments;
  }
  const tempComments = [];
  for (let i = 0; i < comments.length; i++) {
    if (
      !R.prop('from', comments[i]) &&
      R.path(['metadata', 'userId'], comments[i - 1]) ===
        R.path(['metadata', 'userId'], comments[i])
    ) {
      if (tempComments.length) {
        tempComments[tempComments.length - 1].push(comments[i]);
      } else {
        tempComments.push([comments[i]]);
      }
    } else {
      tempComments.push([comments[i]]);
    }
  }
  return tempComments;
}

const groupedOrdered = comments =>
  R.groupBy(groupByTimeId, R.map(addTimeIdToComment, R.values(comments)));

const groupedOrderedComments = createSelector(
  typeSelector,
  orderedStreamCommentsSelector,
  orderedDirectCommentsSelector,
  orderedThreadCommentsSelector,
  (type, streamComments, directComments, threadComments) => {
    if (type === 'Stream') {
      return groupedOrdered(streamComments);
    }
    if (type === 'Direct') {
      return groupedOrdered(directComments);
    }
    if (type === 'Thread') {
      return groupedOrdered(threadComments);
    }
    return [];
  },
);

const sectionCommentsSelector = createSelector(
  groupedOrderedComments,
  comments => {
    return Object.keys(comments).map(key => {
      return {
        title: key,
        data: groupEventComments(comments[key]),
      };
    });
  },
);

function mapStateToProps(state, ownProps) {
  // console.log('CommentList.mapStateToProps: ', sectionCommentsSelector(state, ownProps), state.comments, ownProps);
  return {
    comments: sectionCommentsSelector(state, ownProps),
  };
}

@connect(mapStateToProps)
export default class CommentList extends Component {
  static displayName = 'CommentList';
  static propTypes = {
    comments: PropTypes.array,
    type: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.keyExtractor = this.keyExtractor.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
  }
  keyExtractor(item, index) {
    return index;
  }
  renderItem({ item }) {
    const { type } = this.props;
    if (item.length > 1) {
      return (
        <CommentsGroup
          type={type}
          key={item[0].id}
          comments={item}
          numberOfShownComments={3}
        />
      );
    }
    return <Comment type={type} key={item[0].id} comment={item[0]} />;
  }
  getItemLayout = (data, index) => ({
    length: data.length,
    offset: 200 * index,
    index,
  });
  componentDidMount() {
    const params = {
      animated: false,
      itemIndex: 0,
      sectionIndex: this.props.comments.length - 1,
    };
    // console.log(
    //   '---------------------',
    //   this.props.comments.length - 1,
    //   this.props.comments[this.props.comments.length - 1].data.length - 1,
    //   this.props.comments,
    // );
    setTimeout(() => this.sectionList.scrollToLocation(params), 1000);
  }
  renderSectionHeader({ section }) {
    return <TimeSeparator value={section.title} />;
  }
  render() {
    return (
      <SectionList
        ref={node => {
          this.sectionList = node;
        }}
        getItemLayout={this.getItemLayout}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
        renderSectionHeader={this.renderSectionHeader}
        sections={this.props.comments}
      />
    );
  }
}
