/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import uuid from 'uuid';
import {
  Animated,
  Platform,
  Clipboard,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import moment from 'moment';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { getComments, getFiles } from '../../actions';
import Comment from '../../components/Comment';
import InputToolbar from '../../components/InputToolbar';
import { filesSelector } from '../../selectors';

const typeSelector = (state, ownProps) => R.propOr(null, 'type', ownProps);

const userIdSelector = state => R.pathOr(null, ['userData', 'userId'], state);

const streamIdSelector = (state, ownProps) =>
  R.propOr(null, 'streamId', ownProps);

const threadIdSelector = (state, ownProps) =>
  ownProps.threadId || ownProps.id || ownProps._id;

const contactIdSelector = (state, ownProps) =>
  R.propOr(null, 'contactId', ownProps);

// const filesSelector = state => R.propOr({}, 'files', state);

const commentsSelector = state => R.propOr({}, 'comments', state);
const contactsSelector = state => R.propOr({}, 'contacts', state);
// const threadsSelector = state => R.propOr({}, 'threads', state);

const contactSelector = createSelector(
  contactsSelector,
  contactIdSelector,
  (contacts, contactId) => R.propOr({}, contactId, contacts),
);

const sortByCreatedAt = R.sortBy(R.prop('createdAt'));

const hydratedCommentsSelector = createSelector(
  commentsSelector,
  contactsSelector,
  (comments, contacts) =>
    R.reverse(
      sortByCreatedAt(
        R.map(
          comment =>
            R.assoc(
              'user',
              R.propOr({}, R.prop('from', comment), contacts),
              comment,
            ),
          R.values(comments),
        ),
      ),
    ),
);

function addTimeIdToComment(comment) {
  const date = moment(comment.createdAt);
  return R.assoc(
    'timeId',
    `${date.year()}/${date.month()}/${date.date()}`,
    comment,
  );
}

function groupByTimeId(comment) {
  return comment.timeId;
}

const streamCommentsSelector = createSelector(
  hydratedCommentsSelector,
  streamIdSelector,
  (comments, streamId) =>
    R.groupBy(
      groupByTimeId,
      R.map(
        addTimeIdToComment,
        R.reverse(R.filter(comment => comment.streamId === streamId, comments)),
      ),
    ),
);

const filesForCurrentStreamSelector = createSelector(
  hydratedCommentsSelector,
  filesSelector,
  streamIdSelector,
  (comments, files, streamId) => {
    return R.map(
      att => R.propOr({}, R.prop('id', att), files),
      R.flatten(
        R.map(comment => {
          return R.map(
            att => ({ id: R.path(['data', 'id'], att), type: att.type }),
            R.filter(
              att => ['image', 'file'].indexOf(att.type) !== -1,
              R.propOr([], 'att', comment),
            ),
          );
        }, R.filter(comment => comment.streamId === streamId, comments)),
      ),
    );
  },
);

const filesForCurrentThreadSelector = createSelector(
  hydratedCommentsSelector,
  filesSelector,
  threadIdSelector,
  (comments, files, threadId) => {
    return R.map(
      att => R.propOr({}, R.prop('id', att), files),
      R.flatten(
        R.map(comment => {
          return R.map(
            att => ({ id: R.path(['data', 'id'], att), type: att.type }),
            R.filter(
              att => ['image', 'file'].indexOf(att.type) !== -1,
              R.propOr([], 'att', comment),
            ),
          );
        }, R.filter(comment => comment.threadId === threadId, comments)),
      ),
    );
  },
);

const filesForCurrentDirectSelector = createSelector(
  userIdSelector,
  contactSelector,
  hydratedCommentsSelector,
  filesSelector,
  (userId, contact, comments, files) => {
    const billingType = R.prop('billingType', contact);
    const contactEmail = R.path(['basicData', 'email', 0], contact);
    const originUserIds = R.propOr([], 'originContactIds', contact);
    const filteredComments =
      billingType !== 'contacts'
        ? R.filter(
            comment =>
              (comment.to.includes(contact._id) && comment.from === userId) ||
              (comment.to.includes(userId) && comment.from === contact._id),
            comments,
          )
        : R.filter(
            comment =>
              comment.to.includes(contact._id) ||
              comment.from === contact._id ||
              originUserIds.includes(comment.from) ||
              (contactEmail && comment.metadata.to === contactEmail) ||
              (contactEmail && comment.metadata.from === contactEmail),
            comments,
          );
    return R.map(
      att => R.propOr({}, R.prop('id', att), files),
      R.flatten(
        R.map(comment => {
          return R.map(
            att => ({ id: R.path(['data', 'id'], att), type: att.type }),
            R.filter(
              att => ['image', 'file'].indexOf(att.type) !== -1,
              R.propOr([], 'att', comment),
            ),
          );
        }, filteredComments),
      ),
    );
  },
);

const filterFiles = files => R.filter(file => !!file.id, files);

const filesForCurrentViewSelector = createSelector(
  typeSelector,
  filesForCurrentStreamSelector,
  filesForCurrentThreadSelector,
  filesForCurrentDirectSelector,
  (type, streamFiles, threadFiles, directFiles) => {
    if (type === 'Stream') {
      return filterFiles(streamFiles);
    }
    if (type === 'Thread') {
      return filterFiles(threadFiles);
    }
    if (type === 'Direct') {
      return filterFiles(directFiles);
    }
    return [];
  },
);

const threadCommentsSelector = createSelector(
  hydratedCommentsSelector,
  threadIdSelector,
  (comments, threadId) =>
    R.groupBy(
      groupByTimeId,
      R.map(
        addTimeIdToComment,
        R.reverse(R.filter(comment => comment.threadId === threadId, comments)),
      ),
    ),
);

const directCommentsSelector = createSelector(
  userIdSelector,
  contactSelector,
  hydratedCommentsSelector,
  (userId, contact, comments) => {
    const billingType = R.prop('billingType', contact);
    const contactEmail = R.path(['basicData', 'email', 0], contact);
    const originUserIds = R.propOr([], 'originContactIds', contact);
    const filteredComments =
      billingType !== 'contacts'
        ? R.filter(
            comment =>
              (comment.to.includes(contact._id) && comment.from === userId) ||
              (comment.to.includes(userId) && comment.from === contact._id),
            comments,
          )
        : R.filter(
            comment =>
              comment.to.includes(contact._id) ||
              comment.from === contact._id ||
              originUserIds.includes(comment.from) ||
              (contactEmail && comment.metadata.to === contactEmail) ||
              (contactEmail && comment.metadata.from === contactEmail),
            comments,
          );
    return R.groupBy(
      groupByTimeId,
      R.map(
        addTimeIdToComment,
        R.reverse(R.filter(comment => !comment.threadId, filteredComments)),
      ),
    );
  },
);

const reverseFlattenFilteredArray = R.compose(
  R.reverse,
  R.filter(_ => _),
  R.flatten,
  R.map(_ => [{ _id: uuid.v4(), header: true, title: _.title }].concat(_.data)),
);

const sectionCommentsSelector = createSelector(
  typeSelector,
  streamCommentsSelector,
  directCommentsSelector,
  threadCommentsSelector,
  (type, streamComments, directComments, threadComments) => {
    if (type === 'Stream') {
      const stream = Object.keys(streamComments).map(key => ({
        title: key,
        data: streamComments[key],
      }));
      return reverseFlattenFilteredArray(stream);
    }
    if (type === 'Direct') {
      const direct = Object.keys(directComments).map(key => ({
        title: key,
        data: directComments[key],
      }));
      return reverseFlattenFilteredArray(direct);
    }
    if (type === 'Thread') {
      const thread = Object.keys(threadComments).map(key => ({
        title: key,
        data: threadComments[key],
      }));
      return reverseFlattenFilteredArray(thread);
    }
    return [];
  },
);
const stickyHeadersIndex = createSelector(sectionCommentsSelector, comments =>
  R.map(comment => comment.header && comments.indexOf(comment), comments),
);

const userSelector = createSelector(
  contactsSelector,
  userIdSelector,
  (contacts, userId) => R.propOr({}, userId, contacts),
);

const mapStateToProps = (state, ownProps) => ({
  type: typeSelector(state, ownProps),
  streamId: streamIdSelector(state, ownProps),
  comments: sectionCommentsSelector(state, ownProps),
  userId: userIdSelector(state),
  user: userSelector(state),
  contacts: state.contacts,
  streams: state.streams,
  stickyHeadersIndex: stickyHeadersIndex(state, ownProps),
  files: filesForCurrentViewSelector(state, ownProps),
  qwerty: threadIdSelector(state, ownProps),
});

const mapDispatchToProps = {
  getComments,
  getFiles,
};

const isSame = (prev, current, next) => {
  const currentDate = Date.now();
  return {
    isSameUserPrev:
      R.path(['user', '_id'], current) === R.path(['user', '_id'], prev),
    isSameUserNext:
      R.path(['user', '_id'], current) === R.path(['user', '_id'], next),
    isSameTimePrev:
      Math.abs(
        R.propOr(currentDate, 'createdAt', current) -
          R.propOr(currentDate, 'createdAt', prev),
      ) <=
      15 * 60 * 1000,
    isSameTimeNext:
      Math.abs(
        R.propOr(currentDate, 'createdAt', next) -
          R.propOr(currentDate, 'createdAt', current),
      ) <=
      15 * 60 * 1000,
  };
};

@connect(mapStateToProps, mapDispatchToProps)
export default class CommentList extends Component {
  static displayName = 'CommentList';
  static propTypes = {
    type: PropTypes.string,
    streamId: PropTypes.string,
    getFiles: PropTypes.func.isRequired,
    getComments: PropTypes.func.isRequired,
    comments: PropTypes.array,
    threadId: PropTypes.string,
    userId: PropTypes.string,
    user: PropTypes.object,
    contactId: PropTypes.string,
    dontRenderInputToolbar: PropTypes.bool,
    navigator: PropTypes.object,
    files: PropTypes.arrayOf(PropTypes.object),
  };
  static defaultProps = {
    styles: {},
  };
  constructor(props) {
    super(props);
    this.isCloseToTop = this.isCloseToTop.bind(this);
    this.handleOnLoadEarlier = this.handleOnLoadEarlier.bind(this);
    this.handleOnLongPress = this.handleOnLongPress.bind(this);
    this.renderInputToolbar = this.renderInputToolbar.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.keyExtractor = this.keyExtractor.bind(this);
    this.loadComments = this.loadComments.bind(this);
    this.state = { isLoadingEarlier: false, sticky: [] };
  }
  handleOnLoadEarlier() {
    this.setState({ isLoadingEarlier: true });
    if (this.props.type === 'Stream') {
      if (this.props.streamId) {
        this.props.getComments(
          {
            streamId: this.props.streamId,
            skip: this.props.comments.length,
            limit: 30,
          },
          () => this.setState({ isLoadingEarlier: false }),
        );
      }
    }
    if (this.props.type === 'Direct') {
      if (this.props.userId && this.props.contactId) {
        this.props.getComments(
          {
            to: [this.props.userId],
            from: [this.props.contactId],
            skip: this.props.comments.length,
            limit: 30,
          },
          () => this.setState({ isLoadingEarlier: false }),
        );
      }
    }
  }
  loadComments() {
    if (this.props.threadId) {
      this.props.getComments(
        {
          threadId: this.props.threadId,
          skip: this.props.comments.length,
          limit: 30,
          force: true,
        },
        () => this.setState({ isLoadingEarlier: false }),
      );
    }
  }
  // eslint-disable-next-line class-methods-use-this
  isCloseToTop({ layoutMeasurement, contentOffset, contentSize }) {
    const paddingToTop = 80;
    return (
      contentSize.height - layoutMeasurement.height - paddingToTop <=
      contentOffset.y
    );
  }
  renderInputToolbar() {
    if (this.props.dontRenderInputToolbar) {
      return null;
    }
    return (
      <InputToolbar
        type={this.props.type}
        streamId={this.props.streamId}
        threadId={this.props.threadId}
        contactId={this.props.contactId}
        refCommentList={this.refCommentList}
      />
    );
  }
  // eslint-disable-next-line class-methods-use-this
  handleOnLongPress(context, currentMessage) {
    if (currentMessage) {
      if (
        !currentMessage.type ||
        ['mail', 'telegram', 'livechat'].indexOf(currentMessage.type) !== -1
      ) {
        const options = ['Copy Text', 'Cancel'];
        const cancelButtonIndex = options.length - 1;
        context.actionSheet().showActionSheetWithOptions(
          {
            options,
            cancelButtonIndex,
          },
          buttonIndex => {
            switch (buttonIndex) {
              case 0: {
                const text = currentMessage.att
                  .map(attachment => {
                    if (attachment.type === 'text') {
                      return attachment.data.text;
                    }
                    if (attachment.type === 'mail') {
                      return attachment.data.text;
                    }
                    return '';
                  })
                  .join();
                Clipboard.setString(text);
                break;
              }
              default: {
                break;
              }
            }
          },
        );
      }
    }
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item) {
    return item._id;
  }
  renderItem({ item, index }) {
    if (!item) return null;
    return item.header ? (
      <View
        style={{
          flex: 0,
          backgroundColor: 'rgba(78, 73, 99, 0.3)',
          borderRadius: 10,
          paddingVertical: 7,
          marginVertical: 8,
          paddingHorizontal: 10,
          alignSelf: 'center',
        }}
      >
        <Text
          style={{
            fontWeight: '500',
            fontSize: 15,
            color: '#ffffff',
          }}
        >
          {moment(item.title.split('/')).calendar(null, {
            sameDay: '[Today]',
            nextDay: 'DD MMM',
            nextWeek: 'DD MMM',
            lastDay: '[Yesterday]',
            lastWeek: 'DD MMM',
            sameElse: 'DD MMM',
          })}
        </Text>
      </View>
    ) : (
      <Comment
        comment={item}
        type={this.props.type}
        filesContext={this.props.files}
        isUserMessage={
          item.metadata.userId === this.props.userId ||
          item.from === this.props.userId
        }
        isSameUser={isSame(
          this.props.comments[index + 1],
          item,
          this.props.comments[index - 1],
        )}
        navigator={this.props.navigator}
      />
    );
  }
  render() {
    return (
      <Animated.View
        style={[
          { flex: 1, backgroundColor: '#ffffff' },
          this.props.styles.container,
        ]}
      >
        {this.props.type === 'Thread' &&
          !this.props.loaded &&
          this.props.comments.length >= 30 && (
            <TouchableOpacity
              style={{
                paddingVertical: 16,
                height: 64,
                alignItems: 'center',
              }}
              onPress={this.loadComments}
            >
              <Text style={{ fontSize: 20, color: '#1073d7' }}>
                {'Previous comments'}
              </Text>
            </TouchableOpacity>
          )}
        <Animated.View
          style={[
            { flex: 1 },
            this.props.type === 'Thread' &&
              !this.props.loaded &&
              this.props.comments.length >= 30 && {
                borderTopWidth: StyleSheet.hairlineWidth,
                borderTopColor: 'rgba(40,30,42,.95)',
              },
          ]}
        >
          <FlatList
            keyExtractor={this.keyExtractor}
            data={this.props.comments}
            renderItem={this.renderItem}
            inverted={true}
            onEndReachedThreshold={0.5}
            onEndReached={this.handleOnLoadEarlier}
          />
        </Animated.View>
        {this.renderInputToolbar()}
        {Platform.OS === 'ios' && <KeyboardSpacer />}
      </Animated.View>
    );
  }
}
