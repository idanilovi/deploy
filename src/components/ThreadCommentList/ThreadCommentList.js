import R from 'ramda';
import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import React, { Component } from 'react';
import { SectionList } from 'react-native';
import CommentsGroup from '../CommentsGroup';
import Comment from '../Comment';
import TimeSeparator from '../TimeSeparator';

const sortByCreatedAt = R.sortBy(R.prop('createdAt'));

const commentsSelector = state => R.propOr({}, 'comments', state);

const threadIdSelector = (state, ownProps) =>
  R.pathOr(
    R.prop('threadId', ownProps),
    ['navigation', 'state', 'params', 'id'],
    ownProps,
  );

const orderedThreadCommentsSelector = createSelector(
  commentsSelector,
  threadIdSelector,
  (comments, threadId) => {
    const filteredComments = R.filter(
      comment => comment.threadId === threadId,
      R.values(comments),
    );
    return sortByCreatedAt(filteredComments);
  },
);

function addTimeIdToComment(comment) {
  const date = moment(comment.createdAt);
  return R.assoc(
    'timeId',
    `${date.year()}/${date.month()}/${date.date()}`,
    comment,
  );
}

function groupByTimeId(comment) {
  return comment.timeId;
}

function groupEventComments(comments) {
  if (!Array.isArray(comments) || comments.length === 0) {
    return comments;
  }
  const tempComments = [];
  for (let i = 0; i < comments.length; i++) {
    if (
      !R.prop('from', comments[i]) &&
      R.path(['metadata', 'userId'], comments[i - 1]) ===
        R.path(['metadata', 'userId'], comments[i])
    ) {
      if (tempComments.length) {
        tempComments[tempComments.length - 1].push(comments[i]);
      } else {
        tempComments.push([comments[i]]);
      }
    } else {
      tempComments.push([comments[i]]);
    }
  }
  return tempComments;
}

const groupedOrdered = comments =>
  R.groupBy(groupByTimeId, R.map(addTimeIdToComment, R.values(comments)));

const groupedOrderedComments = createSelector(
  orderedThreadCommentsSelector,
  threadComments => groupedOrdered(threadComments) || [],
);

const sectionCommentsSelector = createSelector(
  groupedOrderedComments,
  comments =>
    Object.keys(comments).map(key => ({
      title: key,
      data: groupEventComments(comments[key]),
    })),
);

const mapStateToProps = (state, ownProps) => ({
  comments: sectionCommentsSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class CommentList extends Component {
  static displayName = 'CommentList';
  static propTypes = {
    comments: PropTypes.array,
    type: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.keyExtractor = this.keyExtractor.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
  }
  // eslint-disable-next-line class-methods-use-this
  keyExtractor(item, index) {
    return index;
  }
  renderItem({ item }) {
    const { type } = this.props;
    if (item.length > 1) {
      return (
        <CommentsGroup
          type={type}
          key={item[0].id}
          comments={item}
          numberOfShownComments={3}
        />
      );
    }
    return <Comment type={type} key={item[0].id} comment={item[0]} />;
  }
  // eslint-disable-next-line class-methods-use-this
  renderSectionHeader({ section }) {
    return <TimeSeparator value={section.title} />;
  }
  render() {
    return (
      <SectionList
        style={{ flex: 1 }}
        ref={node => {
          this.sectionList = node;
        }}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
        renderSectionHeader={this.renderSectionHeader}
        sections={this.props.comments}
      />
    );
  }
}
