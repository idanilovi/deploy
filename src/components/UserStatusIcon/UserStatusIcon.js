import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, View } from 'react-native';
import Icon from '../Icon';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  icon: {
    borderRadius: 8.5,
    borderWidth: 4,
    width: 17,
    height: 17,
    backgroundColor: 'transparent',
    borderColor: 'hsla(0,0%,100%,.3)',
  },
});

export default class UserStatusIcon extends PureComponent {
  static displayName = 'UserStatusIcon';
  static propTypes = {
    status: PropTypes.string,
  };
  constructor() {
    super();
    this.renderIcon = this.renderIcon.bind(this);
  }
  renderIcon() {
    const { status, billingType } = this.props;
    if (billingType === 'users') {
      return (
        <Icon
          name="Circle"
          viewBox="0 0 1472 1472"
          width={24}
          height={24}
          fill={status === 'on' ? '#00c69e' : 'hsla(0,0%,100%,.3)'}
        />
      );
    }
    if (billingType === 'bots') {
      return (
        <View style={{ marginHorizontal: 4 }}>
          <Icon
            name="BotBlf"
            viewBox="0 0 4724 4724"
            width={16}
            height={16}
            fill={status === 'on' ? '#00c69e' : 'hsla(0,0%,100%,.3)'}
          />
        </View>
      );
    }
    return (
      <View
        style={{
          width: 17,
          height: 17,
        }}
      />
    );
  }
  render() {
    return <View style={styles.container}>{this.renderIcon()}</View>;
  }
}
