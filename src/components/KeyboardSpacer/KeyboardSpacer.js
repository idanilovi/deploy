/* eslint-disable no-underscore-dangle */
import React from 'react';
import { Platform } from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

const _KeyboardSpacer = () => {
  if (Platform.OS === 'ios') {
    return <KeyboardSpacer />;
  }
  return null;
};

_KeyboardSpacer.displayName = 'KeyboardSpacer';
export default _KeyboardSpacer;
