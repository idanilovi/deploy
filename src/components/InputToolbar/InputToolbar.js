/* eslint-disable react/prop-types */
/* eslint-disable no-console */
import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Dimensions,
  Platform,
  ScrollView,
  TouchableOpacity,
  TextInput,
  View,
  Switch,
} from 'react-native';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import Parser from 'simple-text-parser';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from '../../components/Icon';
import AttachmentsPreview from '../../components/AttachmentsPreview';
import AutoGrowTextInput from '../../components/AutoGrowTextInput';
import ContactListItem from '../../components/ContactListItem';
import {
  create,
  setText,
  clear,
  addAttachment,
  addMention,
} from '../../reducers/ui/commentForm';
import { sendComment } from '../../actions';
import global from '../../global';

const { width } = Dimensions.get('window');

const contactsSelector = state => R.propOr({}, 'contacts', state);
const streamsSelectors = state => R.propOr({}, 'streams', state);

const typeSelector = (state, ownProps) => R.propOr(null, 'type', ownProps);

const streamIdSelector = (state, ownProps) =>
  R.propOr(null, 'streamId', ownProps);

const threadIdSelector = (state, ownProps) =>
  R.propOr(null, 'threadId', ownProps);

const contactIdSelector = (state, ownProps) =>
  R.propOr(null, 'contactId', ownProps);

const commentFormsSelector = state => R.path(['ui', 'commentForm'], state);

const commentFormSelector = createSelector(
  typeSelector,
  streamIdSelector,
  threadIdSelector,
  contactIdSelector,
  commentFormsSelector,
  (type, streamId, threadId, contactId, commentForms) => {
    if (type === 'Stream') {
      return R.path([type, streamId], commentForms);
    }
    if (type === 'Direct') {
      return R.path([type, contactId], commentForms);
    }
    if (type === 'Thread') {
      return R.path([type, threadId], commentForms);
    }
    return {};
  },
);

const idSelector = createSelector(
  typeSelector,
  streamIdSelector,
  threadIdSelector,
  contactIdSelector,
  (type, streamId, threadId, contactId) => {
    if (type === 'Stream') {
      return streamId;
    }
    if (type === 'Direct') {
      return contactId;
    }
    if (type === 'Thread') {
      return threadId;
    }
    return null;
  },
);

const mapStateToProps = (state, ownProps) => ({
  id: idSelector(state, ownProps),
  streams: streamsSelectors(state),
  contacts: contactsSelector(state),
  commentForm: commentFormSelector(state, ownProps),
});

const mapDispatchToProps = {
  create,
  addAttachment,
  addMention,
  clear,
  setText,
  sendComment,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class InputToolbar extends Component {
  static displayName = 'InputToolbar';
  static defaultProps = {
    commentForm: {
      text: '',
      show: true,
      attachments: [],
      mentions: [],
      focus: false,
    },
  };
  static propTypes = {
    contacts: PropTypes.object,
    setText: PropTypes.func,
    clear: PropTypes.func,
    addAttachment: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.parseText = this.parseText.bind(this);
    this.getMentionsContainerHeight = this.getMentionsContainerHeight.bind(
      this,
    );
    this.handleOnItemPress = this.handleOnItemPress.bind(this);
    this.handleOnSend = this.handleOnSend.bind(this);
    this.handleOnChangeText = this.handleOnChangeText.bind(this);
    this.handleOnActionsPress = this.handleOnActionsPress.bind(this);
    this.handleOnValueChange = this.handleOnValueChange.bind(this);
    this.state = {
      isLoadingEarlier: false,
      text: '',
      selectUser: false,
      originalText: '',
      value: '',
      keyword: '',
      data: [],
      inputFocus: false,
      showModal: false,
      contacts: [],
      attachments: [],
      currentText: '',
      focusOnInput: false,
      tag: '',
      isPublic: false,
    };
  }
  componentWillMount() {
    if (this.props.id && this.props.type) {
      if (!this.props.commentForm) {
        this.props.create(this.props.id, this.props.type);
      }
    }
  }
  handleOnActionsPress() {
    const options = { multiple: true, includeBase64: true };
    ImagePicker.openPicker(options)
      .then(images => {
        this.props.addAttachment(this.props.id, this.props.type, images);
      })
      .catch(error => console.error(error));
  }
  handleOnChangeText(_text) {
    const {
      commentForm: { text },
    } = this.props;
    if (
      text[text.length - 1] === '@' &&
      (text[text.length - 2] === ' ' || !text[text.length - 2])
    ) {
      this.setState({ showModal: false });
    }
    this.props.setText(this.props.id, this.props.type, this.parseText(_text));
  }
  handleOnSend() {
    const {
      commentForm: { text, attachments, mentions },
    } = this.props;
    if (!text && !attachments.length) return;

    let currentText = text;
    mentions.map(_ => {
      currentText = currentText.replace(_.name, _.tag);
    });

    if (!this.props.streamId && !this.props.threadId && this.props.contactId) {
      global.tracker.trackEvent('Нажатия на кнопочки', 'comment direct');
    }
    if (this.props.streamId && !this.props.threadId && !this.props.contactId) {
      global.tracker.trackEvent('Нажатия на кнопочки', 'comment stream');
    }
    if (this.props.streamId && this.props.threadId && !this.props.contactId) {
      global.tracker.trackEvent('Нажатия на кнопочки', 'comment thread');
    }
    if (this.props.type === 'Direct') {
      if (this.props.contactId) {
        this.props.sendComment({
          text: currentText,
          to: this.props.contactId ? [this.props.contactId] : [],
          streamId: this.props.streamId,
          threadId: this.props.threadId,
          contactId: this.props.contactId,
          attachments,
        });
      }
    } else {
      this.props.sendComment({
        text: currentText,
        to: mentions.map(mention => mention.tag.slice(1, -1)),
        streamId: this.props.streamId,
        threadId: this.props.threadId,
        contactId: this.props.contactId,
        attachments,
        isPublic: this.state.isPublic,
      });
    }
    this.props.clear(this.props.id, this.props.type);
    if (this.props.refCommentList) {
      if (this.props.refCommentList.scrollToEnd) {
        setTimeout(
          () => this.props.refCommentList.scrollToEnd({ animated: true }),
          20,
        );
      }
      if (this.props.refCommentList.scrollToBottom) {
        this.props.refCommentList.scrollToBottom({ animated: true });
      }
    }
  }
  handleOnItemPress(contact) {
    const {
      commentForm: { text },
    } = this.props;
    const _text =
      text[text.length - 1] === '@'
        ? text.slice(0, -1)
        : text.slice(0, -(this.state.tag.length - 1));
    this.props.setText(
      this.props.id,
      this.props.type,
      this.parseText(
        `${_text[_text.length - 1] === ' ' ? _text : `${_text} `}@${
          contact._id
        }@`,
      ),
    );
    this.setState({
      showModal: false,
      tag: '',
    });
  }
  parseText(currText) {
    const parser = new Parser();
    let text = currText;
    if (text.length === 1 && text === '@') {
      text = ' @';
    }
    const { contacts } = this.props;
    this.messageTextInput.focus();
    parser.addRule(/\@[\S]{24}@/gi, tag => {
      const cleanTag = tag.slice(1, -1);
      const contact = contacts && contacts[cleanTag];
      this.setState({
        showModal: false,
      });
      const value = `@${contact &&
        contact.basicData &&
        contact.basicData.name}  `;
      this.props.addMention(this.props.id, this.props.type, {
        tag,
        name: `@${R.path(['basicData', 'name'], contact)}`,
      });
      return {
        type: 'tag',
        text: value,
        tag: cleanTag,
      };
    });
    parser.addRule(/ @([a-zа-я0-9ё]+)?((! ))*$/gi, (tag, cleanTag) => {
      this.setState({ showModal: true });
      const stream = this.props.streams[this.props.streamId];
      const filteredContacts = Object.values(contacts || []).filter(
        _ => _.billingType === 'users' || _.billingType === 'bots',
      );
      let currContacts = [];
      if (cleanTag && cleanTag !== ' ') {
        currContacts = filteredContacts.filter(
          _ =>
            R.pathOr('', ['basicData', 'name'], _)
              .toLowerCase()
              .indexOf(cleanTag.toLowerCase()) + 1,
        );
      } else if (!cleanTag || cleanTag === ' ') {
        currContacts = filteredContacts.filter(
          _ => stream && stream.roles.includes(_._id),
        );
      }
      this.setState({
        contacts: currContacts,
        tag,
      });
      return {
        type: 'tag',
        text: tag,
        tag: cleanTag,
      };
    });
    return parser.render(text);
  }
  renderActions() {
    return (
      <TouchableOpacity
        style={{
          flex: 0,
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'flex-end',
          paddingVertical: 16,
        }}
        onPress={this.handleOnActionsPress}
      >
        <Icon
          name="Attachment"
          widht={18}
          height={18}
          viewBox="0 0 24 24"
          fill="#a8a4b9"
        />
      </TouchableOpacity>
    );
  }
  handleOnValueChange() {
    this.setState({
      isPublic: !this.state.isPublic,
    });
  }
  renderSend() {
    return (
      <View style={{ flexDirection: 'row' }}>
        {this.props.type === 'Thread' && (
          <Switch
            style={{ marginRight: 10 }}
            value={this.state.isPublic}
            onValueChange={this.handleOnValueChange}
          />
        )}
        <TouchableOpacity
          onPress={this.handleOnSend}
          style={{
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'flex-end',
            paddingHorizontal: 10,
            paddingVertical: 16,
          }}
        >
          <Icon
            name="Send"
            height={18}
            width={23}
            viewBox="0 0 23 18"
            fill="#a8a4b9"
          />
        </TouchableOpacity>
      </View>
    );
  }
  renderComposer() {
    const {
      commentForm: { text },
    } = this.props;
    return (
      <AutoGrowTextInput
        ref={ref => {
          this.messageTextInput = ref;
        }}
        autoGrow={true}
        // style={{ flex: 1 }}
        onChangeText={this.handleOnChangeText}
        value={text}
        multiline={true}
        placeholder="Write a message..."
        autoCorrect={true}
        underlineColorAndroid="transparent"
        blurOnSubmit={false}
        onFocus={() => this.setState({ focusOnInput: true })}
        onBlur={() => this.setState({ focusOnInput: false })}
      />
    );
  }
  renderMentions() {
    return this.state.contacts.map(contact => (
      <ContactListItem
        key={contact._id}
        showIcon
        style={{
          contactItem: {
            backgroundColor: '#f3eff3',
            marginBottom: 4,
          },
          contactItemText: {
            color: '#212121',
          },
          contactItemPosition: {
            color: '#212121',
          },
        }}
        contact={contact}
        type={R.propOr(null, 'billingType', contact)}
        onPress={() => this.handleOnItemPress(contact)}
      />
    ));
  }
  renderAttachments() {
    const {
      commentForm: { attachments },
    } = this.props;
    return <AttachmentsPreview attachments={attachments} />;
  }
  renderMentionsContainer() {
    if (this.state.showModal && this.state.contacts.length) {
      return (
        <ScrollView
          key="MentionsContainer"
          keyboardShouldPersistTaps="always"
          style={{
            flex: 0,
            position: 'absolute',
            width,
            height: this.getMentionsContainerHeight(this.state.contacts),
            bottom: Platform.OS === 'ios' ? 330 : 64,
            left: 0,
            backgroundColor: '#fff',
            paddingHorizontal: 8,
            paddingVertical: 4,
          }}
        >
          {this.renderMentions()}
        </ScrollView>
      );
    }
    return null;
  }
  // eslint-disable-next-line class-methods-use-this
  getMentionsContainerHeight(items) {
    if (items.length >= 3) {
      return 172;
    }
    if (items.length === 2) {
      return 112;
    }
    if (items.length === 1) {
      return 64;
    }
    return 0;
  }
  renderInputContainer() {
    return (
      <View style={{ backgroundColor: '#ffffff' }}>
        <View
          key="InputContainer"
          // TODO ебучий хак, потому что столкнулся с проблемой того что нельзя задать
          // borderLeftWidth с borderRadius
          style={{
            flexDirection: 'column',
            borderRadius: 8,
            borderColor: '#ccc7e2',
            backgroundColor: '#ccc7e2',
            paddingLeft: 4,
            marginBottom: 8,
            marginHorizontal: 8,
            ...(this.state.focusOnInput
              ? {
                  paddingTop: 1,
                  paddingBottom: 1,
                  paddingRight: 1,
                }
              : {}),
            ...(this.state.isPublic
              ? {
                  backgroundColor: '#f9e400',
                }
              : {}),
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              borderRadius: 8,
              alignItems: 'center',
              backgroundColor: this.state.focusOnInput ? '#ffffff' : '#f3eff2',
            }}
          >
            {this.renderActions()}
            {this.renderComposer()}
            {this.renderSend()}
          </View>
          <View>{this.renderAttachments()}</View>
        </View>
      </View>
    );
  }
  render() {
    return [this.renderMentionsContainer(), this.renderInputContainer()];
  }
}
