import PropTypes from 'prop-types';
import R from 'ramda';
import React, { Component } from 'react';
import {
  Dimensions,
  Animated,
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { getComments, getThread, getThreads } from '../../actions';
import global from '../../global';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width,
    height,
    zIndex: 1000,
  },
});

const userIdSelector = state => R.pathOr(null, ['userData', 'userId'], state);
const streamIdSelector = (state, ownProps) =>
  R.propOr(null, 'streamId', ownProps);

const commentsSelector = state => R.propOr({}, 'comments', state);
const contactsSelector = state => R.propOr({}, 'contacts', state);

const sortByCreatedAt = R.sortBy(R.prop('createdAt'));

const hydratedCommentsSelector = createSelector(
  commentsSelector,
  contactsSelector,
  (comments, contacts) =>
    R.reverse(
      sortByCreatedAt(
        R.map(
          comment =>
            R.assoc(
              'user',
              R.propOr({}, R.prop('from', comment), contacts),
              comment,
            ),
          R.values(comments),
        ),
      ),
    ),
);

const streamCommentsSelector = createSelector(
  hydratedCommentsSelector,
  streamIdSelector,
  (comments, streamId) =>
    R.filter(comment => comment.streamId === streamId, comments),
);

const userSelector = state =>
  R.prop(R.path(['userData', 'userId'], state), R.prop('contacts', state));

const userDataSelector = state => R.prop('userData', state);

const imgUrlSelector = createSelector(
  userSelector,
  userDataSelector,
  (user, userData) =>
    R.propOr(
      '',
      'url',
      R.propOr(
        {},
        R.pathOr('', ['background', 'fileId'], user),
        R.propOr({}, 'backgrounds', userData),
      ),
    ),
);

const threadsSelector = state => R.propOr({}, 'threads', state);

const statusesSelector = createSelector(
  streamIdSelector,
  state => state.statuses,
  (streamId, statuses) =>
    R.filter(status => status.streamId === streamId, R.values(statuses)),
);

const threadsIdsByStreamIdSelector = createSelector(
  streamIdSelector,
  threadsSelector,
  (streamId, threads) =>
    R.map(
      thread => R.prop('id', thread),
      R.filter(
        thread => R.prop('streamId', thread) === streamId,
        R.values(threads),
      ),
    ),
);

const mapStateToProps = (state, ownProps) => ({
  streams: state.streams,
  imgUrl: imgUrlSelector(state, ownProps),
  threadIds: threadsIdsByStreamIdSelector(state, ownProps),
  statuses: statusesSelector(state, ownProps),
  comments: streamCommentsSelector(state, ownProps),
  contacts: contactsSelector(state, ownProps),
  userId: userIdSelector(state),
});

const mapDispatchToProps = {
  getComments,
  getThread,
  getThreads,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class Loader extends Component {
  static displayName = 'Loader';
  static propTypes = {
    type: PropTypes.string,
    color: PropTypes.string,
    getComments: PropTypes.func.isRequired,
    getThread: PropTypes.func.isRequired,
    getThreads: PropTypes.func.isRequired,
    children: PropTypes.node,
    streamId: PropTypes.string,
    userId: PropTypes.string,
    threadId: PropTypes.string,
    contactId: PropTypes.string,
    threadChildrenIds: PropTypes.arrayOf(PropTypes.string),
    toScroll: PropTypes.bool,
  };
  constructor(props) {
    super(props);
    this.state = { loading: true };
    this.timer = setTimeout(() => this.setState({ loading: false }), 30000);
    this.handleLoading = this.handleLoading.bind(this);
    this.handleLoaded = this.handleLoaded.bind(this);
  }
  handleLoaded() {
    this.setState({ loading: false });
    if (this.props.toScroll) {
      setTimeout(
        () => global.refCommentList && global.refCommentList.scrollToEnd(),
        700,
      );
    }
    clearTimeout(this.timer);
  }
  async handleLoading() {
    const { type, streamId, threadId, contactId, userId } = this.props;
    if (type === 'stream') {
      await this.props.getComments({
        streamId,
        skip: 0,
        limit: 30,
      });
      await this.props.getThreads({ streamId });
      this.handleLoaded();
    }
    if (type === 'thread') {
      await this.props.getThread(threadId);
      await this.props.getComments({
        threadId,
        skip: 0,
        limit: 30,
        force: true,
      });
      this.handleLoaded();
    }
    if (type === 'direct') {
      await this.props.getComments({
        to: [userId],
        from: [contactId],
        skip: 0,
        limit: 30,
      });
      this.handleLoaded();
    }
  }
  componentDidMount() {
    this.handleLoading();
  }
  render() {
    const { color, children } = this.props;
    const { loading } = this.state;
    return (
      <Animated.View style={{ flex: loading ? 0 : 1 }}>
        {loading && (
          <View style={[styles.container, { backgroundColor: color }]}>
            <ActivityIndicator animating={true} color="#ffffff" size="large" />
          </View>
        )}
        <View style={{ flex: loading ? 0 : 1 }}>{children}</View>
      </Animated.View>
    );
  }
}
