import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, StyleSheet, TextInput, View } from 'react-native';
import { sendComment } from '../../actions';
import {
  setCommentFormText,
  clearCommentForm,
} from '../../reducers/ui/commentForm';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 0,
    padding: 5,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
  },
  input: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    fontSize: 18,
    flex: 1,
  },
  button: {
    flex: 0,
    height: 24,
  },
});

const commentFormSelector = state => R.path(['ui', 'commentForm'], state);

const mapStateToProps = state => ({
  commentForm: commentFormSelector(state),
});

const mapDispatchToProps = {
  sendComment,
  setCommentFormText,
  clearCommentForm,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class CommentForm extends Component {
  static displayName = 'CommentForm';
  static propTypes = {
    show: PropTypes.bool,
    sendComment: PropTypes.func.isRequired,
    streamId: PropTypes.string,
    threadId: PropTypes.string,
    userId: PropTypes.string,
    commentForm: PropTypes.shape({
      text: PropTypes.string,
    }),
    setCommentFormText: PropTypes.func.isRequired,
    clearCommentForm: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnChangeText = this.handleOnChangeText.bind(this);
  }
  handleOnChangeText(value) {
    this.props.setCommentFormText(value);
  }
  handleOnPress() {
    const { text } = R.prop('commentForm', this.props);
    if (!text) return;
    this.props.sendComment({
      text,
      to: this.props.userId ? [this.props.userId] : [],
      streamId: this.props.streamId,
      threadId: this.props.threadId,
    });
    this.props.clearCommentForm();
  }
  render() {
    const { text, show } = R.prop('commentForm', this.props);
    if (this.props.show && show) {
      return (
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            onChangeText={this.handleOnChangeText}
            placeholder="Write a message..."
            underlineColorAndroid="transparent"
            value={text}
          />
          <Button
            style={styles.button}
            title="Send"
            onPress={this.handleOnPress}
          />
        </View>
      );
    }
    return null;
  }
}
