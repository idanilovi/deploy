import React, { Component } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#ffffff',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: '#b2b2b2',
  },
  preview: {
    padding: 4,
  },
  imagePreview: {
    width: 48,
    height: 48,
    borderRadius: 4,
  },
});

export default class AttachmentsPreview extends Component {
  static displayName = 'AttachmentsPreview';
  static propTypes = {
    attachments: PropTypes.arrayOf(PropTypes.object),
  };
  static defaultProps = {
    attachments: [],
  };
  constructor(props) {
    super(props);
    this.renderAttachments = this.renderAttachments.bind(this);
  }
  renderAttachments() {
    return this.props.attachments.map((attachment, index) => (
      <View key={index} style={styles.preview}>
        <Image source={{ uri: attachment.path }} style={styles.imagePreview} />
      </View>
    ));
  }
  render() {
    if (this.props.attachments.length) {
      return <View style={styles.container}>{this.renderAttachments()}</View>;
    }
    return null;
  }
}
