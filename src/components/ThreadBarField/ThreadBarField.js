import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Text, TextInput, View } from 'react-native';

export default class ThreadBarField extends Component {
  static displayName = 'ThreadBarField';
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    unit: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.state = { editing: false, text: '' };
    this.startEditing = this.startEditing.bind(this);
    this.stopEditing = this.stopEditing.bind(this);
    this.handleChangeText = this.handleChangeText.bind(this);
  }
  startEditing() {
    this.setState({ editing: true });
  }
  stopEditing() {
    this.setState({ editing: false });
  }
  handleChangeText(text) {
    this.setState({ text });
  }
  render() {
    const { label, value, unit } = this.props;
    const { editing } = this.state;
    if (!editing) {
      return (
        <View
          style={{ flexDirection: 'row', height: 48, alignItems: 'center' }}
        >
          <Text
            style={{
              fontSize: 18,
              fontStyle: 'normal',
              fontWeight: '400',
              color: '#9e9e9e',
            }}
          >
            {label}
          </Text>
          <Text
            style={{
              paddingLeft: 4,
              color: '#212121',
              fontSize: 18,
            }}
            onPress={this.startEditing}
          >
            {value}
          </Text>
          <Text style={{ paddingLeft: 4, color: '#212121', fontSize: 18 }}>
            {unit}
          </Text>
        </View>
      );
    }
    return (
      <View style={{ flexDirection: 'row', height: 48, alignItems: 'center' }}>
        <Text
          style={{
            fontSize: 18,
            fontStyle: 'normal',
            fontWeight: '400',
            color: '#9e9e9e',
          }}
        >
          {label}
        </Text>
        <TextInput
          autoFocus
          style={{
            flex: 1,
            paddingLeft: 4,
            color: '#212121',
            fontSize: 18,
          }}
          onChangeText={this.handleChangeText}
          onBlur={() => this.stopEditing()}
          value={value.toString()}
        />
        <Text style={{ paddingLeft: 4, color: '#212121', fontSize: 18 }}>
          {unit}
        </Text>
      </View>
    );
  }
}
