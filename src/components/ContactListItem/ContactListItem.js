import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Text,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import ContactIcon from '../ContactIcon';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexWrap: 'nowrap',
    flexDirection: 'row',
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 4,
    paddingBottom: 4,
  },
  contactItem: {
    height: 48,
    borderRadius: 4,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1,
  },
  addon: { flex: 0 },
  placeholder: {
    fontSize: 18,
    fontWeight: '200',
    paddingLeft: 16,
    color: '#4e4963',
  },
  contactItemText: {
    fontSize: 16,
    fontWeight: '600',
    color: '#ffffff',
    paddingLeft: 16,
  },
  contactItemPosition: {
    fontSize: 14,
    fontWeight: '300',
    color: '#ffffff',
    paddingLeft: 16,
  },
  contactInformation: {
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  contactIcon: {
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: 'red',
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contactIconText: {
    fontSize: 16,
    fontWeight: '600',
    color: '#ffffff',
  },
});

export default class ContactListItem extends Component {
  static displayName = 'ContactListItem';
  static propTypes = {
    contact: PropTypes.shape({
      name: PropTypes.string,
      color: PropTypes.string,
    }),
    showIcon: PropTypes.bool,
    onPress: PropTypes.func,
    onAddonPress: PropTypes.func,
    style: PropTypes.object,
    placeholder: PropTypes.string,
    renderAddon: PropTypes.bool,
  };
  static defaultProps = {
    style: {},
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnAddonPress = this.handleOnAddonPress.bind(this);
    this.renderContactName = this.renderContactName.bind(this);
    this.renderContactPosition = this.renderContactPosition.bind(this);
    this.renderContactInformation = this.renderContactInformation.bind(this);
    this.renderPlacholder = this.renderPlacholder.bind(this);
    this.renderAddon = this.renderAddon.bind(this);
  }
  handleOnPress() {
    if (this.props.onPress) {
      if (this.props.contact) {
        this.props.onPress(this.props.contact._id);
      } else {
        this.props.onPress();
      }
    }
    return null;
  }
  handleOnAddonPress(userId) {
    if (this.props.onAddonPress) {
      this.props.onAddonPress(userId);
    }
    return null;
  }
  renderContactName() {
    return (
      <Text
        style={[
          styles.contactItemText,
          R.propOr(null, 'contactItemText', this.props.style),
        ]}
        ellipsizeMode="tail"
        numberOfLines={1}
      >
        {R.pathOr('', ['basicData', 'name'], this.props.contact)}
      </Text>
    );
  }
  renderContactPosition() {
    return (
      <Text
        style={[
          styles.contactItemPosition,
          R.propOr(null, 'contactItemPosition', this.props.style),
        ]}
        ellipsizeMode="tail"
        numberOfLines={1}
      >
        {R.pathOr('', ['hrData', 'position'], this.props.contact)}
      </Text>
    );
  }
  renderPlacholder() {
    return (
      <Text
        style={[
          styles.placeholder,
          R.propOr(null, 'placeholder', this.props.style),
        ]}
        ellipsizeMode="tail"
        numberOfLines={1}
      >
        {this.props.placeholder}
      </Text>
    );
  }
  renderContactInformation() {
    if (this.props.contact && !R.isEmpty(this.props.contact)) {
      return [this.renderContactName(), this.renderContactPosition()];
    }
    if (this.props.placeholder) {
      return this.renderPlacholder();
    }
    return null;
  }
  renderAddon(userId) {
    if (this.props.renderAddon) {
      return (
        <TouchableOpacity
          style={styles.addon}
          onPress={() => this.handleOnAddonPress(userId)}
        >
          <Icon size={32} name="ios-close-outline" color="#cccccc" />
        </TouchableOpacity>
      );
    }
    return null;
  }
  render() {
    return (
      <TouchableHighlight
        underlayColor="rgba(242, 242, 245, 1)"
        style={[styles.container, this.props.style.container]}
        onPress={this.handleOnPress}
      >
        <View
          style={[
            styles.contactItem,
            R.propOr(null, 'contactItem', this.props.style),
          ]}
        >
          <ContactIcon
            show={this.props.showIcon}
            id={R.propOr(null, '_id', this.props.contact)}
            // name={R.pathOr('', ['basicData', 'name'], this.props.contact)}
            // color={R.propOr(null, 'color', this.props.contact)}
            // type={R.propOr(null, 'billingType', this.props.contact)}
          />
          <View style={styles.contactInformation}>
            {this.renderContactInformation()}
          </View>
          {this.renderAddon(R.prop('_id', this.props.contact))}
        </View>
      </TouchableHighlight>
    );
  }
}
