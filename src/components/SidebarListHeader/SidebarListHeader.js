import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View } from 'react-native';

export default class SidebarListHeader extends PureComponent {
  static displayName = 'SidebarListHeader';
  static propTypes = {
    item: PropTypes.node,
  };
  render() {
    return <View>{this.props.item}</View>;
  }
}
