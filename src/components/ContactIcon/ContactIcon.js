import R from 'ramda';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  contactIcon: {
    width: 29,
    height: 29,
    borderRadius: 14.5,
    backgroundColor: 'transparent',
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contactIconText: {
    fontSize: 14,
    fontWeight: '200',
    color: '#ffffff',
  },
});

const contactSelector = (state, ownProps) => {
  const contact = R.propOr(
    R.propOr({}, 'contact', ownProps),
    R.prop('id', ownProps),
    R.prop('contacts', state),
  );
  if (ownProps.customer) {
    return {
      ...contact,
      basicData: R.propOr(
        R.prop('basicData', contact._source),
        'basicData',
        contact,
      ),
      ...(contact.billingType
        ? { billingType: contact.billingType }
        : { billingType: R.propOr('contacts', 'billingType', contact) }),
    };
  }
  return contact;
};

const mapStateToProps = (state, ownProps) => ({
  contact: contactSelector(state, ownProps),
});

@connect(mapStateToProps)
export default class ContactIcon extends PureComponent {
  static displayName = 'ContactIcon';
  static propTypes = {
    name: PropTypes.string,
    color: PropTypes.string,
    show: PropTypes.bool,
    styles: PropTypes.object,
    type: PropTypes.string,
    id: PropTypes.string,
    contact: PropTypes.shape({
      basicData: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.arrayOf(PropTypes.string),
      }),
      color: PropTypes.string,
      billingType: PropTypes.string,
      isAdmin: PropTypes.bool,
    }),
    showAdminIcon: PropTypes.bool,
  };
  static defaultProps = {
    styles: {},
  };
  constructor(props) {
    super(props);
    this.renderContactInitials = this.renderContactInitials.bind(this);
  }
  renderContactInitials() {
    const {
      contact: { basicData: { name } = {}, billingType, botId, logo } = {},
    } = this.props;
    if (billingType === 'users' || billingType === 'contacts') {
      if (name) {
        const fullName = name.toUpperCase().split(' ');
        const nameFirstLetter = fullName[0] ? fullName[0][0] : '';
        const surnameFirstLetter = fullName[1] ? fullName[1][0] : '';
        return `${nameFirstLetter}${surnameFirstLetter}`;
      }
      return 'NN';
    }
    if (billingType === 'bots' || botId) {
      if (logo) {
        return logo;
      }
      switch (botId) {
        case '5a8581db17bf50d16513e6c1': {
          return 'GIF';
        }
        case '5a8547c04eb6b66121809805': {
          return 'PST';
        }
        case '5a819c28842780addaaeef99': {
          return 'TST';
        }
        case '5a8548934eb6b66121809806': {
          return 'RMD';
        }
        case '5a797957def72cfc15f2e0e4': {
          return 'F*K';
        }
        default: {
          return 'NN';
        }
      }
    }
    return 'NN';
  }
  render() {
    const { contact: { color, billingType } = {}, show } = this.props;
    if (show) {
      return (
        <View
          style={[
            styles.contactIcon,
            color
              ? {
                  backgroundColor: color,
                  borderColor: color,
                  borderStyle: 'solid',
                  // borderBottomWidth: 1,
                }
              : {
                  backgroundColor: '#fff',
                  // borderWidth: 1,
                  borderBottomWidth: 1,
                  borderBottomColor: '#a8a4b9',
                  // borderStyle: 'dashed',
                  borderWidth: 1,
                  borderRadius: 50,
                  borderStyle: 'dashed',
                  borderColor: '#a8a4b9',
                },
            billingType === 'contacts' || color === '#ffffff'
              ? {
                  backgroundColor: '#fff',
                  borderWidth: 1,
                  borderColor: '#a8a4b9',
                  borderStyle: 'solid',
                }
              : {},
            billingType === 'bots'
              ? {
                  borderRadius: 4.35,
                }
              : {},
            this.props.styles.container,
          ]}
        >
          <Text
            style={[
              styles.contactIconText,
              this.props.styles.text,
              billingType === 'contacts' || color === '#ffffff'
                ? {
                    color: '#837f91',
                  }
                : {},
            ]}
          >
            {this.renderContactInitials()}
          </Text>
        </View>
      );
    }
    return null;
  }
}
