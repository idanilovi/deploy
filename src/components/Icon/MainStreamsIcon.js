import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Path } from 'react-native-svg';

export default class MainStreamsIcon extends PureComponent {
  static displayName = 'MainStreamsIcon';
  static propTypes = {
    fill: PropTypes.string,
  };
  render() {
    return (
      <Path
        fill="none"
        fillRule="nonzero"
        stroke={this.props.fill || '#2B2A29'}
        strokeWidth="236"
        strokeLinecap="round"
        strokeLinejoin="round"
        stroke-miterlimit="22.926"
        d="M1313.03 2574.67c47.68-304.13 233.5-438.62 470.75-408.98 188.39 21.99 356.33 94.39 576.22 194.31 219.89 99.92 387.83 172.32 576.22 194.31 237.25 29.64 423.07-104.85 470.75-408.98"
      />
    );
  }
}
