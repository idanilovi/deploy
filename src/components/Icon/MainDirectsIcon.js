import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { G, Circle, Path } from 'react-native-svg';

export default class MainDirectIcon extends PureComponent {
  static displayName = 'MainDirectIcon';
  static propTypes = {
    fill: PropTypes.string,
  };
  render() {
    return (
      <G fill="none">
        <Circle
          fill="none"
          stroke={this.props.fill || 'black'}
          strokeWidth="236.05"
          strokeLinecap="round"
          strokeLinejoin="round"
          stroke-miterlimit="22.9256"
          cx="2360.45"
          cy="2360.43"
          r="509.63"
        />
        <Path
          fill="none"
          stroke={this.props.fill || 'black'}
          strokeWidth="236.05"
          strokeLinecap="round"
          strokeLinejoin="round"
          stroke-miterlimit="22.9256"
          d="M2870.08 2360.43l0 127.42c0,211.02 171.2,382.21 382.26,382.21 211.07,0 382.26,-171.19 382.26,-382.26l0 -127.42c-0.04,-703.56 -570.59,-1273.96 -1274.15,-1273.96 -703.51,0 -1274.05,570.54 -1274.05,1274.06 0,703.56 570.54,1274.1 1274.05,1274.1 279.98,0 552.35,-92.41 774.64,-262.55"
        />
      </G>
    );
  }
}
