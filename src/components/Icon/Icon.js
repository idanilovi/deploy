import React, { PureComponent } from 'react';
import SvgIcon from 'react-native-svg-icon';
import svgs from './svgs';

export default class Icon extends PureComponent {
  static displayName = 'Icon';
  static defaultProps = {
    fill: '#fff',
    viewBox: '0 0 50 50',
  };
  render() {
    return <SvgIcon {...this.props} svgs={svgs} />;
  }
}
