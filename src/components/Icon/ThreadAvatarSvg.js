import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Svg, { Path, G } from 'react-native-svg';

export default class ThreadAvatarSvg extends PureComponent {
  static displayName = 'MainNotificationsIcon';
  static propTypes = {
    fill: PropTypes.string,
    strokeDasharray: PropTypes.string,
  };
  render() {
    return (
      <Svg height="33" width="33" viewBox={this.props.viewBox}>
        <G fill="transparent">
          <Path
            // viewBox={this.props.viewBox}
            strokeWidth={3}
            d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
            stroke="rgba(255, 255, 255, 0.2)"
          />
          <Path
            // viewBox={this.props.viewBox}
            strokeWidth={3}
            stroke={this.props.fill || '#2B2A29'}
            strokeDasharray={this.props.dasharray}
            d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
          />
        </G>
      </Svg>
    );
  }
}
