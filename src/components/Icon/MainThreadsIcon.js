import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Polyline } from 'react-native-svg';

export default class MainThreadsIcon extends PureComponent {
  static displayName = 'MainThreadsIcon';
  static propTypes = {
    fill: PropTypes.string,
  };
  render() {
    return (
      <Polyline
        fill="none"
        fillRule="nonzero"
        stroke={this.props.fill || '#2B2A29'}
        strokeWidth="236.05"
        strokeLinecap="round"
        strokeLinejoin="round"
        stroke-miterlimit="22.9256"
        points="1606.92,3156.85 3114.08,2415.24 1606.92,1564.15 "
      />
    );
  }
}
