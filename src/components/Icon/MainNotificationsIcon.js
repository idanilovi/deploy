import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Path } from 'react-native-svg';

export default class MainNotificationsIcon extends PureComponent {
  static displayName = 'MainNotificationsIcon';
  static propTypes = {
    fill: PropTypes.string,
  };
  render() {
    return (
      <Path
        fill="none"
        fillRule="evenodd"
        clipRule="evenodd"
        stroke={this.props.fill || '#2B2A29'}
        strokeWidth="236.05"
        stroke-miterlimit="22.9256"
        d="M2421.59 3438.34c-36.01,58.42 -94.2,58.78 -130.44,0l-283.53 -459.87c-433.31,-51.07 -769.42,-419.63 -769.42,-866.87 0,-482.09 390.98,-872.89 872.65,-872.89l499.3 0c481.96,0 872.65,390.68 872.65,872.89 0,450.19 -340.94,820.77 -778.29,867.85l-282.92 458.89z"
      />
    );
  }
}
