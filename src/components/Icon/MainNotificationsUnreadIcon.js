import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Path, G, Circle } from 'react-native-svg';

export default class MainNotificationsIcon extends PureComponent {
  static displayName = 'MainNotificationsIcon';
  static propTypes = {
    fill: PropTypes.string,
    fillCircle: PropTypes.string,
  };
  render() {
    return (
      <G>
        <Path
          id="Rectangle-154"
          fill="none"
          fillRule="nonzero"
          stroke={this.props.fill || '#2B2A29'}
          strokeWidth="73.6"
          stroke-miterlimit="22.9256"
          d="M755.05 1072.07c-11.23,18.21 -29.37,18.33 -40.67,0l-88.41 -143.39c-135.1,-15.92 -239.9,-130.84 -239.9,-270.29 0,-150.31 121.91,-272.16 272.09,-272.16l155.68 0c150.28,0 272.09,121.81 272.09,272.16 0,140.37 -106.3,255.92 -242.67,270.6l-88.21 143.08z"
        />
        <Circle fill="#ffffff" cx="1212.45" cy="438.69" r="100.57" />
      </G>
    );
  }
}
