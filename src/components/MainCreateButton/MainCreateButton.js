import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  ScrollView,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';
import Icons from '../Icon';

const styles = StyleSheet.create({
  createButton: {
    flex: 0,
    backgroundColor: 'transparent',
    borderRadius: 18,
    height: 32,
    width: 32,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  addIcon: {
    backgroundColor: 'transparent',
    color: '#ffffff',
  },
  checkmark: {
    backgroundColor: 'transparent',
    color: '#000000',
    paddingVertical: 18,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    // height: 144,
  },
  view: {
    flex: 1,
    position: 'absolute',
    width: 330,
    paddingHorizontal: 8,
    paddingVertical: 8,
    borderRadius: 8,
    backgroundColor: '#ffffff',
  },
  button: {
    padding: 20,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '400',
  },
  inputView: {
    flexDirection: 'row',
  },
  textInput: {
    width: 280,
  },
});

export default class MainCreateButton extends Component {
  static displayName = 'MainCreateButton';
  static propTypes = {
    userId: PropTypes.string,
    navigator: PropTypes.object,
    styles: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.state = {
      userId: this.props.userId,
      update: false,
    };
  }
  componentWillReceiveProps(nextprops) {
    if (this.props.userId !== nextprops.userId) {
      this.setState({
        update: true,
      });
    }
  }
  // eslint-disable-next-line class-methods-use-this
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.update) {
      this.setState({
        update: false,
      });
      return true;
    }

    return false;
  }
  handleOnPress() {
    this.props.createThread(
      {
        statusId: '',
        title: '',
        streamId: '',
        roles: [this.props.userId],
        authorId: this.props.userId,
      },
      id =>
        this.props.navigator.push({
          screen: 'workonflow.Thread',
          title: '',
          passProps: {
            id,
            threadId: id,
            type: 'Thread',
            focusOnTitle: true,
          },
        }),
    );
  }
  render() {
    return (
      <TouchableOpacity
        key="add_button"
        onPress={this.handleOnPress}
        style={styles.createButton}
      >
        <Icons
          name="NewPlus"
          width={30}
          height={30}
          fill="rgba(0, 197, 157, 1)"
          viewBox="0 0 550.28 451.28"
        />
      </TouchableOpacity>
    );
  }
}
