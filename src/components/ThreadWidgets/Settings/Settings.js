import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Dimensions, Switch, StyleSheet, Text, View } from 'react-native';
import Modal from 'react-native-modal';
import Button from '../../Button';

const styles = StyleSheet.create({
  container: { paddingVertical: 16, paddingHorizontal: 16 },
  modalContainer: {
    flex: 1,
    position: 'absolute',
    backgroundColor: '#ffffff',
    paddingVertical: 16,
    paddingHorizontal: 16,
    borderRadius: 8,
    flexDirection: 'column',
  },
  label: {
    fontSize: 18,
    fontWeight: '400',
    color: '#9e9e9e',
  },
  value: {
    paddingLeft: 4,
    fontWeight: '300',
    fontSize: 18,
    color: '#212121',
  },
  unit: {
    paddingLeft: 4,
    color: '#212121',
    fontSize: 18,
    fontWeight: '300',
  },
  icon: { paddingLeft: 4 },
});

export default class Settings extends Component {
  static displayName = 'Settings';
  static propTypes = {
    streamId: PropTypes.string,
    settings: PropTypes.object,
    onChange: PropTypes.func,
    onSave: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.state = { show: false, prevSettings: props.settings };

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.toggle = this.toggle.bind(this);

    this.renderButtons = this.renderButtons.bind(this);
    this.renderSettingsItems = this.renderSettingsItems.bind(this);
    this.renderSettingsItem = this.renderSettingsItem.bind(this);
    this.renderModalContent = this.renderModalContent.bind(this);

    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnCancel = this.handleOnCancel.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
  }
  show() {
    this.setState({ show: true });
  }
  hide() {
    this.setState({ show: false });
  }
  toggle() {
    this.setState({ show: !this.state.show });
  }
  handleOnChange(type) {
    if (this.props.onChange) {
      const widgets = R.assoc(
        type,
        { type, on: !R.pathOr(false, [type, 'on'], this.props.settings) },
        this.props.settings,
      );
      this.props.onChange(this.props.streamId, widgets);
    }
  }
  handleOnSave() {
    if (this.props.onSave) {
      this.props.onSave(this.props.streamId, this.props.settings);
    }
    this.hide();
  }
  handleOnCancel() {
    if (this.props.onChange) {
      if (!R.equals(this.props.settings, this.state.prevSettings)) {
        this.props.onChange(this.props.streamId, this.state.prevSettings);
      }
    }
    this.hide();
  }
  renderSettingsItems() {
    const settingsMap = {
      Main: ['WorkflowStatus', 'Responsible', 'Customers'],
      Time: ['DataTime', 'Priority'],
      Resources: ['TimeBudget', 'Budget', 'Points'],
    };
    return R.map(
      key => (
        <View key={key}>
          <Text style={{ color: '#8d889e', fontSize: 16, fontWeight: '600' }}>
            {key}
          </Text>
          {R.map(
            value =>
              this.renderSettingsItem(
                R.propOr(
                  { type: value, on: false },
                  value,
                  this.props.settings,
                ),
              ),
            R.propOr([], key, settingsMap),
          )}
        </View>
      ),
      R.keys(settingsMap),
    );
  }
  renderButtons() {
    return [
      <Button
        key="cancel-button"
        title="Cancel"
        styles={{ title: { color: '#212121' } }}
        onPress={this.handleOnCancel}
      />,
      <Button
        key="save-button"
        title="Save"
        disabled={R.equals(this.props.settings, this.state.prevSettings)}
        styles={{
          button: { backgroundColor: '#0d71d7' },
          disabled: { backgroundColor: '#EEEEEE' },
        }}
        onPress={this.handleOnSave}
      />,
    ];
  }
  renderModalContent() {
    return (
      <View
        style={[
          styles.modalContainer,
          { width: Dimensions.get('window').width - 48 },
        ]}
      >
        <View>{this.renderSettingsItems()}</View>
        <View
          style={{
            paddingVertical: 8,
            paddingHorizontal: 16,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
          }}
        >
          {this.renderButtons()}
        </View>
      </View>
    );
  }
  renderSettingsItem({ type, on }) {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: 16,
          paddingVertical: 8,
        }}
      >
        <Text style={{ color: '#4e4963', fontSize: 14, fontWeight: '300' }}>
          {type}
        </Text>
        <Switch value={on} onValueChange={() => this.handleOnChange(type)} />
      </View>
    );
  }
  render() {
    return (
      <Modal
        isVisible={this.state.show}
        style={styles.modal}
        useNativeDriver={true}
        onBackdropPress={this.hide}
        onBackButtonPress={this.hide}
      >
        {this.renderModalContent()}
      </Modal>
    );
  }
}
