import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import {
  Dimensions,
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
} from 'react-native';
import Modal from 'react-native-modal';
import moment from 'moment';
import Calendar from 'react-native-day-picker';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'rgba(211, 210,211, 1)',
  },
  wrapper: {
    flexDirection: 'row',
    flex: 1,
    height: 70,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
    // paddingVertical: 26,
  },
  calendarContainer: {
    flex: 0,
    height: height - 130,
    paddingVertical: 16,
    paddingHorizontal: 16,
    borderRadius: 8,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontSize: 18,
    fontWeight: '400',
    color: '#4e4963',
  },
  value: {
    fontSize: 18,
    fontWeight: '400',
    color: '#4e4963',
  },
  buttonContainer: {
    paddingTop: 16,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  button: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 4,
  },
});

export default class Deadline extends PureComponent {
  static displayName = 'Deadline';
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.array,
    previousValue: PropTypes.array,
    threadId: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { show: false, previousValue: props.value };
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.handleOnSelectionChange = this.handleOnSelectionChange.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnCancel = this.handleOnCancel.bind(this);
    this.renderDate = this.renderDate.bind(this);
    this.renderSeparator = this.renderSeparator.bind(this);
    this.renderButton = this.renderButton.bind(this);
    this.renderModal = this.renderModal.bind(this);
  }
  show() {
    this.setState({ show: true });
  }
  hide() {
    this.setState({ show: false });
  }
  handleOnSelectionChange(current, previous) {
    const currentDate = Date.parse(current);
    const previousDate = Date.parse(previous);
    if (previous) {
      this.props.onChange(this.props.threadId, [previousDate, currentDate]);
    } else {
      this.props.onChange(this.props.threadId, [null, currentDate]);
    }
  }
  handleOnSubmit() {
    this.props.onSubmit(this.props.threadId, this.props.value);
    this.hide();
  }
  handleOnCancel() {
    this.props.onChange(this.props.threadId, this.state.previousValue);
    this.hide();
  }
  renderModal() {
    if (this.state.show) {
      const from = new Date();
      from.setDate(from.getDate() - 16);
      const to = new Date();
      const startDate = new Date();
      startDate.setMonth(startDate.getMonth());
      return (
        <Modal
          isVisible={this.state.show}
          style={styles.modal}
          useNativeDriver={true}
          onBackdropPress={this.hide}
          onBackButtonPress={this.hide}
          backdropColor="rgba(40,30,42,1)"
          backdropOpacity={1}
        >
          <View style={styles.calendarContainer}>
            <Calendar
              monthsCount={12}
              startFromMonday={true}
              startDate={startDate}
              selectFrom={from}
              selectTo={to}
              isFutureDate={true}
              // Calendars width, should be divided on 7 without remainder or
              //  may cause unpredictable behaviour.
              width={width - 64 - (width - 64) % 7}
              onSelectionChange={this.handleOnSelectionChange}
            />
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={[styles.button, { backgroundColor: 'transparent' }]}
                onPress={this.handleOnCancel}
              >
                <Text style={styles.value}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.button,
                  { backgroundColor: '#0d71d7', marginLeft: 16 },
                ]}
                onPress={this.handleOnSubmit}
              >
                <Text style={[styles.value, { color: '#ffffff' }]}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      );
    }
    return null;
  }
  renderSeparator() {
    const { value } = this.props;
    if (value) {
      if (value[0] && value[1]) {
        return <Text style={styles.value}>{` - `}</Text>;
      }
      return null;
    }
    return null;
  }
  // eslint-disable-next-line class-methods-use-this
  renderDate(date) {
    if (date) {
      return (
        <Text style={styles.value} ellipsizeMode="tail" numberOfLines={1}>
          {moment(date).format('DD MMM')}
        </Text>
      );
    }
    return null;
  }
  renderButton() {
    const { label, value } = this.props;
    return (
      <TouchableOpacity onPress={this.show}>
        <View style={styles.wrapper}>
          <Text style={styles.label}>{label}</Text>
          <Text>
            {this.renderDate(R.propOr(null, 0, value))}
            {this.renderSeparator()}
            {this.renderDate(R.propOr(null, 1, value))}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        {this.renderButton()}
        {this.renderModal()}
      </View>
    );
  }
}
