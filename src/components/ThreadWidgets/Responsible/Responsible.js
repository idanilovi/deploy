import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Navigation } from 'react-native-navigation';
import { StyleSheet } from 'react-native';
import ContactListItem from '../../ContactListItem';

export default class Responsible extends PureComponent {
  static displayName = 'Responsible';
  static propTypes = {
    items: PropTypes.array,
    currentItem: PropTypes.object,
    threadId: PropTypes.string,
    onСrossPress: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.handleOnPress = this.handleOnPress.bind(this);
    this.handleOnCrossPress = this.handleOnCrossPress.bind(this);
  }
  handleOnPress() {
    const { items, currentItem, threadId } = this.props;
    // eslint-disable-next-line no-underscore-dangle
    if (currentItem._id) {
      Navigation.showModal({
        screen: 'workonflow.ThreadContactActions',
        navigatorButtons: {},
        navigatorStyle: {
          navBarHidden: true,
        },
        passProps: {
          items,
          currentItem,
          threadId,
          type: 'responsible',
        },
      });
    } else {
      Navigation.showModal({
        screen: 'workonflow.ThreadResponsible',
        passProps: {
          items,
          currentItem,
          threadId,
          type: 'responsible',
        },
      });
    }
  }
  handleOnCrossPress(userId) {
    this.props.onСrossPress(userId);
  }
  render() {
    return (
      <ContactListItem
        showIcon
        placeholder="Set responsible"
        style={{
          container: {
            height: 64,
            borderBottomColor: 'rgba(211, 210,211, 1)',
            borderBottomWidth: StyleSheet.hairlineWidth,
          },
          contactListItem: {
            backgroundColor: '#e2e2e2',
            marginBottom: 8,
          },
          contactItemText: {
            fontSize: 18,
            fontWeight: '700',
            color: '#4e4963',
          },
        }}
        contact={this.props.currentItem}
        onPress={this.handleOnPress}
      />
    );
  }
}
