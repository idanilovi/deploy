import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, StyleSheet, TouchableHighlight, Text } from 'react-native';
import { Navigation } from 'react-native-navigation';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'rgba(211, 210,211, 1)',
  },
  wrapper: {
    flexDirection: 'row',
    flex: 1,
    height: 70,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
    // paddingVertical: 26,
  },
  label: {
    fontSize: 18,
    fontWeight: '400',
    color: '#4e4963',
  },
  value: {
    paddingLeft: 4,
    fontWeight: '400',
    fontSize: 18,
    color: '#4e4963',
  },
});

export default class Points extends PureComponent {
  static displayName = 'Points';
  static propTypes = {
    threadId: PropTypes.string,
    value: PropTypes.number,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onSlidingComplete: PropTypes.func.isRequired,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { threadId } = this.props;
    Navigation.showModal({
      screen: 'workonflow.ThreadPoints',
      passProps: {
        threadId,
      },
    });
  }
  render() {
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={this.handleOnPress}
        underlayColor="rgba(242, 242, 245, 1)"
      >
        <View style={styles.wrapper}>
          <Text style={styles.label}>{this.props.label}</Text>
          <Text style={styles.value}>
            {this.props.value || this.props.placeholder}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }
}
