import R from 'ramda';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Switch,
} from 'react-native';
import Icon from '../../Icon';
import { setPriority, threadByIdSelector } from '../../../reducers/threads';
import { setThreadPriority } from '../../../actions';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'rgba(211, 210,211, 1)',
  },
  wrapper: {
    flexDirection: 'row',
    flex: 1,
    height: 70,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
    // paddingVertical: 26,
  },
  label: {
    fontSize: 18,
    fontWeight: '400',
    color: '#4e4963',
  },
  value: {
    paddingLeft: 4,
    fontWeight: '400',
    fontSize: 18,
    color: '#4e4963',
  },
  icon: { paddingLeft: 4 },
  circle: {
    position: 'absolute',
    width: 28,
    height: 28,
    borderRadius: 14,
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#aaa6ba',
  },
});

const mapStateToProps = (state, ownProps) => ({
  thread: threadByIdSelector(state, ownProps),
});

const mapDispatchToProps = {
  setPriority,
  setThreadPriority,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class Priority extends Component {
  static displayName = 'Priority';
  static propTypes = {
    threadId: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    thread: PropTypes.shape({
      priority: PropTypes.string,
    }),
    setPriority: PropTypes.func.isRequired,
    setThreadPriority: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.handleOnValueChange = this.handleOnValueChange.bind(this);
  }
  handleOnValueChange() {
    if (R.propOr('NORMAL', 'priority', this.props.thread) === 'HIGH') {
      this.props.setPriority(this.props.threadId, 'NORMAL');
      this.props.setThreadPriority(this.props.threadId, 'NORMAL');
      return 'handled';
    }
    this.props.setPriority(this.props.threadId, 'HIGH');
    this.props.setThreadPriority(this.props.threadId, 'HIGH');
    return 'handled';
  }
  render() {
    const isHighPriority =
      R.propOr('NORMAL', 'priority', this.props.thread) === 'HIGH';
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={this.handleOnValueChange}
        underlayColor="rgba(242, 242, 245, 1)"
      >
        <View style={styles.wrapper}>
          <View style={{ flexDirection: 'row' }}>
            <Text
              style={[styles.label, isHighPriority && { color: '#e30613' }]}
            >
              {this.props.label}
            </Text>
            <View style={{ paddingLeft: 4 }}>
              {isHighPriority ? (
                <Icon
                  name="HighPriority"
                  width={18}
                  height={18}
                  fill="#e30613"
                  viewBox="0 0 1000 1000"
                />
              ) : null}
            </View>
          </View>
          <Switch
            value={isHighPriority}
            onValueChange={this.handleOnValueChange}
          />
        </View>
      </TouchableHighlight>
    );
  }
}
