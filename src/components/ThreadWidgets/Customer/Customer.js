import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native';
import { Navigation } from 'react-native-navigation';
import ContactIcon from '../../ContactIcon';

export default class Responsible extends PureComponent {
  static displayName = 'Responsible';
  static propTypes = {
    threadId: PropTypes.string,
    customer: PropTypes.object,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { customer, threadId } = this.props;
    Navigation.showModal({
      screen: 'workonflow.ThreadContactActions',
      navigatorButtons: {},
      navigatorStyle: {
        navBarHidden: true,
      },
      passProps: {
        currentItem: this.props.customer,
        threadId,
        type: 'customer',
        header: 'Customer',
      },
    });
  }
  render() {
    const { customer = {} } = this.props;
    // eslint-disable-next-line no-underscore-dangle
    if (customer._id) {
      return (
        <TouchableHighlight
          underlayColor="rgba(242, 242, 245, 1)"
          onPress={this.handleOnPress}
        >
          <View>
            <View style={{ paddingTop: 15, paddingHorizontal: 16 }}>
              <Text
                style={{ fontWeight: '400', fontSize: 15, color: '#a8a4b9' }}
              >
                Customer
              </Text>
            </View>
            <View
              style={{
                height: 64,
                paddingHorizontal: 16,
                justifyContent: 'center',
                alignItems: 'flex-start',
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: 'rgba(211, 210,211, 1)',
              }}
            >
              <View style={{ flexDirection: 'row' }}>
                <ContactIcon
                  show={true}
                  id={R.propOr('', '_id', this.props.customer)}
                  // name={R.pathOr(
                  //   '',
                  //   ['basicData', 'name'],
                  //   this.props.customer,
                  // )}
                  // color={R.propOr('#ffffff', 'color', this.props.customer)}
                  // type={R.propOr(null, 'billingType', this.props.customer)}
                />
                <View
                  style={{
                    flexDirection: 'column',
                    paddingHorizontal: 18,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: '700',
                      color: '#4e4963',
                    }}
                  >
                    {R.pathOr('', ['basicData', 'name'], this.props.customer)}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '400',
                      color: '#4e4963',
                    }}
                  >
                    {R.pathOr(
                      R.pathOr(
                        R.pathOr('', ['position'], this.props.customer),
                        ['basicData', 'position'],
                        this.props.customer,
                      ),
                      ['hrData', 'position'],
                      this.props.customer,
                    )}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableHighlight>
      );
    }
    return (
      <TouchableHighlight
        underlayColor="rgba(242, 242, 245, 1)"
        onPress={this.handleOnPress}
      >
        <View>
          <View style={{ paddingTop: 15, paddingHorizontal: 16 }}>
            <Text style={{ fontWeight: '400', fontSize: 15, color: '#a8a4b9' }}>
              Customer
            </Text>
          </View>
          <View
            style={{
              height: 64,
              paddingHorizontal: 16,
              justifyContent: 'center',
              alignItems: 'flex-start',
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderBottomColor: 'rgba(211, 210,211, 1)',
            }}
          >
            <Text>{'add customer to thread'}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
