import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Navigation } from 'react-native-navigation';
import ContactListItem from '../../ContactListItem';

export default class Roles extends PureComponent {
  static displayName = 'Roles';
  static propTypes = {
    threadId: PropTypes.string,
    items: PropTypes.array,
    currentItems: PropTypes.array,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderRoles = this.renderRoles.bind(this);
  }
  handleOnPress(currentItem = {}) {
    const { items, threadId } = this.props;
    // eslint-disable-next-line no-underscore-dangle
    if (currentItem._id) {
      Navigation.showModal({
        screen: 'workonflow.ThreadContactActions',
        navigatorButtons: {},
        navigatorStyle: {
          navBarHidden: true,
        },
        passProps: {
          items,
          currentItem,
          threadId,
          type: 'follower',
          header: 'Follower',
        },
      });
    } else {
      Navigation.showModal({
        screen: 'workonflow.ThreadResponsible',
        passProps: {
          items,
          currentItem,
          threadId,
          type: 'follower',
          header: 'Follower',
        },
      });
    }
  }
  renderRoles() {
    if (this.props.currentItems.length) {
      return this.props.currentItems.map(item => (
        <ContactListItem
          // eslint-disable-next-line no-underscore-dangle
          key={item._id}
          showIcon
          placeholder="Add follower"
          style={{
            container: {
              height: 64,
              borderBottomColor: 'rgba(211, 210,211, 1)',
              borderBottomWidth: StyleSheet.hairlineWidth,
            },
            contactListItem: {
              backgroundColor: '#e2e2e2',
              marginBottom: 8,
            },
            contactItemText: {
              fontSize: 18,
              fontWeight: '700',
              color: '#4e4963',
            },
          }}
          contact={item}
          onPress={() => this.handleOnPress(item)}
        />
      ));
    }
    return null;
  }
  render() {
    return (
      <View>
        <View
          style={{ paddingTop: 30, paddingBottom: 15, paddingHorizontal: 16 }}
        >
          <Text style={{ fontWeight: '400', fontSize: 15, color: '#a8a4b9' }}>
            Followers
          </Text>
        </View>
        {this.renderRoles()}
        <ContactListItem
          showIcon
          placeholder="Add follower"
          style={{
            container: {
              height: 64,
              borderBottomColor: 'rgba(211, 210,211, 1)',
              borderBottomWidth: StyleSheet.hairlineWidth,
            },
            contactListItem: {
              backgroundColor: '#e2e2e2',
              marginBottom: 8,
            },
            contactItemText: {
              fontSize: 18,
              fontWeight: '700',
              color: '#4e4963',
            },
          }}
          onPress={this.handleOnPress}
        />
      </View>
    );
  }
}
