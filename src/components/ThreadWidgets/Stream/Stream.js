import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native';
import { Navigation } from 'react-native-navigation';

export default class Stream extends PureComponent {
  static displayName = 'Stream';
  static propTypes = {
    placeholder: PropTypes.string,
    items: PropTypes.array,
    currentItem: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
      color: PropTypes.string,
    }),
    threadId: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { items, currentItem, threadId } = this.props;
    Navigation.showModal({
      screen: 'workonflow.ThreadStream',
      passProps: {
        items,
        currentItem,
        threadId,
      },
    });
  }
  render() {
    return (
      <TouchableHighlight
        underlayColor="rgba(242, 242, 245, 1)"
        onPress={this.handleOnPress}
      >
        <View
          style={{
            flex: 1,
            height: 64,
            borderBottomColor: 'rgba(211, 210,211, 1)',
            borderBottomWidth: StyleSheet.hairlineWidth,
            paddingVertical: 20,
            paddingHorizontal: 12,
          }}
        >
          <Text
            style={{
              fontSize: 18,
              fontWeight: '400',
              color: '#4e4963',
            }}
          >
            {`~${R.propOr(
              this.props.placeholder,
              'name',
              this.props.currentItem,
            )}`}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }
}
