import R from 'ramda';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native';
import { Navigation } from 'react-native-navigation';

export default class Status extends PureComponent {
  static displayName = 'Status';
  static propTypes = {
    placeholder: PropTypes.string,
    items: PropTypes.array,
    currentItem: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
      color: PropTypes.string,
    }),
    threadId: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
    this.renderButtonFooter = this.renderButtonFooter.bind(this);
    this.renderButtonFooterItems = this.renderButtonFooterItems.bind(this);
  }
  handleOnPress() {
    const { items, currentItem, threadId } = this.props;
    Navigation.showModal({
      screen: 'workonflow.ThreadStatus',
      passProps: {
        items,
        currentItem,
        threadId,
      },
    });
  }
  renderButtonFooter() {
    return this.renderButtonFooterItems();
  }
  renderButtonFooterItems() {
    if (this.props.items.length) {
      return this.props.items.map(item => {
        return (
          <View
            key={item.id}
            style={[
              {
                flex: 1,
                height: 8,
                borderRadius: 4,
                marginHorizontal: 1,
              },
              {
                backgroundColor:
                  item.id === R.propOr(null, 'id', this.props.currentItem)
                    ? item.color || '#ffffff'
                    : '#cccccc',
              },
            ]}
          />
        );
      });
    }
    return null;
  }
  render() {
    return (
      <TouchableHighlight
        underlayColor="rgba(242, 242, 245, 1)"
        onPress={this.handleOnPress}
      >
        <View
          style={{
            flex: 1,
            height: 64,
            borderBottomColor: 'rgba(211, 210,211, 1)',
            borderBottomWidth: StyleSheet.hairlineWidth,
            paddingVertical: 6,
            paddingHorizontal: 12,
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}
          >
            <Text
              style={{
                fontSize: 18,
                fontWeight: '400',
                color: R.propOr('#cccccc', 'color', this.props.currentItem),
              }}
            >
              {R.propOr(this.props.placeholder, 'name', this.props.currentItem)}
            </Text>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 8,
            }}
          >
            {this.renderButtonFooter()}
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
