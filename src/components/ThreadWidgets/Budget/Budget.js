import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { TouchableHighlight, StyleSheet, Text, View } from 'react-native';
import { Navigation } from 'react-native-navigation';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'rgba(211, 210,211, 1)',
  },
  wrapper: {
    flexDirection: 'row',
    flex: 1,
    height: 70,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
    // paddingVertical: 16,
  },
  label: {
    fontSize: 18,
    fontWeight: '400',
    color: '#4e4963',
  },
  value: {
    paddingLeft: 4,
    fontWeight: '400',
    fontSize: 18,
    color: '#4e4963',
  },
  unit: {
    paddingLeft: 4,
    color: '#4e4963',
    fontSize: 18,
    fontWeight: '400',
  },
});

export default class Budget extends PureComponent {
  static displayName = 'Budget';
  static propTypes = {
    threadId: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    unit: PropTypes.string,
  };
  constructor() {
    super();
    this.handleOnPress = this.handleOnPress.bind(this);
  }
  handleOnPress() {
    const { threadId, unit } = this.props;
    Navigation.showModal({
      screen: 'workonflow.ThreadBudget',
      passProps: {
        threadId,
        unit,
      },
    });
  }
  render() {
    const { unit, value, label } = this.props;
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={this.handleOnPress}
        underlayColor="rgba(242, 242, 245, 1)"
      >
        <View style={styles.wrapper}>
          <Text style={styles.label}>{label}</Text>
          <Text style={styles.value} ellipsizeMode="tail" numberOfLines={1}>
            {value}
            <Text style={styles.unit}>{unit}</Text>
          </Text>
        </View>
      </TouchableHighlight>
    );
  }
}
