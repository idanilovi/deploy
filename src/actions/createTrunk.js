import Socket from './Socket';
import getTrunks from './getTrunks';

export default function createTrunk({
  password = '',
  name = 'default',
  phoneNumber = '',
  operatorId = 'default',
  isSearchAllStream = false,
  redirect = '',
  username,
  variables = {},
  options = {},
  realm,
}) {
  return async dispatch => {
    await Socket.command('Channel.Trunks.create', {
      settingsPm: { isSearchAllStream, redirect },
      trunks: {
        name,
        operatorId,
        phoneNumber,
        sip: { options, params: { username, password, realm }, variables },
      },
    });
    dispatch(getTrunks({}));
  };
}
