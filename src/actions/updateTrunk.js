import R from 'ramda';
import Socket from './Socket';
import getTrunks from './getTrunks';

export default function updateTrunk({ id, updateTrunk }) {
  return async dispatch => {
    const response = await Socket.command('Channel.Trunks.update', {
      trunkId: id,
      updateTrunk,
    });
    if (response.code !== 200) return;
    dispatch(getTrunks({}));
  };
}
