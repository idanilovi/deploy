import Socket from './Socket';
import { getTrunks } from './getTrunks';

export default function deleteTrunk({ trunkId }) {
  return async dispatch => {
    await Socket.command('Channel.Trunks.remove', {
      trunkId,
    });
    dispatch(getTrunks({}));
  };
}
