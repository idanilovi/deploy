import getFilesUrl from './getFilesUrl';

export default function getFile(id) {
  return async dispatch => {
    dispatch(getFilesUrl(id));
  };
}
