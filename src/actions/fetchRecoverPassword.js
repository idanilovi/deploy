export default function fetchRecoverPassword({
  email,
  successCallback,
  failureCallback,
}) {
  return async () => {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(`${apiEndpoint}/recover`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({ email: email.toLowerCase() }),
    });
    const response = await fetch(request);
    if (response.status === 204) {
      if (successCallback) {
        successCallback(response);
      }
    } else if (failureCallback) {
      const responseJSON = await response.json();
      failureCallback(responseJSON);
    }
  };
}
