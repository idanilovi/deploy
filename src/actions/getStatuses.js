import Socket from './Socket';
import { receiveStatuses } from '../reducers/statuses';

export default function action({ streamId = null, id = null }) {
  return async function getStatuses(dispatch) {
    const response = await Socket.command('Status.read', {
      ...(streamId ? { streamId } : {}),
      ...(id ? { id } : {}),
    });
    // eslint-disable-next-line no-shadow
    const statuses = response.data.map((status) => {
      const { _id, ...rest } = status;
      return {
        id: _id,
        ...rest,
      };
    });
    dispatch(receiveStatuses(statuses));
  };
}
