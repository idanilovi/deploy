import Socket from './Socket';

export default function setThreadPoints(threadId, rank) {
  return async function action() {
    await Socket.command('Thread.setRank', {
      id: threadId,
      mainRank: rank,
    });
  };
}
