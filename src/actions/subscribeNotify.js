import Socket from './Socket';

export default function subscribeNotify(token) {
  return async function action() {
    const res = await Socket.command('Notifications.subscribe', {
      registrationToken: token,
    });
  };
}
