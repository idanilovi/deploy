import R from 'ramda';
import Socket from './Socket';
import { receiveFile, receiveFiles, requestFile } from '../reducers/files';
import FileCache from '../helpers/FileCache';

export default function getFilesUrl({ id, force = false }, callback = null) {
  if (!id) {
    throw new Error('Request file without id!');
  }
  return async (dispatch, getState) => {
    const filesInState = R.prop('files', getState());
    if (R.contains(id, R.keys(filesInState)) && !force) {
      return R.prop(id, filesInState);
    }
    const existsInCache = await FileCache.exists({ id });
    if (existsInCache) {
      if (Array.isArray(id)) {
        const filesInCache = await Promise.all(
          id.map(_ => FileCache.get({ id: _, force })),
        );
        dispatch(receiveFiles(filesInCache));
        return filesInCache;
      }
      const fileInCache = await FileCache.get({ id, force });
      dispatch(receiveFile(fileInCache));
      return fileInCache;
    }

    if (Array.isArray(id)) {
      id.map(_ => dispatch(requestFile(_)));
    } else {
      dispatch(requestFile(id));
    }

    const response = await Socket.command(
      'File.getGETUrl',
      {
        ...(Array.isArray(id)
          ? { ids: id, expires: 259200 }
          : { id, expires: 259200 }),
      },
      5000,
    );

    if (response.data) {
      if (Array.isArray(response.data)) {
        const filesInCache = await Promise.all(
          response.data.map(_ => FileCache.get({ ..._, force })),
        );
        dispatch(receiveFiles(filesInCache));
        return filesInCache;
      }
      if (callback) {
        callback(response.data.url);
      }
      const fileInCache = await FileCache.get({
        ...response.data,
        force,
      });
      dispatch(receiveFile(fileInCache));
      return fileInCache;
    }

    return [];
  };
}
