import { receiveTeams } from '../reducers/teams';

export default function fetchAccessTeams(cb) {
  return async function action(dispatch) {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(`${apiEndpoint}/access`, {
      method: 'GET',
    });
    const response = await fetch(request);
    if (response.status === 200) {
      const teams = await response.json();
      dispatch(receiveTeams(teams));
      if (cb) {
        if (teams.length === 1) {
          const { teamId, url, userId, teamName } = teams[0];
          await cb({ teamId, to: url, userId, teamName });
        }
      }
    }
  };
}
