/* eslint-disable no-underscore-dangle */
import Socket from './Socket';

export default function setThreadSequence({ streamId, threadId }) {
  return async dispatch => {
    const response = await Socket.command('Stream.setThreadsSequence', {
      id: streamId,
      threadId,
    });
    return;
  };
}
