/* eslint-disable no-underscore-dangle */
import Socket from './Socket';
import { receiveStatus } from '../reducers/statuses';

export default function createStatus({ title, color, streamId, type }) {
  return async dispatch => {
    const response = await Socket.command('Status.create', {
      name: title,
      type: type || 'In progress',
      color: color || '#000000',
      streamId,
    });

    if (response.code === 200 && response.data) {
      const status = {
        _id: response.data,
        color: color || '#000000',
        streamId,
        name: title,
        type: type || 'In progress',
      };
      dispatch(receiveStatus(status));
      return response.data;
    }
    return null;
  };
}
