/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import Socket from './Socket';
import { setUserData } from '../reducers/userdata';

export default function setContactOrderUsers({ seen, unSeen }) {
  return async (dispatch, getState) => {
    const userId = R.pathOr('', ['userData', 'userId'], getState());
    const contact = R.pathOr({}, ['contacts', userId], getState());
    const order = R.pathOr([], ['orderUsers'], contact);

    await Socket.command('Contact.setOrderUsers', {
      seen: R.uniq(order.seen.concat(seen)),
      unSeen: R.uniq(order.unSeen.concat(unSeen)),
    });
    dispatch(
      setUserData({
        visibleUsers: {
          seen: R.uniq(order.seen.concat(seen)),
          unSeen: R.uniq(order.unSeen.concat(unSeen)),
        },
      }),
    );
    //   const status = {
    //     _id: response.data,
    //     color: color || '#000000',
    //     streamId,
    //     name: title,
    //     type: type || 'In progress',
    //   };
    //   dispatch(receiveStatus(status));
    // }
  };
}
