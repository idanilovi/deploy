import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';

export default function pushController(data) {
  Navigation.dismissModal();
  setTimeout(() => {
    if (data.threadId) {
      Navigation.showModal({
        screen: 'workonflow.Thread',
        title: '',
        passProps: {
          id: data.threadId,
          threadId: data.threadId,
          type: 'Thread',
          title: data.headerBody,
          toScroll: true,
          customLeftButton: true,
        },
        navigatorStyle: {},
        navigatorButtons: {},
        animationType: 'slide-up',
      });
      return;
    }
    if (data.contactId) {
      Navigation.showModal({
        screen: 'workonflow.Direct',
        title: '',
        passProps: {
          title: data.fromName,
          contactId: data.contactId,
          type: 'Direct',
          customLeftButton: true,
          initialPage: 0,
        },
        navigatorStyle: {},
        navigatorButtons: {},
        animationType: 'slide-up',
      });
      return;
    }
  }, 700);
}
