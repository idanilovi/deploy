import Socket from './Socket';
import getFilesUrl from './getFilesUrl';
import getDataFromAsyncStorage from './getDataFromAsyncStorage';
import { setUserData } from '../reducers/userdata';

export default function getUserSettings() {
  return async function action(dispatch, getState) {
    let response = {};
    try {
      response = await Socket.command('Library.Images.read', {}, 1000);
    } catch (error) {
      console.log(error);
    }
    const { data: { imageIds = [], imageIdsDefault = [] } = {} } = response;
    let files = [];
    try {
      files = await dispatch(getFilesUrl({ id: imageIds }));
    } catch (error) {
      console.log(error);
    }
    const backgroundsObject = {};
    files.concat(imageIdsDefault).map(file => {
      backgroundsObject[file.id] = file;
    });
    const teamName = await dispatch(getDataFromAsyncStorage('teamName'));
    const { contacts = {}, userData: { userId } = {} } = getState();
    const { orderStreams, orderUsers } = contacts[userId];
    dispatch(
      setUserData({
        visibleStreams: orderStreams,
        visibleUsers: orderUsers,
        backgrounds: backgroundsObject,
        teamName,
      }),
    );
  };
}
