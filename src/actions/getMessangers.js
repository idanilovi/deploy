import Socket from './Socket';
import { receiveMessangers } from '../reducers/messangers';

export default function getMessangers({ id }) {
  return async dispatch => {
    const response = await Socket.command('Telegram.read', {
      ...(id ? { id } : {}),
    });
    if (response.code === 200) {
      dispatch(receiveMessangers(response.data));
    }
  };
}
