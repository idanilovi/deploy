import Socket from './Socket';

export default function createDescription(threadId) {
  return async () => {
    await Socket.command('Thread.Description.create', {
      threadId,
      content: JSON.stringify({}),
    });
  };
}
