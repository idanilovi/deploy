import R from 'ramda';
import Socket from './Socket';
import setThreadStatus from './setThreadStatus';
import { setStatus as _setThreadStatus } from '../reducers/threads';

export default function setThreadStream(threadId, streamId) {
  return async (dispatch, getState) => {
    await Socket.command('Thread.setStream', {
      id: threadId,
      streamId,
    });
    const statusId = R.pathOr(
      '',
      ['streams', streamId, 'threadStatuses', 0],
      getState(),
    );
    if (statusId) {
      dispatch(setThreadStatus(threadId, statusId));
      dispatch(_setThreadStatus(threadId, statusId));
    }
  };
}
