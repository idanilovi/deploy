/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import Socket from './Socket';
import { receiveDescription } from '../reducers/description';

export default function getDescription({ id, force = false }) {
  return async (dispatch, getState) => {
    if (!id) {
      return;
    }
    try {
      const stateData = R.pathOr({}, ['descriptions', id], getState());
      const stateDataIsEmpty = R.isEmpty(stateData) || force;
      if (stateDataIsEmpty) {
        const response = await Socket.command('Thread.Description.read', {
          id,
        });
        if (response.data && response.data[0]) {
          const description = response.data[0];
          if (typeof response.data[0].content === 'string') {
            try {
              description.content = JSON.parse(description.content);
            } catch (error) {
              console.error(JSON.stringify(id, error));
            }
          }
          description.id = description._id;
          delete description._id;
          dispatch(receiveDescription(description));
          return description;
        }
      }
      return stateData;
    } catch (error) {
      console.error(JSON.stringify(error));
    }
  };
}
