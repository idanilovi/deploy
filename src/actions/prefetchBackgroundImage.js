import RNFetchBlob from 'react-native-fetch-blob';
import { setUserData } from '../reducers/userdata';

export function checkFile(path) {
  return async function action() {
    const result = await RNFetchBlob.fs.exists(path);
    return result;
  };
}

export function fetchFile(url) {
  return async function action() {
    const result = await RNFetchBlob.fetch('GET', url);
    return result;
  };
}

export function writeFile(path, uri) {
  return async function action(dispatch) {
    const file = await dispatch(fetchFile(uri));
    const result = await RNFetchBlob.fs.writeFile(
      path,
      file.base64(),
      'base64',
    );
    return result;
  };
}

export function readFile(path, encoding = 'base64') {
  return async function action() {
    const result = await RNFetchBlob.fs.readFile(path, encoding);
    return result;
  };
}

export default function prefetchBackgroundImage({ id, filename, url }) {
  return async function action(dispatch) {
    const path = `${RNFetchBlob.fs.dirs.CacheDir}/background_images/${id}${
      filename
    }`;
    const fileExists = await dispatch(checkFile(path));
    if (fileExists) {
      dispatch(setUserData({ backgroundImage: path }));
    } else {
      await dispatch(writeFile(path, url));
    }
  };
}
