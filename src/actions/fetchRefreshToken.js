export default function fetchRefreshToken(token) {
  return async function action() {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(`${apiEndpoint}/refresh`, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      method: 'GET',
    });
    const response = await fetch(request);
    if (response.status === 200) {
      const jwt = await response.json();
      console.warn(jwt);
    }
  };
}
