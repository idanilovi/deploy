import Socket from './Socket';
import { receiveTrunks, receiveTrunk } from '../reducers/trunks';

export default function getTrunks({ trunkId }) {
  return async dispatch => {
    const response = await Socket.command('Channel.Trunks.read', {
      ...(trunkId ? { trunkId } : {}),
    });
    if (response.code === 200) {
      if (Array.isArray(response.data)) {
        dispatch(receiveTrunks(response.data));
      } else {
        dispatch(receiveTrunk(response.data));
      }
    }
  };
}
