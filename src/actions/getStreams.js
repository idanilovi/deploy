import Socket from './Socket';
import { receiveStreams } from '../reducers/streams';

export default function action() {
  return async function getStreams(dispatch) {
    const response = await Socket.command('Stream.read', { query: {} });
    const streams = response.data.map(stream => {
      const { _id, ...rest } = stream;
      return {
        id: _id,
        ...rest,
      };
    });
    dispatch(receiveStreams(streams));
  };
}
