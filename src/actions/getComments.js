import R from 'ramda';
import Socket from './Socket';
import { receiveComments } from '../reducers/comments';
import {
  requestStreamComments,
  receiveStreamComments,
} from '../reducers/streams';
import {
  requestThreadComments,
  receiveThreadComments,
  setLoaded,
} from '../reducers/threads';

export default function getComments(
  {
    threadId = null,
    streamId = null,
    skip = 0,
    limit = 100,
    id = null,
    to = [],
    from = [],
    ids = null,
    force = false,
  },
  cb,
) {
  return async (dispatch, getState) => {
    const query = {
      ...(id ? { id } : {}),
      ...(ids ? { ids } : {}),
      ...(threadId ? { threadId } : {}),
      ...(streamId ? { streamId } : {}),
      ...(to.length > 0 ? { to } : {}),
      ...(from.length > 0 ? { from } : {}),
      limit,
      skip,
    };
    if (threadId) {
      const stateThreadData = R.pathOr({}, ['threads', threadId], getState());
      const threadCommentsLoaded = R.propOr(
        false,
        'commentsLoaded',
        stateThreadData,
      );
      if (!threadCommentsLoaded || force) {
        dispatch(requestThreadComments(threadId));
        const response = await Socket.command('Comment.read', { query });
        const comments = response.data.map(comment => {
          const { _id, ...rest } = comment;
          return {
            _id,
            ...rest,
          };
        });
        if (comments.length < 30) {
          dispatch(setLoaded(threadId, true));
        }
        dispatch(receiveComments(comments));
        if (cb) {
          cb(comments);
        }
        dispatch(receiveThreadComments(threadId));
        return comments;
      }
      return [];
    }
    if (streamId && !threadId) {
      const stateStreamData = R.pathOr({}, ['streams', streamId], getState());
      const isStreamCommentsLoading = R.propOr(
        false,
        'commentsLoading',
        stateStreamData,
      );
      const streamCommentsLoaded = R.propOr(
        0,
        'commentsLoaded',
        stateStreamData,
      );
      if (!isStreamCommentsLoading && streamCommentsLoaded <= skip) {
        dispatch(requestStreamComments(streamId));
        const response = await Socket.command('Comment.read', { query });
        const comments = response.data.map(comment => {
          const { _id, ...rest } = comment;
          return {
            _id,
            ...rest,
          };
        });
        dispatch(receiveComments(comments));
        if (cb) {
          cb(comments);
        }
        dispatch(receiveStreamComments(streamId, skip));
        return comments;
      }
      return [];
    }
    const response = await Socket.command('Comment.read', { query });
    const comments = response.data.map(comment => {
      const { _id, ...rest } = comment;
      return {
        _id,
        ...rest,
      };
    });
    dispatch(receiveComments(comments));
    if (cb) {
      cb(comments);
    }
    return comments;
  };
}
