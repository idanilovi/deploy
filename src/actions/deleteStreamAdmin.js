import Socket from './Socket';

export default function deleteStreamAdmin(streamId, userId) {
  return async () => {
    await Socket.command('Stream.revokeAdmin', {
      id: streamId,
      userId,
    });
  };
}
