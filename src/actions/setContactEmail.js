import Socket from './Socket';
import getContact from './getContact';

export default function init({ id, email, index, isCustom = false }) {
  return async (dispatch, getState) => {
    if (isCustom) {
      const state = getState();
      const contact = state.contacts[id];
      contact.customFields[index].value = email;
      await Socket.command('Contact.update', {
        query: { _id: id },
        update: { $set: { customFields: contact.customFields } },
      });
    } else {
      await Socket.command('Contact.setEmail', { id, email });
    }
    dispatch(getContact(id, true));
  };
}
