import Socket from './Socket';

export default function setThreadBudget(threadId, budget) {
  return async function action() {
    await Socket.command('Thread.setBudget', { id: threadId, budget });
  };
}
