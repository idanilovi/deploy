/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import Socket from './Socket';
import { receiveStream } from '../reducers/streams';

export default function getStream(id, force = false) {
  return async (dispatch, getState) => {
    const stateData = R.pathOr({}, ['streams', id], getState());
    const stateDataIsEmpty = R.isEmpty(stateData) || force;
    if (stateDataIsEmpty) {
      const response = await Socket.command('Stream.read', {
        query: {
          id,
        },
      });
      if (response.data && response.data[0]) {
        dispatch(
          receiveStream({
            id: response.data[0]._id,
            ...response.data[0],
          }),
        );
        return response.data[0];
      }
      return null;
    }
    return stateData;
  };
}
