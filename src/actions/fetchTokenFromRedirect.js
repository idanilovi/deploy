import URI from 'urijs';
import setMultipleDataToAsyncStorage from './setMultipleDataToAsyncStorage';

export default function fetchTokenFromRedirect({ teamId, to, userId }) {
  return async function action(dispatch) {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(
      `${apiEndpoint}/redirect?teamId=${teamId}&to=${to}`,
    );
    const response = await fetch(request);
    if (response.status === 200) {
      const parsedUrl = URI.parse(response.url);
      if (parsedUrl.hostname) {
        const parsedQuery = URI.parseQuery(parsedUrl.query);
        await dispatch(
          setMultipleDataToAsyncStorage([
            ['token', parsedQuery.token],
            ['teamId', teamId],
            ['to', to],
            ['userId', userId],
          ]),
        );
      }
    }
  };
}
