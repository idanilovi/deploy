import Socket from './Socket';
import { receiveContact } from '../reducers/contacts';

export default function setChannelAdmin({ userId }) {
  return async (dispatch, getState) => {
    const contact = getState().contacts[userId];
    await Socket.command('Channel.Admin.give', { userId });
    dispatch(receiveContact({ ...contact, isChannelAdmin: true }));
  };
}
