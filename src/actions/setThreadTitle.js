import Socket from './Socket';

export default function action(threadId, title) {
  return async function setThreadTitle() {
    await Socket.command('Thread.setTitle', {
      id: threadId,
      title,
    });
  };
}
