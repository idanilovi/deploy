import { Navigation } from 'react-native-navigation';
import {
  getDataFromAsyncStorage,
  fetchCheckToken,
  getSocketConnection,
  getContact,
  getUserSettings,
  fetchLoginRequest,
  fetchTokenFromRedirect,
} from './index';

async function initUser(token, userId, dispatch) {
  await dispatch(getSocketConnection(token, Navigation));
  await dispatch(getContact(userId));
  await dispatch(getUserSettings());
  Navigation.startSingleScreenApp({
    screen: {
      screen: 'workonflow.Main',
      // title: '   ',
    },
    passProps: {},
    appStyle: {
      orientation: 'portrait',
    },
  });
  return;
}

export default function getNavigateLocation() {
  return async dispatch => {
    const token = await dispatch(getDataFromAsyncStorage('token'));
    if (!token) {
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.Login',
          title: 'Login',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }

    const TokenResponse = await dispatch(fetchCheckToken({ token }));
    if (TokenResponse.result) {
      await initUser(token, TokenResponse.verified.userId, dispatch);
      return;
    }

    const email = await dispatch(getDataFromAsyncStorage('email'));
    const password = await dispatch(getDataFromAsyncStorage('password'));
    if (!email && !password) {
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.Login',
          title: 'Login',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }

    await fetchLoginRequest({ email, password });
    const teamId = await dispatch(getDataFromAsyncStorage('teamId'));
    const userId = await dispatch(getDataFromAsyncStorage('userId'));
    const to = await dispatch(getDataFromAsyncStorage('to'));
    if (!teamId && !userId && !to) {
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.LoginTeams',
          title: 'LoginTeams',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }

    await fetchTokenFromRedirect({ teamId, userId, to });
    const newToken = await dispatch(getDataFromAsyncStorage('token'));
    if (!newToken) {
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'workonflow.Login',
          title: 'Login',
        },
        passProps: {},
        appStyle: {
          orientation: 'portrait',
        },
      });
      return;
    }

    await initUser(newToken, userId, dispatch);
    return;
  };
}
