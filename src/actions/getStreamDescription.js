/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import Socket from './Socket';
import { receiveDescription } from '../reducers/description';

export default function getStreamDescription({ streamId, force = false }) {
  return async (dispatch, getState) => {
    if (!streamId) {
      return;
    }
    try {
      const stateData = R.pathOr({}, ['descriptions', streamId], getState());
      const stateDataIsEmpty = R.isEmpty(stateData) || force;
      if (stateDataIsEmpty) {
        const response = await Socket.command('Stream.Description.read', {
          id: streamId,
        });
        if (response.data) {
          const description = response.data;
          if (typeof response.data.content === 'string') {
            try {
              description.content = JSON.parse(description.content);
            } catch (error) {
              console.error(JSON.stringify(streamId, error));
            }
          }
          description.id = description._id;
          delete description._id;
          dispatch(receiveDescription(description));
          return description;
        }
      }
      return stateData;
    } catch (error) {
      console.error(JSON.stringify(error));
    }
  };
}
