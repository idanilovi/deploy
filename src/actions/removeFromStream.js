import Socket from './Socket';
import getStream from './getStream';

export default function init({ id, userId }) {
  return async dispatch => {
    await Socket.command('Stream.deleteUser', { id, user: userId });
    dispatch(getStream(id, true));
  };
}
