import Socket from './Socket';

export default function setStreamResponsibleUser(streamId, userId) {
  return async () => {
    await Socket.command('Stream.setResponsibleUser', { id: streamId, userId });
  };
}
