import R from 'ramda';
import Socket from './Socket';
import { receivePayments } from '../reducers/payments';
import { setUserData } from '../reducers/userdata';

export default function getBillingPayments({ registerDate = false }) {
  return async (dispatch, getState) => {
    if (registerDate) {
      const res = await Socket.command('Team.read', {});
      if (res.code === 200) {
        const { userData } = getState();
        userData.registerDate = res.data.createdAt;
        userData.paid = res.data.paid;
        setUserData(userData);
      }
    }
    const response = await Socket.command('Billing.getPayments', {});
    if (response.code !== 200) return;
    const payments = response.data;
    dispatch(receivePayments(payments));
  };
}
