export default function fetchRegistrationRequest({
  email,
  name = 'User',
  teamName,
  password,
  successCallback,
  failureCallback,
}) {
  return async () => {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(`${apiEndpoint}/register`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        email: email.toLowerCase(),
        name,
        teamName,
        pass: password,
      }),
    });
    const response = await fetch(request);
    const responseJSON = await response.json();
    if (response.status === 200) {
      if (successCallback) {
        successCallback(responseJSON);
      }
    } else if (failureCallback) {
      failureCallback(responseJSON);
    }
  };
}
