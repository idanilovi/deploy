import Socket from './Socket';
import { receiveContact } from '../reducers/contacts';

export default function revokeChannelAdmin({ userId }) {
  return async (dispatch, state) => {
    const contact = state.contacts[userId];
    await Socket.command('Channel.Admin.revoke', { userId });
    dispatch(receiveContact({ ...contact, isChannelsAdmin: false }));
  };
}
