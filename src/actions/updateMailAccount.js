import Socket from './Socket';
import { getMails } from '../actions';

export default function updateMailAccount({
  id,
  email,
  password,
  name,
  privated,
  userId,
  outgoing,
  streamId,
  visibleInStreams,
  token,
  isVisibleToAll,
}) {
  return async dispatch => {
    const response = await Socket.command('Mail.Account.update', {
      id,
      query: {
        ...(email ? { email } : {}),
        ...(password ? { password } : {}),
        ...(name ? { name } : {}),
        ...(privated ? { private: privated } : {}),
        ...(userId ? { userId } : {}),
        ...(outgoing ? { outgoing } : {}),
        ...(streamId ? { streamId } : {}),
        ...(token ? { token } : {}),
        ...(visibleInStreams ? { visibleInStreams } : {}),
        ...(isVisibleToAll ? { isVisibleToAll } : {}),
      },
    });
    if (response.code === 200) {
      dispatch(getMails({}));
    }
  };
}
