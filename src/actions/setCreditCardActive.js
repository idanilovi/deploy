import R from 'ramda';
import Socket from './Socket';
import { getCreditCards } from './getCreditCards';

export default function setCreditCardActive(id) {
  return async dispatch => {
    const response = await Socket.command('Billing.setActive', { id });
    if (response.code !== 200) return;
    dispatch(getCreditCards());
  };
}
