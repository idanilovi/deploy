import Socket from './Socket';
import { receiveContacts } from '../reducers/contacts';

export default function getTeamUsers(contacts) {
  return async function action(dispatch, getState) {
    dispatch(receiveContacts(contacts));
    const response = await Socket.command('Auth.read', {});
    const channelAdmins = await Socket.command('Channel.Admin.read', {}).catch(
      err => console.log('AAAAAerr: ', err),
    );
    const newContacts = contacts.map(contact => {
      const teamOptions = response.data.filter(
        options => options.userId === contact._id,
      )[0];
      return {
        ...contact,
        isAdmin: teamOptions ? teamOptions.isAdmin : false,
        isActive: teamOptions ? teamOptions.isActive : false,
        isChannelsAdmin:
          channelAdmins && Array.isArray(channelAdmins.data)
            ? channelAdmins.data.includes(contact._id)
            : false,
      };
    });
    dispatch(receiveContacts(newContacts));
  };
}
