import fetchAccessTeams from './fetchAccessTeams';

export default function fetchAcceptAccess({
  teamId,
  url,
  successCallback,
  failureCallback,
}) {
  return async dispatch => {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(`${apiEndpoint}/access`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        teamId,
        url,
        accept: true,
      }),
    });
    const response = await fetch(request);
    const responseJSON = await response.json();
    if (response.status === 200) {
      if (successCallback) {
        successCallback(responseJSON);
        dispatch(fetchAccessTeams());
      }
    } else if (failureCallback) {
      failureCallback(responseJSON);
    }
  };
}
