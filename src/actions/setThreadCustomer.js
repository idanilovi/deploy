import Socket from './Socket';
import { setCustomer } from '../reducers/threads';
import { getContact } from '../actions';

export default function action({ id, customerId }) {
  return async dispatch => {
    dispatch(setCustomer(id, customerId));
    Socket.command('Thread.setCustomerId', {
      id,
      ...(customerId ? { customerId } : {}),
    });
    dispatch(getContact(customerId));
  };
}
