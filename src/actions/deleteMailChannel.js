import Socket from './Socket';
import { getMails } from '../actions';

export default function deleteTelegramChannel({ id }) {
  return async dispatch => {
    const response = await Socket.command('Mail.Account.delete', {
      id,
    });
    if (response.code === 200) {
      dispatch(getMails({ id }));
    }
  };
}
