import R from 'ramda';
import Socket from './Socket';
import { receiveCards } from '../reducers/cards';

export default function getCreditCards() {
  return async dispatch => {
    const response = await Socket.command('Billing.getCards', {});
    if (response.code !== 200) return;
    const cards = response.data;
    dispatch(receiveCards(cards));
  };
}
