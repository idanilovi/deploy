import Socket from './Socket';

export default function deleteStreamRole(streamId, userId) {
  return async () => {
    await Socket.command('Stream.deleteUser', {
      id: streamId,
      user: userId,
    });
  };
}
