import FileCache from '../helpers/FileCache';
import { receiveFiles } from '../reducers/files';

export default function getFilesFromCache() {
  return async dispatch => {
    const files = await FileCache.getCachedFiles();
    dispatch(receiveFiles(files));
  };
}
