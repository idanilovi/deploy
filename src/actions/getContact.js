/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import Socket from './Socket';
import { receiveContact } from '../reducers/contacts';

export default function getContact(id, force = false) {
  return async (dispatch, getState) => {
    const stateData = R.pathOr({}, ['contacts', id], getState());
    const stateDataIsEmpty = R.isEmpty(stateData) || force;
    if (stateDataIsEmpty) {
      const response = await Socket.command('Contact.read', {
        query: { _id: id },
      });
      if (response.data && response.data[0]) {
        dispatch(receiveContact(response.data[0]));
      }
      return response.data;
    }
    return stateData;
  };
}
