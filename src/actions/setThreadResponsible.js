import Socket from './Socket';
import { setResponsible } from '../reducers/threads';

export default function action({ threadId, userId }) {
  return async function setThreadResponsible(dispatch) {
    dispatch(setResponsible(threadId, userId));
    Socket.command('Thread.setResponsibleUser', {
      id: threadId,
      userId,
    });
  };
}
