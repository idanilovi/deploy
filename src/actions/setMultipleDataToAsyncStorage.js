import { AsyncStorage } from 'react-native';

export default function setMultipleDataToAsyncStorage(data) {
  return async function action() {
    try {
      await AsyncStorage.multiSet(data);
    } catch (error) {
      console.error(error);
    }
  };
}
