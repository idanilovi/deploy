import Socket from './Socket';
import { receiveMails } from '../reducers/mails';

export default function getMails({ id }) {
  return async dispatch => {
    const response = await Socket.command('Mail.Account.read', {
      ...(id ? { id } : {}),
      isPrivate: true,
    });
    if (response.code === 200) {
      dispatch(receiveMails(response.data));
    }
  };
}
