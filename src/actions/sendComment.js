import R from 'ramda';
import uuid from 'uuid';
import Socket from './Socket';
import { receiveComment, deleteComment } from '../reducers/comments';
import uploadCommentAttachment from './uploadCommentAttachment';
import getFiles from './getFiles';

export default function action({
  threadId,
  text,
  to,
  streamId,
  contactId,
  attachments,
  isPublic = false,
}) {
  return async function sendComment(dispatch, getState) {
    // dispatch(receiveComment({
    //   _id:
    // }));
    // TODO по-моему это хуйня раз можно отправить коммент от другого пользователя
    const userId = R.path(['userData', 'userId'], getState());
    // console.log(attachments)
    const att = await Promise.all(
      R.map(
        attachment =>
          dispatch(
            uploadCommentAttachment({
              file: attachment,
              // TODO особенности нашей архитектуры
              streamId: streamId || contactId,
              threadId,
              authorId: userId,
            }),
          ),
        attachments,
      ),
    );
    const shadowComment = {
      _id: uuid.v4(),
      att: [{ type: 'text', data: { text } }].concat(att),
      createdAt: Date.now(),
      from: userId,
      to,
      streamId: streamId || null,
      threadId: threadId || null,
      metadata: {
        userId,
      },
      type: null,
      updatedAt: Date.now(),
      isPublic,
    };
    dispatch(getFiles({ ids: att.map(attachment => attachment.data.id) }));
    dispatch(receiveComment(shadowComment));
    const comment = {
      ...(streamId ? { streamId } : {}),
      ...(threadId ? { threadId } : {}),
      to,
      from: userId,
      metadata: { userId },
      att: [{ type: 'text', data: { text } }].concat(att),
      isPublic,
    };
    const response = await Socket.command('Thread.Comment.create', {
      ...comment,
    });
    dispatch(deleteComment(shadowComment._id));
    dispatch(receiveComment({ ...shadowComment, _id: response.data }));
  };
}
