import 'react-native-console-time-polyfill';
import global from '../global';

const pendingRequests = new Map();

const Socket = {
  async command(key, value, timeout = 0) {
    const requestId = `${key}:${JSON.stringify(value)}`;
    if (pendingRequests.has(requestId)) {
      return pendingRequests.get(requestId);
    }
    const promise = new Promise((resolve, reject) => {
      if (global.socket) {
        // console.time(requestId);
        global.socket.emit('command', { key, value, timeout }, response => {
          pendingRequests.delete(requestId);
          if (response.code === 200 || response.status === 200) {
            // console.timeEnd(requestId);
            resolve(response);
          } else {
            reject(
              new Error(
                `-> [ERR] ${key}: ${JSON.stringify(value)} | ${JSON.stringify(
                  response.message,
                )}`,
              ),
            );
          }
        });
      } else {
        reject(new Error(`Socket is not defined ${key}`));
      }
    });
    pendingRequests.set(requestId, promise);
    return promise;
  },
};

export default Socket;
