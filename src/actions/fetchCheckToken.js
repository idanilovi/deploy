export default function fetchCheckToken({ token }) {
  return async function action() {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(`${apiEndpoint}/check`, {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({ token }),
    });
    const response = await fetch(request);
    if (response.status === 200) {
      const responseJSON = await response.json();
      return responseJSON;
    }
    return { result: false };
  };
}
