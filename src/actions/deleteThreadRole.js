import Socket from './Socket';
import { receiveThread, deleteRole } from '../reducers/threads';

export default function deleteThreadRole({ threadId, userId }) {
  return async dispatch => {
    dispatch(deleteRole(threadId, userId));
    const response = await Socket.command('Thread.removeRole', {
      id: threadId,
      userId,
    });
    if (response.data) {
      const { _id, ...rest } = response.data;
      const thread = { id: _id, ...rest };
      dispatch(receiveThread(thread));
    }
  };
}
