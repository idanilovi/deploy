import Socket from './Socket';

export default function setStreamWidgets(streamId, widgets) {
  return async function action() {
    await Socket.command('Stream.update', {
      id: streamId,
      type: 'set',
      value: { 'settings.widgets': widgets },
    });
  };
}
