import R from 'ramda';
import Socket from './Socket';
import { receiveComment } from '../reducers/comments';
import getFile from './getFile';

export default function getComment(id, force = false) {
  return async (dispatch, getState) => {
    const stateData = R.pathOr({}, ['comments', id], getState());
    const stateDataIsEmpty = R.isEmpty(stateData) || force;
    if (stateDataIsEmpty) {
      const response = await Socket.command('Comment.read', { query: { id } });
      const comment = response.data[0];
      comment.att.map(attachment => {
        if (attachment.type === 'image') {
          dispatch(getFile(attachment.data.id));
        }
      });
      dispatch(receiveComment(comment));
      return comment;
    }
    return stateData;
  };
}
