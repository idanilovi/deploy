import R from 'ramda';
import Socket from './Socket';
import { receiveThreads } from '../reducers/threads';
import {
  requestStreamThreads,
  receiveStreamThreads,
} from '../reducers/streams';

export default function getThreads({
  streamId = null,
  ids = null,
  id = null,
  responsibleUserId,
  isCountChildren = true,
  authorId = null,
}) {
  return async (dispatch, getState) => {
    if (!streamId && R.isEmpty(ids) && !responsibleUserId) return;
    if (streamId) {
      const stateStreamData = R.pathOr({}, ['streams', streamId], getState());
      const isStreamThreadsLoaded = R.propOr(
        false,
        'threadsLoaded',
        stateStreamData,
      );
      if (!isStreamThreadsLoaded) {
        dispatch(requestStreamThreads(streamId));
        const response = await Socket.command('Thread.read', {
          query: { streamId, isCountChildren },
        });
        if (response.data && response.data.length) {
          const threads = response.data.map(thread => {
            const { _id, ...rest } = thread;
            return { id: _id, ...rest };
          });
          dispatch(receiveThreads(threads));
        }
        dispatch(receiveStreamThreads(streamId));
        return;
      }
      return;
    }
    const response = await Socket.command('Thread.read', {
      query: {
        ...(streamId ? { streamId } : {}),
        ...(ids ? { ids } : {}),
        ...(id ? { id } : {}),
        ...(responsibleUserId ? { responsibleUserId } : {}),
        ...(authorId ? { authorId } : {}),
        isCountChildren,
      },
    });
    const threads = response.data.map(thread => {
      const { _id, ...rest } = thread;
      return { id: _id, ...rest };
    });
    dispatch(receiveThreads(threads));
  };
}
