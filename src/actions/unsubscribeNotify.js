import Socket from './Socket';
import { AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';

export default function unsubscribeNotify({
  registrationToken,
  deleteInstance = true,
}) {
  return async function action() {
    const token = await AsyncStorage.getItem('registerPushToken');
    await Socket.command('Notifications.unsubscribe', {
      registrationToken: registrationToken || token,
    });
    await AsyncStorage.removeItem('registerPushToken');
    if (deleteInstance) {
      firebase.messaging().deleteInstanceId();
    }
  };
}
