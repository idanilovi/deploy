import R from 'ramda';
import Socket from './Socket';
import { receiveNotifications } from '../reducers/notifications';
import { getThreads } from '../actions';

export default function getNotification() {
  return async dispatch => {
    const response = await Socket.command('Notifications.read', {});
    const notifications = R.pathOr([], ['data', 'comments'], response);
    const notifyThreadIds = R.filter(
      id => id,
      R.map(notify => notify.threadId, notifications),
    );
    dispatch(receiveNotifications(notifications));
    dispatch(getThreads({ ids: R.values(notifyThreadIds) }));
  };
}
