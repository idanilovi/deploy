import Socket from './Socket';

export default function addStreamAdmin(streamId, userId) {
  return async () => {
    await Socket.command('Stream.giveAdmin', {
      id: streamId,
      userId,
    });
  };
}
