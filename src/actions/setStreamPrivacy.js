import Socket from './Socket';

export default function setStreamPrivacy(id) {
  return async () => {
    await Socket.command('Stream.setPrivate', { id });
  };
}
