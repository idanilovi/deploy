import R from 'ramda';
import Socket from './Socket';
import { getCreditCards } from './getCreditCards';

export default function removeCreditCard(id) {
  return async dispatch => {
    const response = await Socket.command('Billing.removeCard', { id });
    if (response.code !== 200) return;
    dispatch(getCreditCards());
  };
}
