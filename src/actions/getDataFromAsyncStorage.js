import { AsyncStorage } from 'react-native';

export default function getDataFromAsyncStorage(key) {
  return async function action() {
    const data = await AsyncStorage.getItem(key);
    return data;
  };
}
