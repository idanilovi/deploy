import Socket from './Socket';
import { getMessangers } from '../actions';

export default function updateTelegramChannel({ id, name, token, streamId }) {
  return async dispatch => {
    const response = await Socket.command('Telegram.update', {
      id,
      ...(name ? { name } : {}),
      ...(token ? { token } : {}),
      ...(streamId ? { streamId } : {}),
    });
    if (response.code === 200) {
      dispatch(getMessangers({}));
    }
  };
}
