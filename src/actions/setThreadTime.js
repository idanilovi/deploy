import Socket from './Socket';

export default function setThreadTime(threadId, resource) {
  return async function action() {
    await Socket.command('Thread.setResource', { id: threadId, resource });
  };
}
