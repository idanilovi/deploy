import Socket from './Socket';
import { getMessangers } from '../actions';

export default function createTelegramChannel({ name, token, streamId }) {
  return async dispatch => {
    const response = await Socket.command('Telegram.create', {
      name,
      token,
      streamId,
    });
    if (response.code === 200) {
      dispatch(getMessangers({}));
    }
  };
}
