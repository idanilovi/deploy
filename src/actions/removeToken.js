import { AsyncStorage } from 'react-native';

export default function action() {
  return async function removeToken() {
    try {
      await AsyncStorage.removeItem('token');
      console.log('Token removed');
    } catch (error) {
      console.error(error);
    }
  };
}
