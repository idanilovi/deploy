/* eslint-disable no-underscore-dangle */
import Socket from './Socket';
import { receiveThread } from '../reducers/threads';
// import createDescription from './createDescription';
import setThreadSequence from './setThreadSequence';

export default function createThread(
  { streamId, statusId = '', roles = [], title = '', authorId },
  callback,
) {
  return async dispatch => {
    const response = await Socket.command('Thread.create', {
      streamId: streamId || '',
      status: statusId || '',
      title: title || '',
      roles,
      authorId,
    });
    if (response.data) {
      const thread = {
        archived: false,
        createdAt: Date.now(),
        customFields: [],
        deadline: [],
        responsibleUserId: '',
        roles,
        status: statusId,
        streamId,
        title: 'new thread',
        type: '',
        updatedAt: Date.now(),
        id: response.data,
        authorId,
      };
      if (streamId) {
        await dispatch(
          setThreadSequence({ threadId: response.data, streamId }),
        );
      }
      await dispatch(receiveThread(thread));
      if (callback) {
        callback(response.data);
      }
      return response.data;
    }
  };
}
