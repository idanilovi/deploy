import Socket from './Socket';
import getNotifications from './getNotifications';

export default function readNotification({ commentId, commentIds }) {
  return async dispatch => {
    const query = {
      ...(commentId ? { commentId } : {}),
      ...(commentIds ? { commentIds } : {}),
    };
    await Socket.command('Notifications.readComment', query);
    dispatch(getNotifications());
  };
}
