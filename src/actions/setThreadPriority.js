import Socket from './Socket';

export default function setThreadPriority(threadId, priority) {
  return async function action() {
    await Socket.command('Thread.setPriority', { id: threadId, priority });
  };
}
