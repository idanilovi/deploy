import Socket from './Socket';

export default function addStreamRole(streamId, userId) {
  return async () => {
    await Socket.command('Stream.setUser', {
      id: streamId,
      user: userId,
    });
  };
}
