import Socket from './Socket';

export default function init(query) {
  return async dispatch => {
    const result = await Socket.command('Search.search', {
      index: 'contacts',
      matches: [
        { match: { billingType: 'contacts' } },
        {
          multi_match: {
            fields: [
              'basicData.name',
              'basicData.email',
              'basicData.phone',
              'customFields.value',
            ],
            query,
            type: 'phrase_prefix',
          },
        },
      ],
      size: 30,
      fields: ['basicData', 'customFields'],
    });
    if (result.code === 200) {
      return result.data;
    }
    return [];
  };
}
