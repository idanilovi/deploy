import Socket from './Socket';
import { setStatus } from '../reducers/threads';

export default function setThreadStatus(threadId, statusId) {
  return async dispatch => {
    // dispatch(setStatus(threadId, statusId));
    await Socket.command('Thread.setStatus', {
      id: threadId,
      status: statusId,
    });
  };
}
