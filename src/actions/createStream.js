/* eslint-disable no-underscore-dangle */
import Socket from './Socket';
import { setContactOrderStreams, createStatus, getStatuses } from '../actions';
import { receiveStream } from '../reducers/streams';

export default function createStream({ title, userId }) {
  return async dispatch => {
    const response = await Socket.command('Stream.create', {
      name: title || `Stream ${Date.now()}`,
      roles: [],
      threadStatuses: [],
      threadPrioritites: [],
      visibility: [],
      seenBy: {},
      settings: {
        widgets: {
          DataTime: {
            on: true,
            options: 'deadline',
            type: 'DataTime',
          },
          Priority: {
            on: true,
            type: 'Priority',
          },
          Responsible: {
            on: true,
            type: 'Responsible',
          },
          WorkflowStatus: {
            on: true,
            type: 'WorkflowStatus',
          },
          StreamSelect: {
            on: true,
            type: 'StreamSelect',
          },
        },
        notify: {
          [`${userId}`]: 'all',
        },
      },
      isEmpty: true,
    });
    if (response.code === 200 && response.data) {
      dispatch(
        setContactOrderStreams({ seen: [response.data.id], unSeen: [] }),
      );
      const status1 = await dispatch(
        createStatus({
          title: 'Wait',
          color: '#4051b5',
          streamId: response.data.id,
          type: 'Wait',
        }),
      );
      const status2 = await dispatch(
        createStatus({
          title: 'In progress',
          color: '#e91e63',
          streamId: response.data.id,
          type: 'In progress',
        }),
      );
      const status3 = await dispatch(
        createStatus({
          title: 'Done',
          color: '#009688',
          streamId: response.data.id,
          type: 'Done',
        }),
      );
      await dispatch(getStatuses({ streamId: response.data.id }));
      const stream = {
        id: response.data.id,
        createdAt: Date.now(),
        name: title || `Stream ${Date.now()}`,
        admins: [userId],
        isPrivate: true,
        index: Date.now(),
        roles: [userId],
        threadStatuses: [status1, status2, status3],
        threadPrioritites: [],
        visibility: [],
        threadsSequence: [],
        owner: userId,
        seenBy: {},
        settings: {
          widgets: {
            DataTime: {
              on: true,
              options: 'deadline',
              type: 'DataTime',
            },
            Priority: {
              on: true,
              type: 'Priority',
            },
            Responsible: {
              on: true,
              type: 'Responsible',
            },
            WorkflowStatus: {
              on: true,
              type: 'WorkflowStatus',
            },
            StreamSelect: {
              on: true,
              type: 'StreamSelect',
            },
          },
          notify: {
            [`${userId}`]: 'all',
          },
        },
      };
      dispatch(receiveStream(stream));
      return { id: response.data.id, name: title };
    }
    return null;
  };
}
