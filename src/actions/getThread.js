/* eslint-disable no-underscore-dangle */
import R from 'ramda';
import Socket from './Socket';
import { receiveThread } from '../reducers/threads';
import getContacts from './getContacts';
import getDescription from './getDescription';

export default function getThread(id, force = false, isCountChildren = true) {
  if (!id) {
    throw new Error('Request thread without id!');
  }
  return async (dispatch, getState) => {
    const stateData = R.pathOr({}, ['threads', id], getState());
    const stateDataIsEmpty = R.isEmpty(stateData) || force;
    if (stateDataIsEmpty) {
      const response = await Socket.command('Thread.read', {
        query: {
          id,
          isCountChildren,
        },
      });
      if (response.data && response.data[0]) {
        const thread = response.data[0];
        thread.id = thread._id;
        delete thread._id;
        await dispatch(getContacts(['contacts'], thread.roles));
        await dispatch(getDescription({ id }));
        dispatch(receiveThread(thread));
      }
      return response;
    }
    return stateData;
  };
}
