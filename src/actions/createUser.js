import Socket from './Socket';
import { setContactOrderUsers } from '../actions';

export default function createUser(email, successCallback) {
  return async dispatch => {
    const response = await Socket.command('Auth.Access.invite', {
      email,
      name: email,
      url: 'https://workonflow.com',
    });
    if (response.data) {
      dispatch(
        setContactOrderUsers({ seen: [response.data.contactId], unSeen: [] }),
      );
      if (successCallback) {
        successCallback(response.data.contactId);
      }
    }
  };
}
