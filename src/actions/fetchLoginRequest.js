import fetchAccessTeams from './fetchAccessTeams';
import setMultipleDataToAsyncStorage from './setMultipleDataToAsyncStorage';

export default function fetchLoginRequest({
  email,
  password,
  successCallback,
  failureCallback,
}) {
  return async function action(dispatch) {
    const apiEndpoint = 'https://login.workonflow.com/api';
    const request = new Request(`${apiEndpoint}/auth`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({ email: email.toLowerCase(), password }),
    });
    const response = await fetch(request);
    if (response.status === 200) {
      dispatch(
        setMultipleDataToAsyncStorage([
          ['email', email],
          ['password', password],
        ]),
      );
      dispatch(fetchAccessTeams());
      if (successCallback) {
        successCallback();
      }
    } else if (failureCallback) {
      failureCallback('Invalid email or password');
    }
  };
}
