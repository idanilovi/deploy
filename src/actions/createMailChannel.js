import Socket from './Socket';
import { getMails } from '../actions';

export default function createMailChannel({
  email,
  password,
  name,
  privated,
  userId,
  outgoing,
  streamId,
  token,
  redirectUrl,
  visibleInStreams,
  isVisibleToAll,
}) {
  return async dispatch => {
    const response = await Socket.command('Mail.Account.create', {
      email,
      password,
      name,
      private: privated,
      userId,
      outgoing,
      streamId,
      token,
      redirectUrl,
      visibleInStreams,
      isVisibleToAll,
    });
    if (response.code === 200) {
      dispatch(getMails({}));
    }
  };
}
