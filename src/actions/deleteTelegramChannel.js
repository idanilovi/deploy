import Socket from './Socket';
import { getMessangers } from '../actions';

export default function deleteTelegramChannel({ id }) {
  return async dispatch => {
    const response = await Socket.command('Telegram.delete', {
      id,
    });
    if (response.code === 200) {
      dispatch(getMessangers({ id }));
    }
  };
}
