import { removeToken, closeSocketConnection } from '../actions';

export default function action(callback) {
  return async function unauth(dispatch) {
    await dispatch(removeToken());
    await dispatch(closeSocketConnection());
    callback();
  };
}
