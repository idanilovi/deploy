import { AsyncStorage, Platform } from 'react-native';

export default function clearAsyncStorage() {
  return async function action() {
    try {
      if (Platform.OS === 'ios') {
        await AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
      } else {
        await AsyncStorage.clear();
      }
    } catch (error) {
      console.error(error);
    }
  };
}
