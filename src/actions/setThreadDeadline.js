import Socket from './Socket';

export default function setThreadDeadline(threadId, deadline) {
  return async function action() {
    await Socket.command('Thread.setDeadline', {
      id: threadId,
      timestamp: deadline,
    });
  };
}
