import Socket from './Socket';
import getNotifications from './getNotifications';

export default function init(query) {
  return async dispatch => {
    await Socket.command('Notifications.removeComments', query);
    dispatch(getNotifications());
  };
}
