import Socket from './Socket';
import getFilesUrl from './getFilesUrl';
// import { receiveFile, receiveFiles } from '../reducers/files';

export default function action({ streamId, threadId, id, ids, authorId }) {
  return async function getFiles(dispatch) {
    const response = await Socket.command('File.read', {
      ...(streamId ? { streamId } : {}),
      ...(threadId ? { threadId } : {}),
      ...(id ? { id } : {}),
      ...(ids ? { ids } : {}),
      ...(authorId ? { authorId } : {}),
    });
    if (response.data) {
      if (Array.isArray(response.data)) {
        const fileIds = response.data.map(_ => _.id);
        if (fileIds.length) {
          dispatch(getFilesUrl({ id: fileIds }));
        }
      } else {
        dispatch(getFilesUrl({ id: response.data.id }));
      }
    }
    return response.data;
  };
}
