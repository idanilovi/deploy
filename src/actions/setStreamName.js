import Socket from './Socket';

export default function setStreamName(id, name) {
  return async () => {
    await Socket.command('Stream.setName', { id, name });
  };
}
