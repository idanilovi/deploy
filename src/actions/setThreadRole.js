import Socket from './Socket';

export default function setThreadRole({ threadId, userId }) {
  return async () => {
    await Socket.command('Thread.setRole', {
      id: threadId,
      userId,
    });
  };
}
