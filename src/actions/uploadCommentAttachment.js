import { decode } from 'base64-arraybuffer';
import uuid from 'uuid';
import Socket from './Socket';
import getFile from './getFile';

export default function action({ file, streamId, threadId, authorId }) {
  return async function uploadCommentAttachment() {
    const fsResponse = await Socket.command('File.getPUTUrl', {
      filename:
        file.fileName ||
        file.filename ||
        file.path.split('/')[file.path.split('/').length - 1] ||
        `${uuid.v4()}.jpg`,
      streamId,
      threadId,
      authorId,
      size: file.fileSize || file.size,
    });
    await fetch(fsResponse.data.url, {
      method: 'PUT',
      headers: {
        'Cache-Control': 'public,max-age=3600',
        'Content-Type': 'multipart/form-data',
      },
      body: decode(file.data),
    });
    return {
      data: {
        id: fsResponse.data.id,
      },
      type: 'image',
    };
  };
}
