import Socket from './Socket';
import { getTeamUsers } from './';

export default function getContacts(billingType = ['users', 'bots'], ids) {
  return async dispatch => {
    const query = {
      ...(ids ? { _id: { $in: ids } } : {}),
      ...(billingType ? { billingType: { $in: billingType } } : {}),
    };
    const response = await Socket.command('Contact.read', { query });
    if (response.data && response.data.length) {
      dispatch(getTeamUsers(response.data));
    }
  };
}
