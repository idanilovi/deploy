import Socket from './Socket';
import getContact from './getContact';

export default function init({ id, name }) {
  return async dispatch => {
    await Socket.command('Contact.setName', { id, name });
    dispatch(getContact(id, true));
  };
}
