import jwtDecode from 'jwt-decode';
import Socket from 'socket.io-client';
import { Platform, AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';
import global from '../global';
import { setUserData } from '../reducers/userdata';
import { setPosition, setStatus } from '../reducers/threads';
import {
  getComment,
  getStatuses,
  getNotifications,
  getThread,
  getThreads,
  getContacts,
  getContact,
  getStreams,
  getStream,
  subscribeNotify,
  unsubscribeNotify,
  pushController,
  getBillingPayments,
  getCreditCards,
  getUserSettings,
} from '../actions';
import { Navigation } from 'react-native-navigation';

export default function getSocketConnection(token) {
  return async function action(dispatch) {
    const decodedToken = jwtDecode(token);
    const socketEndpoint = decodedToken.wss.replace(/^(wss)/, 'https');
    let userData = null;
    let socket = Socket.connect(socketEndpoint, {
      timeout: 85000,
      query: { token },
      transports: ['websocket'],
    });
    global.socket = socket;
    socket.on('login', response => {
      firebase.messaging().requestPermission();
      firebase.auth().onAuthStateChanged(async () => {
        const registerToken = await AsyncStorage.getItem('registerPushToken');
        firebase
          .messaging()
          .getToken()
          .then(async Devicetoken => {
            if (registerToken === Devicetoken) return;
            await dispatch(subscribeNotify(Devicetoken));
            await AsyncStorage.setItem('registerPushToken', Devicetoken);
          });
      });
      firebase.messaging().onTokenRefresh(async data => {
        const registerToken = await AsyncStorage.getItem('registerPushToken');
        if (registerToken) {
          await dispatch(
            unsubscribeNotify({
              registrationToken: registerToken,
              deleteInstance: false,
            }),
          );
        }
        await dispatch(subscribeNotify(data));
        await AsyncStorage.setItem('registerPushToken', data);
      });

      firebase.notifications().onNotificationOpened(notificationOpen => {
        pushController(notificationOpen.notification._data);
      });

      userData = response;
      dispatch(setUserData(response));
      dispatch(getContacts());
      dispatch(getContacts(['contacts']));
      dispatch(getThreads({ responsibleUserId: response.userId }));
      dispatch(getThreads({ authorId: response.userId }));
      dispatch(getStatuses({}));
      dispatch(getStreams());
      dispatch(getNotifications());
      dispatch(getCreditCards());
      dispatch(getBillingPayments({ registerDate: true }));
    });
    socket.on('error', error => {
      console.log(error);
    });
    socket.on('connect_error', error => {
      console.log('connect_error');
    });
    socket.on('connect', () => {
      socket.emit('subscribe', { resource: 'Thread' });
      socket.emit('subscribe', { resource: 'Thread.Description' });
      socket.emit('subscribe', { resource: 'Thread.Event' });
      socket.emit('subscribe', { resource: 'Comment' });
      socket.emit('subscribe', { resource: 'Stream' });
      socket.emit('subscribe', { resource: 'Contact' });
      socket.emit('subscribe', { resource: 'Contact.Status' });
      socket.emit('subscribe', { resource: 'Notifications' });

      socket.on('broadcast', message => {
        // TODO Давай вынесем в отдельные обработчики
        if (message.routingKey === `${userData.teamId}.Thread.updated`) {
          return dispatch(getThread(message.content.threadId, true));
        }
        if (
          message.routingKey === `${userData.teamId}.Thread.updatedPosition`
        ) {
          return dispatch(
            setPosition(message.content.id, message.content.position),
          );
        }
        if (message.routingKey === `${userData.teamId}.Thread.created`) {
          if (message.content.type === 'setStatus') {
            return dispatch(
              setStatus(
                message.content.threadId,
                message.content.metadata.statusId,
              ),
            );
          }
          return dispatch(getThread(message.content.threadId, true));
        }
        if (message.routingKey === `${userData.teamId}.Stream.created`) {
          return dispatch(getStream(message.content.id, true));
        }
        if (message.routingKey === `${userData.teamId}.Stream.updated`) {
          return dispatch(getStream(message.content.id, true));
        }
        if (message.routingKey === `${userData.teamId}.Status.created`) {
          return dispatch(getStatuses({ id: message.content.id }));
        }
        if (message.routingKey === `${userData.teamId}.Contact.updated`) {
          return dispatch(getContact(message.content.id, true));
        }
        if (message.routingKey === `${userData.teamId}.Contact.created`) {
          return dispatch(getContact(message.content.id, true));
        }
        if (
          message.routingKey === `${userData.teamId}.Comment.created` &&
          message.content.userId !== userData.userId
        ) {
          return dispatch(getComment(message.content.id, true));
        }
        if (
          (message.routingKey ===
            `${userData.teamId}.Notifications.sentComment` &&
            message.content.userIds.includes(userData.userId)) ||
          (message.routingKey ===
            `${userData.teamId}.Notifications.removedComments` &&
            message.headers.userId === userData.userId) ||
          (message.routingKey ===
            `${userData.teamId}.Notifications.readComment` &&
            message.headers.userId === userData.userId) ||
          (message.routingKey ===
            `${userData.teamId}.Notifications.unreadComment` &&
            message.headers.userId === userData.userId)
        ) {
          return dispatch(getNotifications());
        }
      });
    });
    socket.on('reconnect_failed', () => {
      console.log('reconnect_failed: ');
      // Navigation.startSingleScreenApp({
      //   screen: { screen: 'workonflow.Reload' },
      //   passProps: {},
      //   appStyle: { orientation: 'portrait' },
      // });
    });
    socket.on('reconnect_error', () => {
      console.log('reconnect_error: ');
      // Navigation.startSingleScreenApp({
      //   screen: { screen: 'workonflow.Reload' },
      //   passProps: {},
      //   appStyle: { orientation: 'portrait' },
      // });
    });
    socket.on('reconnecting', () => {
      console.log('reconnecting: ');
      // Navigation.startSingleScreenApp({
      //   screen: { screen: 'workonflow.Reload' },
      //   passProps: {},
      //   appStyle: { orientation: 'portrait' },
      // });
    });
    socket.on('reconnect_attempt', () => {
      console.log('reconnect_attempt: ');
      // Navigation.startSingleScreenApp({
      //   screen: { screen: 'workonflow.Reload' },
      //   passProps: {},
      //   appStyle: { orientation: 'portrait' },
      // });
    });
    socket.on('reconnect', () => {
      console.log('reconnect: ');
      // Navigation.startSingleScreenApp({
      //   screen: { screen: 'workonflow.Reload' },
      //   passProps: {},
      //   appStyle: { orientation: 'portrait' },
      // });
    });
    socket.on('disconnect', () => {
      console.log('disconnect: ');
      // Navigation.startSingleScreenApp({
      //   screen: { screen: 'workonflow.Reload' },
      //   passProps: {},
      //   appStyle: { orientation: 'portrait' },
      // });
    });
  };
}
