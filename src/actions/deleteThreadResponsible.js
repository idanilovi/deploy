import Socket from './Socket';
import { receiveThread, deleteResponsible } from '../reducers/threads';

export default function action({ threadId }) {
  return async function deleteThreadResponsible(dispatch) {
    dispatch(deleteResponsible(threadId));
    const response = await Socket.command('Thread.removeResponsibleUser', {
      id: threadId,
    });
    if (response.data) {
      const { _id, ...rest } = response.data;
      const thread = { id: _id, ...rest };
      dispatch(receiveThread(thread));
    }
  };
}
