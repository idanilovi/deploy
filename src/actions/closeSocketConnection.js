import global from '../global';

export default function closeSocketConnection() {
  return async function action() {
    if (global.socket) {
      global.socket.close();
      delete global.socket;
    }
  };
}
