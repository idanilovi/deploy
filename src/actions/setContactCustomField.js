import Socket from './Socket';
import uuid from 'uuid';

export default function setContactCustomField(
  id,
  label,
  value,
  type = 'custom',
) {
  // eslint-disable-line no-shadow
  return async function action() {
    await Socket.command('Contact.update', {
      query: { _id: id },
      update: {
        $addToSet: {
          customFields: {
            id: uuid.v4(),
            label,
            value,
            type,
          },
        },
      },
    });
  };
}
