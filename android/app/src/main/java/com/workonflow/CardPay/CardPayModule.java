package com.workonflow;

import android.content.Intent;
import android.widget.Toast;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

//import ru.cloudpayments.sdk;
// import ru.cloudpayments.sdk.business.domain.model.BaseResponse;
// import ru.cloudpayments.sdk.view.PaymentTaskListener;


/**
 * Created by idani on 14.03.2018.
 */

public class CardPayModule extends ReactContextBaseJavaModule {

    @Override
    public String getName() {
        return "CardPay";
    }

    public CardPayModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void createCardCryptogram(String cardNumber, String cvv, String expDate, String publicId, Callback cryptoCallback) throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        ru.cloudpayments.sdk.ICard card = ru.cloudpayments.sdk.CardFactory.create(cardNumber, expDate, cvv);
        String cryptoCard = card.cardCryptogram(publicId);
        if(card.isValidNumber()) {
            cryptoCallback.invoke(cryptoCard);
        }
    }

}
