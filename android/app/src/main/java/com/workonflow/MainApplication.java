package com.workonflow;

import android.app.Application;
import cl.json.ShareApplication;
import com.workonflow.CardPayPackage;
import com.facebook.react.ReactApplication;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.realm.react.RealmReactPackage;
import org.wonday.pdf.RCTPdfView;
import com.reactlibrary.RNReactNativeDocViewerPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import cl.json.RNSharePackage;
import com.reactnative.photoview.PhotoViewPackage;
import com.reactnativenavigation.NavigationApplication;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.horcrux.svg.SvgPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.rnfs.RNFSPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import java.util.Arrays;
import java.util.List;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import android.content.Intent;
import com.reactnativenavigation.controllers.ActivityCallbacks;
import com.facebook.appevents.AppEventsLogger;

public class MainApplication extends NavigationApplication implements ShareApplication, ReactApplication {
    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

    protected static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }
    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
        mCallbackManager = new CallbackManager.Factory().create();
        return Arrays.<ReactPackage>asList(
            new VectorIconsPackage(), 
            new SvgPackage(),
            new PickerPackage(),
            new PhotoViewPackage(),
            new RNSharePackage(),
            new RNFetchBlobPackage(),
            new RNFSPackage(),
            new RNSoundPackage(),
            new LinearGradientPackage(),
            new SplashScreenReactPackage(),
            new RNReactNativeDocViewerPackage(),
            new RCTPdfView(),
            new RNFirebasePackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseAuthPackage(),
            new RNFirebaseNotificationsPackage(),
            new CardPayPackage(),
            new RealmReactPackage(),
            new FBSDKPackage(mCallbackManager)
        );

    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }

    @Override
    public String getFileProviderAuthority() {
        return "com.workonflow.provider";
    }
    @Override
    public void onCreate() {
        super.onCreate();

        // add this
        setActivityCallbacks(new ActivityCallbacks() {
            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent data) {
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
            }
        });

        FacebookSdk.sdkInitialize(getApplicationContext());

        // If you want to use AppEventsLogger to log events.
        AppEventsLogger.activateApp(this);
    }
}
