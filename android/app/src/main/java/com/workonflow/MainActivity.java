package com.workonflow;

import android.os.Bundle;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import com.facebook.react.ReactActivity;
import com.reactnativenavigation.controllers.SplashActivity;
import org.devio.rn.splashscreen.SplashScreen;
import com.workonflow.BuildConfig;

public class MainActivity extends SplashActivity {
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    protected String getMainComponentName() {
        return "WorkOnFlow";
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);  // here
        if (BuildConfig.BUILD_TYPE.equals("release")) {
            Fabric.with(this, new Crashlytics());
        }
        super.onCreate(savedInstanceState);
    }
}
